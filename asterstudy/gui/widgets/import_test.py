# -*- coding: utf-8 -*-

# Copyright 2016-2017 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""
Import test dialog
------------------

Implementation of class *TestImportDialog* for AsterStudy application.

"""
import os
import os.path as osp
import re
import stat
import time
from functools import partial

from PyQt5 import Qt as Q

from ...common import run_and_monitor
from ...datamodel import CATA
from .texteditor import TextEditor


class TestImportDialog(Q.QDialog):
    """Class that defines an import code_aster testcase dialog within the
     *AsterStudy* application."""

    astergui = _thread = _output = tests_dir = None
    tests_dir = files = cache = preview = results = last_search = None

    def __init__(self, astergui, parent=None):
        """
        Create/intialize the Test import dialog.

        Arguments:
            parent (Optional[QWidget]): Parent widget. Defaults to
                *None*.
        """
        # pragma pylint: disable=too-many-locals
        self.astergui = astergui
        parent = self.main_window if parent is None else parent
        super().__init__(parent)

        self.setWindowTitle('Import code_aster test case...')

        reg_font = Q.QFont("Arial", 10)
        self.setFont(reg_font)

        qsp = Q.QSizePolicy

        # Initialize the GUI layouts for the dialog
        # Set up the main layout that expands over the whole dialog
        vlayout = Q.QVBoxLayout(self)
        vlayout.setSpacing(5)

        # Divide this in a top and buttom layouts
        # > The top layout contains everything except the save and
        #   cancel buttons
        # > The bottom layout contains the save and cancel buttons
        top_hlayout = Q.QHBoxLayout()
        bottom_hlayout = Q.QHBoxLayout()
        vlayout.addItem(top_hlayout)
        vlayout.addItem(bottom_hlayout)

        # Inside the top layout include a left side and right side layouts
        # > The left side includes the search bar, the file navigator,
        #   and the description box, vertically aligned
        # > The right side contains a preview of the test case .comm file
        left_vlayout = Q.QVBoxLayout()
        right_vlayout = Q.QVBoxLayout()
        right_vlayout.setContentsMargins(5, 0, 0, 0)
        top_hlayout.addItem(left_vlayout)
        top_hlayout.addItem(right_vlayout)

        # L E F T   S I D E    ( T O P )
        # Widget combining:
        #  >> Search bar: layout, label message, and line edit
        #  >> File navigator
        search_widget = Q.QWidget(self)
        search_layout = Q.QVBoxLayout(search_widget)
        search_layout.setContentsMargins(0, 0, 0, 5)  # left,top,right,bottom
        search_label = Q.QLabel(self)
        search_label.setText('Search query (test name, command, etc)')
        self.search_line = Q.QLineEdit(self)
        for wid in [search_label, self.search_line]:
            wid.setSizePolicy(qsp.Expanding, qsp.Fixed)
            wid.setMaximumHeight(20)
        search_widget.setSizePolicy(qsp.Expanding, qsp.Minimum)

        search_layout.addWidget(search_label)
        search_layout.addWidget(self.search_line)
        # left_vlayout.addItem(search_layout)

        self.file_navigator = TestCaseTree(self)
        self.file_navigator.setModel(TestCaseTreeModel())

        search_layout.addWidget(self.file_navigator)

        # Test case description
        description_widget = Q.QWidget(self)
        description_layout = Q.QVBoxLayout(description_widget)
        description_label = Q.QLabel(self)
        description_label.setText('Description')

        self.description = Q.QTextEdit(self)
        self.description.setReadOnly(True)

        description_layout.addWidget(description_label)
        description_layout.addWidget(self.description)

        ##########################################################

        splitter = Q.QSplitter(Q.Qt.Vertical, self)
        splitter.setSizePolicy(qsp.MinimumExpanding, qsp.Expanding)

        splitter.addWidget(search_widget)
        splitter.addWidget(description_widget)

        left_vlayout.addWidget(splitter)

        # R I G H T   S I D E    ( T O P )
        # Preview box
        self.test_preview = TextEditor(self)
        commands = [j for i in [CATA.get_category(i) for i in \
                                    CATA.get_categories("showall")] for j in i]
        self.test_preview.setKeywords(commands, 0, Q.QColor("#ff0000"))
        self.test_preview.setReadOnly(True)
        self.astergui.preferencesChanged.connect(self.test_preview.updateSettings)
        self.test_preview.updateSettings(self.astergui.preferencesMgr())

        right_vlayout.addWidget(self.test_preview)

        # B O T T O M
        # Cancel and import buttons
        spacer = Q.QSpacerItem(0, 0, qsp.Expanding, qsp.Minimum)
        self.load_btn = Q.QPushButton(self)
        self.load_btn.setText('Import')
        self.cancel_btn = Q.QPushButton(self)
        self.cancel_btn.setText('Cancel')
        bottom_hlayout.addItem(spacer)
        bottom_hlayout.addWidget(self.cancel_btn)
        bottom_hlayout.addWidget(self.load_btn)
        bottom_hlayout.setSpacing(10)

        self.setup_controllers()

    def setup_controllers(self):
        """
        Setup the gui controllers and interactivity
        """

        # Initialize variables
        study = self.astergui.study()
        self.tests_dir = study.history.tests_path

        self.files = []
        if osp.exists(self.tests_dir):
            self.files = os.listdir(self.tests_dir)
        self.cache = {'full': {}, 'terms': {}}
        self.preview = {'contents': '', 'description': ''}
        self.results = []
        self.last_search = ''

        # Launch the search when the text is changing or
        # when the enter key is pressed:
        self.search_line.textChanged.connect(self.find_results)
        self.search_line.returnPressed.connect(self.find_results)

        # Close dialog
        self.cancel_btn.clicked.connect(self.do_cancel)

        # Import the test case to the module
        self.load_btn.clicked.connect(self.do_import)

        # Show the test case resume and .comm file
        self.file_navigator.selectionModel().selectionChanged.connect(
            self.show_testcase)
        self.file_navigator.model().set_fstruct([])

        # main_window = get_salome_pyqt().getDesktop()
        self.resize(min(self.main_window.width(), 1300),
                    min(self.main_window.height()-50, 1080))

    @property
    def main_window(self):
        """
        Returns the Qt main window
        """
        if self.astergui is not None:
            return self.astergui.mainWindow()
        from ..salomegui import get_salome_pyqt
        return get_salome_pyqt().getDesktop()

    def show_testcase(self):
        """
        show the test case in two items :
        - the resume item
        - the commands file item
        """
        self.test_preview.setEnabled(False)
        self.description.setEnabled(False)

        selection = self.file_navigator.selection()
        if not selection:
            return

        selected_file = selection[0][0]
        if not valid_comm_file(selected_file):
            return

        run_and_monitor(partial(self.parse_aster_test, selected_file),
                        self.update_preview)

    def parse_aster_test(self, fname):
        """
        Parses the contents and the description of the aster test case
        """
        self.preview = {}
        self.preview['contents'] = read_text_file(fname)
        solvers_def = {'THER_LINEAIRE': 'Linear thermal analysis',
                       'THER_NON_LINE': 'Non-linear thermal analysis',
                       'STAT_NON_LINE': 'Non linear static analysis',
                       'MECA_STATIQUE': 'Linear static analysis',
                       'MODE_STATIQUE': 'Static modes analysis',
                       'CALC_MODES': 'Natural frequency analysis',
                       'DYNA_VIBRA': 'Vibrational dynamics analysis',
                       'DYNA_LINE': 'Linear dynamics analysis',
                       'DYNA_NON_LINE': 'Non-linear dynamics analysis',
                       'CALC_FLUI_STRU': 'Fluid-structure analysis',
                       'DYNA_SPEC_MODAL': 'Spectral dynamics analysis (modal basis)',
                       'DYNA_ALEA_MODAL': 'Random vibrations analysis (modal basis)',
                       }
        header = '<h3>Test case : {}</h3>'.format(
            osp.splitext(osp.basename(fname))[0])
        for soper in solvers_def:
            count = self.preview['contents'].count(soper)
            if count:
                header += '{}: {} ({})<br>\n'.format(
                    solvers_def[soper], count, soper)

        self.preview['description'] = '{}\n<hr>\n{}'.format(
            header,
            filter_description(self.preview['contents']))

    def update_preview(self):
        """
        Updates the description and .comm file contents preview
        """
        self.test_preview.setText(self.preview['contents'])
        self.description.setText(self.preview['description'])
        if self.preview['contents']:
            self.test_preview.setEnabled(True)
            self.description.setEnabled(True)

    def do_import(self):
        """
        Import the test case in the asterstudy module
        """
        sel = self.file_navigator.selection()
        if not sel:
            return False
        selected_file = sel[0][0]
        if not selected_file:
            return False

        self._output = osp.splitext(osp.basename(selected_file))[0]
        return self.accept()

    def find_results(self):
        """
        Callback to the text changed signal of the search line input, only executes
        search immediately if no other search is in progress
        """
        # Do not multiply the search threads.. just wait for the existing ones to end
        if self._thread:
            if self._thread.is_alive():
                # An existing search thread is detected, just wait...
                return
        search_text = self.search_line.text().strip()
        if search_text != self.last_search:
            self._thread = run_and_monitor(partial(self.find_in_testcases, search_text),
                                           callback=partial(self.finalize_search_results),
                                           notifs=None, timeout=200)
            self.last_search = search_text

    def finalize_search_results(self):
        """
        Called when a given search thread is finished, either updates the view
        or launches a final search if meanwhile the user has changed the query
        """
        search_text = self.search_line.text().strip()
        if self.last_search == search_text:
            self.file_navigator.model().set_fstruct(self.results)
        else:
            self.find_results()

    def do_cancel(self):
        """
        Close the search dialog
        """
        self._output = ''
        self.reject()

    @property
    def output(self):
        """str: Attribute that holds the widget output filename."""
        return self._output

    def find_in_testcases(self, search_text):
        """
        Find the user query in all the tests (.comm files)
        """
        patterns = search_text.split()
        self.results = []
        if not patterns:
            return

        if search_text in self.cache['full']:
            self.results = self.cache['full'][search_text]
            return

        result_files = []
        for search_term in patterns:
            search_term = search_term.lower()
            if search_term in self.cache['terms']:
                result_files.append(self.cache['terms'][search_term])
                continue
            result_files.append([])
            for fname in self.files:
                if valid_comm_file(fname):
                    # Search the filename
                    if search_term in fname:
                        result_files[-1].append(fname)
                        continue
                    # Search the contents
                    file_path = os.path.join(self.tests_dir, fname)
                    contents = read_text_file(file_path)
                    if re.search(search_term, contents, re.I):
                        result_files[-1].append(fname)
            self.cache['terms'][search_term] = result_files[-1]

        if len(result_files) > 1:
            files = set(result_files[0]).intersection(*result_files)
        else:
            files = result_files[0]

        results = []
        for file in files:
            file_path = os.path.join(self.tests_dir, file)
            fstat = os.stat(file_path)
            size = fstat.st_size/1024.
            time_stamp = fstat[stat.ST_MTIME]
            results.append({'name' : file,
                            'parent': self.tests_dir,
                            'path' : file_path,
                            'type' : 'file',
                            'size': size,
                            'modified': time_stamp
                            })

        self.cache['full'][search_text] = results
        self.results = results


#########################################
# File view


class TestCaseTree(Q.QTreeView):
    """
    Overloaded QTreeView class providing customized selection and context menu
    for the a local or remote folder/files tree view.
    """

    def __init__(self, *args):
        """Create navigator view asociated with the results tab."""
        Q.QTreeView.__init__(self, *args)
        self.setFont(self.parent().font())

        self.setSelectionMode(Q.QTreeView.ExtendedSelection)
        self.setAllColumnsShowFocus(False)
        self.header().setDefaultAlignment(Q.Qt.AlignCenter)

    # pragma pylint: disable=invalid-name
    def setModel(self, model):
        """
        Set model for the tree view.
        """
        Q.QTreeView.setModel(self, model)
        self.model().view = self

        self.setItemDelegate(BoldDelegate(self))
        self.customize(self)

    def customize(self, tree=None):
        """
        Called by setModel and by model's clear method, insure
        that the customizations are reinforced at all times
        """
        if not tree:
            tree = self
        tree.setSizePolicy(Q.QSizePolicy.MinimumExpanding,
                           Q.QSizePolicy.Expanding)

        tree.model().setHorizontalHeaderLabels(
            ['Name', 'Size', 'Last Modification'])

        tree.setColumnWidth(0, 250)
        tree.setColumnWidth(1, 100)
        tree.setColumnWidth(2, 150)
        tree.setMinimumWidth(250)

        tree.expandToDepth(1)
        tree.setIndentation(20)

        tree.setSortingEnabled(True)
        tree.setHeaderHidden(False)

        tree.setHorizontalScrollBarPolicy(Q.Qt.ScrollBarAlwaysOff)
        tree.setAlternatingRowColors(False)

    def selection(self):
        """
        Returns the full paths of the selected files or folders
        """
        model = self.model()
        return [(model.get_path(index), model.get_type(index)) for index in self.selectedIndexes()]


class TreeSizeItem(Q.QStandardItem):
    """
    StandardItem giving the size in kb
    """
    def __init__(self, *args):
        Q.QStandardItem.__init__(self, *args)

    def __lt__(self, other):
        return self.numeric() < other.numeric()

    def numeric(self):
        """
        Returns the numeric value of the item (size in KB)
        """
        text = self.text()
        return float(text.replace('kB', '').strip())


class TestCaseTreeModel(Q.QStandardItemModel):
    """
    Tree Model side bar for folders/files navigation
    """

    updated = Q.pyqtSignal()

    fstruct = view = loaded = tests_path = None

    def __init__(self, *args):
        """Create navigator tree model"""
        Q.QStandardItemModel.__init__(self, *args)

    def set_fstruct(self, fstruct, root='code_aster'):
        """Sets the reference file structure on display"""
        Q.QStandardItemModel.clear(self)

        fstruct = sorted(fstruct, key=lambda x: x['path'])
        self.fstruct = fstruct

        if self.fstruct:
            folders = {}
            tests_path = self.fstruct[0]['parent']
            cid = os.path.basename(tests_path)

            root_item = Q.QStandardItem('{}/{}'.format(root, cid))
            self.appendRow(root_item)
            folders[tests_path] = root_item

            self.tests_path = tests_path

            for entry in self.fstruct:
                parent = folders[entry['parent']]
                items = [None]*5
                if entry['name'].startswith('fort.'):
                    continue
                items[0] = Q.QStandardItem(entry['name'])
                if 'size' in entry:
                    items[1] = TreeSizeItem('{:g} kB   '.format(round(100.*entry['size']/100)))
                    items[2] = Q.QStandardItem(str(entry['modified']))
                else:
                    items[1] = Q.QStandardItem('')
                    items[2] = Q.QStandardItem('')
                items[3] = Q.QStandardItem(entry['path'])
                items[4] = Q.QStandardItem(entry['type'])

                parent.appendRow(items)

                if entry['type'] == 'dir':
                    folders[entry['path']] = items[0]


        self.layoutChanged.emit()
        self.view.customize(self.view)
        self.sort(0)

    def clear(self, *args):
        """Clears the tree view contents"""
        Q.QStandardItemModel.clear(self, *args)

    # pragma pylint: disable=no-self-use,unused-argument
    def flags(self, index):
        """Returns the item flags for the given tree index."""
        col = index.column()
        if col == 0:
            return Q.Qt.ItemIsEnabled | Q.Qt.ItemIsSelectable
        return Q.Qt.ItemIsEnabled

    def data(self, index, role=Q.Qt.DisplayRole):
        """
        Returns data stored by model index for given role.
        Used in this context to decorate files according to their type.
        """
        if not self.fstruct:
            return super().data(index, role)

        _, col = index.row(), index.column()

        # if role == Q.Qt.DecorationRole:
        #     if col == 0:
        #         ftype = self.get_type(index)
        #         return(CFG.icons[ftype])
        #     return None

        if role == Q.Qt.TextAlignmentRole:
            if col == 1:
                return Q.Qt.AlignRight | Q.Qt.AlignVCenter
            return Q.Qt.AlignLeft | Q.Qt.AlignVCenter

        if role == Q.Qt.DisplayRole:
            if col == 2:
                text = super().data(index, role)
                if text:
                    # return time.ctime(int(text))
                    return time.strftime("%Y/%m/%d   %H:%M:%S", time.localtime(int(text)))

            if col == 0:
                fname = super().data(index, role)
                return osp.splitext(fname)[0]

        if role == Q.Qt.SizeHintRole:
            if col == 0:
                return Q.QSize(250, 20)

        return super().data(index, role)

    def get_path(self, index):
        """
        Returns the full path for a given item
        """
        path = self.data(index.sibling(index.row(), 3))
        return path if path else self.tests_path

    def get_type(self, index):
        """
        Returns the full path for a given item
        """
        ftype = self.data(index.sibling(index.row(), 4))
        return ftype if ftype else 'dir'

    def refresh(self):
        """
        Refresh action callback
        """
        self.updated.emit()

class BoldDelegate(Q.QStyledItemDelegate):
    """
    Item delegate for the navigation tree allowing to handle
    setting bold effect to selected files or folders fields.
    """

    def paint(self, painter, option, index):
        """
        On paint callback, check if the item needs to be bold
        """
        # get the full tree model
        model = index.model()
        bold = False

        current_path = model.get_path(index)
        if current_path and current_path == model.loaded:
            bold = True

        if bold:
            option.font.setWeight(Q.QFont.Bold)

        Q.QStyledItemDelegate.paint(self, painter, option, index)


def set_bold(action):
    """
    Decorates a given action's display in bold font
    """
    font = action.font()
    font.setBold(True)
    action.setFont(font)

def read_text_file(file_path):
    """
    Utility function for reading a text file in utf-8 or latin-1 formats
    """
    # Read contents as binary first, then try to decode...
    with open(file_path, 'rb') as text_file:
        file_contents_bytes = text_file.read()
    try:
        file_contents = file_contents_bytes.decode("utf-8")
    except UnicodeDecodeError:
        file_contents = file_contents_bytes.decode("latin-1")
    return file_contents

def filter_description(contents):
    """
    return the resume of the given path file
    """
    import html
    writeline = False
    header = True
    description = ''
    for line in contents.split('\n'):
        line = line.strip()

        if header:
            if ('DEBUT' in line or 'POURSUITE' in line):
                header = False
                description += '<br>\n'
        if not line.startswith('#'):
            continue

        line = line.replace('#', '').strip()
        line = line.replace('person_in_charge', 'Person in charge')
        if line.startswith('You should have received'):
            writeline = True
            continue
        if line.startswith('-----------') or line.startswith('along with '):
            continue

        if writeline and line.strip():
            text = '<b>{}</b><br>' if header else '<b>&nbsp; |</b><i>&nbsp;{}</i><br>\n'
            # description += text.format(line.lower().capitalize())
            description += text.format(html.escape(line).replace('  ', '&nbsp; '))

    return description

def valid_comm_file(fname):
    """
    Verifies if a given filename is conforming to the extension
    requirements.
    """
    if fname.endswith('.comm'):
        return True

    if '.com' in fname:
        return True

    return False
