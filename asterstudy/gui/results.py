# -*- coding: utf-8 -*-

# Copyright 2023 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""
Results
---------

This module implements the *Results* tab the *AsterStudy* application.
See `Results` class for more details.
"""

# pylint: disable=no-member,broad-except
# for paraview module

import os

from PyQt5 import Qt as Q

from ..common import (CFG, common_filters, get_file_name, is_medfile,
                      translate, wait_cursor)
from ..post import (RESULTS_PV_LAYOUT_NAME, RESULTS_PV_VIEW_NAME, BaseRep,
                    ColorRep, LocalFrameRep, ModesRep, PlotWindow, ResultFile,
                    WarpRep, clear_pipeline, create_axes, dbg_print,
                    get_active_selection, get_pv_mem_use, is_locframe,
                    process_gui_events, pvcontrol, selection_plot,
                    selection_probe, show_min_max)
from ..post.navigator import (OverlayBar, RepresentationParams,
                              ResultsNavigatorModel, ResultsNavigatorTree)
from . import get_icon

__all__ = ["Results"]

# note: the following pragma is added to prevent pylint complaining
#       about functions that follow Qt naming conventions;
#       it should go after all global functions

# pragma pylint: disable=too-many-instance-attributes


class Results(Q.QWidget):
    """Class that controls the behavior of the Results tab of the
     *AsterStudy* application."""

    # GUI related objects
    # ===================
    # >> pv_view: salome (view) of type ParaView (initializes paraview)
    # >> res_splitter: results tab main widget, based on a vertical splitter
    # >> pv_widget : the paraview widget

    # Paraview internals
    # ==================
    # >> pv_layout: paraview layout, specific for asterstudy
    # >> ren_view: paraview RenderView added inside pv_layout

    # Paraview key pipeline objects
    # =============================
    # >> current: currently active paraview reader source
    # >> previous: a dictionnary of all reader sources (for optimized access)
    # >> min_max_src: sources for displaying minimum and maximum values

    # Representation objects
    # ======================
    # >> shown: representation currently (related to source "current")
    #           at display

    _loader = res_splitter = pv_widget = pv_view = pv_layout = None
    ren_view = pv_overlay = toolbuttons = current = previous = None
    pv_widget_children = play_btn = pause_btn = outline_btn = None
    minmax_btn = infobar_label = shown = filename_combo = None
    delete_all_action = None

    min_max_src = []
    probing = True

    def __init__(self, astergui, parent=None):
        """
        Create/intialize the results tab.

        Arguments:
            parent (Optional[QWidget]): Parent widget. Defaults to
                *None*.
        """
        Q.QWidget.__init__(self, parent)

        self.setSizePolicy(Q.QSizePolicy.Expanding,
                           Q.QSizePolicy.Expanding)

        self.astergui = astergui
        self.previous = {}

        font = Q.QFont()
        font.setFamily("Arial")
        font.setPointSize(10)
        self.setFont(font)

        # Set up the main GUI layout that expands over the whole view
        hlayout = Q.QHBoxLayout(self)
        self.res_splitter = Q.QSplitter(Q.Qt.Horizontal, self)
        self.res_splitter.setSizePolicy(Q.QSizePolicy.Expanding,
                                        Q.QSizePolicy.Expanding)
        hlayout.addWidget(self.res_splitter)

        # pv_container = Q.QWidget(self)
        # pv_container.setSizePolicy(Q.QSizePolicy.Expanding,
        #                            Q.QSizePolicy.Expanding)
        # self.res_layout = Q.QHBoxLayout(pv_container)
        # # hlayout.addWidget(pv_container)
        # self.res_splitter.addWidget(pv_container)

        # Initialize the results navigator tree
        self.navtree = ResultsNavigatorTree(self)
        navmodel = ResultsNavigatorModel()
        navmodel.set_results(self)
        self.navtree.setModel(navmodel)

        side_container = Q.QSplitter(Q.Qt.Vertical, self)
        side_container.setMinimumWidth(420)
        side_container.setSizePolicy(Q.QSizePolicy.MinimumExpanding,
                                     Q.QSizePolicy.Expanding)

        # vlayout = Q.QVBoxLayout(side_container)

        # Initialize the parameter entry zone, to be positioned
        # just below the results navigator tree
        self.params = RepresentationParams()
        self.params.setMinimumHeight(350)
        self.params.applied.connect(self.apply_params)

        # Initialize the memory usage bar just below the parameter
        # entry zone, contains a progress bar and a clear button
        mem_widget = Q.QWidget()
        mem_widget.setMinimumHeight(35)
        mem_hlayout = Q.QHBoxLayout(mem_widget)

        mem_label = Q.QLabel(mem_widget)
        mem_label.setText('Memory')
        mem_label.setFont(font)
        mem_widget.setMinimumWidth(120)
        mem_hlayout.addWidget(mem_label)

        self.mem_bar = Q.QProgressBar(mem_widget)
        self.mem_bar.setFormat(r"%p% (%v MB used / %m MB total)")

        mem_hlayout.addWidget(self.mem_bar)
        # TODO some cases of freezing, temporarly disabled (see issue29214)
        add_button(mem_hlayout,
                   tooltip='Clear-up paraview pipeline',
                   icon='PVC Clear', callback=self.clear_paraview_pipeline)

        fname_widget = Q.QWidget()
        fname_hlayout = Q.QHBoxLayout(fname_widget)
        fname_hlayout.setContentsMargins(0, 0, 0, 0)
        fname_hlayout.setSpacing(2)
        self.filename_combo = Q.QComboBox()
        self.filename_combo.currentTextChanged.connect(
            self.file_selection_changed)

        # Add the combo to the layout
        fname_hlayout.addWidget(self.filename_combo)

        # Add a load button
        tool_tip = translate('AsterStudy',
                             'Load a new results file in MED format')
        load_med_btn = add_button(fname_hlayout,
                                  tooltip=tool_tip,
                                  icon='PVC Probe',
                                  callback=self.load_result)
        load_med_btn.setSizePolicy(Q.QSizePolicy.Fixed,
                                   Q.QSizePolicy.Fixed)
        load_med_btn.setFixedHeight(20)
        load_med_btn.setFixedWidth(20)

        font = Q.QFont()
        font.setFamily("Monospace")
        font.setPointSize(7)
        self.filename_combo.setFont(font)

        # Add the 4 widgets to the sidebar
        side_container.addWidget(fname_widget)
        side_container.addWidget(self.navtree)
        side_container.addWidget(self.params)
        side_container.addWidget(mem_widget)

        self.res_splitter.addWidget(side_container)
        self.refresh_navigator()
        self.orientation = None

    def set_as_working_tab(self):
        """
        Shows the results tab in the astergui workspace
        """
        from . import WorkingMode

        self.astergui.workSpace().main.setTabEnabled(
            WorkingMode.ResultsMode, True)
        self.astergui.workSpace().setWorkingMode(
            WorkingMode.ResultsMode)

    def refresh_navigator(self):
        """
        Refreshes items in the navigator side bar based on the
        information in the self.concepts
        """
        # Start by clearing the model the concepts
        self.navtree.model().clear()

        if self.current:
            filename = str(self.current.path)
            home_dir = os.getenv('HOME', None)
            if home_dir is not None:
                filename = filename.replace(home_dir, '~')

            self.filename_combo.blockSignals(True)
            if self.filename_combo.findText(filename) < 0:
                self.filename_combo.addItem(filename)
            self.filename_combo.setCurrentText(filename)
            self.filename_combo.blockSignals(False)

            for concept in self.current.concepts:
                if not concept.fields:
                    continue

                title = concept.name
                title = 'Other' if title == '<root>' else title

                parent = Q.QStandardItem(title)
                for field in concept.fields:
                    child1 = Q.QStandardItem(field.name)
                    child2 = Q.QStandardItem(field.info['label'])
                    child3 = Q.QStandardItem(field.info['pv-key'])
                    parent.appendRow([child1, child2, child3])

                self.navtree.model().appendRow(parent)

        self.navtree.customize()

    def detach(self, keep_pipeline=False):
        """
        Function called upon deactivating asterstudy, allows to properly
        remove all layouts and view from the ParaView View (salome View)
        """
        from .salomegui import get_salome_pyqt
        import pvsimple as pvs

        # Clears the PV widget and removes the event filter (right click
        # behavior) from all of its children.
        if self.pv_widget:
            for child in self.pv_widget_children:
                if child:
                    try:
                        child.removeEventFilter(self)
                    except BaseException: # pragma pylint: disable=broad-except
                        pass
            self.pv_widget_children = []
            self.pv_widget = None

        # This forces the creation of new overlay buttons upon restarting
        # the AsterStudy results tab
        self.pv_overlay = None
        self.toolbuttons = None

        # Deletes the active view and layout from paraview
        self.ren_view = None
        pvs.RemoveLayout(self.pv_layout)
        self.pv_layout = None

        # Close the (salome) view corresponding to 'ParaView'
        if self.pv_view:
            get_salome_pyqt().closeView(self.pv_view)
        self.pv_view = None

        if self.current:
            pvs.SetActiveSource(self.current.full_source)
        pvs.UpdatePipeline()
        pvs.Render()
        process_gui_events()

        # Unblock the Paravis DeleteAll action
        if self.delete_all_action:
            self.delete_all_action.blockSignals(False)

        # Clear all sources and proxies, leaving no trace!
        if not keep_pipeline:
            # ##################################################################
            # if self.current is not set to None this allows to reload the last
            # MED result file automatically upon reactivation of asterstudy
            # ##################################################################
            self.current = None
            self.previous = {}
            self.shown = None

            clear_pipeline()

    def init_paraview(self, full_load_pv=True):
        """
        Initializes, if necessary, paraview and creates a dedicated pvsimple
        view in the results tab.
        """
        from .salomegui import (get_salome_pyqt, get_salome_gui)

        if not self.pv_view:
            import time
            dbg_print(">> Initializing PV view for the results tab...")
            start = time.time()

            # A Paraview (view) already exists ?
            views = get_salome_pyqt().findViews('ParaView')
            if not views:
                if full_load_pv:
                    # This is necessary so that the app does not freeze
                    # upon playing an animation
                    paravis = get_salome_gui().getComponentUserName(
                        str('PARAVIS'))
                    get_salome_pyqt().activateModule(paravis)

                    # Block the Paravis DeleteAll action temporarily
                    # (see issue31372)
                    import SalomePyQt
                    menu = get_salome_pyqt().getPopupMenu(SalomePyQt.Edit)
                    self.delete_all_action = [a for a in menu.actions()
                                              if a.text() == 'Delete All'][0]
                    self.delete_all_action.blockSignals(True)

                    # Suppress all VTK errors and warnings
                    from paraview import vtk
                    vtk_out = vtk.vtkOutputWindow()
                    vtk_out.SetDisplayMode(vtk_out.NEVER)
                    vtk.vtkLogger.SetStderrVerbosity(vtk.vtkLogger.VERBOSITY_OFF)

                    # Reset the stdout for messages (overridden by Paravis)
                    import sys
                    sys.stdout = sys.__stdout__

                    asterstudy = get_salome_gui().getComponentUserName(
                        str('ASTERSTUDY'))
                    get_salome_pyqt().activateModule(asterstudy)

                    self._loader.terminate()
                else:
                    # This works OK but the animations freeze the app
                    get_salome_pyqt().createView('ParaView', True, 0, 0, True)

                views = get_salome_pyqt().findViews('ParaView')

            # get the last view (all others are empty)
            self.pv_view = views[-1]

            get_salome_pyqt().activateViewManagerAndView(self.pv_view)
            self.update_pv_layout_view()
            end = time.time()
            dbg_print("  Finished in %d seconds..." % int(end - start))

            self._finalize_pv_widget()
        else:
            self.update_pv_layout_view()
            self._finalize_pv_widget()

        if self.current:
            self.redraw()

    def _finalize_pv_widget(self):
        """
        References toolbuttons (for interactive selection) and updates the
        overlay widget if needed
        """
        if self.pv_widget:
            if not self.toolbuttons:
                self._add_toolbuttons()
            if not self.pv_overlay:
                self._add_overlay()

    def _add_toolbuttons(self):
        """
        Shortcut for referencing the toolbuttons that may need to be
        automatically activated for point and cell selections

        requires : self.pv_widget
        """
        self.toolbuttons = {'Interactive Select Cells On': None,
                            'Interactive Select Points On': None,
                            'Select Points On (d)': None,
                            'Select Cells On (s)': None,
                            }
        to_find = list(self.toolbuttons.keys())
        # Backward search since AsterStudy PV Layout is added after
        # the default one! ==>                              [::-1]
        for tbutt in self.pv_widget.findChildren(Q.QToolButton)[::-1]:
            if not to_find:
                break
            for tooltip in to_find:
                if tooltip in tbutt.toolTip():
                    self.toolbuttons[tooltip] = tbutt
                    to_find.remove(tooltip)
                    break

    def _add_overlay(self):
        """
        Add an overlay widget to the main pv_widget with a few buttons
        to control the view, save screenshots, etc.

        requires : self.pv_widget
        """

        # START
        # ol_height = 56 # Overlay height in pixels
        ol_height = 62  # Overlay height in pixels
        self.pv_overlay = OverlayBar(self.pv_widget, height=ol_height,
                                     botline=(0, 0, 255, 2)
                                     )

        # >> Buttons toolbar items
        #    Start with an empty shell widget, used for parenting toolbar
        #    buttons and enforcing a simple horizontal layout with a
        #    right spacer
        hlayo = Q.QHBoxLayout()
        hlayo.setContentsMargins(5, 0, 5, 5)
        hlayo.setSpacing(5)
        add_button(hlayo, tooltip='Refresh view',
                   icon='PVC Refresh',
                   callback=self.redraw)
        add_separator(hlayo)

        # Camera controls
        add_button(hlayo, tooltip='Project view to X (YZ-plane)',
                   icon='PVC XProj',
                   callback=lambda: pvcontrol(self, 'xproj'))
        add_button(hlayo, tooltip='Project view to Y (XZ-plane)',
                   icon='PVC YProj',
                   callback=lambda: pvcontrol(self, 'yproj'))
        add_button(hlayo, tooltip='Project view to Z (XY-plane)',
                   icon='PVC ZProj',
                   callback=lambda: pvcontrol(self, 'zproj'))
        add_button(hlayo, tooltip='Toggle orthogonal/perspective camera projection',
                   icon='PVC Ortho',
                   callback=lambda: pvcontrol(self, 'ortho_proj'))
        add_separator(hlayo)

        # Time and animation controls
        add_button(hlayo, tooltip='First time step',
                   icon='PVC TFirst',
                   callback=lambda: pvcontrol(self, 'first'))
        add_button(hlayo, tooltip='Previous time step',
                   icon='PVC TPrev',
                   callback=lambda: pvcontrol(self, 'tprev'))
        self.play_btn = add_button(hlayo, tooltip='Play',
                                   icon='PVC Play',
                                   callback=lambda: pvcontrol(self, 'play'))
        self.pause_btn = add_button(hlayo, tooltip='Pause',
                                    icon='PVC Pause',
                                    callback=lambda: pvcontrol(self, 'pause'))
        self.pause_btn.setVisible(False)
        add_button(hlayo, tooltip='Next time step',
                   icon='PVC TNext',
                   callback=lambda: pvcontrol(self, 'tnext'))
        add_button(hlayo, tooltip='Last time step',
                   icon='PVC TLast',
                   callback=lambda: pvcontrol(self, 'last'))
        add_separator(hlayo)

        # Display controls
        self.outline_btn = add_button(hlayo, tooltip='Toggle bounding box',
                                      icon='PVC Outline',
                                      callback=lambda: pvcontrol(
                                          self, 'outline'),
                                      checkable=True)
        add_button(hlayo, tooltip='Toggle reference position',
                   icon='PVC Reference',
                   callback=lambda: pvcontrol(self, 'reference'),
                   checkable=False)
        self.minmax_btn = add_button(hlayo, tooltip='Toggle min/max',
                                     icon='PVC MinMax',
                                     callback=lambda: pvcontrol(
                                         self, 'min_max'),
                                     checkable=True)
        add_button(hlayo, tooltip='Rescale colorbar to current step range',
                   icon='PVC Rescale',
                   callback=lambda: pvcontrol(self, 'rescale_colorbar'),
                   checkable=False)

        # TODO ADD THE PROBE CONTROL HERE
        add_separator(hlayo)
        add_button(hlayo, tooltip='Probe values on one or more points or cells',
                   icon='PVC Probe',
                   callback=lambda: pvcontrol(self, 'probe'))
        add_button(hlayo, tooltip='Plot data over time for a single point or cell',
                   icon='PVC Plot',
                   callback=lambda: pvcontrol(self, 'plot_over_time'))
        add_separator(hlayo)

        # Export view & animation
        add_button(hlayo, tooltip='Save a screenshot of the current representation',
                   icon='PVC Screenshot',
                   callback=lambda: pvcontrol(self, 'screenshot'))
        add_button(hlayo, tooltip='Save a movie of the current animation',
                   icon='PVC Movie',
                   callback=lambda: pvcontrol(self, 'movie'))
        spacer = Q.QSpacerItem(25, 25, hPolicy=Q.QSizePolicy.Expanding)
        hlayo.addItem(spacer)

        # >> Information labels bar
        self.infobar_label = Q.QLabel(self.pv_overlay)
        hlayo.addWidget(self.infobar_label)
        self.update_infobar()

        self.pv_overlay.setLayout(hlayo)

    def update_pv_layout_view(self, full_update=True):
        """
        Updates or creates a new PV layout and view for AsterStudy
        post processing in the Results tab
        """
        # pragma pylint: disable=too-many-locals
        import pvsimple as pvs
        from .salomegui import get_salome_pyqt

        pv_layout = pvs.GetLayoutByName(RESULTS_PV_LAYOUT_NAME)
        if not pv_layout:
            pv_layout = pvs.CreateLayout(name=RESULTS_PV_LAYOUT_NAME)
        views = pvs.GetViewsInLayout(pv_layout)
        if not views:
            pv_view = pvs.CreateRenderView(guiName=RESULTS_PV_VIEW_NAME)
            # Needed starting paraview 5.7
            pvs.AssignViewToLayout(view=pv_view, layout=pv_layout)
            views = [pv_view]
        self.ren_view = views[0]
        self.pv_layout = pv_layout

        # Activate anti aliaising
        self.renderer.UseFXAAOn()

        # Some basic customizations
        pvs.SetActiveView(self.ren_view)
        pvs.LoadPalette(paletteName='WhiteBackground')

        if not full_update:
            return

        self.add_logo(CFG.rcfile('results-pv-bg.png'), 'salome_meca 2023')
        self.orientation = self.add_orientation_axes()

        # Reinforce event filter installation for capturing mouse
        # and keyboard events on the imported paraview view
        self.pv_widget = get_salome_pyqt().getViewWidget(self.pv_view)
        self.pv_widget.setSizePolicy(Q.QSizePolicy.Expanding,
                                     Q.QSizePolicy.Expanding)
        self.res_splitter.addWidget(self.pv_widget)
        # The following insures that the view is refreshed
        self.res_splitter.widget(1).setVisible(True)
        self.res_splitter.setVisible(False)
        self.res_splitter.setVisible(True)

        pvs.Render()
        ################################################################
        # COMMENTED : RETRIEVE THE 3D PV FRAME
        # frames = self.pv_widget.findChildren(Q.QFrame)
        # for frame in frames[::-1]:
        #     if type(frame) == Q.QFrame:
        #         break
        # self.pv_frame = frame
        # self.pv_frame.setContextMenuPolicy(Q.Qt.CustomContextMenu)
        # self.pv_frame.customContextMenuRequested.connect(
        #    lambda: results.navtree.contextMenuEvent('Representation'))
        ################################################################
        # To create the context menu;
        # context = Q.QContextMenuEvent(Q.QContextMenuEvent.Mouse,
        #                               self.cursor().pos())
        # Q.QCoreApplication.postEvent(self.pv_frame, context)
        ################################################################

        self.pv_widget_children = [self.pv_widget]
        self.pv_widget_children += self.pv_widget.findChildren(Q.QWidget)

        to_ignore = []
        if self.pv_overlay:
            to_ignore = [self.pv_overlay] + \
                self.pv_overlay.findChildren(Q.QWidget)

        for child in self.pv_widget_children:
            if not child in to_ignore:
                child.installEventFilter(self)

        # Account for the possibility that some readers are deleted by the user
        # in the paravis module, keep only the available readers
        proxy_man = pvs.servermanager.ProxyManager()
        available_sources = list(
            proxy_man.GetProxiesInGroup('sources').values())
        unavailable = []
        for path in self.previous:
            prev, _ = self.previous[path]
            if not prev.full_source in available_sources:
                unavailable.append(path)
                continue

            for source in ['filter_source', 'extract_source', 'source',
                           'mode_source', 'dup_source']:
                src = getattr(self.current, source)
                if src and not src in available_sources:
                    setattr(self.current, source, None)

        for path in unavailable:
            self.previous[path][0].full_source = None
            self.previous.pop(path, None)

        self.filename_combo.blockSignals(True)
        self.filename_combo.clear()
        for pre in self.previous:
            home_dir = os.getenv('HOME', None)
            if home_dir is not None:
                pre = pre.replace(home_dir, '~')
            self.filename_combo.addItem(pre)
        self.filename_combo.blockSignals(False)

        if self.current:
            do_delete = False
            for source in ['filter_source', 'extract_source', 'source',
                           'full_source', 'mode_source', 'dup_source']:
                src = getattr(self.current, source)
                if src and src not in available_sources:
                    setattr(self.current, source, None)
                    do_delete = True
            if do_delete:
                self.current = None

        BaseRep.refresh_available_sources()

    def clear_paraview_pipeline(self):
        """
        Clears up intermediate paraview pipeline sources and
        refreshes the current representation
        """
        wait_cursor(True)
        self.shown.clear_sources()
        self.clear_readers()
        self.redraw()
        wait_cursor(False)

    def clear_readers(self):
        """
        Clears readers from the paraview pipeline not relevant
        to the current representation
        """
        import pvsimple as pvs
        pxm = pvs.servermanager.ProxyManager()


        # Efficient access to paraview pipeline source proxies
        pvsrc_proxies = pxm.GetProxiesInGroup('sources')
        source_objs = list(pvsrc_proxies.values())
        source_names = [k[0] for k in list(pvsrc_proxies.keys())]

        # Utility function that deletes a paraview proxy
        def remove_pvs_source(src):
            """
            Given a paraview object (source), unregister the proxy
            from the pipeline, then delete the object
            """
            if not src:
                return False
            try:
                if src in source_objs:
                    src_name = source_names[source_objs.index(src)]
                    pxm.UnRegisterProxy('sources', src_name, src)
                pvs.Delete(src)
                del src
                return True
            except RuntimeError:
                return False

        to_remove = []
        for path in self.previous:
            if path == self.current.path:
                continue
            prev, _ = self.previous[path]

            for source in ['filter_source', 'extract_source', 'source',
                           'full_source', 'mode_source', 'dup_source',
                           'mesh_proxy']:
                if hasattr(prev, source):
                    src = getattr(prev, source)
                    setattr(prev, source, None)
                    remove_pvs_source(src)
            to_remove.append(path)

        for path in to_remove:
            self.previous.pop(path, None)

        for source in ['mode_source', 'dup_source']:
            if hasattr(self.current, source):
                src = getattr(self.current, source)
                setattr(self.current, source, None)
                remove_pvs_source(src)

        del pvsrc_proxies
        del source_objs
        del source_names


    def minmax_shown(self):
        """Returns whether the minmax button is checked"""
        return self.minmax_btn.isChecked()

    def set_minmax_shown(self):
        """Checks the minmax button"""
        self.minmax_btn.setChecked(True)

    def outline_shown(self):
        """Returns whether the outline button is checked"""
        return self.outline_btn.isChecked()

    def set_outline_shown(self):
        """Checks the outline button"""
        self.outline_btn.setChecked(True)

    def load_med_result(self, med_fn, loader):
        """
        Load a results file in MED format

        Arguments:
            med_fn (string): full path to the MED filename to be loaded
            loader
        """
        if not med_fn:
            dbg_print("Invalid med file")
            return

        self._loader = loader
        self._loader.start()
        Q.QTimer.singleShot(50, lambda: self.load_med_result_call(med_fn))

    def load_med_result_call(self, med_fn, full_load_pv=True):
        """
        Load a results file in MED format

        Arguments:
            med_fn (string): full path to the MED filename to be loaded
        """
        # Initialize paraview widget in asterstudy gui
        # (this can take a few seconds on first load)
        new_load = True

        self.init_paraview(full_load_pv=full_load_pv)
        self.shown = None

        modif_time = os.path.getmtime(med_fn)
        if med_fn in self.previous:
            # This file has already been read, check if the modification date
            # is identifical to the previous load, if so then just set it as current
            new_load = (modif_time != self.previous[med_fn][1])

        if new_load:
            if not self._loader:
                from . salomegui import LoadingMessage
                self._loader = LoadingMessage(self, 'Please wait...', True)
                self._loader.start()

            res = ResultFile(med_fn)

            # Check if there are indeed fields that can be represented
            if res.is_empty():
                if self._loader:
                    self._loader.terminate()
                wait_cursor(False)

                if self.astergui:
                    msg = translate("AsterStudy",
                                    "The provided MED file contains no "
                                    "result concepts or fields.\n")
                    buttons = Q.QMessageBox.Ok
                    Q.QMessageBox.warning(self.main_window, "AsterStudy",
                                          msg, buttons, Q.QMessageBox.Ok)

                return

            self.current = res
            self.previous.update({self.current.path: (self.current, modif_time)})
        else:
            self.current = self.previous[med_fn][0]

        self.ren_view.ResetCamera()

        self.refresh_navigator()
        pvcontrol(self, 'first')

        # Show displacement field preferentially by default
        for concept in self.current.concepts:
            for field in concept.fields:
                if 'DEPL' in field.name:
                    self.represent(field, WarpRep)
                    pvcontrol(self, 'resetview')
                    return

        # If not found, show the first field that is found
        for concept in self.current.concepts:
            for field in concept.fields:
                self.represent(field)
                pvcontrol(self, 'resetview')
                return

    def apply_params(self):
        """
        Called when the parameters are changed from the parameters
        modification box (and the Apply button clicked)
        """
        rep = self.params.rep
        if rep:
            new_opts = self.params.values()
            self.represent(rep.field, rep.__class__, False, **new_opts)

    def represent(self, field, repclass=None, forced=False, **opts):
        """
        Adds or overloads a representation of a result field

        field: ConceptField, field to be represented
        repclass: class definition of the representation
        opts: additional options for initializing the representation
        """
        import pvsimple as pvs

        # self.ren_view = pvs.FindViewOrCreate(RESULTS_PV_VIEW_NAME, 'RenderView')
        # print "in results.represent"
        # print "   >> self.ren_view", self.ren_view

        # Activate the hour-glass animation
        wait_cursor(True)

        # Default representation
        if repclass is None:
            repclass = LocalFrameRep if is_locframe(field) else ColorRep

        updated = False
        # if isinstance(self.shown, repclass) and not forced:
        if isinstance(self.shown, repclass) and not forced:
            if self.shown.field == field:
                self.shown.update_(opts)
                updated = True

        if not updated:
            pvs.HideAll(self.ren_view)

            # Uncheck minmax
            self.minmax_btn.setChecked(False)
            #######################################################
            # Here is the actual call to the representation class #
            #######################################################
            self.shown = repclass(field, **opts)
            # Animate modes upon the initialization of a ModesRep
            if isinstance(self.shown, ModesRep):
                self.shown.animate()

        if self.minmax_shown():
            show_min_max(self.shown)

        wait_cursor(False)

        # Render the new or modified representation
        pvs.Render()

        # Update the infobar (text above the representation view)
        self.update_infobar()

        if updated:
            self.params.update_params()
        else:
            self.params.set_representation(self.shown)

        # Insure that the results tab is the one being shown,
        # hide the loader overlay, deactivate the hour-glass
        self.set_as_working_tab()
        if self._loader:
            self._loader.terminate()
        wait_cursor(False)

    def update_infobar(self):
        """
        Method used to update the information bar below the post-processing
        controls based on the shown field (uses self.shown)
        """
        info = 'No data loaded'
        fsuffix = ''
        if self.shown:
            field, opts = self.shown.field, self.shown.opts
            comp = opts.get('Component', '')
            if 'ColorField' in opts:
                cfield = opts['ColorField']
                if cfield != field:
                    fsuffix = ', colored by %s' % (
                        cfield.info['label'].split('(')[0])
                    if len(cfield.info['components']) > 1:
                        fsuffix += ' [%s]' % (comp)

            if not fsuffix:
                if len(field.info['components']) > 1:
                    fsuffix = ' [%s]' % (comp)

            ctime = self.ren_view.ViewTime
            # info = '<B>Concept :</B> %s; '\
            #        '<B>Field :</B> %s%s; '\
            #        '<B>Current time/frequency :</B> %g'\
            #        %(field.concept.name,
            #          field.info['label'], fsuffix, ctime)

            info = '<B><span style="color: #ffffff; background-color: #1d71b8;">'\
                   '&nbsp;Concept&nbsp;</B></span>&nbsp;%s'\
                   '&nbsp;<B><span style="color: #ffffff; background-color: #1d71b8;">'\
                   '&nbsp;Field&nbsp;</B></span>&nbsp;%s%s'\
                   '&nbsp;<B><span style="color: #ffffff; background-color: #1d71b8;">'\
                   '&nbsp;Time/Frequency&nbsp;</B></span>&nbsp;%g'\
                   % (field.concept.name,
                      field.name, fsuffix, ctime)

        if self.infobar_label:
            self.infobar_label.setText(info)

        if self.mem_bar:
            current, available = get_pv_mem_use()
            self.mem_bar.setRange(0, int(available / 1024.))
            self.mem_bar.setValue(int(current / 1024.))

    def redraw(self):
        """
        Redraws the current field
        """
        self.update_pv_layout_view(full_update=False)
        if self.shown and self.current:
            repclass = self.shown.__class__
            field, opts = self.shown.field, self.shown.opts
            self.shown = None
            self.represent(field, repclass, **opts)
            pvcontrol(self, 'resetview')

    # pragma pylint: disable=invalid-name
    def eventFilter(self, source, event):
        """
        EventFilter for capturing mouse clicks over the ParaView
        widget.
        """
        if not hasattr(self, 'pv_widget_children'):
            return 0

        if source in self.pv_widget_children:
            if event.type() == Q.QEvent.MouseButtonPress:
                # self.on_click_callback()
                if hasattr(event, 'button'):
                    if event.button() == Q.Qt.RightButton:
                        return 1
            elif event.type() == Q.QEvent.MouseButtonRelease:
                self.on_click_callback()
                if hasattr(event, 'button'):
                    if event.button() == Q.Qt.RightButton:
                        self.navtree.contextMenuEvent('Representation')
                        return 1
            elif event.type() == Q.QEvent.MouseButtonDblClick:
                pvcontrol(self, 'clear_selection')
                return 1

        return Q.QWidget.eventFilter(self, source, event)

    def on_click_callback(self):
        """
        Callback to launch either a probing or plot operation based
        on the user selection
        """
        if not self.shown:
            return

        if not self.shown.pickable:
            return

        selection, _, _ = get_active_selection(self.shown.source)
        if selection:
            self.probe_plot_callback()
        else:
            Q.QTimer.singleShot(100, self.probe_plot_callback)

    def probe_plot_callback(self):
        """
        Delayed probe as to allow selection to be coined
        """
        if not self.shown:
            return

        selection, _, _ = get_active_selection(self.shown.source)
        if selection:
            if self.probing:
                selection_probe(self)
            else:
                selection_plot(self)
                self.probing = True

    def plot(self, data, variable):
        """
        Adds a popup dialog with a plot of the given data
        """
        dialog = Q.QDialog(self)
        dialog.ui = PlotWindow(data=data, variable=variable)
        dialog.ui.setWindowTitle('AsterStudy - Selection plot over time')
        dialog.ui.show()

    @Q.pyqtSlot(str)
    def file_selection_changed(self, filename):
        """
        Slot called upon changing the filename combo (bottom left sidebar).
        Loads the chosen file, replaces "~" by the home dir if needed.
        """
        if '~' in filename:
            home_dir = os.getenv('HOME', None)
            if home_dir is not None:
                filename = filename.replace('~', home_dir)

        self.load_med_result_call(filename, full_load_pv=False)

    def load_result(self):
        """Browse and load a results file in MED format in the Results tab."""
        file_name = get_file_name(1, self.main_window,
                                  "Select a results file in MED format", "",
                                  [common_filters()[0], common_filters()[-1]])
        if not file_name:
            return

        if not is_medfile(file_name):
            msg = translate("AsterStudy",
                            "The selected file is not in MED format.")
            Q.QMessageBox.critical(self.main_window,
                                   "AsterStudy",
                                   msg)
            return

        self.load_med_result_call(file_name)

    @property
    def main_window(self):
        """
        Returns the Qt main window
        """
        if self.astergui is not None:
            return self.astergui.mainWindow()
        from .salomegui import get_salome_pyqt
        return get_salome_pyqt().getDesktop()

    def add_orientation_axes(self):
        """
        Adds custom orientation axes and hides the default one provided
        by paraview
        """
        import vtk

        # For some reason Paraview does not always like setting the
        # default orientation axes visibility using python...
        try:
            self.ren_view.OrientationAxesVisibility = 0
        except BaseException:
            pass

        axes = create_axes()
        axes.SetTotalLength(2., 2., 2.)

        orientation = vtk.vtkOrientationMarkerWidget()
        orientation.SetOrientationMarker(axes)
        orientation.SetInteractor(self.interactor)
        orientation.EnabledOn()
        orientation.InteractiveOff()
        orientation.SetViewport(0.0, 0.15, 0.20, 0.35)
        orientation.SetDefaultRenderer(self.renderer)

        return orientation


    def add_logo(self, file_name, text=''):
        """
        Adds version text and software logo in bottom left position
        """
        import vtk
        if text:
            actor = vtk.vtkTextActor()
            actor.SetPosition(65, 30)
            actor.GetTextProperty().SetFontSize(12)
            actor.GetTextProperty().SetColor(0.0, 0.0, 0.0)
            actor.SetInput(text)
            actor.PickableOff()
            self.renderer.AddActor2D(actor)

        reader = vtk.vtkPNGReader()
        reader.SetFileName(file_name)
        reader.Update()

        mapper = vtk.vtkImageMapper()
        mapper.SetInputData(reader.GetOutput())
        mapper.SetColorWindow(255)
        mapper.SetColorLevel(127.5)

        image = vtk.vtkActor2D()
        image.SetMapper(mapper)
        image.PickableOff()

        self.renderer.AddActor(image)

        # Disabled because it does not change anything in SALOME 9.10
        # self.ren_view.Background = [1.0, 1.0, 1.0]

    @property
    def renderer(self):
        """
        Returns the paraview renderer associated to the ren_view
        """
        return self.ren_view.SMProxy.GetRenderer()

    @property
    def interactor(self):
        """
        Returns the paraview renderer associated to the ren_view
        """
        return self.ren_view.SMProxy.GetInteractor()

def add_button(layout, name='', tooltip='', icon=None,
               callback=None, checkable=False):
    """
    Adds a push button to the given parent widget and layout with some
    user-defined properties
    """
    button = Q.QPushButton(name)

    if tooltip:
        button.setToolTip(tooltip)
    if icon:
        button.setIcon(get_icon(icon))
    button.setCheckable(checkable)
    if checkable:
        button.toggled.connect(callback)
    else:
        button.clicked.connect(callback)

    layout.addWidget(button)
    return button


def add_separator(layout, width=2, color='#fff'):
    """
    Adds a "fake" separator
    """
    sep = Q.QWidget()
    sep.setMinimumWidth(width)
    sep.setMaximumWidth(width)
    # White makes the separator hidden
    sep.setStyleSheet('background-color:{};'.format(color))
    sep.setSizePolicy(Q.QSizePolicy.Fixed,
                      Q.QSizePolicy.Minimum)
    layout.addWidget(sep)
    return sep
