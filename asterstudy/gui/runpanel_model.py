# -*- coding: utf-8 -*-

# Copyright 2016-2018 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""
Run panel - Model
-----------------

This module implements the Qt model used for the Run Panel.

"""

import os.path as osp

from PyQt5 import Qt as Q

from ..datamodel.job_informations import JobInfos
from ..datamodel.parametric import output_commands

# note: the following pragma is added to prevent pylint complaining
#       about functions that follow Qt naming conventions;
#       it should go after all global functions
# pragma pylint: disable=invalid-name


class RunPanelModel(Q.QStandardItemModel):
    r"""Model for the Run Panel used for the Job Informations

    - Root item

        - row 0: param (1 column)

        - row 1: table (nb variables rows, 4 columns)

        - row 2: commands (1 column)
    """
    # indexes of "scalar" values in param column
    #   fields for the GUI state
    KGui = ('casename', 'obsshape', 'npypath', 'npyshape', 'okshape')
    #   - "in table" fields
    KInTab = ('input_vars', 'input_init', 'input_bounds')
    #   + job informations
    Key2Row = {key: i for i, key in enumerate(
        set(JobInfos.Defs).union(KGui).difference(KInTab))}

    def __init__(self, parent):
        super().__init__(parent=parent)
        root = self.invisibleRootItem()
        root.appendRows(
            [Q.QStandardItem(),
             Q.QStandardItem(),
             Q.QStandardItem()])
        self.table.setColumnCount(4)

    def reset(self):
        """Remove all items except the structure"""
        # clear() remove all items + headers...
        self.param.setRowCount(0)
        self.table.setRowCount(0)
        self.commands.setRowCount(0)

    @property
    def param(self):
        """*Item*: Attribute that holds the 'param' item."""
        return self.invisibleRootItem().child(0, 0)

    @property
    def table(self):
        """*Item*: Attribute that holds the 'table' item."""
        return self.invisibleRootItem().child(1, 0)

    @property
    def commands(self):
        """*Item*: Attribute that holds the 'commands' item."""
        return self.invisibleRootItem().child(2, 0)

    def data(self, index, role):
        """Retun data for the given role"""
        value = super().data(index, role)

        if index.parent() == self.table.index():
            if index.column() == 0:
                if role == Q.Qt.FontRole:
                    font = Q.QFont()
                    font.setBold(True)
                    return font
                if role == Q.Qt.ForegroundRole:
                    return Q.QColor('blue')
                if role == Q.Qt.TextAlignmentRole:
                    return Q.Qt.AlignVCenter + Q.Qt.AlignLeft

            if role == Q.Qt.DisplayRole:
                if value is None:
                    return ""
                return str(value)

            if role == Q.Qt.TextAlignmentRole:
                return Q.Qt.AlignVCenter + Q.Qt.AlignRight
        return value

    def appendRowInTable(self, name, value, mini, maxi, checked):
        """Append a row in the table part of the model"""
        item = Q.QStandardItem(name)
        item.setCheckable(True)
        item.setCheckState(Q.Qt.Checked if checked else Q.Qt.Unchecked)

        parent = self.table
        row = parent.rowCount()
        parent.setChild(row, 0, item)
        self.setData(self.index(row, 1, parent.index()), value)
        self.setData(self.index(row, 2, parent.index()), mini)
        self.setData(self.index(row, 3, parent.index()), maxi)

    def appendCommand(self, filename, unit, para):
        """Store a command"""
        item = Q.QStandardItem()
        item.setText(osp.basename(filename))
        item.setData((filename, unit, para), Q.Qt.UserRole)
        item.setToolTip(filename)

        parent = self.commands
        row = parent.rowCount()
        parent.setChild(row, item)

    def findCommand(self, unit):
        """Returns the index of the item for the given file unit
        (the filename or parameters names may have changed.).

        Arguments:
            unit (int): File unit number.

        Returns:
            int: Index of item, or -1.
        """
        for row in range(self.commands.rowCount()):
            idx = self.index(row, 0, self.commands.index())
            data = self.data(idx, Q.Qt.UserRole)
            if data and data[1] == unit:
                return row
        return -1

    def set_param(self, key, value):
        """Helper function to set "scalar" datas"""
        row = self.Key2Row[key]
        item = Q.QStandardItem()
        item.setData(value, Q.Qt.UserRole)

        parent = self.param
        parent.setChild(row, item)

    def get_param(self, key):
        """Helper function to get "scalar" datas"""
        row = self.Key2Row[key]
        idx = self.index(row, 0, self.param.index())
        return self.data(idx, Q.Qt.UserRole)

    def used_variables(self):
        """Get the selected variables as a list of [name, initial, bounds]"""
        values = []
        tbx = self.table.index()
        for row in range(self.table.rowCount()):
            item = self.table.child(row, 0)
            if item.checkState() == Q.Qt.Checked:
                values.append([
                    str(item.text()),
                    self.data(self.index(row, 1, tbx), Q.Qt.EditRole),
                    [
                        self.data(self.index(row, 2, tbx), Q.Qt.EditRole),
                        self.data(self.index(row, 3, tbx), Q.Qt.EditRole)
                    ]
                ])
        return values

    def import_from(self, case):
        """Import values from the given *Case*."""
        self.reset()
        if case:
            if case.has_remote:
                self.set_param('remote_folder', case.model.remote_folder_base)
            case.job_infos.add_defaults(overwrite=False)
            get = case.job_infos.get
            variables = sorted(case.variables.items())
            commands = output_commands(case)
        else:
            get = JobInfos().get
            variables = []
            commands = []
        # 'commands' must be filled before 'output_unit'
        for command in commands:
            unit = command.storage.get('UNITE')
            stage = command.stage
            filename = stage.handle2file(unit)
            if unit is None or filename is None:
                continue
            self.appendCommand(filename, unit, command.storage.get("NOM_PARA"))
        # param
        for key in JobInfos.Defs:
            if key not in RunPanelModel.KInTab:
                self.set_param(key, get(key))
        # table
        for name, variable in variables:
            value = variable.evaluation
            if not isinstance(value, (int, float)):
                continue
            invars = get("input_vars")
            used = name in invars
            mini, maxi = None, None
            if used:
                mini, maxi = get("input_bounds")[invars.index(name)]
            self.appendRowInTable(name, value, mini, maxi, used)

    def export_as_jobinfos(self):
        """Export the internal state as a *JobInfos* object.

        Returns:
            JobInfos: New *JobInfos* object.
        """
        job_infos = JobInfos()
        setter = job_infos.set
        # param
        for key in JobInfos.Defs:
            if key not in RunPanelModel.KInTab:
                setter(key, self.get_param(key))
        # table
        used = self.used_variables()
        setter("input_vars", [info[0] for info in used])
        setter("input_init", [info[1] for info in used])
        setter("input_bounds", [info[2] for info in used])
        # commands: nothing
        return job_infos

    def export_as_dict(self):
        """Export the internal state as a *dict* object.

        Returns:
            dict: Parameters values as a dict.
        """
        return self.export_as_jobinfos().asdict()
