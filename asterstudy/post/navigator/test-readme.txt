"""
# IN ORDER TO RECOVER THE RESULTS CLASS FROM SALOME PYTHON
import ASTERSTUDYGUI as asgui
maingui = asgui.get_aster_gui()
results = maingui.work_space.view(8)

# IN ORDER TO LAUNCH ALL POST PROCESSING TESTS
./check_salome.sh -c --report=post 'test_post_*.py'

# REPRINT COVERAGE
./check_coverage.sh --report=post --report-only

# IN ORDER TO LAUNCH ALL TESTS
./check_salome.sh -ac --report=post



+ coverage report --rcfile=/home/J64371/dev/smeca/salome-codeaster-study/.coveragerc --fail-under=50 --skip-covered '--include=asterstudy/post/*'
Name                                                Stmts   Miss  Cover   Missing
---------------------------------------------------------------------------------
asterstudy/post/navigator/categorical_colorbar.py     186    160    14%   26-30, 33-35, 38-39, 47-48, 54-62, 66-78, 83-105, 110-124, 131, 135-155, 161-198, 205-240, 249-270, 279-301, 306-308, 311, 314-316, 319-320
asterstudy/post/navigator/group_filter.py              84     61    27%   34, 41, 46-51, 55, 60, 64-69, 73-94, 100-103, 109-111, 117-118, 125-148, 153-155, 158-159, 162, 165-166
asterstudy/post/navigator/model.py                     32     22    31%   43, 46, 49-50, 54, 63-82
asterstudy/post/navigator/overlay_bar.py               40     31    22%   34-51, 55-56, 60, 63-80, 84, 88
asterstudy/post/navigator/params.py                   472    426    10%   80-93, 96-100, 107-187, 194-300, 314-352, 360-386, 393-413, 421-547, 560-569, 575-599, 607-612, 616-621, 624-627, 631-636, 639-645, 648-654, 657-665, 668-675, 678-683, 686, 699-712, 721-730, 736-754
asterstudy/post/navigator/view.py                     290    272     6%   48-55, 61-65, 72-90, 96-111, 123-436, 444-471, 479-480, 486-488, 494-522
asterstudy/post/plotter/graph_canvas.py                72     59    18%   46-71, 74-77, 83-94, 100-119, 125-149
asterstudy/post/plotter/plot_window.py                 50     42    16%   40-87, 91-97, 101
asterstudy/post/plotter/table.py                      341    295    13%   28-41, 54-57, 61, 71, 75-83, 87-106, 113-184, 190-193, 197-215, 219-225, 231, 236-272, 277-281, 289-310, 316-319, 324, 334-337, 342-348, 353-358, 363-378, 394-398, 402, 406, 410, 414-423, 428-457, 461-473, 478-488, 492-508, 514-535, 539-541, 545-553, 560-567, 572-574, 577, 589-593, 599-601, 604-607
asterstudy/post/representation/base_rep.py            257     26    90%   98-107, 165, 171-180, 402, 411, 506-509, 512, 514
asterstudy/post/representation/color_rep.py           212     11    95%   73, 76, 153, 210, 388-392, 408, 419-420
asterstudy/post/representation/modes_rep.py           145     22    85%   45, 86-89, 93, 96-109, 182, 198, 201, 246, 254-256
asterstudy/post/representation/vector_rep.py           58      2    97%   79-80
asterstudy/post/representation/warp_rep.py            102      6    94%   165-170
asterstudy/post/result_data.py                        138      1    99%   191
asterstudy/post/utils.py                              591    368    38%   61, 90, 133, 155, 219-221, 231-249, 264, 336, 347-349, 357-358, 365-569, 577-591, 600-636, 652, 700-701, 704, 713-725, 733-802, 809-866, 883, 897-936, 953, 979-983, 991
---------------------------------------------------------------------------------
TOTAL                                                3175   1804    43%


"""
