# Module AsterStudy

## Développement

Cloner le dépôt (en *SSH*) :

```bash
mkdir -p $HOME/dev/smeca
cd $HOME/dev/smeca
git clone git@gitlab.pleiade.edf.fr:salomemeca/modules/salome-asterstudy.git
```

Si l'accès en *SSH* ne fonctionne pas, on peut faire :

```bash
git clone https://gitlab.pleiade.edf.fr/salomemeca/modules/salome-asterstudy.git
```

Se placer sur la branche souhaitée ou créer une nouvelle branche...

### Nommage des branches

Les branches **edf/default** et **default** du dépôt Mercurial sont
respectivement devenues **master** et **occ**.

## Environnement de développement

On développe dans un conteneur de salome_meca. Le nom évolue selon la version
de salome_meca. On utilise dans les commandes suivantes un alias pour suivre
les changements de nom.

Les conteneurs peuvent être trouvés [sur cette page](http://codeaster.pages.pleiade.edf.fr/lab/salome-meca-containers/tools/changelog.html).

```bash
alias ct=${HOME}/containers/salome_meca-lgpl-2021.0.0-1-20210811-scibian-9
```

Pour les vérifications, le lancement des tests unitaires, le *code-coverage*,
etc., il faut installer quelques prérequis.
Le script de lancement du conteneur isole `$HOME/.local` de la machine hôte.
Il n'est donc pas nécessaire d'utiliser virtualenv.

### Installation des prérequis avec `pip`

On aura besoin de passer le proxy. On peut le faire hors ou dans le conteneur,
les variables d'environnement `http_proxy` et `https_proxy` vont suivre.

Dans l'environnement du conteneur (`ct --shell`), on fait :

```bash
module load python
python3 -m pip install --user --upgrade pip
python3 -m pip install --user -r .requirements.txt
```

Le script du conteneur fait que *pip* utilise en fait
`$HOME/.config/aster/salome_meca-edf-2020/.local`.

## Installation

Si on ne souhaite pas construire la documentation, il suffit de faire
(simplement `bash` dans le conteneur) :

```bash
ct --shell -- ./dev/install.sh -f
```

La construction de la documentation nécessitant d'importer `salome`, il est
nécessaire d'être dans un `salome shell`, soit :

```bash
ct -- shell -- ./dev/install.sh -f --enable-docs
```

## Lancement de l'interface

```bash
ct --asterstudy
```

## Vérifications

On fait les vérifications **après** `dev/install.sh` et dans l'environnement
du conteneur avec `--asterstudy`:

```bash
ct --shell -- ./dev/install.sh -f
# puis
ct --asterstudy --shell
# ./check.sh --help
```

Enchainement de toutes les vérifications :

```bash
./check.sh
```

### Lint

```bash
./check.sh lint
# ou bien
salome shell -- ./dev/check_lint.sh -p
```

### Documentation

```bash
./check.sh docs
# ou bien
salome shell -- ./dev/check_docs.sh
```

### Tests unitaires

#### Sans couverture de code

Lancement des tests unitaires en parallèle pour ceux qui n'ont pas besoin de
session SALOME, en séquentiel pour les autres :

```bash
./check.sh tests
# ou bien
salome test -R ASTERSTUDY -j 12
```

Labels :

- `SMECA_NO_SESSION`:  Tests qui n'ont pas besoin de session SALOME (et qui peuvent
être lancés en parallèle).

- `SMECA_USE_SESSION` : Tests qui doivent être lancés avec une session SALOME active.

- `SMECA_INTEGR` : Liste très restreinte.

- `SMECA_COVERAGE` : Lancement avec `coverage`, tests nommés `ASTDYCOV_*`.

#### Avec vérification de la couverture de code

> TODO: ne fonctionne pas bien encore, remonter la limite à 100% (97% aujourd'hui).

Lancement des tests unitaires en parallèle pour ceux qui n'ont pas besoin de
session SALOME, en séquentiel pour les autres (automatique dans le fichier *ctest*) :

```bash
./check.sh coverage
# ou bien
rm ~/.asterstudy/.coverage*
salome test -R ASTDYCOV -j 12

# pour afficher le rapport de couverture :
./check.sh report
```

Les tests définis pour la couverture de code ne sont pas nommés `ASTERSTUDY_xxx`
mais `ASTDYCOV_xxx` et ne contiennent pas de labels `SMECA_xxx` mais `SMCOV_xxx`.
afin de ne pas être lancés dans les procédures d'intégration générales.

### Conversions, persistence et performance

```bash
./check.sh comm2study
./check.sh comm2code
./check.sh persistence
./check.sh performance
# ou bien
salome shell -- ./dev/check_comm2study.sh -j 12
salome shell -- ./dev/check_comm2code.sh -j 12
salome shell -- ./dev/check_persistence.sh -j 12
salome shell -- ./dev/check_performance.sh
```

## Débogage

Pour déboguer le code de l'interface graphique, utiliser `debugpy`.
Voir dans `salomegui.py`, c'est dans la méthode `activate()` que l'on initialise
et attend le client.

Lancer les tests via `salome test` n'est pas rapide quand on veux simplement
lancer un test unitaire très rapidement.
Quand cela est possible, on peut charger les modules Python nécessaires et
ajuster `PYTHONPATH` pour trouver `asterstudy` et les modules utilisés pour les
tests :

```bash
module load numpy python3-pyqt
PYTHONPATH=.:test:$PYTHONPATH python test/datamodel/test_xxxx.py
```

## Remarques

#### Note pour la version stable

Si on veut tester une autre version à la place de la version *stable*,
on peut surcharger `Code_aster_stable-XXX` avec ``--bind`` au lancement du
conteneur et dans `~/.astkrc/prefs`.

Voir quels catalogues de code_aster sont utilisés par les tests unitaires :

```bash
. dev/env.sh
python3 test/sequential/_check_versions.py
```

#### Répertoire `Testing` de ctest

*ctest* essaie de créer le répertoire `Testing/Temporary` là où est le fichier
`CTestTestfile.cmake`, donc dans `appli_xxx/bin/salome/test/` qui n'est pas
accessible en écriture.
Ce répertoire permet, entre autres, d'utiliser l'option `--rerun-failed`
pour relancer uniquement les tests en erreur.

Avec le conteneur, on peut créer un répertoire et pointer dessus :

```bash
mkdir /tmp/Testing
# puis
ct --bind /tmp/Testing:/opt/salome_meca/appli_V2021.0_scibian_9/bin/salome/test/Testing ...
```
