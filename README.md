# AsterStudy Module for SALOME

## Development

For EDF developers, see [EDF Development guidelines](README_edf.md).

## Installation

See INSTALL file.

## Launch

To launch SALOME with ASTERSTUDY module, use the following command:

```bash
salome -mASTERSTUDY
```

Note:

- In order to be able to use ASTERSTUDY module with SALOME, you have to specify
  ASTERSTUDY_ROOT_DIR environment variable:

   ```bash
   export ASTERSTUDY_ROOT_DIR=<installation_directory>
   ```

   Here, `<installation_directory>` is an installation folder of AsterStudy
   application.

## License

See COPYING file.
