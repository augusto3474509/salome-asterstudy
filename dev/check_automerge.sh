#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

check_merge_main()
{
    local urlrepo=$(pwd)
    local name=$(basename ${urlrepo})
    local wrkdir=$(mktemp -d)
    local branch=$(hg branch)
    local log=$(mktemp)

    echo -n "Checking merge ${branch} to default... "
    (
        cd ${wrkdir}
        hg clone ${urlrepo} > ${log} 2>&1

        cd ${name}
        rset="descendants('${branch}') and ancestors(last(default))"
        if [ $(hg log --rev "${rset}" | wc -l) != 0 ]
        then
            echo -n "default is a descendant of ${branch}... "
            return 0
        fi

        hg merge --tool=":fail" "${branch}" < /dev/null >> ${log} 2>&1 > ${log} 2>&1
        local mergeok=${?}

        cd ${HOME}
        rm -rf ${wrkdir}
        return ${mergeok}
    )
    ret=${?}

    test "${ret}" = "0" && ok=ok || ok=failed
    echo ${ok}

    if [ "${ok}" != "ok" ]; then
        printf "\nOutput+Error:\n"
        cat ${log}
    fi
    rm -f ${log}

    return ${ret}
}

check_merge_main
exit ${?}
