#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

update_translations_usage()
{
    echo "Update and compile translation file(s)"
    echo
    echo "Usage: $(basename ${0}) [options]"
    echo
    echo "By default, only update translation file(s) from the project sources."
    echo "To compile translations, specify -c option."
    echo
    echo "Options:"
    echo
    echo "  --help (-h)        Print this help information and exit."
    echo
    echo "  --compile (-c)     Compile translations. Default: OFF."
    echo
    exit 0
}

update_translations_main()
{
    local compile=0

    # parse command line
    local option
    while getopts ":-:hc" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) update_translations_usage ;;
                compile ) compile=1 ;;
                * ) echo "Wrong option: --${OPTARG}" > /dev/stderr ; exit 1 ;;
            esac
        else
            case ${option} in
                h ) update_translations_usage ;;
                c ) compile=1 ;;
                ? ) echo "Wrong option" > /dev/stderr ; exit 1 ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    # get root source directory
    local root=$(readlink -f $(dirname "${0}")/..)

    local result=0

    # run pylupdate
    tmpfile=/tmp/$(basename ${0})${$}
    ${root}/cmake/pylupdate -e py,res -s -o ${tmpfile} ${root}/asterstudy ${root}/resources/extra
    result=$((result+${?}))
    test ${result} -gt 0 && echo "Cannot run pylupdate" && return 1

    # run Qt lupdate
    lupdate -extensions py -locations none -no-obsolete ${tmpfile} -ts ${root}/resources/AsterStudy_msg_fr.ts
    result=$((result+${?}))
    rm -f ${tmpfile}
    test ${result} -gt 0 && echo "Cannot run lupdate" && return 1

    # run Qt lrelease
    if [ "${compile}" = "1" ]
    then
	lrelease ${root}/resources/AsterStudy_msg_fr.ts -qm ${root}/resources/AsterStudy_msg_fr.qm
	result=$((result+${?}))
	rm -f ${tmpfile}
	test ${result} -gt 0 && echo "Cannot run lrelease" && return 1
    fi
    return 0
}

update_translations_main "${@}"
exit ${?}
