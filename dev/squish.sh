#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

########################################################################
# Configure and run Squish.
########################################################################

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

squish_main()
{
    # Squish + OpenGL
    export SQUISH_GRABWINDOW_CLASSES=QGLWidget,QWSGLWindowSurface,SVTK_RenderWindowInteractor,OCCViewer_ViewPort3d,pqQVTKWidget

    # Squish + GTK
    export SWT_GTK3=0

    # Force Squish to update all test screenshots during playing
    export SQUISH_LEARN_SCREENSHOTS=0

    # Path to globally shared scripts
    export SQUISH_SCRIPT_DIR=${ASTERSTUDYDIR}/test/squish/shared/scripts

    # use the simulator runner in tests (see `datamodel.engine`)
    export ASTERSTUDY_SIMULATOR=1

    local RASTER=$(which raster)

    # configure AUTs (applications under test) to Squish
    squishserver --config addAUT asterstudy.sh ${ASTERSTUDYDIR}/bin
    squishserver --config addAUT raster $(dirname ${RASTER})

    # remove previous 'envvars' and 'envvars_bak' files if any
    rm -rf ${ASTERSTUDYDIR}/test/squish/suite_*/envvars*

    # create an empty 'envvars'
    local dir
    for dir in ${ASTERSTUDYDIR}/test/squish/suite_*/
    do
        touch ${dir}/envvars
    done

    # first arg is `-f` or `--some-option`
    # or there are no args
    if [ ${#} -eq 0 ] || [ "${1#-}" != "${1}" ]
    then
        # run squishide <args>
        exec squishide "${@}"
    fi

    exec "${@}"
}

squish_main "${@}"
