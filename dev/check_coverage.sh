#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

################################################################################
# Print help on this script and exit.
################################################################################

check_coverage_usage()
{
    echo "Execute non-regression tests for the project."
    echo
    echo "Usage: $(basename ${0}) [options] [TEST ...]"
    echo
    echo "By default, executes tests in all modules. Optionally, module(s) to check can be"
    echo "explicitly specified to the script."
    echo
    echo "Examples:"
    echo "  $ $(basename ${0}) test_category.py"
    echo "  $ $(basename ${0}) test_category.py:TestCategory"
    echo "  $ $(basename ${0}) test_category.py:TestCategory.test_default_category"
    echo "  $ $(basename ${0}) test_category.py test_command.py"
    echo
    echo "Options:"
    echo
    echo "  --help                   Print this help information and exit."
    echo "   -h"
    echo
    echo "  --parallel=1/0           Run tests in parallel. Default: OFF."
    echo "   -p 1/0                  WARNING: this option has effect only"
    echo "                           with coverage>=4.0."
    echo
    echo "  --branches=1/0           Include branch coverage into coverage"
    echo "   -b 1/0                  report. Default: OFF."
    echo
    echo "  --stable                 Use stable dictionary of categories. Without this option,"
    echo "   -o                      a real dictionary is used."
    echo
    echo "  --report-only            Don't run tests, only print report."
    echo "   -r                      Default: OFF."
    echo
    echo "  --report=PACKAGE         Report given package(s) only."
    echo "   -k PACKAGE              Supported packages: $(echo $(check_coverage_packages) | tr " " ",")."
    echo "                           Default: $(check_coverage_default_packages)."
    echo
    echo "  --cumulative             Generate cumulative coverage report for all packages."
    echo "   -c                      Default: OFF."
    echo
    echo "  --exclude=PATTERN        Exclude PATTERN from report."
    echo "   -e PATTERN              Default: no exludes."
    exit 0
}

################################################################################
# Print supported packages.
################################################################################

check_coverage_packages()
{
    echo api common datamodel gui assistant salome
}

################################################################################
# Print default packages.
################################################################################

check_coverage_default_packages()
{
    echo datamodel
}

################################################################################
# Extract packages (process 'all' option)
################################################################################

check_coverage_get_packages()
{
    echo ${@} | tr " " "\n" | grep -q -E "^all$" && echo all && return
    echo ${@}
}

################################################################################
# Test if given option's value is a valid boolean (1 or 0).
# Print error and exit, if not.
################################################################################

check_coverage_test_bool()
{
    local opt=${1}
    local val=${2}

    if [ "${val}" = "" ]
    then
        echo "ERROR: Missing value for option '${opt}'"
        exit 1
    fi

    if [ "${val}" != "1" -a "${val}" != "0" ]
    then
        echo "ERROR: Wrong value for option '${opt}': ${val}"
        exit 1
    fi
}

################################################################################
# Print long option's value.
################################################################################

check_coverage_parse_long_option()
{
    local val
    echo "${1}" | grep -q "=" && val=$(echo "${1}" | sed -e s'%^[^=]\+=%%')
    echo ${val}
}

################################################################################
# Print coverage limit value for given package.
################################################################################

check_coverage_cover_limit()
{
    local pkg=${1}
    if [ -z "${ABSOLUTE_APPLI_PATH}" -a -z "${APPLI}" -a "${pkg}" = "datamodel" ]
    then
        pkg=${pkg}_without_salome
    fi
    local limit=$(sed -n -e "s/^limit_${pkg}[[:space:]]*=[[:space:]]*\([0-9]\+\)/\1/p" ${cfg_file})
    echo ${limit:-100}
}

################################################################################
# Test if given option's value is in predefined enumerator.
# Print error and exit, if not.
################################################################################

check_coverage_test_items()
{
    local opt=${1}
    local known=${2}
    local val=${3}

    if [ "${val}" = "" ]
    then
        echo "ERROR: Missing value for option '${opt}'"
        exit 1
    fi

    local item
    for item in $(echo ${val} | tr "," " ")
    do
        echo ${known} | tr " " "\n" | grep -qe "\<${item}\>"
        if [ "${?}" != "0" ]
        then
            echo "ERROR: Wrong value for option '${opt}': ${item} is not any of '${known}'"
            exit 1
        fi
    done
}

################################################################################
# Test test script's base name (cut off path and test case name)
################################################################################

check_coverage_get_base_name()
{
    echo $(basename $(echo ${1} | awk -F: '{print $1}'))
}

################################################################################
# Test if base names of two test scripts match
################################################################################

check_coverage_match_test_name()
{
    local name1=$(check_coverage_get_base_name ${1})
    local name2=$(check_coverage_get_base_name ${2})
    test "${name1}" = "${name2}" && return 0
    return 1
}

################################################################################
# Main function.
################################################################################

check_coverage_main()
{
    #-------------------
    # Set-up defaults
    #-------------------

    local parallel=0
    local branches=0
    local stable=0
    local packages=$(check_coverage_default_packages)
    local only_report=0
    local single_report=0
    local static_check=1
    local excludes

    local root_dir=${ASTERSTUDYDIR}
    local test_dir=${root_dir}/test
    local cfg_file=${test_dir}/.coveragerc

    #-------------------
    # Parse command line
    #-------------------

    local option
    while getopts ":-:hp:b:ok:rce:" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help )
                    check_coverage_usage
                    ;;
                parallel* )
                    parallel=$(check_coverage_parse_long_option ${OPTARG})
                    check_coverage_test_bool parallel ${parallel}
                    ;;
                branches* )
                    branches=$(check_coverage_parse_long_option ${OPTARG})
                    check_coverage_test_bool branches ${branches}
                    ;;
                stable )
                    stable=1
                    ;;
                report-only )
                    only_report=1
                    ;;
                report* )
                    packages=$(echo $(check_coverage_parse_long_option ${OPTARG}) | tr "," " ")
                    check_coverage_test_items report "$(check_coverage_packages) all" "${packages}"
                    packages=$(check_coverage_get_packages "${packages}")
                    ;;
                cumulative )
                    single_report=1
                    ;;
                exclude* )
                    local exclude=$(echo $(check_coverage_parse_long_option ${OPTARG}) | sed -e "s%^/%*/%;s%/$%/*%")
                    test "${excludes}" = "" && excludes="--omit=${exclude}" || excludes="${excludes},${exclude}"
                    ;;
                * )
                    echo "ERROR: Wrong option: --${OPTARG}"
                    exit 1
                    ;;
            esac
        else
            case ${option} in
                h )
                    check_coverage_usage
                    ;;
                p* )
                    parallel=${OPTARG}
                    check_coverage_test_bool parallel ${parallel}
                    ;;
                b* )
                    branches=${OPTARG}
                    check_coverage_test_bool branches ${branches}
                    ;;
                o )
                    stable=1
                    ;;
                r )
                    only_report=1
                    ;;
                k* )
                    packages=$(echo ${OPTARG} | tr "," " ")
                    check_coverage_test_items report "$(check_coverage_packages) all" "${packages}"
                    packages=$(check_coverage_get_packages "${packages}")
                    ;;
                c )
                    single_report=1
                    ;;
                e* )
                    local exclude=$(echo ${OPTARG} | sed -e "s%^/%*/%;s%/$%/*%")
                    test "${excludes}" = "" && excludes="--omit=${exclude}" || excludes="${excludes},${exclude}"
                    ;;
                ? )
                    echo "ERROR: Wrong option"
                    exit 1
                    ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    #--------------------------
    # Collect test files to run
    #--------------------------

    local files
    while [ ${#} -gt 0 ]
    do
        local file=${1}
        local path=$(check_coverage_get_base_name ${file})
        path=$(find ${test_dir} -name "${path}")
        if [ "${path}" != "" ]
        then
            path=$(dirname ${path})/${file}
            files="${files} ${path}"
        else
            echo "ERROR: can't find file '${file}'"
            return 1
        fi
        shift
    done

    #-------------------------------------------
    # Force parallel option to 0 if coverage < 4
    #-------------------------------------------

    if [ $($(which coverage || which python-coverage) --version | grep -c 'version [4-9]') -eq 0 ]
    then
        test "${parallel}" = "1" && echo "WARNING: old coverage is found; --parallel option is forced to 0"
        parallel=0
    fi

    #-------------------------------------------------------------------
    # Set ASTERSTUDY_USE_OLD_DICT to 1 if stable categories dict is used
    #-------------------------------------------------------------------

    test "${stable}" = "1" && export ASTERSTUDY_USE_OLD_DICT=1

    #------------------------------------------------------------------------
    # Set ASTERSTUDY_WITHIN_TESTS to 1 to signalize that app is under testing
    #------------------------------------------------------------------------

    export ASTERSTUDY_WITHIN_TESTS=1

    #-----------------------------------------------------------------------------------------
    # Perform static check: "pragma: no cover" directive usage should not increase
    # NOTE: since remote tests are not included in coverage tests, this is not always possible
    #-----------------------------------------------------------------------------------------

    if [ "${static_check}" = "1" ]
    then
        local cover_debt_limit=17
        local cover_debt=0
        local package
        for package in $(check_coverage_default_packages)
        do
            local nocover=$(find ${root_dir}/asterstudy/${package} -name "*.py" -exec grep -E "pragma:.+no.+cover" "{}" ";" | wc -l)
            cover_debt=$((cover_debt+nocover))
        done
        if ((cover_debt > cover_debt_limit))
        then
            echo "ERROR: Cover debt exceeds its limit (${cover_debt} > ${cover_debt_limit})"
            exit 1
        fi
    fi

    #---------------
    # Global options
    #---------------

    local cover_options="--rcfile=${cfg_file}"

    #-------------
    # Now run tests
    #-------------

    local result=0

    if [ "${only_report}" != "1" ]
    then
        #---------------
        # Run options
        #---------------

        local cover_run_options
        test "${branches}" = "1" && cover_run_options="${cover_run_options} --branch"
        local nose_options="-c ${test_dir}/.noserc"

        # what tests to run
        test "${files}" = "" && files=" ${test_dir}/sequential ${test_dir}/salome ${test_dir}/postclose ${test_dir}/datamodel ${test_dir}/guimodel"

        # erase previous results
        coverage erase ${cover_options}
        rm -f results.coverage.runseq results.coverage.runpara .coverage .coverage.*

        if [ "${parallel}" = "1" ]
        then
            # parallel option is ON
            # we run parallel and sequential tests separately
            # then combine results

            # get list of tests that can be run only in sequential mode
            local sequential=$(awk -F= 'BEGIN {ok=0} /^sequential[[:space:]]*=/ {ok=1; printf($2); next} /^[^[:space:]]+/ {ok=0} {if(ok) printf($0) } END {printf("\n")}' ${cfg_file})

            # for each sequential test check if it is in the list of test cases to run
            # if yes, keep them in the list of sequential tests to run
            # if no, remove it from the list of sequential tests to run
            local seqf testf
            for seqf in ${sequential}
            do
                local ok=0
                for testf in ${files}
                do
                    if [ -d ${testf} ]
                    then
                        local found=$(find ${testf} -name $(basename ${seqf}))
                        test "${found}" != "" && ok=1 && break
                    else
                        check_coverage_match_test_name ${testf} ${seqf} && ok=1 && break
                    fi
                done
                test "${ok}" != "1" && sequential=$(echo ${sequential} | tr " " "\n" | grep -v -e "^${seqf}$" | tr "\n" " ")
            done

            # for each test to run check if it is in the list of sequential tests
            # if yes, remote it from the list of parallel tests to run
            local files_seq
            for testf in ${files}
            do
                test -d ${testf} && continue
                local ok=0
                for seqf in ${sequential}
                do
                    check_coverage_match_test_name ${testf} ${seqf} && ok=1 && break
                done
                test "${ok}" = "1" && files=$(echo ${files} | tr " " "\n" | grep -v -e "^${testf}$" | tr "\n" " ") && files_seq="${files_seq} ${testf}"
            done

            # now for each remaining sequential test
            # - get its real path to be specified to nosetests
            # - add it to ignore list for parallel run
            for seqf in ${sequential}
            do
                local ok=0
                for testf in ${files_seq}
                do
                    check_coverage_match_test_name ${testf} ${seqf} && ok=1 && break
                done
                echo ${seqf} ${ok}
                if [ "${ok}" != "1" ]
                then
                    local path=$(find ${test_dir} -name "${seqf}")
                    if [ "${path}" != "" ]
                    then
                        files_seq="${files_seq} ${path}"
                    fi
                fi
            done
            # options for ignore
            local nose_options_ignore=$(for seqf in ${files_seq} ; do echo "--ignore-files=$(check_coverage_get_base_name ${seqf})" ; done | sort -u | tr "\n" " ")

            # options for parallel execution
            local processes=$(($(nproc)-1)) && if ((processes<2)) ; then processes=1 ; fi
            local nose_options_para="--processes=${processes} --process-timeout=1000"
            local cover_options_para="--parallel-mode --concurrency=multiprocessing"

            # run parallel tests
            if [ "${files}" != "" ]
            then
                (set -x && coverage run ${cover_options} ${cover_run_options} ${cover_options_para} $(which nosetests) ${nose_options} ${nose_options_para} ${nose_options_ignore} ${files})
                test "${?}" = "0" || result=$((result+1))
                coverage combine ${cover_options} .coverage.*
                mv .coverage results.coverage.runpara
            fi

            # run sequentional tests
            if [ "${files_seq}" != "" ]
            then
                (set -x && coverage run ${cover_options} ${cover_run_options} $(which nosetests) ${nose_options} ${files_seq})
                test "${?}" = "0" || result=$((result+1))
                mv .coverage results.coverage.runseq
            fi
        else
            # parallel option is OFF
            # we run all tests sequentially
            # NOTE: 'parallel' option in [run] section of .coveragerc file should be set to False!
            (set -x && coverage run ${cover_options} ${cover_run_options} $(which nosetests) ${nose_options} ${files})
            iret=${?}
            echo "coverage exit code: ${iret}"
            # salome exits with non zero...
            # if [ ${iret} -eq 139 ]
            # then
            #     iret=0
            # fi
            iret=0
            test "${iret}" = "0" || result=$((result+1))
            test -f .coverage && mv .coverage results.coverage.runseq
        fi

        #----------------
        # Generate report
        #----------------

        # combine all reports to a single report
        coverage combine ${cover_options} results.coverage.run*
        # generate XML output
        coverage xml ${cover_options}
    fi

    #------------
    # Show report
    #------------

    if [ ! -f .coverage ]
    then
        echo "ERROR: Coverage results are not found"
        return 1
    fi

    if [ "${single_report}" = "1" ]
    then
        # single_report option is ON
        # generate cumulative report
        local min_percentage=$(check_coverage_cover_limit ${packages})
        local include_option
        test "${packages}" = "all" || include_option="--include="$(echo ${packages} | tr " " "\n" | awk 'BEGIN {a=0} {if(a) printf(","); printf("asterstudy/%s/*",$1); a=1}')
        (set -x && coverage report ${cover_options} --fail-under=${min_percentage} --skip-covered ${include_option} ${excludes})
        if [ "${?}" != "0" ]
        then
            echo "ERROR: TOTAL coverage for package(s) '${packages}' did not reach minimum required: ${min_percentage}%"
            result=$((result+1))
        fi
    else
        # single_report option is OFF
        # generate separate reports for every package being tested
        test "${packages}" = "all" && packages=$(check_coverage_packages)
        local package
        for package in ${packages}
        do
            local min_percentage=$(check_coverage_cover_limit ${package})
            (set -x && coverage report ${cover_options} --fail-under=${min_percentage} --skip-covered --include=asterstudy/${package}/* ${excludes})
            err=${?}
            if [ "${err}" != "0" ]
            then
                echo "ERROR: TOTAL coverage for package '${package}' did not reach minimum required: ${min_percentage}%"
                result=$((result+1))
            fi
        done
    fi

    return ${result}
}

check_coverage_main "${@}"
exit ${?}
