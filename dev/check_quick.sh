#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

check_quick_usage()
{
    echo "Execute non-regression tests for the project."
    echo
    echo "Usage: $(basename ${0}) [options] [TEST ...]"
    echo
    echo "Run minor set of tests for quick testing."
    echo
    echo "Options:"
    echo
    echo "  --help (-h)            Print this help information and exit."
    echo
    echo "  --parallel (-p)        Run tests in parallel."
    echo
    echo "  --with-timer (-t)      Enable timings."
    echo
    echo "  --with-coverage (-c)   Enable coverage."
    echo
    echo "  --stable (-o)          Use stable dictionary of categories. Without this option,"
    echo "                         a real dictionary is used."
    echo
    exit 0
}

check_quick_main()
{
    local parallel=0
    local timer=0
    local stable=0
    local coverage=0

    local root_dir=${ASTERSTUDYDIR}
    local test_dir=${root_dir}/test

    local option
    while getopts ":-:hptco" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) check_quick_usage ;;
                parallel ) parallel=1 ;;
                with-timer ) timer=1 ;;
                with-coverage ) coverage=1 ;;
                stable ) stable=1 ;;
                * ) echo "Wrong option: --${OPTARG}" ; exit 1 ;;
            esac
        else
            case ${option} in
                h ) check_quick_usage ;;
                p ) parallel=1 ;;
                t ) timer=1 ;;
                c ) coverage=1 ;;
                o ) stable=1 ;;
                ? ) echo "Wrong option" ; exit 1 ;;
        esac
        fi
    done
    shift $((OPTIND - 1))

    local options
    options="${options} -c ${test_dir}/.noserc"
    test "${timer}" = "1" && options="${options} --with-timer"
    test "${parallel}" = "1" && options="${options} --processes=$(nproc)"

    local tests=""
    tests+=" test_engine_salome.py:test_remote_copy_utils"
    tests+=" test_engine_salome.py:test_remote_utils"
    tests+=" test_engine_salome.py:test_submission_failure1"
    tests+=" test_engine_salome.py:test_submission_failure2"
    tests+=" test_engine_salome.py:test_closures"
    tests+=" test_engine_salome.py:test_singleton"
    tests+=" test_engine_salome.py:test_infos"

    local files
    local file path
    for file in ${tests} XXX
    do
	test "${file}" = "XXX" && continue
        path=$(echo ${file} | awk -F: '{print $1}')
        path=$(find ${test_dir} -name "${path}")
        if [ "${path}" != "" ]
        then
            path=$(dirname ${path})/${file}
            files="${files} ${path}"
        else
            echo "Error: can't find file ${file}"
            return 1
        fi
    done

    test "${files}" = "" && echo "Error: empty list of tests" && return 1

    test "${stable}" = "1" && export ASTERSTUDY_USE_OLD_DICT=1

    export ASTERSTUDY_WITHIN_TESTS=1

    local result=0

    if [ "${coverage}" = "1" ]
    then
        local cover_options="--rcfile=${test_dir}/.coveragerc"
        local cover_run_options=""
        test "${branches}" = "1" && cover_run_options+=" --branch"
        local cover_options_para=""
        test "${parallel}" = "1" && cover_options_para+=" --parallel-mode --concurrency=multiprocessing"
        local nose_options="-c ${test_dir}/.noserc"
        local nose_options_para=""
        test "${parallel}" = "1" && nose_options_para+=" --processes=${processes} --process-timeout=1000"
        local report_options="--skip-covered"
                # --fail-under=${min_percentage} 
        local include_options="--include=asterstudy/datamodel/engine/*"
        local exclude_options=""

        set -x
        coverage erase ${cover_options}
        coverage run ${cover_options} ${cover_run_options} ${cover_options_para} $(which nosetests) ${nose_options} ${nose_options_para} ${nose_options_ignore} ${files}
        result=$((result+${?}))
        coverage xml ${cover_options}
        result=$((result+${?}))
        coverage report ${cover_options} ${report_options} ${include_options} ${exclude_options}
        result=$((result+${?}))
    else
        set -x
        nosetests ${options} ${files}
        result=${?}
    fi

    return ${result}
}

check_quick_main "${@}"
exit ${?}
