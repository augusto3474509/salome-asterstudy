#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

########################################################################
# Without arguments: starts bash session with AsterStudy environment.
# With arguments: executes command with specified parameters in 
# bash session with AsterStudy environment.
########################################################################

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    dir=$(pwd) && cd $(dirname ${0})/.. && . ./dev/env.sh && cd ${dir}
fi

if [ ${#} -eq 0 ]
then
    echo Unix Shell for asterstudy-$(python3 -c "from asterstudy.common import version; print(version())")
    /bin/bash
else
    /bin/bash -c "${*}"
fi
