#!/bin/bash

# Copyright 2016 - 2018 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

usage()
{
    echo "usage: $(basename ${0}) [options]"
    echo
    echo "  Check the project within a SALOME session."
    echo
    echo "    -h/--help     Print this help message and exit."
    echo "    -a/--all      Run all checkings."
    echo "    -i/--install  Check the unittests in the installed environment."
    echo "    -t/--test     Check the unittests only (check_tests.sh)."
    echo "                  This is the default action."
    echo "    -c/--cover    Check the code coverage (check_coverage.sh)."
    echo "    -l/--lint     Check the coding conventions (check_lint.sh)."
    echo
    echo "    -r/--remote   Execute only the tests that need a remote server"
    echo "                  (to be used with --test)."
    echo
    echo "    -q/--quick    Run minor set of tests."
    echo
    echo "  Other arguments are passed to the child script."
    echo
    echo "  If it exists, the file '.env_check_salome.sh' is sourced inside"
    echo "  the SALOME shell."
    exit 1
}

run_main()
{
    local SALOME=$(which salome)
    local portfile=$(mktemp /tmp/salome-port-XXXXX)
    local runscript=$(mktemp /tmp/salome-runscript-XXXXX)
    local func=check_tests.sh
    local remote=0
    local root=$(readlink -f $(dirname "${0}")/..)
    local opts

    local option
    while getopts ":-:haitcldrq" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) usage ;;
                all ) func=check.sh ;;
                install ) func=check_install.sh ;;
                test ) func=check_tests.sh ;;
                cover ) func=check_coverage.sh ;;
                lint ) func=check_lint.sh ;;
                doc ) func=check_docs.sh ;;
                quick ) func=check_quick.sh ;;
                remote ) remote=1 ;;
                * ) opts="${opts} --${OPTARG}" ;;
            esac
        else
            case ${option} in
                h ) usage ;;
                a ) func=check.sh ;;
                i ) func=check_install.sh ;;
                t ) func=check_tests.sh ;;
                c ) func=check_coverage.sh ;;
                l ) func=check_lint.sh ;;
                d ) func=check_docs.sh ;;
                q ) func=check_quick.sh ;;
                r ) remote=1 ;;
                * ) opts="${opts} -${OPTARG}" ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    if [ ${remote} -eq 1 ]
    then
        export TEST_REMOTE=1
        if [ $(echo "${opts}" | egrep -c '\-p') -ne 0 ]
        then
            printf "Remote testcases can not be run in parallel "
            printf " (test_engine_salome_remote.py will fail!).\n\n"
            return 1
        fi
        func=check_tests.sh
        if [ ${#} -eq 0 ]
        then
            printf "\nINFO: Only testcases needing a remote server will be checked.\n\n"
            # Should be done in check_tests but currently only one test
            # using salome is subject to problem in parallel.
            local ltc i
            for i in $(ls ${root}/test/**/*remote.py); do ltc="${ltc} $(basename ${i})"; done
            opts="${ltc}"
            printf "List of testcases:\n    ${opts}\n\n"
        else
            printf "List of testcases:\n    ${@}\n\n"
        fi
    fi

    printf "Starting SALOME in text mode...\n"
    ${SALOME} start -t --ns-port-log=${portfile} 2> /dev/null
    local port=$(cat ${portfile})
    rm -f ${portfile}

    printf "\nSALOME session is running on port ${port}.\n\n"

    printf "Running: ${root}/dev/${func} ${opts} ${@}\n"
    cat << EOF > ${runscript}
#!/bin/bash
${root}/dev/${func} ${opts} ${@}
EOF
    chmod 755 ${runscript}

    time ${SALOME} shell --port=${port} -- ${runscript}
    iret=${?}
    if [ ${iret} -eq 139 ]
    then
        iret=0
    fi

    printf "\nStopping SALOME session on port ${port}...\n"
    ${SALOME} kill ${port} 2> /dev/null

    if [ ${iret} -eq 0 ]
    then
        rm -f ${runscript}
    fi

    return ${iret}
}

run_main "${@}"
exit ${?}
