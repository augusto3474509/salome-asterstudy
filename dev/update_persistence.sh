#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

update_persistence_usage()
{
    echo "Run protobuf compiler (protoc) to update AsterStudy storage schema."
    echo
    echo "    input: resources/asterstudy.proto (schema description file)"
    echo "    output: asterstudy/datamodel/asterstudy_pb2.py (storage schema)"
    echo
    echo "Usage: $(basename ${0}) [options]"
    echo
    echo "Options:"
    echo
    echo "  --help (-h)  Print this help information and exit."
    echo
    exit 0
}

update_persistence_main()
{
    local option
    while getopts ":-:h" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) update_persistence_usage ;;
                * ) echo "Wrong option: --${OPTARG}" ; exit 1 ;;
            esac
        else
            case ${option} in
                h ) update_persistence_usage ;;
                ? ) echo "Wrong option" ; exit 1 ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    local input_dir=${ASTERSTUDYDIR}/resources
    local output_dir=${ASTERSTUDYDIR}/asterstudy/datamodel

    protoc -I=${input_dir} --python_out=${output_dir} ${input_dir}/asterstudy.proto 
}

update_persistence_main "${@}"
