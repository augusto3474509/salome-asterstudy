#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

install_rootdir()
{
    readlink -f $(dirname "${0}")/..
}

install_usage()
{
    local def_prefix=$(install_rootdir)/prefix

    echo "Install AsterStudy application."
    echo
    echo "Usage: $(basename ${0}) [options]"
    echo
    echo "By default, AsterStudy is installed into ${def_prefix}."
    echo
    echo "Options:"
    echo
    echo "  --help (-h)            Print this help information and exit."
    echo
    echo "  --prefix=DIR (-p DIR)  Installation directory. Default: ${def_prefix}."
    echo
    echo "  --verbose (-v)         Verbose mode ON. Default: ON"
    echo "  --silent (-s)          Verbose mode OFF."
    echo
    echo "  --force (f)            Force removal or existing AsterStudy installation. Default: OFF"
    echo
    echo "  --enable-salome        Install SALOME wrappings. Default: ON."
    echo "  --disable-salome       Not install SALOME wrappings."
    echo
    echo "  --enable-docs          Install documentation. Default: OFF."
    echo "  --disable-docs         Not install documentation."
    echo
    echo "  --enable-pyc           Install .pyc files. Default: OFF."
    echo "  --disable-pyc          Not install .pyc files."
    echo
    echo "  --force-salome-meca    Force usage of code_aster catalogs from SalomMeca. Default: OFF."
    echo
    exit 0
}

install_main()
{
    local working_dir=$(install_rootdir)
    local builddir=build
    local def_prefix=$(install_rootdir)/prefix
    local prefix=${def_prefix}
    local verbose=1
    local force_remove=0
    local enable_salome=ON
    local enable_docs=OFF
    local enable_pyc=OFF
    local force_salome_meca=OFF
    local wckey
    local label

    OPTS=$(getopt -o hp:vsfb: --long help,prefix:,verbose,silent,force,enable-salome,disable-salome,enable-docs,disable-docs,enable-pyc,disable-pyc,force-salome-meca,builddir:,wckey:,label: -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        _error "invalid arguments." >&2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) install_usage ;;
            -v | --verbose ) verbose=1 ;;
            -s | --silent ) verbose=0 ;;
            -f | --force ) force_remove=1 ;;
            --enable-salome ) enable_salome=ON ;;
            --disable-salome ) enable_salome=OFF ;;
            --enable-docs ) enable_docs=ON ;;
            --disable-docs ) enable_docs=OFF ;;
            --enable-pyc ) enable_pyc=ON ;;
            --disable-pyc ) enable_pyc=OFF ;;
            --force-salome-meca ) force_salome_meca=ON ;;
            --wckey ) wckey="$2" ; shift ;;
            --label ) label="$2" ; shift ;;
            -p | --prefix ) prefix="$2"; shift ;;
            -b | --builddir ) builddir="$2"; shift ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done

    test -d ${prefix} && prefix=$(cd ${prefix} && pwd)

    rm -fr ${working_dir}/${builddir} || return 1

    if [ "${prefix}" = "${def_prefix}" ] || [ "${force_remove}" = "1" ]
    then
        rm -fr ${prefix}/* ||  return 1
    fi

    mkdir ${working_dir}/${builddir} || return 1

    cd ${working_dir}/${builddir}

    declare -a opts
    opts+=( "${working_dir}" )
    opts+=( "-DCMAKE_INSTALL_PREFIX=${prefix}" )
    opts+=( "-DENABLE_SALOME=${enable_salome}" )
    opts+=( "-DENABLE_DOCUMENTATION=${enable_docs}" )
    opts+=( "-DCOMPILE_PYFILES=${enable_pyc}" )
    opts+=( "-DFORCE_SALOMEMECA=${force_salome_meca}" )
    [ ! -z "${wckey}" ] && opts+=( "-DWCKEY=${wckey}" )
    [ ! -z "${label}" ] && opts+=( "-DVERSION_LABEL=${label}" )

    if [ "${verbose}" = "1" ]
    then
        cmake "${opts[@]}" || return 1
    else
        cmake "${opts[@]}" >& /dev/null || return 1
    fi

    if [ "${verbose}" = "1" ]
    then
        make install || return 1
    else
        make install >& /dev/null || return 1
    fi

    # copy generated qm translations to src dir to make squish tests work correctly
    test "${enable_salome}" = "ON" && local rcdir=${prefix}/share/salome/resources/asterstudy \
        || local rcdir=${prefix}/share/resources/asterstudy
    cp ${rcdir}/*.qm ${working_dir}/resources

    cd ${rcdir} && ln -s SalomeAppSL.xml SalomeApp.xml
}

install_main "${@}"
