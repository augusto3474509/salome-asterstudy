#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

clean_usage()
{
    echo "Clean working directory."
    echo
    echo "Usage: $(basename ${0}) [options]"
    echo
    echo "By default, performs soft cleaning. With --force (-f) option performs hard cleaning."
    echo
    echo "Options:"
    echo
    echo "  --help (-h)            Print this help information and exit."
    echo
    echo "  --force (-f)           Perform hard cleaning. Default: OFF."
    echo
    exit 0
}

clean_main()
{
    local force=0
    local working_dir=$(readlink -f $(dirname "${0}")/..)

    local option
    while getopts ":-:hf" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) clean_usage ;;
                force ) force=1 ;;
                * ) echo "Wrong option: --${OPTARG}" ; exit 1 ;;
            esac
        else
            case ${option} in
                h ) clean_usage ;;
                f ) force=1 ;;
                ? ) echo "Wrong option" ; exit 1 ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    echo -n "Cleaning working directory..."

    set -x
    find ${working_dir} -name __pycache__ -prune -exec rm -rf {} ";"
    find ${working_dir} -name "*.pyc" -exec rm -f {} ";"
    find ${working_dir} -name "*~" -exec rm -f {} ";"
    find ${working_dir} -name "*.qm" -exec rm -f {} ";"
    find ${working_dir} -name "pylint_*" -exec rm -f {} ";"
    find ${working_dir} -name CMakeFiles -prune -exec rm -rf {} ";"

    local files
    files+=" coverage.xml nosetests.xml .coverage .coverage.* results.coverage.run*"
    files+=" CMakeCache.txt install_manifest.txt cmake_install.cmake CTestTestfile.cmake Makefile"
    files+=" envvars envvars_bak"
    local file
    for file in ${files}
    do
        find ${working_dir} -name "${file}" -exec rm -rf {} ";"
    done

    test "${force}" = "1" && rm -rf ${working_dir}/prefix ${working_dir}/build

    echo OK
}

clean_main "${@}"
exit ${?}
