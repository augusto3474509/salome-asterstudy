#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

check_coverage_gui_main()
{
    local result=0
    $(dirname ${0})/coverage_runner.sh "guimodel/test_conceptseditor.py" "asterstudy/gui/widgets/conceptseditor.py" 98 || result=$((result+1))
    $(dirname ${0})/coverage_runner.sh "guimodel/test_assistant.py" "asterstudy/assistant/*" 86 || result=$((result+1))
    $(dirname ${0})/coverage_runner.sh "guimodel/test_file_descriptors_model.py" "asterstudy/gui/datafiles/model.py" 92 || result=$((result+1))
    $(dirname ${0})/coverage_runner.sh "guimodel/test_infoview.py" "asterstudy/gui/infoview/*" 56 || result=$((result+1))
    $(dirname ${0})/coverage_runner.sh "guimodel/test_issue_1719.py" "asterstudy/gui/cmdtexteditor.py" 88 || result=$((result+1))
    return ${result}
}

check_coverage_gui_main
exit ${?}
