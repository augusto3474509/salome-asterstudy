#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

check_usage()
{
    echo "Check project."
    echo
    echo "Usage: $(basename ${0}) [options]"
    echo
    echo "Options:"
    echo
    echo "  --help (-h)         Print this help information and exit."
    echo "  --parallel (-p)     Run tests in parallel."
    echo "  --silent (-s)       Switch OFF verbosity."
    echo "  --quick (-q)        Perform quick check (only major steps)."
    echo "  --stop (-n)         Do not run next steps in case of failure."
    echo "  --stable (-o)       Use stable dictionary of categories."
    echo "  --edfci (-e)        Run tests for CI on EDF servers."
    echo
    exit 0
}

check_func()
{
    local func=${1}
    local opts=${func}_options

    echo '======================================================================'
    echo "Check ${func}"
    echo '======================================================================'

    if [ "${verbose}" = "0" ]
    then
        ${dir}/check_${func}.sh ${!opts} >& /dev/null
    else
        ${dir}/check_${func}.sh ${!opts}
    fi
    return ${?}
}

check_main()
{
    local dir=$(readlink -f $(dirname "${0}"))

    local parallel=0
    local verbose=1
    local quick=0
    local stop=0
    local stable=0
    local edfci=0

    local option
    while getopts ":-:hpsqnoe" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) check_usage ;;
                parallel ) parallel=1 ;;
                silent ) verbose=0 ;;
                quick ) quick=1 ;;
                stop ) stop=1 ;;
                stable ) stable=1 ;;
                edfci ) edfci=1 ;;
                * ) echo "Wrong option: --${OPTARG}" ; exit 1 ;;
            esac
        else
            case ${option} in
                h ) check_usage ;;
                p ) parallel=1 ;;
                s ) verbose=0 ;;
                q ) quick=1 ;;
                n ) stop=1 ;;
                o ) stable=1 ;;
                e ) edfci=1 ;;
                ? ) echo "Wrong option" ; exit 1 ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    local tests_options
    local coverage_options
    local install_options
    local lint_options
    local comm2code_options='clean'
    local comm2study_options='clean'
    local performance_options='clean'
    local persistence_options='clean'

    # run in parallel?
    if [ "${parallel}" = "1" ]
    then
        tests_options="${tests_options} --parallel"
        lint_options="${lint_options} --parallel"
        install_options="${install_options} --parallel"
        comm2code_options="${comm2code_options} --jobs=$(nproc)"
        comm2study_options="${comm2study_options} --jobs=$(nproc)"
        # performance_options="${performance_options} --jobs=$(nproc)"
        persistence_options="${persistence_options} --jobs=$(nproc)"
    fi

    # run with stable catalogue?
    if [ "${stable}" = "1" ]
    then
        tests_options="${tests_options} --stable"
        coverage_options=="${tests_options} --stable"
    fi

    # check_install is not stable enough
    local do_inst=0

    # identify list of checks to run
    local functions
    if [ "${quick}" = "1" ]
    then
        # shorten list for quick check
        functions="lint tests docs"
    elif [ "${edfci}" = "1" ]
    then
        # performance tests are not run during ci because of non-reproducibility
        # lint temporarly disabled because of failure during pylint installation
        functions="coverage docs comm2study comm2code persistence install"
        install_options="-v"
    else
        # default list of checks
        functions="lint coverage docs comm2study comm2code persistence performance"
        # don't run install check
        test "${do_inst}" = "1" && functions="${functions} install"
    fi

    local result func
    result=0

    for func in ${functions}
    do
        local ok
        check_func ${func}
        test "${?}" = "0" && ok=OK || ok=FAILED
        test "${ok}" = "OK" || result=$((result+1))
        eval ${func}_ok=${ok}
        if [ ${stop} -eq 1 ] && [ ${result} -gt 0 ]
        then
            # stop after first error
            break
        fi
    done

    echo
    echo '======================================================================'
    echo "Summary"
    echo '======================================================================'

    for func in ${functions}
    do
        local ok=${func}_ok
        printf "%-15s : %s\n" ${func} ${!ok}
    done

    echo '----------------------------------------------------------------------'
    echo "Result: ${result}"
    echo

    return ${result}
}

check_main "${@}"
exit ${?}
