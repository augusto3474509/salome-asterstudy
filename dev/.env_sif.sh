# Environment to install AsterStudy within a Singularity container
# based on Scibian 9

# Add here what is not done by 'salome shell'
where_am_i=$(dirname ${BASH_SOURCE[0]})

# TODO limit to only those needed for installation
# dependencies are listed in appli_XXXX/salome_common.py
module load "numpy/1.15.1" \
            "openturns/1.17.0" \
            "pv/5.9.0.0" \
            "python/3.6.5" \
            "python3-docutils/0.12" \
            "python3-jinja/2.7.3" \
            "python3-matplotlib/3.0.3" \
            "python3-pygments/2.0.2" \
            "python3-pyqt/5.15.2" \
            "python3-scipy/0.19.1" \
            "python3-sphinx/1.7.6" \
            "python36-sphinx-rtd-theme-mat303/0.4.3" \
            "qt/5.15.2"

# tools needed for development (installed by pip3)
export PATH=${HOME}/.local/bin:/opt/salome_meca/appli_V2020.0.1_scibian_9:${PATH}

# # Re-prepend ASTERSTUDY paths after sourcing SALOME environment
# export PATH=${ASTERSTUDYDIR}/bin:${ASTERSTUDYDIR}/dev:${PATH}
# export PYTHONPATH=${ASTERSTUDYDIR}:${ASTERSTUDYDIR}/test:${PYTHONPATH}

# # to force code_aster execution to set PYTHONHOME to the SALOME one
# unset ABSOLUTE_APPLI_PATH
