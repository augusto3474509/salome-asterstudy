#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

check_install_usage()
{
    echo "Execute build procedure's check.."
    echo
    echo "Usage: $(basename ${0}) [options]"
    echo
    echo "Options:"
    echo
    echo "  --verbose (-v)         Pass -V option to ctest."
    echo
    echo "  --parallel (-p)        Run tests in parallel. This option disables '-v'."
    echo
    echo "  --help (-h)            Print this help information and exit."
    echo
    exit 0
}

check_install_main()
{
    local parallel=0
    local working_dir=$(readlink -f $(dirname "${0}")/..)
    local verbose=0

    local option
    while getopts ":-:hpv" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                parallel ) parallel=1 ;;
                verbose ) verbose=1 ;;
                help ) check_install_usage ;;
                * ) echo "Wrong option: --${OPTARG}" ; exit 1 ;;
            esac
        else
            case ${option} in
                p ) parallel=1 ;;
                v ) verbose=1 ;;
                h ) check_install_usage ;;
                ? ) echo "Wrong option" ; exit 1 ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    local options=""
    test "${verbose}" = "1" && options="-V"
    test "${parallel}" = "1" && options="-j$(nproc)"

    # clean up working directory
    ${working_dir}/dev/clean.sh -f || return 1
    # install
    ${working_dir}/dev/install.sh -vf --enable-salome --disable-pyc || return 1
    # run tests
    (
        cd ${working_dir}/build
        ( make test ARGS="${options}" || touch failed ) | tee .log
        egrep -v '^[0-9]+:' .log > ctest.log
        egrep '^[0-9]+:' .log > ctest_details.log
    )
    local result=0
    test -f ${working_dir}/build/failed && result=1
    rm -f ${working_dir}/build/failed
    return ${result}
}

check_install_main "${@}"
exit ${?}
