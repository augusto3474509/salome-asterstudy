#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

if [ "${ASTERSTUDYDIR}x" = "x" ]
then
    ASTERSTUDY_SILENT_MODE=1 # avoid possible printouts from env.sh
    pushd $(pwd) > /dev/null && cd $(dirname ${0}) && . ./env.sh && popd > /dev/null
fi

check_tests_usage()
{
    echo "Execute non-regression tests for the project."
    echo
    echo "Usage: $(basename ${0}) [options] [TEST ...]"
    echo
    echo "By default, executes tests in all modules. Optionally, module(s) to check can be"
    echo "explicitly specified to the script."
    echo
    echo "Examples:"
    echo "  $ $(basename ${0}) test_category.py"
    echo "  $ $(basename ${0}) test_category.py:TestCategory"
    echo "  $ $(basename ${0}) test_category.py:TestCategory.test_default_category"
    echo "  $ $(basename ${0}) test_category.py test_command.py"
    echo
    echo "Options:"
    echo
    echo "  --help (-h)            Print this help information and exit."
    echo
    echo "  --parallel (-p)        Run tests in parallel."
    echo
    echo "  --with-timer (-t)      Enable timings."
    echo
    echo "  --stable (-o)          Use stable dictionary of categories. Without this option,"
    echo "                         a real dictionary is used."
    echo
    exit 0
}

check_tests_main()
{
    local parallel=0
    local timer=0
    local stable=0

    local root_dir=${ASTERSTUDYDIR}
    local test_dir=${root_dir}/test

    local option
    while getopts ":-:hpto" option ${@}
    do
        if [ "${option}" = "-" ]
        then
            case ${OPTARG} in
                help ) check_tests_usage ;;
                parallel ) parallel=1 ;;
                with-timer ) timer=1 ;;
                stable ) stable=1 ;;
                * ) echo "Wrong option: --${OPTARG}" ; exit 1 ;;
            esac
        else
            case ${option} in
                h ) check_tests_usage ;;
                p ) parallel=1 ;;
                t ) timer=1 ;;
                o ) stable=1 ;;
                ? ) echo "Wrong option" ; exit 1 ;;
            esac
        fi
    done
    shift $((OPTIND - 1))

    local options=""
    options+=" -c ${test_dir}/.noserc"
    test "${timer}" = "1" && options+=" --with-timer"
    local optseq="${options}"
    # following options only for the first (parallel) call to nosetests
    test "${parallel}" = "1" && options+=" --processes=$(nproc)"

    local files
    local seq=0
    if [ ${#} -gt 0 ]
    then
        local file path args
        for file in "${@}"
        do
            path=$(echo ${file} | awk -F: '{print $1}')
            path=$(find ${test_dir} -name "${path}")
            if [ "${path}" != "" ]
            then
                path=$(dirname ${path})/${file}
                files="${files} ${path}"
            else
                echo "Error: can't find file ${file}"
                return 1
            fi
        done
    else
        seq=1
        options+=" --ignore-files=test_sequential[a-z_]*.py --ignore-files='test_post_[a-z_]*\.py' --ignore-files=test_zzzz.py"
    fi

    test "${files}" = "" && files="${test_dir}/sequential ${test_dir}/salome ${test_dir}/postclose ${test_dir}/datamodel ${test_dir}/guimodel"
    test "${stable}" = "1" && export ASTERSTUDY_USE_OLD_DICT=1
    export ASTERSTUDY_WITHIN_TESTS=1

    local result=0

    ( set -x && nosetests ${options} ${files} )
    result=$((result+${?}))
    if [ ${seq} -eq 1 ]
    then
        ( set -x && nosetests ${optseq} ${test_dir}/sequential ${test_dir}/salome ${test_dir}/postclose)
        result=$((result+${?}))
    fi
    return ${result}
}

check_tests_main "${@}"
exit ${?}
