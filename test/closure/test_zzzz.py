# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing feature of group filtering"""

import unittest
from hamcrest import *
from asterstudy.datamodel.engine.salome_runner import has_salome


# probably meaningless when tests are started in separated SALOME sessions
@unittest.skipIf(not has_salome(), "salome is required")
def test_zzzz():
    """
    Tests that all views are closed after paraview tests.
    """
    import pvsimple as pvs

    for view in pvs.GetRenderViews():
        pvs.Delete(view)
        del view
    pxm = pvs.servermanager.ProxyManager()
    pxm.UnRegisterProxies()
    del pxm
    pvs.Disconnect()

    pvs.Connect()


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
