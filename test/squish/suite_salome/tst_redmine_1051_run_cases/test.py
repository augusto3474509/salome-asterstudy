#------------------------------------------------------------------------------
# Issue #1051 - There should be no way to "reuse" result below "execute" stage 

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    createNewStudy()
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )
    activateMenuItem( "Operations", "Add Stage" )
    activateMenuItem( "Operations", "Add Stage" )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    file_path = os.path.join( testdir(), "tst_redmine_1051_run_cases.ajs" )
    saveFile( file_path )

    #--------------------------------------------------------------------------
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)
    use_option( 'Stage_3', Execute )
    keep_results( 'Stage_1', True )
    keep_results( 'Stage_2', True )

    #--------------------------------------------------------------------------
    symbol = tr( 'Run', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
#     clickButton( waitForObject( ":DashboardRunDialog.run_QPushButton" ) )

    #--------------------------------------------------------------------------
    while True:
        waitForObjectItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1")
        clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 65, 11, 0, Qt.LeftButton)
        log = str( waitForObjectExists( ":_TextEdit" ).plainText )
        if 'Run case "RunCase_1" calculations process finished' in log:
            break

        snooze( 1 )

    #--------------------------------------------------------------------------
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)
    run_options( 'Stage_1', {Skip, Reuse, Execute}, Reuse )
    run_options( 'Stage_3', {Skip, Execute}, Execute )
    run_options( 'Stage_2', {Skip, Reuse, Execute}, Execute )
    option = run_options( 'Stage_3', {Skip, Execute} )
    test.verify( option != Reuse ) # There should be no option for the "Reuse"

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton(waitForObject(":Exit.Ok_QPushButton"))
    clickButton(waitForObject(":Close active study.No_QPushButton"))

    #--------------------------------------------------------------------------
    pass
