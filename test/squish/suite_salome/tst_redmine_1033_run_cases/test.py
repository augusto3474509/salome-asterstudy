#------------------------------------------------------------------------------
# Issue #881 - Operations: Import Stage
import filecmp

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # Create Case
    #--------------------------------------------------------------------------

    createNewStudy()
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )
    activateMenuItem( "Operations", "Add Stage" )
    activateMenuItem( "Operations", "Add Stage" )

    #--------------------------------------------------------------------------

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    file_path = os.path.join( testdir(), "beforeComputation.ajs" )
    saveFile( file_path )

    #--------------------------------------------------------------------------
    # Run_Case_1:
    #   * Stage_1: Execute, keep results
    #   * Stage_2: Execute, keep results
    #   * Stage_3: Execute, keep results
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_1', Execute )
    use_option( 'Stage_2', Execute )
    use_option( 'Stage_3', Execute )

    keep_results( 'Stage_1', True )
    keep_results( 'Stage_2', True )

    run_case( 'RunCase_1' )

    snooze(1)
    test.vp("VP1")

    #--------------------------------------------------------------------------
    # RunCase_2:
    #   * Stage_1: Reuse
    #   * Stage_2: Reuse
    #   * Stage_3: Execute, keep results
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_2', Reuse )    # Stage_1 is set to Reuse automatically 
    use_option( 'Stage_3', Execute )

    run_case( 'RunCase_2' )

    snooze(1)
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # RunCase_3:
    #   * Stage_1: Reuse
    #   * Stage_2: Execute, keep results
    #   * Stage_3: Execute, keep results
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_1', Reuse )
    use_option( 'Stage_3', Execute )    # Stage_2 is set to Reuse automatically
    use_option( 'Stage_2', Execute )

    keep_results( 'Stage_2', True )

    run_case( 'RunCase_3' )

    snooze(1)
    test.vp("VP3")

    #--------------------------------------------------------------------------
    # RunCase_4:
    #   * Stage_1: Reuse
    #   * Stage_2: Reuse
    #   * Stage_3: Execute, keep results
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_2', Reuse )    # Stage_1 is set to Reuse automatically 
    use_option( 'Stage_3', Execute )

    run_case( 'RunCase_4' )

    snooze(1)
    test.vp("VP4")

    #--------------------------------------------------------------------------
    # RunCase_5
    #   * Stage_1: Execute, keep results
    #   * Stage_2: Execute, keep results
    #   * Stage_3: Skip
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_2', Execute )    # Stage_1 is set to Reuse automatically 
    use_option( 'Stage_1', Execute )
    use_option( 'Stage_3', Skip )

    keep_results( 'Stage_1', True )

    run_case( 'RunCase_5' )

    snooze(1)
    test.vp("VP5")

    #--------------------------------------------------------------------------
    # Copy as current RunCase_1
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1")
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 65, 11, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_1", 74, 8, 0)
    activateItem(waitForObjectItem(":SALOME*.SalomeModules_QMenu", "Copy As Current"))

    #--------------------------------------------------------------------------
    # RunCase_6:
    #   * Stage_1: Reuse
    #   * Stage_2: Reuse
    #   * Stage_3: Execute, keep results
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_2', Reuse )    # Stage_1 is set to Reuse automatically
    use_option( 'Stage_3', Execute )

    run_case( 'RunCase_6' )

    snooze(1)
    test.vp("VP6")

    #--------------------------------------------------------------------------
    # Remove RunCase_6
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_6")
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_6", 66, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_6", 69, 9, 0)
    activateItem(waitForObjectItem(":SALOME*.SalomeModules_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    snooze(1)
    test.vp("VP7")

    #--------------------------------------------------------------------------
    # Copy as current RunCase_1
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1")
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 65, 11, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_1", 74, 8, 0)
    activateItem(waitForObjectItem(":SALOME*.SalomeModules_QMenu", "Copy As Current"))

    #--------------------------------------------------------------------------
    # Modify Current Case (Add command to Stage_2)
    #--------------------------------------------------------------------------

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------

    showOBItem( "CurrentCase", "Stage_2" )
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_2", 65, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Mesh"))
    activateItem(waitForObjectItem(":Mesh_QMenu", "LIRE\\_MAILLAGE"))

    #--------------------------------------------------------------------------

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")

    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    # Copy as current RunCase_5
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_5")
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_5", 65, 11, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_5", 74, 8, 0)
    activateItem(waitForObjectItem(":SALOME*.SalomeModules_QMenu", "Copy As Current"))

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    snooze(1)
    test.vp("VPS1")

    use_option( 'Stage_2', Execute )  # Stage_1 is set to Reuse automatically
    use_option( 'Stage_1', Reuse )    # Just to check

    #--------------------------------------------------------------------------
    # Remove RunCase_5
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_5")
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_5", 66, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_5", 69, 9, 0)
    activateItem(waitForObjectItem(":SALOME*.SalomeModules_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    snooze(1)
    test.vp("VP8")

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    use_option( 'Stage_1', Execute )  # Just to check
    use_option( 'Stage_2', Execute )  # Just to check

    #--------------------------------------------------------------------------
    # Create Case
    #--------------------------------------------------------------------------

    activateItem(waitForObjectItem(":AsterStudy_QMenuBar", "Operations"))
    activateItem(waitForObjectItem(":Operations_QMenu", "New Case"))
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    snooze(1)
    test.vp("VPS2")

    #--------------------------------------------------------------------------
    # Modify Current Case (Add Stage_1)
    #--------------------------------------------------------------------------

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    snooze(1)
    test.vp("VPS3")

    use_option( 'Stage_1', Execute )  # Just to check

    #--------------------------------------------------------------------------
    # Copy as current RunCase_1
    #--------------------------------------------------------------------------

    waitForObjectItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1")
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 65, 11, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_1", 74, 8, 0)
    activateItem(waitForObjectItem(":SALOME*.SalomeModules_QMenu", "Copy As Current"))

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    snooze(1)
    test.vp("VPS4")
    
    use_option( 'Stage_1', Execute )  # Just to check
    use_option( 'Stage_2', Execute )  # Just to check
    use_option( 'Stage_3', Execute )  # Just to check
    use_option( 'Stage_1', Reuse )    # Just to check
    use_option( 'Stage_2', Reuse )    # Just to check
    use_option( 'Stage_3', Execute )  # Just to check

    #--------------------------------------------------------------------------
    # Modify Current Case (add Stage_4)
    #--------------------------------------------------------------------------

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    snooze(1)
    test.vp("VPS5")

    use_option( 'Stage_1', Execute )  # Just to check
    use_option( 'Stage_2', Execute )  # Just to check
    use_option( 'Stage_3', Execute )  # Just to check
    use_option( 'Stage_4', Execute )  # Just to check
    use_option( 'Stage_1', Reuse )  # Just to check
    use_option( 'Stage_2', Reuse )  # Just to check
    use_option( 'Stage_3', Reuse )  # Just to check

    #--------------------------------------------------------------------------
    # Modify Current Case (Remove Stage_3)
    #--------------------------------------------------------------------------

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    showOBItem( "CurrentCase", "Stage_3" )
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_3", 48, 7, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")

    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)

    snooze(1)
    test.vp("VPS6")

    use_option( 'Stage_1', Execute )  # Just to check
    use_option( 'Stage_2', Execute )  # Just to check
    use_option( 'Stage_1', Reuse )    # Just to check
    use_option( 'Stage_2', Execute )  # Just to check

    #--------------------------------------------------------------------------
    # Exit application
    #--------------------------------------------------------------------------

    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton(waitForObject(":Exit.Ok_QPushButton"))
    clickButton(waitForObject(":Close active study.No_QPushButton"))

    #--------------------------------------------------------------------------
    pass
