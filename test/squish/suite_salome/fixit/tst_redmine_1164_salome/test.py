#------------------------------------------------------------------------------
# Issue # 1164 - SALOME: Open With operation for items in Data Files panel
# Check 'Open In Editor` feature

def main():
    source(findFile("scripts", "common.py"))
    global_start()


def cleanup():
    global_cleanup()


def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickButton(waitForObject(":SALOME*.AsterStudy_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    test.compare(isItemExists("Stage_1", "Mesh", "mesh"), True)

    #--------------------------------------------------------------------------
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")
    symbol = tr('UNITE', 'QPushButton')
    clickButton(waitForObject(symbol))
    med_file_path = os.path.join(casedir(), findFile("testdata", "Mesh_recette.med"))
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":Data Settings_QTabWidget"), "Data Files")
    waitForObject(":DataFiles_QTreeView").expandAll()

    #--------------------------------------------------------------------------
    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.20")

    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.Mesh\\_recette\\.med")
    clickItem(":DataFiles_QTreeView", "Stage\\_1.Mesh\\_recette\\.med", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.Mesh\\_recette\\.med", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In ParaVis"))

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton(waitForObject(":Exit.Ok_QPushButton"))
    clickButton(waitForObject(":Close active study.Close w/o saving_QPushButton"))

    #--------------------------------------------------------------------------
    pass
