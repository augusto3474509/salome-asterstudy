#------------------------------------------------------------------------------
# Issue #1162 - History view: add “keep basis” column to run (CCTP 2.4.5)

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # Create Case with 4 stages
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    createNewStudy()

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    activateMenuItem("Operations", "Add Stage")
    activateMenuItem("Operations", "Add Stage")
    activateMenuItem("Operations", "Add Stage")
    activateMenuItem("Operations", "Add Stage")

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")

    #--------------------------------------------------------------------------
    file_path = os.path.join(testdir(), "study.ajs")
    saveFile(file_path)

    #--------------------------------------------------------------------------
    # Create Run Case 1, keep results only for Stage_3 and Stage_4
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    run_options('Stage_1', Skip | Execute)
    run_options('Stage_2', Skip | Execute)
    run_options('Stage_3', Skip | Execute)
    run_options('Stage_4', Skip | Execute)

    use_option('Stage_1', Execute)
    use_option('Stage_2', Execute)
    use_option('Stage_3', Execute)
    use_option('Stage_4', Execute)

    keep_results('Stage_3', True)

    #--------------------------------------------------------------------------

    run_case('RunCase_1')

    #--------------------------------------------------------------------------
    snooze(1)
    test.vp("VP1")

    #--------------------------------------------------------------------------
    # Create Run Case 2, reuse results from Stage_3 (and, thus, from Stage_1 and Stage_2)
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    run_options('Stage_1', Skip | Reuse | Execute)
    run_options('Stage_2', Skip | Reuse | Execute)
    run_options('Stage_3', Skip | Reuse | Execute)
    run_options('Stage_4', Skip | Reuse | Execute)

    use_option('Stage_3', Reuse)
    use_option('Stage_4', Execute)

    #--------------------------------------------------------------------------
    run_case('RunCase_2')

    #--------------------------------------------------------------------------
    snooze(1)
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # Create Run Case 3, keep results for all stages
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    run_options('Stage_1', Skip | Execute)
    run_options('Stage_2', Skip | Execute)
    run_options('Stage_3', Skip | Execute)
    run_options('Stage_4', Skip | Execute)

    use_option('Stage_4', Execute)
    use_option('Stage_3', Execute)
    use_option('Stage_2', Execute)
    use_option('Stage_1', Execute)

    keep_results('Stage_1', True)
    keep_results('Stage_2', True)
    keep_results('Stage_3', True)
    keep_results('Stage_4', True)

    #--------------------------------------------------------------------------
    run_case('RunCase_3')

    #--------------------------------------------------------------------------
    snooze(1)
    test.vp("VP3")

    #--------------------------------------------------------------------------
    # Exit application
    #--------------------------------------------------------------------------

    activateItem(waitForObjectItem(":AsterStudy_QMenuBar", "File"))
    activateItem(waitForObjectItem(":File_QMenu", "Exit"))
    clickButton(waitForObject(":Exit.Ok_QPushButton"))
    clickButton(waitForObject(":Close active study.Close w/o saving_QPushButton"))

    #--------------------------------------------------------------------------
    pass
