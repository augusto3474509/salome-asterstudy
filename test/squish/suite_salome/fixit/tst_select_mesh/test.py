def main():
    source(findFile("scripts", "common.py"))
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    #[section] Create new study
    test.log("Create new study")

    #[step] Click 'New Study' button of 'Standard' toolbar
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))

    #--------------------------------------------------------------------------
    #[section] Activate Mesh module
    test.log("Activate Mesh module")

    #[step] Click 'Mesh' button of 'Modules' toolbar
    clickButton(waitForObject(":SALOME*.Mesh_QToolButton"))

    #--------------------------------------------------------------------------
    #[section] Import MED file
    test.log("Import MED file")

    #[step] Menu 'File' - 'Import' - 'MED file'
    waitForActivateMenuItem("File", "Import", "MED file")

    #[step] Browse to zzzz121b.med file
    med_file_path = os.path.join(casedir(), findFile("testdata", "zzzz121b.med"))
    waitForObject(":fileNameEdit_QLineEdit").setText(med_file_path)

    #[step] Click 'Open' button
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))

    #[step] Check meshes import from the file
    waitForObjectItem(":objectBrowserDock_QtxTreeView", "Mesh.MUN")
    clickItem(":objectBrowserDock_QtxTreeView", "Mesh.MUN", 30, 6, 0, Qt.LeftButton)
    waitForObjectItem(":objectBrowserDock_QtxTreeView", "Mesh.MZERO")
    clickItem(":objectBrowserDock_QtxTreeView", "Mesh.MZERO", 26, 8, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    #[section] Activate AsterStudy module
    test.log("Activate AsterStudy module")

    #[step] Click 'AsterStudy' button of 'Modules' toolbar
    clickButton(waitForObject(":SALOME*.AsterStudy_QToolButton"))

    #[step] Switch to 'Case View' mode
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    checkOBItem("CurrentCase")

    #--------------------------------------------------------------------------
    #[section] Create stage
    test.log("Create stage")

    #[step] Click 'Add Stage" button of 'Operations' toolbar
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))

    #[check] Check that 'Stage_1' has been added to the tree
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    #[section] Add LIRE_MAILLAGE command
    test.log("Add LIRE_MAILLAGE command")

    #[step] Select 'Stage_1' in data tree; context menu popup: 'Mesh' - 'LIRE_MAILLAGE'
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")

    #[step] Select 'LIRE_MAILLAGE' in data tree; context menu 'Edit'
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.LIRE_MAILLAGE", "Edit")

    #[step] Check on 'FORMAT' checkbox
    symbol = tr('FORMAT', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Select 'MED' in 'FORMAT' combobox
    symbol = tr('FORMAT', 'QComboBox')
    setParameterValue(symbol, "MED")

    #[step] Select 'MUN' in 'UNITE' combobox
    symbol = tr('UNITE', 'QComboBox')
    setParameterValue(symbol, 'MUN')

    #[step] Check on 'NOM_MED' checkbox
    symbol = tr('NOM_MED', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Select 'MUN' in 'NOM_MED' combobox
    symbol = tr('NOM_MED', 'QComboBox')
    setParameterValue(symbol, 'MUN')

    #[step] Click OK button to accept the changes
    clickButton(waitForObject(":OK_QPushButton"))
    #[check] 'Edit command' panel was closed
    waitUntilObjectExists(":qt_splithandle__EditionPanel")

    #[step] Switch to 'Data Files' panel
    clickTab(waitForObject(":Data Settings_QTabWidget"), "Data Files")
    waitForObject(":DataFiles_QTreeView").expandAll()

    #[step] Check that 'MNU' item presents in 'Data Files' tree
    clickItem(":DataFiles_QTreeView", "Stage\\_1.MUN", 10, 10, 0, Qt.LeftButton)

    #[step] Switch back to 'Data Settings' panel
    clickTab(waitForObject(":Data Settings_QTabWidget"), "Data Settings")

    #--------------------------------------------------------------------------
    #[section] Add AFFE_MODELE command
    test.log("Add AFFE_MODELE command")

    #[step] Select 'Stage_1' in data tree; context menu popup: 'Model Definition' - 'AFFE_MODELE'
    activateOBContextMenuItem("CurrentCase.Stage_1", "Model Definition", "AFFE_MODELE")

    #[step] Select 'AFFE_MODELE' in data tree; context menu 'Edit'
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Model Definition.AFFE_MODELE", "Edit")

    #[step] Check on 'AFFE_SOUS_STRUC' checkbox
    symbol = tr('AFFE_SOUS_STRUC', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Click on 'AFFE_SOUS_STRUC's 'Edit' button
    symbol = tr('AFFE_SOUS_STRUC', 'QPushButton')
    clickButton(waitForObject(symbol))

    #[step] Check on 'TOUT' radio button
    symbol = tr('TOUT', 'QRadioButton')
    clickButton(waitForObject(symbol))

    #[step] Click twive OK button to accept the changes
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":OK_QPushButton"))
    #[check] 'Edit command' panel was closed
    waitUntilObjectExists(":qt_splithandle__EditionPanel")

    #--------------------------------------------------------------------------
    #[section] Add AFFE_CHAR_ACOU command
    test.log("Add AFFE_CHAR_ACOU command")

    #[step] Select 'Stage_1' in data tree; context menu popup: 'Show All...'
    #[step] Select 'AFFE_CHAR_ACOU' item in 'Other' section
    #[step] Press 'OK' button
    addCommandByDialog("AFFE_CHAR_ACOU", "Other")

    #[step] Select 'AFFE_CHAR_ACOU' in data tree; context menu 'Edit'
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Other.AFFE_CHAR_ACOU", "Edit")

    #[step] Check on 'FORMAT' checkbox
    symbol = tr('PRES_IMPO', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Press 'Add' button in 'PRES_IMPO' editor
    clickButton(waitForObject(":PRES_IMPO_add_QToolButton"))

    #[step] Press 'Edit...' button in 'PRES_IMPO' / '[0]'
    clickButton(waitForObject(":0_ParameterButton"))

    #[step] Check on 'GROUP_NO' checkbox
    symbol = tr('GROUP_NO', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Press 'Edit...' button in 'GROUP_NO'
    symbol = tr('GROUP_NO', 'QPushButton')
    clickButton(waitForObject(symbol))

    #[step] Switch on 'ORIGINE', 'OPPOSE', 'I', 'J', 'K' items in 'Nodes' sub-tree
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.ORIGINE")
    clickItem(":MESH_QTreeWidget", "Nodes.ORIGINE", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.OPPOSE")
    clickItem(":MESH_QTreeWidget", "Nodes.OPPOSE", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.I")
    clickItem(":MESH_QTreeWidget", "Nodes.I", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.J")
    clickItem(":MESH_QTreeWidget", "Nodes.J", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.K")
    clickItem(":MESH_QTreeWidget", "Nodes.K", 10, 10, 0, Qt.LeftButton)

    #[step] Press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    #[step] Check on 'GROUP_MA' checkbox
    symbol = tr('GROUP_MA', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Press 'Edit...' button in 'GROUP_MA'
    symbol = tr('GROUP_MA', 'QPushButton')
    clickButton(waitForObject(symbol))

    #[step] Switch on 'COUL_11', 'Y_0' and 'Z_1' items in '2D elements' sub-tree
    waitForObjectItem(":MESH_QTreeWidget", "2D elements.COUL\\_11")
    clickItem(":MESH_QTreeWidget", "2D elements.COUL\\_11", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "2D elements.Y\\_0")
    clickItem(":MESH_QTreeWidget", "2D elements.Y\\_0", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "2D elements.Z\\_1")
    clickItem(":MESH_QTreeWidget", "2D elements.Z\\_1", 10, 10, 0, Qt.LeftButton)

    #[step] Switch on 'COUL_7', 'MOITIE1' and 'MOITIE2' items in '3D elements' sub-tree
    waitForObjectItem(":MESH_QTreeWidget", "3D elements.COUL\\_7")
    clickItem(":MESH_QTreeWidget", "3D elements.COUL\\_7", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "3D elements.MOITIE1")
    clickItem(":MESH_QTreeWidget", "3D elements.MOITIE1", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "3D elements.MOITIE2")
    clickItem(":MESH_QTreeWidget", "3D elements.MOITIE2", 10, 10, 0, Qt.LeftButton)

    #[step] Press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    #[step] Press 'Cancel' button; then press 'Yes' button in 'Close' dialog
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #[step] Press 'Close' button; then press 'Yes' button in 'Close' dialog
    clickButton(waitForObject(":Close_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    #[section] Edit LIRE_MAILLAGE command
    test.log("Edit LIRE_MAILLAGE command")

    #[step] Select 'LIRE_MAILLAGE' in data tree; context menu 'Edit'
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.LIRE_MAILLAGE", "Edit")

    #[step] Select 'MZERO' in 'UNITE' combobox
    symbol = tr('UNITE', 'QComboBox')
    setParameterValue(symbol, 'MZERO')

    #[step] Select 'MZERO' in 'NOM_MED' combobox
    symbol = tr('NOM_MED', 'QComboBox')
    setParameterValue(symbol, 'MZERO')

    #[step] Click OK button to accept the changes
    clickButton(waitForObject(":OK_QPushButton"))
    #[check] 'Edit command' panel was closed
    waitUntilObjectExists(":qt_splithandle__EditionPanel")

    #[step] Switch to 'Data Files' panel
    clickTab(waitForObject(":Data Settings_QTabWidget"), "Data Files")
    waitForObject(":DataFiles_QTreeView").expandAll()

    #[step] Check that 'MZERO' item presents in 'Data Files' tree
    clickItem(":DataFiles_QTreeView", "Stage\\_1.MZERO", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    #[section] Edit AFFE_CHAR_ACOU command
    test.log("Edit AFFE_CHAR_ACOU command")

    #[step] Switch to 'Data Settings' panel
    clickTab(waitForObject(":Data Settings_QTabWidget"), "Data Settings")

    #[step] Select 'AFFE_CHAR_ACOU' in data tree; context menu 'Edit'
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Other.AFFE_CHAR_ACOU", "Edit")

    #[step] Check on 'FORMAT' checkbox
    symbol = tr('PRES_IMPO', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Press 'Add' button in 'PRES_IMPO' editor
    clickButton(waitForObject(":PRES_IMPO_add_QToolButton"))

    #[step] Press 'Edit...' button in 'PRES_IMPO' / '[0]'
    clickButton(waitForObject(":0_ParameterButton"))

    #[step] Check on 'GROUP_NO' checkbox
    symbol = tr('GROUP_NO', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Press 'Edit...' button in 'GROUP_NO'
    symbol = tr('GROUP_NO', 'QPushButton')
    clickButton(waitForObject(symbol))

    #[step] Switch on 'ORIGINE', 'OPPOSE', 'I', 'J', 'K' items in 'Nodes' sub-tree
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.ORIGINE")
    clickItem(":MESH_QTreeWidget", "Nodes.ORIGINE", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.OPPOSE")
    clickItem(":MESH_QTreeWidget", "Nodes.OPPOSE", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.I")
    clickItem(":MESH_QTreeWidget", "Nodes.I", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.J")
    clickItem(":MESH_QTreeWidget", "Nodes.J", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "Nodes.K")
    clickItem(":MESH_QTreeWidget", "Nodes.K", 10, 10, 0, Qt.LeftButton)

    #[step] Press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    #[step] Check on 'GROUP_MA' checkbox
    symbol = tr('GROUP_MA', 'QCheckBox')
    clickButton(waitForObject(symbol))

    #[step] Press 'Edit...' button in 'GROUP_MA'
    symbol = tr('GROUP_MA', 'QPushButton')
    clickButton(waitForObject(symbol))

    #[step] Switch on 'COUL_11', 'Y_0' and 'Z_1' items in '2D elements' sub-tree
    waitForObjectItem(":MESH_QTreeWidget", "2D elements.COUL\\_11")
    clickItem(":MESH_QTreeWidget", "2D elements.COUL\\_11", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "2D elements.Y\\_0")
    clickItem(":MESH_QTreeWidget", "2D elements.Y\\_0", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "2D elements.Z\\_1")
    clickItem(":MESH_QTreeWidget", "2D elements.Z\\_1", 10, 10, 0, Qt.LeftButton)

    #[step] Switch on 'COUL_7', 'MOITIE1' and 'MOITIE2' items in '3D elements' sub-tree
    waitForObjectItem(":MESH_QTreeWidget", "3D elements.COUL\\_7")
    clickItem(":MESH_QTreeWidget", "3D elements.COUL\\_7", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "3D elements.MOITIE1")
    clickItem(":MESH_QTreeWidget", "3D elements.MOITIE1", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":MESH_QTreeWidget", "3D elements.MOITIE2")
    clickItem(":MESH_QTreeWidget", "3D elements.MOITIE2", 10, 10, 0, Qt.LeftButton)

    #[step] Press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    #[step] Press 'Cancel' button; then press 'Yes' button in 'Close' dialog
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #[step] Press 'Close' button; then press 'Yes' button in 'Close' dialog
    clickButton(waitForObject(":Close_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    #[section] Exit SALOME
    test.log("Exit SALOME")
    #[step] Menu 'File' - 'Exit'
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    #[step] Press 'OK' in 'Exit' dialog
    clickButton(waitForObject(":Exit.Ok_QPushButton"))
    #[step] Press 'Close w/o saving' in 'Close active study' dialog
    clickButton(waitForObject(":Close active study.Close w/o saving_QPushButton"))
