#------------------------------------------------------------------------------
# Issue # 1164 - SALOME: Open With operation for items in Data Files panel
# Check 'Open In Editor` feature

def main():
    source(findFile("scripts", "common.py"))
    global_start()


def cleanup():
    global_cleanup()


def generateFile(file_path, length=10):
    file_name = ""
    with open(file_path, 'w') as tmp_f:
        for i in range(length):
            text = "This is test file, line nb {}\n".format(i+1)
            tmp_f.write(text)


def browseFile(symbol, filename, click_browse=True):
    if click_browse:
        clickButton(waitForObject(symbol))
    file_path = filename if os.path.isabs(filename) else os.path.join(casedir(), filename)
    setParameterValue(":fileNameEdit_QLineEdit", file_path)
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))


def runCase():
    #--------------------------------------------------------------------------
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    test.compare(isItemExists("Stage_1", "Mesh", "mesh"), True)

    #--------------------------------------------------------------------------
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")
    symbol = tr('UNITE', 'QPushButton')
    file_path_1 = os.path.join(testdir(), 'file1.txt')
    generateFile(file_path_1)
    browseFile(symbol, file_path_1)
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject(":DataFiles_QTreeView").expandAll()

    #--------------------------------------------------------------------------
    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.20")
    clickItem(":DataFiles_QTreeView", "Stage\\_1.file1\\.txt", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.file1\\.txt", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
    test.compare(waitForObjectExists(":OK_QPushButton").enabled, False)
    test.compare(waitForObjectExists(":Apply_QPushButton").enabled, False)
    test.compare(waitForObjectExists(":Close_QPushButton").enabled, True)
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.20")
    clickItem(":DataFiles_QTreeView", "Stage\\_1.file1\\.txt", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.file1\\.txt", 52, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
    waitForObject(":_QTextEdit").setPlainText("aaaaaaaaaaaaaaaaaa")
    test.compare(waitForObjectExists(":OK_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":Apply_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":Close_QPushButton").enabled, True)
    clickButton(waitForObject(":Apply_QPushButton"))
    test.compare(waitForObjectExists(":OK_QPushButton").enabled, False)
    test.compare(waitForObjectExists(":Apply_QPushButton").enabled, False)
    test.compare(waitForObjectExists(":Close_QPushButton").enabled, True)
    waitForObject(":_QTextEdit").append("bbbbbbbbbbbbbbbbbb")
    test.compare(waitForObjectExists(":OK_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":Apply_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":Close_QPushButton").enabled, True)
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    file_path_2 = os.path.join(testdir(), 'file2.txt')
    generateFile(file_path_2, length=1000)

    #--------------------------------------------------------------------------
    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.20")
    clickItem(":DataFiles_QTreeView", "Stage\\_1.file1\\.txt", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.file1\\.txt", 52, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    clickButton(waitForObject(":Filename.Filename_QToolButton"))
    waitForObject(":fileNameEdit_QLineEdit").setText("file2.txt")
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.20")
    clickItem(":DataFiles_QTreeView", "Stage\\_1.file2\\.txt", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.file2\\.txt", 52, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
    clickButton(waitForObject(":AsterStudy.No_QPushButton"))

    #--------------------------------------------------------------------------
    waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.20")
    clickItem(":DataFiles_QTreeView", "Stage\\_1.file2\\.txt", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.file2\\.txt", 52, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
    clickButton(waitForObject(":AsterStudy.Yes_QPushButton"))
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    activateItem(waitForObjectItem(":AsterStudy_QMenuBar", "File"))
    activateItem(waitForObjectItem(":File_QMenu", "Exit"))
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    os.remove(file_path_1)
    os.remove(file_path_2)

    #--------------------------------------------------------------------------
    pass
