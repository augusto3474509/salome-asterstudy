#------------------------------------------------------------------------------
# Issue #984 - Bad processing of validity status when there are multiple dependencies
#   Case 1
#
#   Mesh = LIRE_MAILLAGE(UNITE=20)
#
#   Mat01 = DEFI_MATERIAU(ELAS=_F(E=2000.0, NU=0.3))
#
#   MatF = AFFE_MATERIAU(
#       AFFE=(_F(GROUP_MA='Upper', MATER=Mat01), _F(GROUP_MA='Lower', MATER=Mat01)),
#       MAILLAGE=Mesh
#   )
#
#   Should become invalid on Mat01 deletion

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    text = \
"""
Mesh = LIRE_MAILLAGE(UNITE=20)

Mat01 = DEFI_MATERIAU(ELAS=_F(E=2000.0, NU=0.3))

MatF = AFFE_MATERIAU(
    AFFE=(_F(GROUP_MA='Upper', MATER=Mat01), _F(GROUP_MA='Lower', MATER=Mat01)),
    MAILLAGE=Mesh
)
"""
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    waitForObject( text_editor ).appendPlainText( text )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase", "Stage_1", "Material", "MatF" )
    checkValidity( True, "CurrentCase", "Stage_1", "Material", "Mat01" )
    checkValidity( True, "CurrentCase", "Stage_1", "Mesh", "Mesh" )
    checkValidity( True, "CurrentCase", "Stage_1" )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    item, index = showOBIndex( "CurrentCase", "Stage_1", "Material", "Mat01" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    checkValidity( False, "CurrentCase", "Stage_1", "Material", "MatF" )
    checkOBItemNotExists( "CurrentCase", "Stage_1", "Material", "Mat01" )
    checkValidity( True, "CurrentCase", "Stage_1", "Mesh", "Mesh" )
    checkValidity( False, "CurrentCase", "Stage_1" )
    checkValidity( False, "CurrentCase" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
