#------------------------------------------------------------------------------
# Issue #1679 - Show documentation for command being edited

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # check command 4 doc: none
    check_command4doc('nothing', 'no active study')

    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")
    check_command4doc('nothing', 'History View')
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    selectObjectBrowserItem( "CurrentCase" )
    check_command4doc('nothing', 'Case View, case selected')

    #--------------------------------------------------------------------------
    # create empty stage
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    check_command4doc('nothing', 'Case View, stage selected')

    #--------------------------------------------------------------------------
    # add LIRE_MAILLAGE command 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    check_command4doc('LIRE_MAILLAGE', 'Case View, LIRE_MAILLAGE command selected')
    
    #--------------------------------------------------------------------------
    # add ASSE_MAILLAGE command 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "ASSE_MAILLAGE")
    check_command4doc('ASSE_MAILLAGE', 'Case View, LIRE_MAILLAGE command selected')

    #--------------------------------------------------------------------------
    # select stage
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    check_command4doc('nothing', 'Case View, stage selected')

    #--------------------------------------------------------------------------
    # open LIRE_MAILLAGE for editing
    activateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")
    check_command4doc('LIRE_MAILLAGE', 'Case View, LIRE_MAILLAGE command selected, LIRE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase.Stage_1.Mesh.mesh0" )
    check_command4doc('LIRE_MAILLAGE', 'Case View, ASSE_MAILLAGE selected, LIRE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase.Stage_1.Mesh" )
    check_command4doc('LIRE_MAILLAGE', 'Case View, category selected, LIRE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    check_command4doc('LIRE_MAILLAGE', 'Case View, stage selected, LIRE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase" )
    check_command4doc('LIRE_MAILLAGE', 'Case View, case selected, LIRE_MAILLAGE being edited')
    # open Edition panel
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # open ASSE_MAILLAGE for editing
    activateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh0", "Edit")
    check_command4doc('ASSE_MAILLAGE', 'Case View, ASSE_MAILLAGE command selected, ASSE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase.Stage_1.Mesh.mesh" )
    check_command4doc('ASSE_MAILLAGE', 'Case View, LIRE_MAILLAGE selected, ASSE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase.Stage_1.Mesh" )
    check_command4doc('ASSE_MAILLAGE', 'Case View, category selected, ASSE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    check_command4doc('ASSE_MAILLAGE', 'Case View, stage selected, ASSE_MAILLAGE being edited')
    selectObjectBrowserItem( "CurrentCase" )
    check_command4doc('ASSE_MAILLAGE', 'Case View, case selected, ASSE_MAILLAGE being edited')
    # open Edition panel
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass

def check_command4doc(command, msg):
    test.log('Check command 4 doc: {}'.format(msg))
    openContextMenu(waitForObject(":AsterStudy *.DebugWidget_DebugWidget"), 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.StandardToolbar_QMenu", "cmd for doc"))
    test.compare(str(waitForObject(":AsterStudy *.DebugWidget_DebugWidget").text), command)
