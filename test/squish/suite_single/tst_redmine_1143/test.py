#------------------------------------------------------------------------------
# Issue #1143 - Automatically activate Edit operation when command is added from menu

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(nativenames=False, basicnaming=False)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # [section] Create New study
    # [step] Menu File - New
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # [section] Create empty stage in current case
    # [step] Switch to current case
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    # [step] Select CurrentCase object in Data Settings panel, invoke "Add Stage" item from popup menu
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    # [section] Add command to the stage
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Mesh" / "Read a mesh" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "Read a mesh")
    selectObjectBrowserItem("CurrentCase.Stage_1.Mesh.mesh")
    # [step] Check that editing of command is NOT automatically started

    #--------------------------------------------------------------------------
    # [section] Activate auto-edit feature
    # [step] Menu Edit - Preferences
    activateMenuItem("File", "Preferences...")
    # [step] Switch ON "Automatically activate command edition" check box
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_auto_edit_QCheckBox"))
    # [step] Preferences dialog: press OK button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Add command to the stage
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Mesh" / "Read a mesh" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "Read a mesh")
    selectObjectBrowserItem("CurrentCase.Stage_1.Mesh.mesh")
    # [step] Check that editing of command is automatically started
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Quit application
    # [step] Menu File - Exit
    activateMenuItem("File", "Exit")
    # [step] Press "Discard" button in confirmation dialog
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
