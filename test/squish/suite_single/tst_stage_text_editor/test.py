def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    activateMenuItem("Operations", "Add Stage")
    showOBItem( "CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Material", "DEFI_MATERIAU")
    showOBItem( "CurrentCase", "Stage_1", "Material", "DEFI_MATERIAU")

    #--------------------------------------------------------------------------
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    clickButton(waitForObject(":AsterStudy.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.Stage_1", "Edit")

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    old_text = waitForObject(text_editor).toPlainText()
    test.compare(old_text, "DEFI_MATERIAU = DEFI_MATERIAU()\n")

    new_text = ("m1 = LIRE_MAILLAGE(UNITE=20)\n"
                "m2 = LIRE_MAILLAGE(UNITE=20)\n"
                "m3 = AFFE_MODELE(\n"
                "    AFFE=_F(MODELISATION='3D', PHENOMENE='MECANIQUE', TOUT='OUI'), MAILLAGE=m1\n"
                ")\n"
                "m4 = LIRE_MAILLAGE(UNITE=20)\n")
    waitForObject(text_editor).clear()
    waitForObject(text_editor).appendPlainText(new_text)
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "Stage_1", "Mesh",       "m1")
    showOBItem( "CurrentCase", "Stage_1", "Mesh",       "m2")
    showOBItem( "CurrentCase", "Stage_1", "Mesh",       "m4")
    showOBItem( "CurrentCase", "Stage_1", "Model Definition", "m3")

    #--------------------------------------------------------------------------
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
