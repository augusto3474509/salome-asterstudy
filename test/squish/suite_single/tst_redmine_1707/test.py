#------------------------------------------------------------------------------
# Issue #1707 - Regression: Undo action list does not contain last performed operation

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # create empty stage in current case and check that undo list contains one item
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    check_undo('Add stage')

    #--------------------------------------------------------------------------
    # add command via ShowAll dialog / Apply button 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Show All")
    mouseClick(waitForObjectItem(":_QListWidget", "LIRE\\_MAILLAGE"), 5, 5, 0, Qt.LeftButton)
    clickButton(waitForObject(":Apply_QPushButton"))
    clickButton(waitForObject(":Close_QPushButton"))
    check_undo('Add command')

    #--------------------------------------------------------------------------
    # add command via menu 
    selectObjectBrowserItem( "CurrentCase.Stage_1" )
    waitForActivateMenuItem("Commands", "Mesh", "LIRE_MAILLAGE")
    check_undo('Add command')
    
    #--------------------------------------------------------------------------
    # edit command 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    check_undo('Edit command')
    
    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass

def check_undo(command):
    sendEvent("QMouseEvent", waitForObject(":AsterStudy *.Undo_QToolButton"), QEvent.MouseButtonPress, 35, 10, Qt.LeftButton, 1, 0)
    mouseClick(waitForObjectItem(":Undo_QListWidget", "{}".format(command)), 5, 5, 0, Qt.LeftButton)
    waitForActivateMenuItem("Edit", "Redo")
