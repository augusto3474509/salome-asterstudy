#------------------------------------------------------------------------------
# Issue #1678 - Facilitate display of missing command input in empty drop-down list

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # create empty stage
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add AFFE_MODELE command and open it for editing, check that
    # <no type available> item is present in selector 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Model Definition", "AFFE_MODELE")
    symbol = tr('MAILLAGE', 'QComboBox')
    test.compare(waitForObject(symbol).count, 1)
    test.compare(str(waitForObject(symbol).currentText), '<no maillage_sdaster available>')
    setParameterValue(symbol, '<no maillage_sdaster available>')
    clickButton(waitForObject(":Close_QPushButton"))
    
    #--------------------------------------------------------------------------
    # add LIRE_MAILLAGE command 
    code = """
stage = AsterStdGui.gui.study().history.current_case[-1]
cmd = stage('LIRE_MAILLAGE', 'mesh')
cmd.init({'UNITE':{20:'aaa.txt'},})
"""
    exec_in_console(code)

    #--------------------------------------------------------------------------
    # open AFFE_MODELE command for editing, check that
    # <no type available> item is NOT present in selector 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Model Definition.model", "Edit")
    symbol = tr('MAILLAGE', 'QComboBox')
    test.compare(waitForObject(symbol).count, 1)
    test.compare(str(waitForObject(symbol).currentText), 'mesh')
    clickButton(waitForObject(":Close_QPushButton"))
    
    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
