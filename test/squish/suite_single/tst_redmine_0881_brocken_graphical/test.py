#------------------------------------------------------------------------------
# Issue #881 - Operations: Import Stage
# -  if conversion to Graphical mode was unsuccessful, stage should be created in Text mode

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    bad_file = os.path.join( os.getcwd(), 'tst_redmine_881_brocken_graphical.comm' )
    test.log( "Use '%s' file to create a new stage" % bad_file )
    importStage( bad_file )

    #--------------------------------------------------------------------------
    test.verify( "The stage cannot be converted to graphical mode." \
                 in str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ))
    clickButton( waitForObject( ":AsterStudy.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    item = "CurrentCase.tst\\_redmine\\_881\\_brocken\\_graphical"
    waitForObjectItem( ":_QTreeWidget", item )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    test.compare( str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ),
                  "Cannot convert the stage to graphic mode" )
    clickButton( waitForObject( ":AsterStudy.OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
