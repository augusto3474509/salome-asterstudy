#------------------------------------------------------------------------------
# Issue #1700 - Support “Hide Unused” feature (for Mesh group selection widget) 

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add empty stage in current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add LIRE_MAILLAGE and DEFI_GROUP commands
    med_file = os.path.join( casedir(), findFile( "testdata", "Mesh_recette.med" ) )
    code = """
stage = AsterStdGui.gui.study().history.current_case[-1]
mesh = stage('LIRE_MAILLAGE', 'mesh')
mesh.init({'UNITE':{20:'%s'},})
grp = stage('DEFI_GROUP', 'grp')
grp.init({'MAILLAGE':mesh,})
""" % med_file
    exec_in_console(code)

    #--------------------------------------------------------------------------
    # open DEFI_GROUP command for editing
    item = selectItem("Stage_1", "Mesh", "mesh:5")
    openContextMenu(item, 10, 10, 0)
    activateItem(waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    #--------------------------------------------------------------------------
    # Activate a mesh group selection widget
    clickButton(waitForObject(tr('CREA_GROUP_MA', 'QCheckBox')))
    clickButton(waitForObject(tr('CREA_GROUP_MA_add', 'QToolButton')))
    clickButton(waitForObject(":0_ParameterButton"))
    clickButton(waitForObject(tr('GROUP_MA', 'QRadioButton')))
    clickButton(waitForObject(tr('GROUP_MA', 'QPushButton')))
    # check that all groups are visible
    test.vp("VP1")
   
    #--------------------------------------------------------------------------
    # switch ON 'Hide Unused' button
    clickButton(waitForObject(":Hide Unused_QToolButton"))
    # check that all groups are now hidden
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # switch OFF 'Hide Unused' button
    clickButton(waitForObject(":Hide Unused_QToolButton"))
    # check that all groups are now visible again
    test.vp("VP1")

    #--------------------------------------------------------------------------
    # Select several mesh groups
    mouseClick(waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_mast"), 10, 5, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_slav"), 10, 5, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":_QTreeWidget", "2D elements.Lower"),       10, 5, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":_QTreeWidget", "2D elements.Upper"),       10, 5, 0, Qt.LeftButton)
    # check that all groups are still visible
    test.vp("VP3")

    #--------------------------------------------------------------------------
    # switch ON 'Hide Unused' button
    clickButton(waitForObject(":Hide Unused_QToolButton"))
    # check that some groups are now hidden
    test.vp("VP4")

    #--------------------------------------------------------------------------
    # Deselect currently selected mesh groups
    mouseClick(waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_mast"), 10, 5, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_slav"), 10, 5, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":_QTreeWidget", "2D elements.Lower"),       10, 5, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":_QTreeWidget", "2D elements.Upper"),       10, 5, 0, Qt.LeftButton)
    # check that all groups are now hidden
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # switch OFF 'Hide Unused' button
    clickButton(waitForObject(":Hide Unused_QToolButton"))
    # check that all groups are now visible again
    test.vp("VP1")

    #--------------------------------------------------------------------------
    pass
