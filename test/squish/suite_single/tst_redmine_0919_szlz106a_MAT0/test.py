#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'FATIGUE' )

    symbol = tr( 'FATIGUE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'FATIGUE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'A_BASQUIN', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'A_BASQUIN', 'QLineEdit' )
    setParameterValue( symbol, str( 1.001730939E-14 ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'BETA_BASQUIN', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'BETA_BASQUIN', 'QLineEdit' )
    setParameterValue( symbol, str( 4.065 ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'FATIGUE' )

    symbol = tr( 'FATIGUE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'FATIGUE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'A_BASQUIN', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'A_BASQUIN', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 1.001730939E-14 ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'BETA_BASQUIN', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'BETA_BASQUIN', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 4.065 ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'szlz106a.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file )

    #--------------------------------------------------------------------------
    category = "Material"
    type = "DEFI_MATERIAU"
    name = "MAT0"

    #--------------------------------------------------------------------------
    item = selectItem( "szlz106a", "%s" % category, "%s" % name )
    checkContents( item, name )

    openContextMenu( selectItem( "szlz106a" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    openContextMenu( selectItem( "szlz106a" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    item = selectItem( "szlz106a", "%s" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    populateContents( category, type, name, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
