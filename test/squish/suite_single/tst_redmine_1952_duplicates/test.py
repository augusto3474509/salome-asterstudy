import re

def main():
    source(findFile('scripts', 'common.py'))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def datafilessummary_items():
    model = waitForObject(":DataFilesSummary_Tree").model()

    actual = []
    for row in range(model.rowCount()):
        index = model.index(row, 1)
        label = model.data(index)
        tooltip = model.data(index, Qt.ToolTipRole)
        stage_name, file_path = re.match(r'^<pre><b>Stage</b>: (.+)<br/><b>File</b>: (.+)</pre>$', str(tooltip)).groups()
        actual.append((label, stage_name, file_path))

    return tuple(actual)

def runCase():
    # Create new study
    waitForActivateMenuItem('File', 'New')
    
    exec_in_console("""
stage1 = AsterStdGui.gui.study().history.current_case.create_stage()
stage2 = AsterStdGui.gui.study().history.current_case.create_stage()

# Same file, same id
stage1.add_command('LIRE_MAILLAGE', 'v1').init(dict(UNITE={10:'/tmp/a'}))
stage2.add_command('LIRE_MAILLAGE', 'v2').init(dict(UNITE={10:'/tmp/a'}))

# Same file, different ids
stage1.add_command('LIRE_MAILLAGE', 'v3').init(dict(UNITE={20:'/tmp/b'}))
stage2.add_command('LIRE_MAILLAGE', 'v4').init(dict(UNITE={30:'/tmp/b'}))

# Different files, same id
stage1.add_command('LIRE_MAILLAGE', 'v5').init(dict(UNITE={40:'/tmp/c'}))
stage2.add_command('LIRE_MAILLAGE', 'v6').init(dict(UNITE={40:'/tmp/d'}))
    """)
    
    test.compare((
        ('a', 'Stage_1', '/tmp/a'),
        ('a', 'Stage_2', '/tmp/a'),
        ('b', 'Stage_1', '/tmp/b'),
        ('b', 'Stage_2', '/tmp/b'),
        ('c', 'Stage_1', '/tmp/c'),
        ('d', 'Stage_2', '/tmp/d'),
    ), datafilessummary_items())
