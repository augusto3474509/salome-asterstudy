#------------------------------------------------------------------------------
# Issue #973 - Stage and Case validation method

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    addCommandByDialog( "LIRE_MAILLAGE", "Mesh" )
    item = showOBItem( "CurrentCase", "Stage_1", "Mesh", "LIRE_MAILLAGE" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( ":Name_QLineEdit", "mesh" )

    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    med_file_path = os.path.join( casedir(), findFile( "testdata", "zzzz121b.med" ) )
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(med_file_path))

    clickButton( waitForObject( ":OK_QPushButton" ) )


    checkValidity( True, "CurrentCase", "Stage_1", "Mesh", "mesh" )
    checkValidity( True, "CurrentCase", "Stage_1", "Mesh" )
    checkValidity( True, "CurrentCase", "Stage_1" )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    addCommandByDialog( "MODI_MAILLAGE", "Mesh" )
    checkValidity( True, "CurrentCase", "Stage_1", "Mesh", "mesh" )
    checkValidity( False, "CurrentCase", "Stage_1", "Mesh", "MODI_MAILLAGE" )
    checkValidity( False, "CurrentCase", "Stage_1", "Mesh" )
    checkValidity( False, "CurrentCase", "Stage_1" )
    checkValidity( False, "CurrentCase" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
