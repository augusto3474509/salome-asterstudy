#------------------------------------------------------------------------------
# Issue #1680 - Add stage from template

import shutil

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # copy the sample files to the test directory
    casedir_path = casedir()
    template1_file = os.path.join(casedir_path, "template1.comm")
    template2_file = os.path.join(casedir_path, "template2.comm")

    testdir_path = testdir()
    template_dir = os.path.join(testdir_path, ".config", "salome", "asterstudy_templates")
    if not os.path.isdir(template_dir):
        breakTest("Template directory does not exist: '%s'" % template_dir)

    shutil.copy(template1_file, template_dir)
    shutil.copy(template2_file, template_dir)

    #--------------------------------------------------------------------------
    # create new study
    activateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add two stages from template (one in graphic mode, one in text mode)
    activateOBContextMenuItem("CurrentCase", "Add Stage from Template", "template1.comm")
    activateOBContextMenuItem("CurrentCase", "Add Text Stage from Template", "template2.comm")

    #--------------------------------------------------------------------------
    # check that the stages are imported in correct mode
    activateOBContextMenuItem("CurrentCase.template1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.template1", "Graphical Mode")
    activateOBContextMenuItem("CurrentCase.template2", "Graphical Mode")
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # remove the templates from the disk and check the case context menu 
    template1_file = os.path.join(template_dir, "template1.comm")
    template2_file = os.path.join(template_dir, "template2.comm")

    os.remove(template1_file)
    os.remove(template2_file)

    activateOBContextMenuItem("CurrentCase", "Add Stage from Template", "Template files are not found")

    #--------------------------------------------------------------------------
    # remove the template folder from the disk and check the case context menu
    os.rmdir(template_dir) 

    activateOBContextMenuItem("CurrentCase", "Add Stage from Template", "Template files are not found")

    #--------------------------------------------------------------------------
    pass
