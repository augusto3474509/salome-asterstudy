#------------------------------------------------------------------------------
# Issue #980 - Propagate validity status down to children
#
#   m = LIRE_MAILLAGE(UNITE=20)
#
#   m = MODI_MAILLAGE(MAILLAGE=m, ORIE_PEAU_2D=_F(GROUP_MA='A'))
#
#   m = AFFE_MODELE(
#       AFFE=_F(MODELISATION='C_PLAN', PHENOMENE='MECANIQUE', TOUT='OUI'),
#       MAILLAGE=m
#   )
#
#   Should become invalid on deletion of any of m

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    text = \
"""
m = LIRE_MAILLAGE(UNITE=20)

m = MODI_MAILLAGE(MAILLAGE=m, ORIE_PEAU_2D=_F(GROUP_MA='A'))

m = AFFE_MODELE(
    AFFE=_F(MODELISATION='C_PLAN', PHENOMENE='MECANIQUE', TOUT='OUI'),
    MAILLAGE=m
)
"""
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    waitForObject( text_editor ).appendPlainText( text )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    isValid( False, "Stage_1" )
    isValid( True, "Stage_1", "Mesh" )
    isValid( True, "Stage_1", "Mesh", "m:6" )
    isValid( True, "Stage_1", "Mesh", "m:7" )
    isValid( False, "Stage_1", "Model Definition" )
    isValid( False, "Stage_1", "Model Definition", "m" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Mesh", "m:6" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    isValid( False, "Stage_1" )
    isValid( False, "Stage_1", "Mesh" )
    test.compare( isItemExists( "Stage_1", "Mesh", "m:6" ), False )
    isValid( False, "Stage_1", "Mesh", "m:7" )
    isValid( False, "Stage_1", "Model Definition" )
    isValid( False, "Stage_1", "Model Definition", "m" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Undo_QToolButton" ) )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Mesh", "m:7" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    isValid( False, "Stage_1" )
    isValid( True, "Stage_1", "Mesh" )
    test.compare( isItemExists( "Stage_1", "Mesh", "m:7" ), False )
    isValid( True, "Stage_1", "Mesh", "m:6" )
    isValid( False, "Stage_1", "Model Definition" )
    isValid( False, "Stage_1", "Model Definition", "m" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
