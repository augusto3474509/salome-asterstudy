def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget"), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )

    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Stage_1" ) )

    treeWidget = waitForObject(":_QTreeWidget")

    #--------------------------------------------------------------------------
    addCommandByDialog( "RECU_FONCTION", "Output" )

    test.verify( isItemExpanded( "CurrentCase", "Stage_1", "Output:-1" ) )
    test.verify( isItemSelected( "CurrentCase", "Stage_1", "Output:-1", "RECU_FONCTION:4" ) )

    #--------------------------------------------------------------------------
    treeWidget.collapseAll()

    test.xverify( isItemExpanded( "CurrentCase" ) )

    #--------------------------------------------------------------------------
    addCommandByDialog( "LIRE_TABLE", "Functions and Lists" )

    test.verify( isItemExpanded( "CurrentCase", "Stage_1", "Functions and Lists" ) )
    test.verify( isItemSelected( "CurrentCase", "Stage_1", "Functions and Lists", "LIRE_TABLE" ) )

    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Functions and Lists.LIRE_TABLE", "Edit")
    waitForObject(":qt_splithandle__EditionPanel")

    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    comm_file_path = os.path.join( casedir(), 'zzzz289f.comm' )
    setParameterValue(":fileNameEdit_QLineEdit", comm_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(comm_file_path))

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    treeWidget.collapseAll()

    test.xverify( isItemExpanded( "CurrentCase" ) )

    #--------------------------------------------------------------------------
    addCommandByDialog( "RECU_FONCTION", "Output" )

    test.verify( isItemExpanded( "CurrentCase", "Stage_1", "Output:-2" ) )
    test.verify( isItemSelected( "CurrentCase", "Stage_1", "Output:-2", "RECU_FONCTION:6" ) )

    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_1.Output.RECU\\_FONCTION_2", 94, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    waitForObject(":qt_splithandle__EditionPanel")

    symbol = tr( 'TABLE', 'QRadioButton' )
    clickButton(waitForObjectExists( symbol ))
    setParameterValue( ":TABLE_QComboBox", "LIRE_TABLE" )

    symbol = tr( 'PARA_X', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )
    symbol = tr( 'PARA_X', 'QLineEdit' )
    setParameterValue(symbol, "1")

    symbol = tr( 'PARA_Y', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )
    symbol = tr( 'PARA_Y', 'QLineEdit' )
    setParameterValue(symbol, "1")

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    treeWidget.collapseAll()

    test.xverify( isItemExpanded( "CurrentCase" ) )

    #--------------------------------------------------------------------------
    addCommandByDialog( "RECU_FONCTION", "Output" )

    test.verify( isItemExpanded( "CurrentCase", "Stage_1", "Output:-2" ) )
    test.verify( isItemSelected( "CurrentCase", "Stage_1", "Output:-2", "RECU_FONCTION:7" ) )

    #--------------------------------------------------------------------------
    importStage( os.path.join( casedir(), 'zzzz289f.comm' ), [":Undefined files.OK_QPushButton"] )

    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "zzzz289f" ) )

    #--------------------------------------------------------------------------
    importStage( os.path.join( casedir(), 'zzzz289f.comm' ), [":Undefined files.OK_QPushButton"] )

    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "zzzz289f_1" ) )
