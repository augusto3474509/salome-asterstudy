from shutil import rmtree
from tempfile import mkdtemp

def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    path = mkdtemp()
    try:
        with open(path + "/1.txt", "w"):
            pass
        with open(path + "/2.txt", "w"):
            pass
        
        waitForActivateMenuItem("File", "New")
        clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
        activateMenuItem("Operations", "Add Stage")
    
        mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 67, 0, 0, Qt.LeftButton)
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Mesh.mesh"), 47, 6, 0, Qt.LeftButton)
        clickButton(waitForObject(":UNITE_QPushButton"))
        type(waitForObject(":fileNameEdit_QLineEdit"), path + "/1.txt")
        type(waitForObject(":fileNameEdit_QLineEdit"), "<Return>")
        clickButton(waitForObject(":OK_QPushButton"))
    
        mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 67, 0, 0, Qt.LeftButton)
        doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Mesh.mesh0"), 47, 6, 0, Qt.LeftButton)
        clickButton(waitForObject(":UNITE_QPushButton"))
        type(waitForObject(":fileNameEdit_QLineEdit"), path + "/2.txt")
        type(waitForObject(":fileNameEdit_QLineEdit"), "<Return>")
        clickButton(waitForObject(":OK_QPushButton"))
        
        mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
        
        # leave unmodified, close
        openContextMenu(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 69, 9, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
        
        # write text, try closing
        openContextMenu(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 69, 9, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
        type(waitForObject(":_QTextEdit"), "Hello.")
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
        clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
        
        # write text, save, close
        openContextMenu(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 69, 9, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
        type(waitForObject(":_QTextEdit"), "Hello.")
        clickButton(waitForObject(":Apply_QPushButton"))
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
        
        # edit text, try closing
        openContextMenu(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 69, 9, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
        type(waitForObject(":_QTextEdit"), "<Ctrl+A>")
        type(waitForObject(":_QTextEdit"), "Hello again.")
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
        clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
        
        # edit text, save, close
        openContextMenu(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 69, 9, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
        type(waitForObject(":_QTextEdit"), "<Ctrl+A>")
        type(waitForObject(":_QTextEdit"), "Hello again.")
        clickButton(waitForObject(":Apply_QPushButton"))
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
        
        # erase text, try closing
        openContextMenu(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 69, 9, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
        type(waitForObject(":_QTextEdit"), "<Ctrl+A>")
        type(waitForObject(":_QTextEdit"), "<Delete>")
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
        clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
        
        # erase text, save, close
        openContextMenu(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 69, 9, 0)
        activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Open In Editor"))
        type(waitForObject(":_QTextEdit"), "<Ctrl+A>")
        type(waitForObject(":_QTextEdit"), "<Delete>")
        clickButton(waitForObject(":Apply_QPushButton"))
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
        
        # one more action, to be sure we are not seeing a dialog now
        doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 46, 9, 0, Qt.LeftButton)
    
    finally:
        rmtree(path)
