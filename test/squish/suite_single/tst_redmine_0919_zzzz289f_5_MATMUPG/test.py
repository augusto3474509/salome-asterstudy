#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForPopupItem("Edit")

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'OPTION' )

    symbol = tr( 'OPTION', 'QComboBox' )
    setParameterValue( symbol, "MASS_MECA" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'MODELE' )

    symbol = tr( 'MODELE', 'QComboBox' )
    setParameterValue( symbol, "MODELUPG" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'CHAM_MATER' )

    symbol = tr( 'CHAM_MATER', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'CHAM_MATER', 'QComboBox' )
    setParameterValue( symbol, "CHMAT_Q" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", '' )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name, imported ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForPopupItem("Edit")

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'OPTION' )

    symbol = tr( 'OPTION', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "MASS_MECA" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'MODELE' )

    symbol = tr( 'MODELE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "MODELUPG" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'CHAM_MATER' )

    symbol = tr( 'CHAM_MATER', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'CHAM_MATER', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "CHMAT_Q" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    category = "Other"
    type = "CALC_MATR_ELEM"
    name = "MATMUPG"

    #--------------------------------------------------------------------------
    item = selectItem( "zzzz289f", "%s:-5" % category, "%s" % name )
    checkContents( item, name, imported = True )

    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    popupItem("Graphical Mode")
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    item = selectItem( "zzzz289f", "%s:-5" % category, "%s" % name )
    checkContents( item, name, imported = True )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    name = "MATMUPG1"
    populateContents( category, type, name, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name, imported = False )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name, imported = True )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
