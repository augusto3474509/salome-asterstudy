#------------------------------------------------------------------------------
# Issue #1509 - In DEFI_GROUP/CREA_GROUP_MA, the keywords UNION, INTERSEC, etc do not activate the widget "MeshGroupSelection"

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add empty stage in current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add DEFI_GROUP command
    setClipboardText("cmd = DEFI_GROUP()")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Paste")

    #--------------------------------------------------------------------------
    # check CREA_GROUP_MA keyword
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.cmd", "Edit")
    clickButton(waitForObject(tr('CREA_GROUP_MA', 'QCheckBox')))
    clickButton(waitForObject(tr('CREA_GROUP_MA_add', 'QToolButton')))
    clickButton(waitForObject(":0_ParameterButton"))

    clickButton(waitForObject(tr('GROUP_MA', 'QRadioButton')))
    clickButton(waitForObject(tr('GROUP_MA', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('GROUP_MA', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(tr('INTERSEC', 'QRadioButton')))
    clickButton(waitForObject(tr('INTERSEC', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('INTERSEC', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(tr('UNION', 'QRadioButton')))
    clickButton(waitForObject(tr('UNION', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('UNION', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(tr('DIFFE', 'QRadioButton')))
    clickButton(waitForObject(tr('DIFFE', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('DIFFE', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(":Abort_QPushButton"))
    clickButton(waitForObject(":Abort.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    # check CREA_GROUP_NO keyword
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.cmd", "Edit")
    clickButton(waitForObject(tr('CREA_GROUP_NO', 'QCheckBox')))
    clickButton(waitForObject(tr('CREA_GROUP_NO_add', 'QToolButton')))
    clickButton(waitForObject(":0_ParameterButton"))

    clickButton(waitForObject(tr('GROUP_MA', 'QRadioButton')))
    clickButton(waitForObject(tr('GROUP_MA', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('GROUP_MA', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(tr('GROUP_NO', 'QRadioButton')))
    clickButton(waitForObject(tr('GROUP_NO', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('GROUP_NO', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(tr('INTERSEC', 'QRadioButton')))
    clickButton(waitForObject(tr('INTERSEC', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('INTERSEC', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(tr('UNION', 'QRadioButton')))
    clickButton(waitForObject(tr('UNION', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('UNION', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(tr('DIFFE', 'QRadioButton')))
    clickButton(waitForObject(tr('DIFFE', 'QPushButton')))
    type(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')), "A,B,C")
    clickButton(waitForObject(':OK_QPushButton'))
    clickButton(waitForObject(tr('DIFFE', 'QPushButton')))
    test.compare(waitForObject(tr('MANUAL_INPUT', 'QLineEdit')).text, "A,B,C")
    clickButton(waitForObject(':Cancel_QPushButton'))

    clickButton(waitForObject(":Abort_QPushButton"))
    clickButton(waitForObject(":Abort.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    # exit
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
