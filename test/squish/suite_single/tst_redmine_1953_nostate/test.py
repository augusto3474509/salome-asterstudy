def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem( "Operations", "Add Stage" )
    activateMenuItem( "Operations", "Add Stage" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    
    test.log('Check Stage 2: Execute')
    use_option('Stage_2', Execute, force=True)
    test.compare(Execute, run_option('Stage_1'))
    test.compare(Execute, run_option('Stage_2'))
    
    test.log('Uncheck Stage 1: Execute')
    use_option('Stage_1', Execute, force=True)
    test.compare(Execute, run_option('Stage_1'))
    test.compare(Execute, run_option('Stage_2'))
    
    test.log('Check Stage 1: Skip')
    use_option('Stage_1', Skip, force=True)
    test.compare(Skip, run_option('Stage_1'))
    test.compare(Skip, run_option('Stage_2'))
    
    test.log('Check Stage 1: Execute')
    use_option('Stage_1', Execute, force=True)
    test.log('Check Stage 2: Execute')
    use_option('Stage_2', Execute, force=True)
    test.compare(Execute, run_option('Stage_1'))
    test.compare(Execute, run_option('Stage_2'))
    
    test.log('Uncheck Stage 2: Execute')
    use_option('Stage_2', Execute, force=True)
    test.compare(Execute, run_option('Stage_1'))
    test.compare(Execute, run_option('Stage_2'))