#------------------------------------------------------------------------------
# Issue #1267 - Manage user catalogs

import os

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()
    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    try:
        catadir = os.path.join(testdir(), 'dev', 'codeaster', 'install', 'std', 'lib', 'aster', 'code_aster')
        os.makedirs(catadir)
    except OSError:
        pass

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # switch to 'Catalogs' tab
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Catalogs")

    #--------------------------------------------------------------------------
    # add 'dev' catalog proposed by default
    clickButton(waitForObject(":Preferences_group_user_catalogs.cataview_add_btn_QPushButton"))
    test.compare(str(waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").text), "DEV")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # try to add again 'DEV' catalog in different registers and check that error message is shown
    clickButton(waitForObject(":Preferences_group_user_catalogs.cataview_add_btn_QPushButton"))
    test.compare(str(waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").text), "dev")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))

    waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").setText("DEV")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))

    #--------------------------------------------------------------------------
    # try to add 'stable' catalog in different registers and check that error message is shown
    waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").setText("stable")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))

    waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").setText("STABLE")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))

    #--------------------------------------------------------------------------
    # try to add 'testing' catalog in different registers and check that error message is shown
    waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").setText("testing")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))

    waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").setText("TESTING")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))

    #--------------------------------------------------------------------------
    # try to add 'aaa' catalog with the same path as already added 'dev' item and check that error message is shown
    waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").setText("aaa")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))

    #--------------------------------------------------------------------------
    # close 'Preferences' dialog
    clickButton(waitForObject(":Preferences_group_user_catalogs.Cancel_QPushButton"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")
    # switch to 'Catalogs' tab
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Catalogs")

    #--------------------------------------------------------------------------
    # try to add again 'dev' catalog in different registers and check that error message is shown
    clickButton(waitForObject(":Preferences_group_user_catalogs.cataview_add_btn_QPushButton"))
    test.compare(str(waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").text), "dev")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton_2"))
    clickButton(waitForObject(":Preferences_group_user_catalogs.Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # remove 'dev' item
    clickButton(waitForObject(":cataview_frame.cataview_item_dev_btn_QPushButton"))

    #--------------------------------------------------------------------------
    # close 'Preferences' dialog
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")
    # switch to 'Catalogs' tab
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Catalogs")

    #--------------------------------------------------------------------------
    # add 'dev' catalog proposed by default
    clickButton(waitForObject(":Preferences_group_user_catalogs.cataview_add_btn_QPushButton"))
    test.compare(str(waitForObject(":Preferences_group_user_catalogs.cataview_setup_catalog_title_QLineEdit").text), "dev")
    clickButton(waitForObject(":Preferences_group_user_catalogs.OK_QPushButton"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # Quit application
    activateMenuItem("File", "Exit")

    #--------------------------------------------------------------------------
    pass
