#------------------------------------------------------------------------------
# Issue #1764 - Bad behavior of deactivated command with data file associated

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # create empty stage
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add LIRE_MAILLAGE command 
    code = """
cmd = AsterStdGui.gui.study().history.current_case[-1].add_command('LIRE_MAILLAGE', 'mesh')
cmd.init({'UNITE':{20:'aaa.txt'},})
"""
    exec_in_console(code)

    #--------------------------------------------------------------------------
    # check the 'aaa.txt' item is visible in the 'Data Files' tree
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    test.vp("VP1")

    #--------------------------------------------------------------------------
    # deactivate LIRE_MAILLAGE command
    activateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Deactivate")

    #--------------------------------------------------------------------------
    # check the 'aaa.txt' item is not visible in the 'Data Files' tree
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # re-activate LIRE_MAILLAGE command
    activateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Activate")

    #--------------------------------------------------------------------------
    # check the 'aaa.txt' item is again visible in the 'Data Files' tree, file name is unknown
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    test.vp("VP3")

    #--------------------------------------------------------------------------
    # deactivate again LIRE_MAILLAGE command
    activateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Deactivate")

    #--------------------------------------------------------------------------
    # invoke and cancel 'Delete' operation for LIRE_MAILLAGE command
    activateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Delete")
    clickButton(waitForObject(":Delete.No_QPushButton"))
    
    #--------------------------------------------------------------------------
    # check the 'aaa.txt' item is not visible in the 'Data Files' tree
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # Undo last operation (Deactivate), check that 'Data Files' shows previous state
    waitForActivateMenuItem( "Edit", "Undo" )
    test.vp("VP3")

    #--------------------------------------------------------------------------
    # Undo last operation (Activate), check that 'Data Files' shows previous state
    waitForActivateMenuItem( "Edit", "Undo" )
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # Undo last operation (Deactivate), check that 'Data Files' shows previous state
    waitForActivateMenuItem( "Edit", "Undo" )
    test.vp("VP1")

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
