#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'MAILLAGE' )

    symbol = tr( 'MAILLAGE', 'QComboBox' )
    setParameterValue( symbol, "MAIL_Q" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'AFFE' )

    symbol = tr( 'AFFE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    mouseClick( waitForObject( ":0_QPushButton" ), 10, 10, 0, Qt.LeftButton )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'TOUT' )

    symbol = tr( 'TOUT', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'TOUT', 'QComboBox' )
    setParameterValue( symbol, 'OUI' )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'PHENOMENE' )

    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'MODELISATION' )

    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QComboBox' )
    setParameterValue( symbol, 'AXIS_INCO_UPG' )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )
    clickButton( waitForObject( ":OK_QPushButton" ) )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name, imported ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'MAILLAGE' )

    symbol = tr( 'MAILLAGE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "MAIL_Q" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'AFFE' )

    symbol = tr( 'AFFE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    mouseClick( waitForObject( ":0_QPushButton" ), 10, 10, 0, Qt.LeftButton )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'TOUT' )

    symbol = tr( 'TOUT', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'TOUT', 'QComboBox' )
    test.compare( str(waitForObjectExists( symbol ).currentText ), "OUI" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'PHENOMENE' )

    symbol = tr( 'PHENOMENE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "MECANIQUE" )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'MODELISATION' )

    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "AXIS_INCO_UPG" )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    category = "Model Definition"
    type = "AFFE_MODELE"
    name = "MODELUPG"

    #--------------------------------------------------------------------------
    item = selectItem( "zzzz289f", "%s" % category, "%s" % name )
    checkContents( item, name, imported = True )

    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    item = selectItem( "zzzz289f", "%s" % category, "%s" % name )
    checkContents( item, name, imported = True )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    name = "MODELUP1"
    populateContents( category, type, name, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name, imported = False )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name, imported = True )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
