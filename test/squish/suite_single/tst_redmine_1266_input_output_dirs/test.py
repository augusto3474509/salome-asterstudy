#------------------------------------------------------------------------------
# Issue #1266 - Data Files view: manage input and output directories
# Check data files arrangement between Input / Output directories

import os
import shutil

def main():
    source(findFile("scripts", "common.py"))
    global_start()


def cleanup():
    global_cleanup()


def browseFile(symbol, filename, click_browse=True, save=False):
    if click_browse:
        clickButton(waitForObject(symbol))
    file_path = filename if os.path.isabs(filename) else os.path.join(casedir(), filename)
    setParameterValue(":fileNameEdit_QLineEdit", file_path)
    if save:
        clickButton(waitForObject(":QFileDialog.Save_QPushButton"))
        clickButton(waitForObject(":Select_File_Yes_QPushButton"))
    else:
        clickButton(waitForObject(":QFileDialog.Open_QPushButton"))


def runCase():
    #--------------------------------------------------------------------------
    aaa_path = os.path.join(casedir(), 'aaa.file')
    bbb_path = os.path.join(casedir(), 'bbb.file')
    ccc_path = os.path.join(casedir(), 'ccc.file')
    tmp_dir = os.path.join(testdir(), 'output')

    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add first stage
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))

    #--------------------------------------------------------------------------
    # add LIRE_MAILLAGE command
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")
    browseFile(tr('UNITE', 'QPushButton'), aaa_path)
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # add another LIRE_MAILLAGE command
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh0", "Edit")
    browseFile(tr('UNITE', 'QPushButton'), bbb_path)
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # select CurrentCase in Data Settings
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    # check that file items are shown as children of 'Stage_1' item
    test.vp("VP1")

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # set input dir to the folder with files
    setParameterValue(":dirs_panel_in_dir_QLineEdit", casedir())

    # press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    # check that both commands 'mesh' and 'mesh0' are shown as children of 'CurrentCase'/'Input directory' item
    test.vp("VP2")

    #--------------------------------------------------------------------------
    # edit 'aaa' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory.aaa\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    browseFile(":Filename.Filename_QToolButton", ccc_path)
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, False)
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare((waitForObjectExists(":Mode_QComboBox").currentText), 'in')
    clickButton(waitForObject(":OK_QPushButton"))

    # check that file item 'aaa' has been replaced by 'ccc'
    test.vp("VP3")

    #--------------------------------------------------------------------------
    # edit 'bbb' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory.bbb\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    browseFile(":Filename.Filename_QToolButton", aaa_path)
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, False)
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'in')
    clickButton(waitForObject(":OK_QPushButton"))

    # check that file item 'bbb' has been replaced by 'aaa'
    test.vp("VP4")

    #--------------------------------------------------------------------------
    # add second stage
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))

    #--------------------------------------------------------------------------
    # switch second stage to text mode
    activateOBContextMenuItem("CurrentCase.Stage_2", "Text Mode")

    #--------------------------------------------------------------------------
    # add file to second stage
    #openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_2", 10, 10, 0)
    #activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Add File"))
    clickButton(waitForObject(":DataFilesBase.Add File_QToolButton"))
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, True)
    setParameterValue(":Mode_QComboBox", "out")
    browseFile(":Filename.Filename_QToolButton", aaa_path, save=True)
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    setParameterValue(":Unit.Unit_QLineEdit", "33")
    clickButton(waitForObject(":OK_QPushButton"))
    
    # select CurrentCase in Data Settings
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    # check that now there are two 'aaa' items
    test.vp("VP5")

    #--------------------------------------------------------------------------
    # edit second 'aaa' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory.aaa\\.file_2", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    setParameterValue(":Unit.Unit_QLineEdit", "2")
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'in')
    clickButton(waitForObject(":OK_QPushButton"))

    # check that is not single 'aaa' item
    test.vp("VP6")

    ###################################################################################################
    ###################################################################################################
    ###################################################################################################
    test.warning("Current implementation of UnitPanel does not allow this test to pass completely!!!" )
    test.warning("The end of this test is thus skipped!!!" )
    ###################################################################################################
    ###################################################################################################
    ###################################################################################################
    return


    #--------------------------------------------------------------------------
    # edit 'aaa' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory.aaa\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    browseFile(":Filename.Filename_QToolButton", bbb_path)
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, False)
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'in')
    clickButton(waitForObject(":OK_QPushButton"))

    # check that file item 'aaa' has been replaced by 'bbb'
    test.vp("VP7")
    

    #--------------------------------------------------------------------------
    # switch first stage to text mode
    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    
    # select CurrentCase in Data Settings
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)

    # check that layout is the same as at previous step
    test.vp("VP8")

    #--------------------------------------------------------------------------
    # edit 'bbb' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory.bbb\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    browseFile(":Filename.Filename_QToolButton", aaa_path)
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    setParameterValue(":Unit.Unit_QLineEdit", "44")
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'in')
    clickButton(waitForObject(":OK_QPushButton"))

    # check that file item 'bbb' has been replaced by 'aaa'
    test.vp("VP9")

    #--------------------------------------------------------------------------
    # delete input dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    clickButton(waitForObject(":Delete.No_QPushButton"))

    # check that Input directory has been removed and file items are shown as child of corresponding stages
    test.vp("VP10")

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # set output dir to the folder with files
    setParameterValue(":dirs_panel_out_dir_QLineEdit", casedir())

    # press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    # check that file items are shown as children of 'CurrentCase'/'Output directory' item
    test.vp("VP11")

    #--------------------------------------------------------------------------
    # delete 'ccc' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory.ccc\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    # check that 'ccc' file has been removed
    test.vp("VP12")

    #--------------------------------------------------------------------------
    # delete output dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    # check that Output directory has been removed and file items are removed as well
    test.vp("VP13")

    #--------------------------------------------------------------------------
    # create temporary directory and put files there
    os.makedirs(tmp_dir)
    shutil.copy(aaa_path, tmp_dir)
    shutil.copy(bbb_path, tmp_dir)
    aaa_tmp = os.path.join(tmp_dir, 'aaa.file')
    bbb_tmp = os.path.join(tmp_dir, 'bbb.file')

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # set output dir to the folder with files
    setParameterValue(":dirs_panel_out_dir_QLineEdit", tmp_dir)

    # press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # add file to first stage
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Add File"))
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, True)
    setParameterValue(":Mode_QComboBox", "in")
    browseFile(":Filename.Filename_QToolButton", aaa_tmp)
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    setParameterValue(":Unit.Unit_QLineEdit", "10")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # add file to second stage
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_2", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Add File"))
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, True)
    setParameterValue(":Mode_QComboBox", "inout")
    browseFile(":Filename.Filename_QToolButton", bbb_tmp)
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    setParameterValue(":Unit.Unit_QLineEdit", "11")
    clickButton(waitForObject(":OK_QPushButton"))

    # check that file items are shown as children of 'CurrentCase'/'Output directory' item
    test.vp("VP14")

    #--------------------------------------------------------------------------
    # check 'aaa' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory.aaa\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    test.compare(str(waitForObjectExists(":Unit.Unit_QLineEdit").text), '10')
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'out')
    test.compare(waitForObjectExists(":Exists_QCheckBox").checked, True)
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # check 'bbb' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory.bbb\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    test.compare(str(waitForObjectExists(":Unit.Unit_QLineEdit").text), '11')
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'out')
    test.compare(waitForObjectExists(":Exists_QCheckBox").checked, True)
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # remove output dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Remove Directory"))

    # check that Output directory and file items has been kept in study
    test.vp("VP15")

    #--------------------------------------------------------------------------
    # check 'aaa' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory.aaa\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    test.compare(str(waitForObjectExists(":Unit.Unit_QLineEdit").text), '10')
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'out')
    test.compare(waitForObjectExists(":Exists_QCheckBox").checked, False)
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # check 'bbb' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory.bbb\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    test.compare(str(waitForObjectExists(":Unit.Unit_QLineEdit").text), '11')
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, False)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'out')
    test.compare(waitForObjectExists(":Exists_QCheckBox").checked, False)
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # delete output dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    clickButton(waitForObject(":Delete.No_QPushButton"))

    # check that Output directory has been removed but file items are kept

    test.vp("VP16")

    #--------------------------------------------------------------------------
    # check 'aaa' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.aaa\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    test.compare(str(waitForObjectExists(":Unit.Unit_QLineEdit").text), '10')
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, True)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'in')
    test.compare(waitForObjectExists(":Exists_QCheckBox").checked, False)
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # check 'bbb' file
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_2.bbb\\.file", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))
    test.compare(waitForObjectExists(":Unit.Unit_QLineEdit").enabled, True)
    test.compare(str(waitForObjectExists(":Unit.Unit_QLineEdit").text), '11')
    test.compare(waitForObjectExists(":Mode_QComboBox").enabled, True)
    test.compare(str(waitForObjectExists(":Mode_QComboBox").currentText), 'inout')
    test.compare(waitForObjectExists(":Exists_QCheckBox").checked, False)
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
