def main():
    source(findFile("scripts", "common.py"))
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # check default preferences
    check_vp1()

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # change some preferences in 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_language_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_language_QComboBox", "Français"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox", "Follow style"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox", "North"), 10, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":Preferences_General.Preferences_check_strict_import_mode_QCheckBox"))

    # change some preferences in 'Editor' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Editor")
    clickButton(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_stage_QCheckBox"))
    mouseClick(waitForObject(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit"), 10, 11, 0, Qt.LeftButton)
    setInputFieldValue(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit", "/bin/emacs")

    # press 'Cancel' button
    clickButton(waitForObject(":Preferences.Dialog_cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # check default preferences
    check_vp1()

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # change some preferences in 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    # - set language to French
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_language_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_language_QComboBox", "Français"), 10, 10, 0, Qt.LeftButton)
    # - change toolbar style
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox", "Follow style"), 10, 10, 0, Qt.LeftButton)
    # - change workspace's tab position
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox", "North"), 10, 10, 0, Qt.LeftButton)
    # switch OFF strict import mode
    clickButton(waitForObject(":Preferences_General.Preferences_check_strict_import_mode_QCheckBox"))

    # change some preferences in 'Editor' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Editor")
    # - use external editor
    clickButton(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_stage_QCheckBox"))
    # - change external editor
    mouseClick(waitForObject(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit"), 10, 11, 0, Qt.LeftButton)
    setInputFieldValue(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit", "/bin/emacs")

    # press 'OK' button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    # click 'OK' in confirmation dialog
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # check changed preferences
    check_vp2()

    #--------------------------------------------------------------------------
    # check that language is still English
    test.compare(str(waitForObjectExists(":&File_QAction").text), "&File", "Check English language")

    # check that toolbar style is changed
    test.compare(waitForObjectExists(":AsterStudy *.StandardToolbar_QToolBar").toolButtonStyle, Qt.ToolButtonFollowStyle)

    # check that workspace tab position is changed
    test.compare(waitForObjectExists(":AsterStudy *.Case View_QTabWidget").tabPosition, QTabWidget.North)

    # check that strict import mode is switched OFF
    # - switch to 'Case' view
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    # - import stage
    comm_file = os.path.join( casedir(), 'tst_preferences.comm' )
    test.log( "Use '%s' file to create a new stage" % comm_file )
    importStage( comm_file, [":Undefined files.OK_QPushButton"] )
    showOBItem( "CurrentCase", "tst_preferences" )

    #--------------------------------------------------------------------------
    # exit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    # re-start application
    global_start()

    # NOTE: language should be French at this point

    #--------------------------------------------------------------------------
    # create new study
    activateMenuItem("Fichier", "Nouveau")

    # check French language
    test.compare(str(waitForObjectExists(":&Fichier_QAction").text), "&Fichier", "Check French language")

    # check toolbar style
    test.compare(waitForObjectExists(":AsterStudy *.StandardToolbar_QToolBar").toolButtonStyle, Qt.ToolButtonFollowStyle)

    # check workspace tab position
    test.compare(waitForObjectExists(":AsterStudy *.Vue Cas_QTabWidget").tabPosition, QTabWidget.North)

    #--------------------------------------------------------------------------
    # check preferences in French language
    check_vp3()

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("Fichier", "Préférences...")

    # change some preferences in 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Général")
    # - switch language back to Engish
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_language_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_language_QComboBox", "English"), 10, 10, 0, Qt.LeftButton)
    # switch ON strict import mode
    clickButton(waitForObject(":Preferences_General.Preferences_check_strict_import_mode_QCheckBox"))

    # press 'OK' button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    # click 'OK' in confirmation dialog
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # exit application
    activateMenuItem("Fichier", "Quitter")

    #--------------------------------------------------------------------------
    # re-start application
    global_start()

    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # check that language is still English
    test.compare(str(waitForObjectExists(":&File_QAction").text), "&File", "Check English language")

    #--------------------------------------------------------------------------
    # check changed preferences
    check_vp4()

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # change some preferences in 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    # - change toolbar style
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox", "Only display text"), 10, 10, 0, Qt.LeftButton)
    # - change workspace's tab position
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox", "South"), 10, 10, 0, Qt.LeftButton)

    # press 'OK' button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    # check toolbar style
    test.compare(waitForObjectExists(":AsterStudy *.StandardToolbar_QToolBar").toolButtonStyle, Qt.ToolButtonTextOnly)

    # check workspace tab position
    test.compare(waitForObjectExists(":AsterStudy *.Case View_QTabWidget").tabPosition, QTabWidget.South)

    # check that strict import mode is switched ON
    # - switch to 'Case' view
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    # - import stage
    comm_file = os.path.join( casedir(), 'tst_preferences.comm' )
    test.log( "Use '%s' file to create a new stage" % comm_file )
    importStage( comm_file, [":AsterStudy.Yes_QPushButton", ":Undefined files.OK_QPushButton"] )
    showOBItem( "CurrentCase", "tst_preferences" )

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # change some preferences in 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    # - change toolbar style
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox", "Text appears beside icon"), 10, 10, 0, Qt.LeftButton)
    # - change workspace's tab position
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox", "West"), 10, 10, 0, Qt.LeftButton)

    # press 'OK' button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    # check toolbar style
    test.compare(waitForObjectExists(":AsterStudy *.StandardToolbar_QToolBar").toolButtonStyle, Qt.ToolButtonTextBesideIcon)

    # check workspace tab position
    test.compare(waitForObjectExists(":AsterStudy *.Case View_QTabWidget").tabPosition, QTabWidget.West)

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # change some preferences in 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    # - change toolbar style
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox", "Text appears under icon"), 10, 10, 0, Qt.LeftButton)
    # - change workspace's tab position
    mouseClick(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox"), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox", "East"), 10, 10, 0, Qt.LeftButton)

    # press 'OK' button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    # check toolbar style
    test.compare(waitForObjectExists(":AsterStudy *.StandardToolbar_QToolBar").toolButtonStyle, Qt.ToolButtonTextUnderIcon)

    # check workspace tab position
    test.compare(waitForObjectExists(":AsterStudy *.Case View_QTabWidget").tabPosition, QTabWidget.East)

    #--------------------------------------------------------------------------
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # reset to default preferences
    # - press 'Defaults' button
    clickButton(waitForObject(":Preferences.Dialog_defaults_QPushButton"))
    # - confirm by pressing 'Yes' button
    clickButton(waitForObject(":AsterStudy.Yes_QPushButton"))
    # - close dialog by pressing 'OK' button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # check that preferences were reset to defaults
    check_vp1()

    #--------------------------------------------------------------------------
    # exit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

def check_vp1():
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # check 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_language_QComboBox").currentText), 'English')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_code_aster_version_QComboBox").currentText), 'Use default')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox").currentText), 'Only display icon')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox").currentText), 'West')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_initial_view_QComboBox").currentText), 'History view')
    test.compare(waitForObject(":Preferences_General.Preferences_check_strict_import_mode_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_General.Preferences_spinbox_nblines_limit_QSpinBox").value, 3000)
    test.compare(waitForObject(":Preferences_General.Preferences_check_disable_undo_redo_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox").checked, True)
    # - Documentation group
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_ExternalBrowser_application_QLineEdit").text), '/usr/bin/firefox')
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_doc_base_url_QLineEdit").text), 'http://code-aster.org/doc/')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_doc_language_QComboBox").currentText), 'Use application language')

    # check 'Interface' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    # - Data Settings panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_categories_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_comments_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_expand_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_hide_search_QCheckBox").checked, False)
    # - Data Files panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_stages_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_data_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_related_concepts_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_join_similar_files_QCheckBox").checked, True)
    # - Parameters panel group
    test.compare(str(waitForObject(":Preferences_Interface.Preferences_combo_content_mode_QComboBox").currentText), "None")
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_selector_value_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_selector_values_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_in_selectors_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_external_list_QCheckBox").checked, False)
    # - Dashboard group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_reuse_buttons_QCheckBox").checked, False)
    # - Other settings
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_edit_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_readonly_banner_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_allow_delete_cases_QCheckBox").checked, False)

    # check 'Editor' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Editor")
    test.compare(str(waitForObject(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit").text), "/usr/bin/gedit")
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_stage_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_data_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_msg_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_spinbox_file_size_limit_QSpinBox").value, 256)
    # (can't check 'Font' settings here)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_highlightcurrentline_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_textwrapping_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_centercursoronscroll_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_linenumberarea_QCheckBox").checked, True)
    test.compare(str(waitForObject(":Preferences_group_python_editor.Preferences_combo_PyEditor_completionpolicy_QComboBox").currentText), 'Always')
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_tabspacevisible_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_tabsize_QSpinBox").value, 4)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_verticaledge_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_numbercolumns_QSpinBox").value, 90)

    # check 'Confirmations' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Confirmations")
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_undefined_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_case_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_stages_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_convert_invalid_graphic_stage_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_close_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_abort_QCheckBox").checked, True)

    # press 'Cancel' button
    clickButton(waitForObject(":Preferences.Dialog_cancel_QPushButton"))

def check_vp2():
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # check 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_language_QComboBox").currentText), 'Français')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_code_aster_version_QComboBox").currentText), 'Use default')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox").currentText), 'Follow style')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox").currentText), 'North')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_initial_view_QComboBox").currentText), 'History view')
    test.compare(waitForObject(":Preferences_General.Preferences_check_strict_import_mode_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_General.Preferences_spinbox_nblines_limit_QSpinBox").value, 3000)
    test.compare(waitForObject(":Preferences_General.Preferences_check_disable_undo_redo_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox").checked, True)
    # - Documentation group
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_ExternalBrowser_application_QLineEdit").text), '/usr/bin/firefox')
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_doc_base_url_QLineEdit").text), 'http://code-aster.org/doc/')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_doc_language_QComboBox").currentText), 'Use application language')

    # check 'Interface' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    # - Data Settings panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_categories_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_comments_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_expand_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_hide_search_QCheckBox").checked, False)
    # - Data Files panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_stages_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_data_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_related_concepts_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_join_similar_files_QCheckBox").checked, True)
    # - Parameters panel group
    test.compare(str(waitForObject(":Preferences_Interface.Preferences_combo_content_mode_QComboBox").currentText), "None")
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_selector_value_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_selector_values_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_in_selectors_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_external_list_QCheckBox").checked, False)
    # - Dashboard group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_reuse_buttons_QCheckBox").checked, False)
    # - Other settings
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_edit_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_readonly_banner_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_allow_delete_cases_QCheckBox").checked, False)

    # check 'Editor' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Editor")
    test.compare(str(waitForObject(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit").text), "/bin/emacs")
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_stage_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_data_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_msg_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_spinbox_file_size_limit_QSpinBox").value, 256)
    # can't check 'Font' settings here
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_highlightcurrentline_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_textwrapping_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_centercursoronscroll_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_linenumberarea_QCheckBox").checked, True)
    test.compare(str(waitForObject(":Preferences_group_python_editor.Preferences_combo_PyEditor_completionpolicy_QComboBox").currentText), 'Always')
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_tabspacevisible_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_tabsize_QSpinBox").value, 4)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_verticaledge_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_numbercolumns_QSpinBox").value, 90)

    # check 'Confirmations' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Confirmations")
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_undefined_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_case_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_stages_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_convert_invalid_graphic_stage_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_close_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_abort_QCheckBox").checked, True)

    # press 'Cancel' button
    clickButton(waitForObject(":Preferences.Dialog_cancel_QPushButton"))

def check_vp3():
    # open 'Preferences' dialog
    activateMenuItem("Fichier", "Préférences...")

    # check 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Général")
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_language_QComboBox").currentText), 'Français')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_code_aster_version_QComboBox").currentText), 'Utiliser défaut')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox").currentText), 'Suivre le style')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox").currentText), 'Nord')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_initial_view_QComboBox").currentText), 'Vue Historique')
    test.compare(waitForObject(":Preferences_General.Preferences_check_strict_import_mode_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_General.Preferences_spinbox_nblines_limit_QSpinBox").value, 3000)
    test.compare(waitForObject(":Preferences_General.Preferences_check_disable_undo_redo_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox").checked, True)
    # - Documentation group
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_ExternalBrowser_application_QLineEdit").text), '/usr/bin/firefox')
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_doc_base_url_QLineEdit").text), 'http://code-aster.org/doc/')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_doc_language_QComboBox").currentText), 'Use application language')

    # check 'Interface' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    # - Data Settings panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_categories_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_comments_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_expand_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_hide_search_QCheckBox").checked, False)
    # - Data Files panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_stages_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_data_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_related_concepts_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_join_similar_files_QCheckBox").checked, True)
    # - Parameters panel group
    test.compare(str(waitForObject(":Preferences_Interface.Preferences_combo_content_mode_QComboBox").currentText), "Aucun")
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_selector_value_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_selector_values_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_in_selectors_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_external_list_QCheckBox").checked, False)
    # - Dashboard group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_reuse_buttons_QCheckBox").checked, False)
    # - Other settings
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_edit_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_readonly_banner_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_allow_delete_cases_QCheckBox").checked, False)

    # check 'Editor' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Editeur")
    test.compare(str(waitForObject(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit").text), "/bin/emacs")
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_stage_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_data_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_msg_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_spinbox_file_size_limit_QSpinBox").value, 256)
    # can't check 'Font' settings here
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_highlightcurrentline_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_textwrapping_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_centercursoronscroll_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_linenumberarea_QCheckBox").checked, True)
    test.compare(str(waitForObject(":Preferences_group_python_editor.Preferences_combo_PyEditor_completionpolicy_QComboBox").currentText), 'Toujours')
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_tabspacevisible_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_tabsize_QSpinBox").value, 4)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_verticaledge_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_numbercolumns_QSpinBox").value, 90)

    # check 'Confirmations' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Confirmations")
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_undefined_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_case_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_stages_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_convert_invalid_graphic_stage_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_close_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_abort_QCheckBox").checked, True)

    # press 'Cancel' button
    clickButton(waitForObject(":Preferences.Dialog_cancel_QPushButton"))

def check_vp4():
    # open 'Preferences' dialog
    activateMenuItem("File", "Preferences...")

    # check 'General' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "General")
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_language_QComboBox").currentText), 'English')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_code_aster_version_QComboBox").currentText), 'Use default')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_toolbar_button_style_QComboBox").currentText), 'Follow style')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_tab_position_QComboBox").currentText), 'North')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_workspace_initial_view_QComboBox").currentText), 'History view')
    test.compare(waitForObject(":Preferences_General.Preferences_check_strict_import_mode_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_General.Preferences_spinbox_nblines_limit_QSpinBox").value, 3000)
    test.compare(waitForObject(":Preferences_General.Preferences_check_disable_undo_redo_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_General.Preferences_check_use_business_translations_QCheckBox").checked, True)
    # - Documentation group
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_ExternalBrowser_application_QLineEdit").text), '/usr/bin/firefox')
    test.compare(str(waitForObject(":Preferences_General.Preferences_edit_doc_base_url_QLineEdit").text), 'http://code-aster.org/doc/')
    test.compare(str(waitForObject(":Preferences_General.Preferences_combo_doc_language_QComboBox").currentText), 'Use application language')

    # check 'Interface' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    # - Data Settings panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_categories_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_comments_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_expand_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_hide_search_QCheckBox").checked, False)
    # - Data Files panel group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_stages_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_data_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_related_concepts_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_join_similar_files_QCheckBox").checked, True)
    # - Parameters panel group
    test.compare(str(waitForObject(":Preferences_Interface.Preferences_combo_content_mode_QComboBox").currentText), "None")
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_selector_value_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_sort_selector_values_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_in_selectors_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_external_list_QCheckBox").checked, False)
    # - Dashboard group
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_reuse_buttons_QCheckBox").checked, False)
    # - Other settings
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_auto_edit_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_show_readonly_banner_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Interface.Preferences_check_allow_delete_cases_QCheckBox").checked, False)

    # check 'Editor' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Editor")
    test.compare(str(waitForObject(":Preferences_Editor.Preferences_edit_external_editor_QLineEdit").text), "/bin/emacs")
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_stage_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_data_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_check_use_external_editor_msg_file_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_Editor.Preferences_spinbox_file_size_limit_QSpinBox").value, 256)
    # can't check 'Font' settings here
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_highlightcurrentline_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_textwrapping_QCheckBox").checked, False)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_centercursoronscroll_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_linenumberarea_QCheckBox").checked, True)
    test.compare(str(waitForObject(":Preferences_group_python_editor.Preferences_combo_PyEditor_completionpolicy_QComboBox").currentText), 'Always')
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_tabspacevisible_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_tabsize_QSpinBox").value, 4)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_check_PyEditor_verticaledge_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_group_python_editor.Preferences_spinbox_PyEditor_numbercolumns_QSpinBox").value, 90)

    # check 'Confirmations' page
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Confirmations")
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_undefined_files_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_case_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_delete_stages_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_convert_invalid_graphic_stage_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_close_QCheckBox").checked, True)
    test.compare(waitForObject(":Preferences_Confirmations.Preferences_check_msgbox_parampanel_abort_QCheckBox").checked, True)

    # press 'Cancel' button
    clickButton(waitForObject(":Preferences.Dialog_cancel_QPushButton"))
