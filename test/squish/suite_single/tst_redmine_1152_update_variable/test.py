#------------------------------------------------------------------------------
# Issue #1152 - Edition panel: management of Python variables (CCTP 2.3.2)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1152

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "Commands" ) )
    activateItem( waitForObjectItem( ":Commands_QMenu", "Create Variable" ) )

    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, False )
    test.compare( waitForObjectExists( ":Apply_QPushButton" ).enabled, False )

    symbol = waitForObject( tr( 'Name', 'QLineEdit' ) )
    test.compare( str( symbol.text ), "" )

    symbol.setText( "vitesse" )
    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, False )
    test.compare( waitForObjectExists( ":Apply_QPushButton" ).enabled, False )

    symbol = waitForObject( tr( 'Expression', 'QLineEdit' ) )
    test.compare( str( symbol.text ), "" )

    symbol.setText( "1.e-5" )
    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, True )
    test.compare( waitForObjectExists( ":Apply_QPushButton" ).enabled, True )

    symbol = waitForObject( tr( 'Value', 'QLineEdit' ) )
    test.compare( str( symbol.text ), "1e-05" )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "Commands" ) )
    activateItem( waitForObjectItem( ":Commands_QMenu", "Create Variable" ) )

    waitForObjectExists( tr( 'Name', 'QLineEdit' ) ).setText( "t_0" )
    waitForObjectExists( tr( 'Expression', 'QLineEdit' ) ).setText( "5.e-2 / (8.0 * vitesse)" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "625.0" )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "vitesse" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    waitForObjectExists( tr( 'Expression', 'QLineEdit' ) ).setText( "1.0/1000" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "0.001" )
    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, True )
    test.compare( waitForObjectExists( ":Apply_QPushButton" ).enabled, True )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "t_0" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( tr( 'Name', 'QLineEdit' ) ).text ), "t_0" )
    test.compare( str( waitForObjectExists( tr( 'Expression', 'QLineEdit' ) ).text ), "5.e-2 / (8.0 * vitesse)" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "6.25" )
    test.compare( waitForObjectExists( ":OK_QPushButton" ).enabled, False )
    test.compare( waitForObjectExists( ":Apply_QPushButton" ).enabled, False )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
