#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForPopupItem("Edit")

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    symbol = tr( 'OBJET', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    #--------------------------------------------------------------------------
    symbol = tr( 'OBJET', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    clickButton( waitForObject( ":0_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'VALE_CALC', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'VALE_CALC', 'QLineEdit' )
    setParameterValue( symbol, str( 2.4722322219972E+05 ) )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 2.4722322219972E+05 ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM', 'QLineEdit' )
    setParameterValue( symbol, 'MATRUPL .ME001     .RESL' )
    test.compare( str( waitForObjectExists( symbol ).text ), 'MATRUPL .ME001     .RESL' )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name, imported ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForPopupItem("Edit")

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    symbol = tr( 'OBJET', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    #--------------------------------------------------------------------------
    symbol = tr( 'OBJET', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    check = not object.exists( ":0_QPushButton" )
    if check:
        test.warning( """Bug #932 - 'TEST_RESU / OBJET' keyword is not restored from text""" )
    else:
        clickButton( waitForObject( ":0_QPushButton" ) )

        #--------------------------------------------------------------------------
        symbol = tr( 'VALE_CALC', 'QRadioButton' )
        test.compare( waitForObjectExists( symbol ).checked, True )

        symbol = tr( 'VALE_CALC', 'QLineEdit' )
        test.compare( str( waitForObjectExists( symbol ).text ), str( 2.4722322219972E+05 ) )

        #--------------------------------------------------------------------------
        symbol = tr( 'NOM', 'QLineEdit' )
        test.compare( str( waitForObjectExists( symbol ).text ), 'MATRUPL .ME001     .RESL' )

        #--------------------------------------------------------------------------
        clickButton( waitForObject( ":Cancel_QPushButton" ) )
        pass

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )
    #--------------------------------------------------------------------------
    category = "Other"
    type = "TEST_RESU"
    name = "TEST_RESU"
    var = "_"

    #--------------------------------------------------------------------------
    # check the last TEST_RESU
    item = selectItem( "zzzz289f:2", "%s:-5" % category, "%s:44" % name )
    checkContents( item, var, imported = True )

    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    popupItem("Graphical Mode")
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    item = selectItem( "zzzz289f:2", "%s:-5" % category, "%s:86" % name )
    checkContents( item, var, imported = True )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    populateContents( category, type, var, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, var, imported = False )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, var, imported = True )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
