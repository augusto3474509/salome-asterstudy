#------------------------------------------------------------------------------
# Issue #1146 - Data Files view: list stages in order of their definition in case

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # [section] Create New study
    # [step] Menu File - New
    waitForActivateMenuItem( "File", "New" )

    #--------------------------------------------------------------------------
    # [section] Create two stages
    # [step] Switch to current case
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    # [step] Create first stage "Stage_1"
    # [step] Select CurrentCase object, invoke Add Stage item from context menu
    activateOBContextMenuItem( "CurrentCase", "Add Stage")
    # [step] Create second stage "Stage_2"
    # [step] Select CurrentCase object, invoke Add Stage item from context menu
    activateOBContextMenuItem( "CurrentCase", "Add Stage")
    # [step] Create third stage "Stage_3"
    # [step] Select CurrentCase object, invoke Add Stage item from context menu
    activateOBContextMenuItem( "CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # [section] Rename first stage to "bbb"
    # [step] Select CurrentCase/Stage_1 object, invoke Rename item from context menu
    activateOBContextMenuItem( "CurrentCase.Stage_1", "Rename" )
    # [step] Type "bbb" and press <Enter>
    setParameterValue( ":_QExpandingLineEdit", "bbb" )
    type( waitForObject( ":_QExpandingLineEdit" ), "<Return>" )

    #--------------------------------------------------------------------------
    # [section] Rename second stage to "aaa"
    # [step] Select CurrentCase/Stage_1 object, invoke Rename item from context menu
    activateOBContextMenuItem( "CurrentCase.Stage_2", "Rename" )
    # [step] Type "aaa" and press <Enter>
    setParameterValue( ":_QExpandingLineEdit_2", "aaa" )
    type( waitForObject( ":_QExpandingLineEdit_2" ), "<Return>" )

    #--------------------------------------------------------------------------
    # [section] Check order of stages in Data Files panel
    # [step] Switch to CurrentCase in Data Settings
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    # [step] Check that order of stages in Data Files panel is "bbb", "aaa", "Stage_3"
    waitForObjectItem(":DataFiles_QTreeView", "bbb")
    test.compare(waitForObjectExists(":DataFilesView.bbb_QModelIndex").row, 1)
    waitForObjectItem(":DataFiles_QTreeView", "aaa")
    test.compare(waitForObjectExists(":DataFilesView.aaa_QModelIndex").row, 2)
    waitForObjectItem(":DataFiles_QTreeView", "Stage\_3")
    test.compare(waitForObjectExists(":DataFilesView.Stage_3_QModelIndex").row, 3)

    #--------------------------------------------------------------------------
    # [section] Check that order of stages is permanent
    # [step] Click "File" column in the header of Data Files panel
    mouseClick(waitForObject(":Filename_HeaderViewItem"), 37, 8, 0, Qt.LeftButton)
    # [step] Check that order of stages in Data Files panel is "bbb", "aaa", "Stage_3"
    waitForObjectItem(":DataFiles_QTreeView", "bbb")
    test.compare(waitForObjectExists(":DataFilesView.bbb_QModelIndex").row, 1)
    waitForObjectItem(":DataFiles_QTreeView", "aaa")
    test.compare(waitForObjectExists(":DataFilesView.aaa_QModelIndex").row, 2)
    waitForObjectItem(":DataFiles_QTreeView", "Stage\_3")
    test.compare(waitForObjectExists(":DataFilesView.Stage_3_QModelIndex").row, 3)
    # [step] Click "File" column in the header of Data Files panel
    mouseClick(waitForObject(":Filename_HeaderViewItem"), 37, 8, 0, Qt.LeftButton)
    # [step] Check that order of stages in Data Files panel is "bbb", "aaa", "Stage_3"
    waitForObjectItem(":DataFiles_QTreeView", "bbb")
    test.compare(waitForObjectExists(":DataFilesView.bbb_QModelIndex").row, 1)
    waitForObjectItem(":DataFiles_QTreeView", "aaa")
    test.compare(waitForObjectExists(":DataFilesView.aaa_QModelIndex").row, 2)
    waitForObjectItem(":DataFiles_QTreeView", "Stage\_3")
    test.compare(waitForObjectExists(":DataFilesView.Stage_3_QModelIndex").row, 3)

    #--------------------------------------------------------------------------
    # [section] Switch ON "Sort stages in Data Files panel" Preferences option
    # [step] Menu Edit - Preferences
    activateMenuItem( "File", "Preferences..." )
    # [step] Switch ON "Sort stages in Data Files panel" option
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_sort_stages_QCheckBox"))
    # [step] Click OK in Preferences dialog
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    # [step] Click "File" column in the header of Data Files panel
    mouseClick(waitForObject(":Filename_HeaderViewItem"), 37, 8, 0, Qt.LeftButton)
    # [step] Check that order of stages in Data Files panel is "Stage_3", "aaa", "bbb"
    waitForObjectItem(":DataFiles_QTreeView", "bbb")
    test.compare(waitForObjectExists(":DataFilesView.bbb_QModelIndex").row, 2)
    waitForObjectItem(":DataFiles_QTreeView", "aaa")
    test.compare(waitForObjectExists(":DataFilesView.aaa_QModelIndex").row, 1)
    waitForObjectItem(":DataFiles_QTreeView", "Stage\_3")
    test.compare(waitForObjectExists(":DataFilesView.Stage_3_QModelIndex").row, 0)
    # [step] Click "File" column in the header of Data Files panel
    mouseClick(waitForObject(":Filename_HeaderViewItem"), 37, 8, 0, Qt.LeftButton)
    # [step] Check that order of stages in Data Files panel is "bbb", "aaa", "Stage_3"
    waitForObjectItem(":DataFiles_QTreeView", "bbb")
    test.compare(waitForObjectExists(":DataFilesView.bbb_QModelIndex").row, 1)
    waitForObjectItem(":DataFiles_QTreeView", "aaa")
    test.compare(waitForObjectExists(":DataFilesView.aaa_QModelIndex").row, 2)
    waitForObjectItem(":DataFiles_QTreeView", "Stage\_3")
    test.compare(waitForObjectExists(":DataFilesView.Stage_3_QModelIndex").row, 3)
    # [step] Click "File" column in the header of Data Files panel
    mouseClick(waitForObject(":Filename_HeaderViewItem"), 37, 8, 0, Qt.LeftButton)
    # [step] Check that order of stages in Data Files panel is "Stage_3", "aaa", "bbb"
    waitForObjectItem(":DataFiles_QTreeView", "bbb")
    test.compare(waitForObjectExists(":DataFilesView.bbb_QModelIndex").row, 2)
    waitForObjectItem(":DataFiles_QTreeView", "aaa")
    test.compare(waitForObjectExists(":DataFilesView.aaa_QModelIndex").row, 1)
    waitForObjectItem(":DataFiles_QTreeView", "Stage\_3")
    test.compare(waitForObjectExists(":DataFilesView.Stage_3_QModelIndex").row, 0)

    #--------------------------------------------------------------------------
    # [section] Switch OFF "Sort stages in Data Files panel" Preferences option
    # [step] Menu Edit - Preferences
    activateMenuItem( "File", "Preferences..." )
    # [step] Switch ON "Sort stages in Data Files panel" option
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_sort_stages_QCheckBox"))
    # [step] Click OK in Preferences dialog
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    # [step] Check that order of stages in Data Files panel is "bbb", "aaa", "Stage_3"
    waitForObjectItem(":DataFiles_QTreeView", "bbb")
    test.compare(waitForObjectExists(":DataFilesView.bbb_QModelIndex").row, 1)
    waitForObjectItem(":DataFiles_QTreeView", "aaa")
    test.compare(waitForObjectExists(":DataFilesView.aaa_QModelIndex").row, 2)
    waitForObjectItem(":DataFiles_QTreeView", "Stage\_3")
    test.compare(waitForObjectExists(":DataFilesView.Stage_3_QModelIndex").row, 3)

    #--------------------------------------------------------------------------
    # [section] Quit application
    # [step] Menu File - Exit
    activateMenuItem( "File", "Exit" )
    # [step] Press "Discard" button in confirmation dialog
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
