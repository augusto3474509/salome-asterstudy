#------------------------------------------------------------------------------
# Issue #1698 - Generic "Open With" feature

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    file_name = create_dummy_file('aaa.txt')
    executable_1 = os.path.join(casedir(), 'program1') 
    executable_2 = os.path.join(casedir(), 'program2')

    #--------------------------------------------------------------------------
    # create empty stage in current case and check that undo list contains one item
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add LIRE_MAILLAGE command 
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    setParameterValue( ":fileNameEdit_QLineEdit", file_name )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    # activate 'Open With/Choose Program...' popup menu item
    selectObjectBrowserItem("CurrentCase")
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.aaa\\.txt", 10, 10, 0)
    waitForPopupItem("Open With", "Choose Program...")
    # choose program1
    setParameterValue(":fileNameEdit_QLineEdit", executable_1)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    #--------------------------------------------------------------------------
    # activate 'Open With/program1' popup menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.aaa\\.txt", 10, 10, 0)
    waitForPopupItem("Open With", "program1")

    #--------------------------------------------------------------------------
    # activate 'Open With/Choose Program...' popup menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.aaa\\.txt", 10, 10, 0)
    waitForPopupItem("Open With", "Choose Program...")
    # choose program2
    setParameterValue(":fileNameEdit_QLineEdit", executable_2)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    #--------------------------------------------------------------------------
    # activate 'Open With/program2' popup menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.aaa\\.txt", 10, 10, 0)
    waitForPopupItem("Open With", "program2")

    #--------------------------------------------------------------------------
    # activate 'Open With/program1' popup menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.aaa\\.txt", 10, 10, 0)
    waitForPopupItem("Open With", "program1")

    #--------------------------------------------------------------------------
    # activate 'Open With/program2' popup menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.aaa\\.txt", 10, 10, 0)
    waitForPopupItem("Open With", "program2")

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
