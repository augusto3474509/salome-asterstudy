#------------------------------------------------------------------------------
# Issue #1153 - Edition panel: management of macro keywords (CCTP 2.3.3)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1153

#------------------------------------------------------------------------------
def handleMessageBox(messageBox):
    #--------------------------------------------------------------------------
    test.log("MessageBox opened: '%s' - '%s'" % (messageBox.windowTitle, messageBox.text))
    messageBox.button( messageBox.Yes ).click()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    installEventHandler("MessageBoxOpened", "handleMessageBox")
    activateMenuItem( "Operations", "Add Stage" )

    #--------------------------------------------------------------------------
    snippet = \
"""
mesh = LIRE_MAILLAGE(UNITE=20)
"""
    setClipboardText( snippet )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )

    #--------------------------------------------------------------------------
    isValid( True, "Stage_1", "Mesh", "mesh:6" )
    isValid( True, "Stage_1" )

    #--------------------------------------------------------------------------
    addCommandByDialog( "MACR_ADAP_MAIL", "Post Processing" )

    openContextMenu( selectItem( "Stage_1", "Post Processing", "MACR_ADAP_MAIL" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( waitForObjectExists( ":Name_QLineEdit" ).enabled, False )

    setParameterValue( tr( 'MAILLAGE_N', 'QComboBox' ), "mesh" )
    setParameterValue( tr( 'ADAPTATION', 'QComboBox' ), "RAFFINEMENT_UNIFORME" )

#    test.verify( object.exists( trex( 'MAILLAGE_NP1-.*command.*', 'QToolButton' ) ) )
#    clickButton( waitForObject( trex( 'MAILLAGE_NP1-.*', 'QToolButton' ) ) )
#    test.verify( object.exists( trex( 'MAILLAGE_NP1-.*macro.*', 'QToolButton' ) ) )

    setParameterValue( tr( 'MAILLAGE_NP1', 'QLineEdit' ), "meshout" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    isValid( True, "Stage_1", "Post Processing", "MACR_ADAP_MAIL:7" )
    isValid( True, "Stage_1", "Post Processing", "MACR_ADAP_MAIL:7", "meshout:8" )
    isValid( True, "Stage_1" )

    #--------------------------------------------------------------------------
    addCommandByDialog( "DEFI_GROUP", "Mesh" )

    openContextMenu( selectItem( "Stage_1", "Mesh", "DEFI_GROUP" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( ":Name_QLineEdit", "mesh2" )

    waitForObject( tr( 'DETR_GROUP_MA', 'QCheckBox' ) ).setChecked( True )

    clickButton(waitForObject(":DETR_GROUP_MA_add_QToolButton"))
    clickButton(waitForObject(":0_ParameterButton"))
    clickButton(waitForObject(":NOM_add_QToolButton"))
    type(waitForObject(tr('0', 'QLineEdit')), "group")
    clickButton(waitForObject(":OK_QPushButton"))

    waitForObject( tr( 'MAILLAGE', 'QRadioButton' ) ).setChecked( True )
    comboBox = waitForObject( tr( 'MAILLAGE', 'QComboBox' ) )

    test.verify( comboBox.findText( "meshout" ) != -1 )
    test.verify( comboBox.findText( "mesh" ) != -1 )

    setParameterValue( tr( 'MAILLAGE', 'QComboBox' ), "meshout" )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    
    # WARNING! after merge from 'edf/default' 29/05/2018, name entered in the Edit dialog is not taken into account!
    # as soon as (and if) this is fixed, the code snippet below has to be revised ('reuse' keyword will be not needed)!
    isValid( True, "Stage_1", "Mesh:-3", "meshout:9" )
    test.warning("Name entered in the Edit dialog is not taken into account!")

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Post Processing", "MACR_ADAP_MAIL:7" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObject( tr( 'MAILLAGE_NP1', 'QLineEdit' ) ).text ), "meshout" )
#    test.verify( object.exists( trex( 'MAILLAGE_NP1-.*macro.*', 'QToolButton' ) ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_1" )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )

    snippet = \
"""
mesh = LIRE_MAILLAGE(UNITE=20)

MACR_ADAP_MAIL(ADAPTATION='RAFFINEMENT_UNIFORME',
               MAILLAGE_N=mesh,
               MAILLAGE_NP1=CO('meshout'))

meshout = DEFI_GROUP(reuse=meshout,
                     DETR_GROUP_MA=_F(NOM=('group', )),
                     MAILLAGE=meshout)
"""
    actual = str( waitForObject( text_editor ).toPlainText() )
    test.verify( check_text_eq( snippet, actual ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )

    #--------------------------------------------------------------------------
    pass
