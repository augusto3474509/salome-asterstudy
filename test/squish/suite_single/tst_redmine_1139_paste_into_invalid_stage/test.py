#------------------------------------------------------------------------------
# Issue #1139 - Data Settings view: copy/paste commands (CCTP 2.2.1.4)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1139

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    addCommandByDialog( 'AFFE_CARA_ELEM', 'Model Definition' )

    checkValidity( False, "CurrentCase" )

    #--------------------------------------------------------------------------
    text = \
"""
model = MODI_MODELE()
"""
    setClipboardText(text)

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )

    checkValidity( False, "CurrentCase", "Stage_1", "Model Definition", "model" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
