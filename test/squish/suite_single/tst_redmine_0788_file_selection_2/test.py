#------------------------------------------------------------------------------
# Checks that UNITE combobox is cleared after discarding of changes by Close in panel.
# Checks that UNITE value is saved after accepting of changes by OK in panel.
#------------------------------------------------------------------------------
def main():
    source( findFile( "scripts", "common.py" ) )
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "PRE_GMSH", "Other" )
    checkOBItem( "CurrentCase", "Stage_1", "Other", "PRE_GMSH" )

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1.Other.PRE\\_GMSH", 50, 9, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    symbol = tr( 'UNITE_GMSH', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    def_file_path = os.path.join( casedir(), 'default.file' )
    setParameterValue(":fileNameEdit_QLineEdit", def_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(def_file_path))

    clickButton( waitForObject( ":Close_QPushButton" ) )
    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #--------------------------------------------------------------------------

    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1.Other.PRE\\_GMSH", 50, 9, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.verify(waitForObjectExists( symbol ).enabled)

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare(waitForObjectExists(symbol).currentText, "")
    test.compare(waitForObjectExists(symbol).count, 0)

    symbol = tr( 'UNITE_GMSH', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    def_file_path = os.path.join( casedir(), 'default.file' )
    setParameterValue(":fileNameEdit_QLineEdit", def_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(def_file_path))

    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), "")

    symbol = tr( 'UNITE_MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    out_file_path = os.path.join( testdir(), 'output.file' )
    setParameterValue(":fileNameEdit_QLineEdit", out_file_path)
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(out_file_path))

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1.Other.PRE\\_GMSH", 50, 9, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.verify(waitForObjectExists( symbol ).enabled)

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare(waitForObjectExists(symbol).count, 2)
    test.compare(waitForObjectExists(symbol).currentIndex, 0)
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(def_file_path))

    #--------------------------------------------------------------------------
    anot_file_path = os.path.join( casedir(), 'another.file' )

    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    test.verify(waitForObjectExists( symbol ).enabled)

    symbol = tr( 'UNITE_MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    setParameterValue(":fileNameEdit_QLineEdit", anot_file_path)
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(anot_file_path))

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(def_file_path))

    clickButton( waitForObject( ":Close_QPushButton" ) )
