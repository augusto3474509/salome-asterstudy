#------------------------------------------------------------------------------
# Issue #1152 - Edition panel: management of Python variables (CCTP 2.3.2)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1152

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    addCommandByDialog( "DEFI_CONSTANTE", "Functions and Lists" )
    openContextMenu( selectItem( "Stage_1", "Functions and Lists", "DEFI_CONSTANTE" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( ":Name_QLineEdit", "c1" )

    clickButton( waitForObject( trex( 'VALE.*', 'QToolButton' ) ) )
    comboBox = waitForObject( tr( 'VALE', 'QComboBox' ) )

    # test.verify( comboBox.findText( '<no object selected>' ) )
    test.verify( comboBox.findText( '<Add variable...>' ) != -1 )
    test.compare( 2, comboBox.count )

    #--------------------------------------------------------------------------
    mouseClick( comboBox, 10, 10, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( comboBox, "<Add variable\\.\\.\\.>" ), 57, 11, 0, Qt.LeftButton )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "vitesse" )
    setParameterValue( tr( 'Expression', 'QLineEdit' ), "1.e-5" )
    test.compare( str( waitForObject( tr( 'Value', 'QLineEdit' ) ).text ), "1e-05" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    mouseClick( comboBox, 10, 10, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( comboBox, "<Add variable\\.\\.\\.>" ), 57, 11, 0, Qt.LeftButton )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "t_0" )
    setParameterValue( tr( 'Expression', 'QLineEdit' ), "5.e-2 / ( 8.0 * vitesse )" )
    test.compare( str( waitForObject( tr( 'Value', 'QLineEdit' ) ).text ), "625.0" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    # test.verify( comboBox.findText( '<no object selected>' ) )
    test.verify( comboBox.findText( '<Add variable...>' ) != -1 )
    test.verify( comboBox.findText( 'vitesse' ) != -1 )
    test.verify( comboBox.findText( 't_0' ) != -1 )
    test.compare( 4, comboBox.count )

    setParameterValue( tr( 'VALE', 'QComboBox' ), "t_0" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    isValid( True, "Stage_1", "Functions and Lists", "c1:4" )
    isValid( True, "Stage_1", "Variables", "vitesse:5" )
    isValid( True, "Stage_1", "Variables", "t_0:6" )
    isValid( True, "Stage_1" )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_1" )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )

    snippet = \
"""
vitesse = 1.e-5

t_0 = 5.e-2 / (8.0 * vitesse)

c1 = DEFI_CONSTANTE(VALE=t_0)
"""
    actual = str( waitForObject( text_editor ).toPlainText() )
    test.verify( check_text_eq( snippet, actual ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
