#------------------------------------------------------------------------------
# Issue #1152 - Edition panel: management of Python variables (CCTP 2.3.2)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1152

#------------------------------------------------------------------------------
def handleMessageBox( messageBox ):
    #--------------------------------------------------------------------------
    test.log( "MessageBox opened: '%s' - '%s'" % ( messageBox.windowTitle, messageBox.text ) )
    messageBox.button( messageBox.Yes ).click()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    installEventHandler( "MessageBoxOpened", "handleMessageBox" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    snippet = \
"""
a = 1

a = 2
"""
    setClipboardText( snippet )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )

    isValid( False, "Stage_1" ) # Name duplication
    isValid( True, "Stage_1", "Variables", "a:6" )
    isValid( False, "Stage_1", "Variables", "a:7" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )

    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "Commands" ) )
    activateItem( waitForObjectItem( ":Commands_QMenu", "Create Variable" ) )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "b" )
    setParameterValue( tr( 'Expression', 'QLineEdit' ), "2 * a" )
    test.compare( str( waitForObject( tr( 'Value', 'QLineEdit' ) ).text ), "4" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    isValid( False, "Stage_2" )
    isValid( False, "Stage_2", "Variables", "b:10" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "a:7" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    checkItemNotExists( "Stage_1", "Variables", "a:7" )

    isValid( True, "Stage_1" ) # No name duplication
    isValid( True, "Stage_1", "Variables", "a:6" )

    isValid( True, "Stage_2" )
    isValid( True, "Stage_2", "Variables", "b:10" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_2", "Variables", "b:10" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObject( tr( 'Value', 'QLineEdit' ) ).text ), "2" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
