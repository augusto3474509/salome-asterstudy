#------------------------------------------------------------------------------
# Issue #1139 - Data Settings view: copy/paste commands (CCTP 2.2.1.4)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1139

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    text = \
"""
# this is variable a
a = 10

# this is command DEBUT
DEBUT()

# this is trailing comment
"""
    setClipboardText(text)
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )
    waitForObject( ":_QTreeWidget" ).expandAll()
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "this is variable a" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Copy"))
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Paste"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "this is variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "this is command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Deprecated", "this is trailing comment" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "this is variable a" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Duplicate"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "this is variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "this is command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Deprecated", "this is trailing comment" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "a" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Copy"))
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Paste"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "this is variable a" )
    selectItem( "Stage_1", "Variables", "a:28" )
    selectItem( "Stage_1", "Variables", "this is trailing comment" )
    selectItem( "Stage_1", "Variables", "a:32" )
    selectItem( "Stage_1", "Deprecated", "this is command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "a:32" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Duplicate"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "this is variable a" )
    selectItem( "Stage_1", "Variables", "a:36" )
    selectItem( "Stage_1", "Variables", "this is trailing comment:37" )
    selectItem( "Stage_1", "Variables", "a:38" )
    selectItem( "Stage_1", "Variables", "this is trailing comment:41" )
    selectItem( "Stage_1", "Variables", "a:42" )
    selectItem( "Stage_1", "Deprecated", "this is command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
