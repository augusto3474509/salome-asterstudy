#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "szlz108b.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    setParameterValue( symbol, 'X' )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA_FONC', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'NOM_PARA_FONC', 'QComboBox' )
    setParameterValue( symbol, 'EPSI' )

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    setParameterValue( symbol, 'LINEAIRE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    setParameterValue( symbol, 'LINEAIRE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'PARA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '0', 'QLineEdit' )
    setParameterValue( symbol, str( 0.5 ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '1', 'QLineEdit' )
    setParameterValue( symbol, str( 1. ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'TITRE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'TITRE', 'QLineEdit' )
    setParameterValue( symbol, 'NAPPE DE TAHERI' )

    #--------------------------------------------------------------------------
    symbol = tr( 'DEFI_FONCTION', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'DEFI_FONCTION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '0', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    setParameterValue( symbol, 'LINEAIRE' )

    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    setParameterValue( symbol, 'LINEAIRE' )

    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '0', 'QLineEdit' )
    setParameterValue( symbol, str( 0. ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '1', 'QLineEdit' )
    setParameterValue( symbol, str( 25. ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '2', 'QLineEdit' )
    setParameterValue( symbol, str( 10. ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '3', 'QLineEdit' )
    setParameterValue( symbol, str( 525. ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '1', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    setParameterValue( symbol, 'LINEAIRE' )

    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    setParameterValue( symbol, 'LINEAIRE' )

    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '0', 'QLineEdit' )
    setParameterValue( symbol, str( 0. ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '1', 'QLineEdit' )
    setParameterValue( symbol, str( 50. ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '2', 'QLineEdit' )
    setParameterValue( symbol, str( 10. ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '3', 'QLineEdit' )
    setParameterValue( symbol, str( 550. ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'X' )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA_FONC', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'NOM_PARA_FONC', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'EPSI' )
    if waitForObjectExists( symbol ).currentText != 'EPSI':
        pass

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'LINEAIRE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'LINEAIRE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'PARA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 0.5 ) )

    symbol = tr( '1', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 1. ) )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'TITRE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'TITRE', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, 'NAPPE DE TAHERI' )

    #--------------------------------------------------------------------------
    symbol = tr( 'DEFI_FONCTION', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'DEFI_FONCTION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( '0', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'LINEAIRE' )

    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'LINEAIRE' )

    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 0. ) )

    symbol = tr( '1', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 25. ) )

    symbol = tr( '2', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 10. ) )

    symbol = tr( '3', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 525. ) )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( '1', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'LINEAIRE' )

    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'LINEAIRE' )

    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 0. ) )

    symbol = tr( '1', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 50. ) )

    symbol = tr( '2', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 10. ) )

    symbol = tr( '3', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, str( 550. ) )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'szlz108b.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file )

    #--------------------------------------------------------------------------
    category = "Functions and Lists"
    type = "DEFI_NAPPE"
    name = "F_EPSMAX"

    #--------------------------------------------------------------------------
    item = selectItem( "szlz108b", "%s:-2" % category, "%s" % name )
    checkContents( item, name )

    openContextMenu( selectItem( "szlz108b" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    openContextMenu( selectItem( "szlz108b" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    item = selectItem( "szlz108b", "%s:-2" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    name = "F_EPSMA1"
    populateContents( category, type, name, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
