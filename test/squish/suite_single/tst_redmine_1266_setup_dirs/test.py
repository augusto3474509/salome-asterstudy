#------------------------------------------------------------------------------
# Issue #1266 - Data Files view: manage input and output directories
# Check Input / Output directories management

def main():
    source(findFile("scripts", "common.py"))
    global_start()


def cleanup():
    global_cleanup()


def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    activateOBContextMenuItem("CurrentCase", "Set-up Directories")

    #--------------------------------------------------------------------------
    # browse input dir - select '/usr'
    clickButton(waitForObject(":dirs_panel_in_dir_browse_QPushButton"))
    setParameterValue(":fileNameEdit_QLineEdit", '/usr')
    clickButton(waitForObject(":QFileDialog.Choose_QPushButton"))

    # browse output dir - select '/usr'
    clickButton(waitForObject(":dirs_panel_out_dir_browse_QPushButton"))
    setParameterValue(":fileNameEdit_QLineEdit", '/usr')
    clickButton(waitForObject(":QFileDialog.Choose_QPushButton"))

    # check that same dir cannot be chosen as input and output
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # manually enter '/usr/bin' as output dir
    setParameterValue(":dirs_panel_out_dir_QLineEdit", '/usr/bin')

    # check that output dir cannot be subdir of input dir
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # manually enter '/usr' as output dir
    setParameterValue(":dirs_panel_out_dir_QLineEdit", '/usr')
    # manually enter '/usr/bin' as input dir
    setParameterValue(":dirs_panel_in_dir_QLineEdit", '/usr/bin')

    # check that input dir cannot be subdir of output dir
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # manually enter non-existent directory as input dir
    setParameterValue(":dirs_panel_in_dir_QLineEdit", '/tmp/aaa/bbb/ccc')

    # check that non-existent dir cannot be chosen as input dir
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # manually enter '/usr' as input dir
    setParameterValue(":dirs_panel_in_dir_QLineEdit", '/usr')
    # manually enter non-existent directory as output dir
    setParameterValue(":dirs_panel_out_dir_QLineEdit", '/tmp/aaa/bbb/ccc')

    # check that non-existent can be chosen as output dir
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # edit input dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    # check that input dir cannot be parent dir of output dir
    setParameterValue(":fileNameEdit_QLineEdit", '/tmp')
    clickButton(waitForObject(":QFileDialog.Choose_QPushButton"))
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # edit output dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    # check that input dir cannot be parent dir of output dir
    setParameterValue(":fileNameEdit_QLineEdit", '/')
    clickButton(waitForObject(":QFileDialog.Choose_QPushButton"))
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # check that values entered at previous steps are OK
    test.compare(str(waitForObjectExists(":dirs_panel_in_dir_QLineEdit").text), '/usr')
    test.compare(str(waitForObjectExists(":dirs_panel_out_dir_QLineEdit").text), '/tmp/aaa/bbb/ccc')

    # press 'Cancel' button
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # check that values entered at previous steps are OK
    test.compare(str(waitForObjectExists(":dirs_panel_in_dir_QLineEdit").text), '/usr')
    test.compare(str(waitForObjectExists(":dirs_panel_out_dir_QLineEdit").text), '/tmp/aaa/bbb/ccc')

    # manually enter '/usr' as output dir
    setParameterValue(":dirs_panel_in_dir_QLineEdit", '/tmp')
    # manually enter '/usr/bin' as input dir
    setParameterValue(":dirs_panel_out_dir_QLineEdit", '/usr')

    # press 'OK' button
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # check that values entered at previous steps are OK
    test.compare(str(waitForObjectExists(":dirs_panel_in_dir_QLineEdit").text), '/tmp')
    test.compare(str(waitForObjectExists(":dirs_panel_out_dir_QLineEdit").text), '/usr')

    # press 'Cancel' button
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # delete input dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Input directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # check that input dir has been unset on previous step
    test.compare(str(waitForObjectExists(":dirs_panel_in_dir_QLineEdit").text), '')
    test.compare(str(waitForObjectExists(":dirs_panel_out_dir_QLineEdit").text), '/usr')

    # press 'Cancel' button
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # delete output dir
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase.Output directory", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Delete"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    # invoke Set-up directories menu item
    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "CurrentCase", 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Set-up Directories"))

    # check that input dir has been unset on previous step
    test.compare(str(waitForObjectExists(":dirs_panel_in_dir_QLineEdit").text), '')
    test.compare(str(waitForObjectExists(":dirs_panel_out_dir_QLineEdit").text), '')

    # press 'Cancel' button
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
