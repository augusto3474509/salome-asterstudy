#------------------------------------------------------------------------------
# Issue #881 - Operations: Import Stage

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def checkContens():
    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "zzzz289f" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "MATER" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    checkContens()

    #--------------------------------------------------------------------------
    item = "CurrentCase.zzzz289f"
    waitForObjectItem( ":_QTreeWidget", item )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    #--------------------------------------------------------------------------
    filename = "tst_redmine_881"
    filepath = saveFile( filename )
    test.log( 'Saving %s' % filepath )
    waitFor( "object.exists(':AsterStudy *.Save_QToolButton')", 20000 )
    test.compare( findObject( ":AsterStudy *.Save_QToolButton" ).enabled, False )

    #--------------------------------------------------------------------------
    filepath += '.ajs'
    test.log( 'Restoring %s' % filepath )

    closeFile()
    openFile( filepath )
    #clickButton(waitForObject(":Missing study directory.OK_QPushButton"))
    #clickButton(waitForObject(":Inconsistent study directory.OK_QPushButton"))

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    item = "CurrentCase.zzzz289f"
    waitForObjectItem( ":_QTreeWidget", item )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    checkContens()

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
