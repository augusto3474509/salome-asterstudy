#------------------------------------------------------------------------------
# Issue # 1161 - History view: back-up/restore Current Case
# Check Back-up/Restore feature

def main():
    source(findFile("scripts", "common.py"))
    global_start()


def cleanup():
    global_cleanup()


def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem("File", "New")
    activateMenuItem("Operations", "Back Up")

    #--------------------------------------------------------------------------

    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    activateOBContextMenuItem("CurrentCase", "Back Up")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    test.compare(isItemExists("Stage_1", "Mesh", "mesh"), True)
    activateMenuItem("Operations", "Back Up")

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1", 10, 10, 0, Qt.LeftButton)
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_2", 10, 10, 0, Qt.LeftButton)
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_3", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "View Case (read-only)"))
    checkOBItemNotExists("BackupCase_1", "Stage_1")
    checkOBItemNotExists("BackupCase_1", "Stage_1", "Mesh", "mesh")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Copy As Current"))
    doubleClickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    checkOBItemNotExists("CurrentCase", "Stage_1")
    checkOBItemNotExists("CurrentCase", "Stage_1", "Mesh", "mesh")

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_2", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_2", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "View Case (read-only)"))
    checkOBItem("BackupCase_2", "Stage_1")
    checkOBItemNotExists("BackupCase_2", "Stage_1", "Mesh", "mesh")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_2", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_2", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Copy As Current"))
    doubleClickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    checkOBItem("CurrentCase", "Stage_1")
    checkOBItemNotExists("CurrentCase", "Stage_1", "Mesh", "mesh")

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_3", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_3", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "View Case (read-only)"))
    checkOBItem("BackupCase_3", "Stage_1")
    checkOBItem("BackupCase_3", "Stage_1", "Mesh", "mesh")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_3", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_3", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Copy As Current"))
    doubleClickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    checkOBItem("CurrentCase", "Stage_1")
    checkOBItem("CurrentCase", "Stage_1", "Mesh", "mesh")

    #--------------------------------------------------------------------------
    activateMenuItem("File", "Exit")

    #--------------------------------------------------------------------------
    pass
