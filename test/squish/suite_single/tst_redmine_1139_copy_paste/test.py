#------------------------------------------------------------------------------
# Issue #1139 - Data Settings view: copy/paste commands (CCTP 2.2.1.4)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1139

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    text = \
"""
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'), DEBUG=_F(SDVERI='OUI'))

F_MANSON = DEFI_FONCTION(
    NOM_PARA='EPSI',
    PROL_DROITE='LINEAIRE',
    PROL_GAUCHE='LINEAIRE',
    TITRE='FONCTION DE MANSON_COFFIN',
    VALE=(0.0, 200000.0, 2.0, 0.0)
)
"""
    setClipboardText( text )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )

    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "F_MANSON" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    waitForObject( ":_QTreeWidget" ).expandAll()

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Functions and Lists", "F_MANSON" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Copy" ) )

    snippet = getClipboardText()

    #openContextMenu( selectItem( "Stage_2" ), 10, 10, 0 )
    #activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )
    activateOBContextMenuItem("CurrentCase.Stage_2", "Paste")

    checkOBItem( "CurrentCase", "Stage_2", "Functions and Lists", "F_MANSON" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
