#------------------------------------------------------------------------------
# Issue #980 - Propagate validity status down to children
#
#   m1 = LIRE_MAILLAGE(UNITE=20)
#
#   m1 = MODI_MAILLAGE(MAILLAGE=m1, ORIE_PEAU_2D=_F(GROUP_MA='A'))
#
#   model = AFFE_MODELE(
#       AFFE=_F(MODELISATION='C_PLAN', PHENOMENE='MECANIQUE', TOUT='OUI'),
#       MAILLAGE=m1
#   )
#
#   Should become invalid on deletion of any of m1

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    text = \
"""
m1 = LIRE_MAILLAGE(UNITE=20)

m1 = MODI_MAILLAGE(MAILLAGE=m1, ORIE_PEAU_2D=_F(GROUP_MA='A'))

model = AFFE_MODELE(
    AFFE=_F(MODELISATION='C_PLAN', PHENOMENE='MECANIQUE', TOUT='OUI'),
    MAILLAGE=m1
)
"""
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    waitForObject( text_editor ).appendPlainText( text )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase" )
    checkValidity( True, "CurrentCase", "Stage_1" )
    checkValidity( True, "CurrentCase", "Stage_1", "Mesh" )
    isValid( True, "Stage_1", "Mesh", "m1:6" )
    isValid( True, "Stage_1", "Mesh", "m1:7" )
    checkValidity( True, "CurrentCase", "Stage_1", "Model Definition" )
    checkValidity( True, "CurrentCase", "Stage_1", "Model Definition", "model" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "m1:6" )
    openContextMenu( item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkValidity( False, "CurrentCase" )
    checkValidity( False, "CurrentCase", "Stage_1" )
    checkValidity( False, "CurrentCase", "Stage_1", "Mesh" )
    isValid( False, "Stage_1", "Mesh", "m1:7" )
    checkValidity( False, "CurrentCase", "Stage_1", "Model Definition" )
    checkValidity( False, "CurrentCase", "Stage_1", "Model Definition", "model" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Undo_QToolButton" ) )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "m1:7" )
    openContextMenu( item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkValidity( False, "CurrentCase" )
    checkValidity( False, "CurrentCase", "Stage_1" )
    checkValidity( True, "CurrentCase", "Stage_1", "Mesh" )
    isValid( True, "Stage_1", "Mesh", "m1:6" )
    checkValidity( False, "CurrentCase", "Stage_1", "Model Definition" )
    checkValidity( False, "CurrentCase", "Stage_1", "Model Definition", "model" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
