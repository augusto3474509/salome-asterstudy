#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForPopupItem("Edit")

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    symbol = tr( 'COMPTAGE', 'QComboBox' )
    setParameterValue( symbol, 'NIVEAU' )

    #--------------------------------------------------------------------------
    symbol = tr( 'DOMMAGE', 'QComboBox' )
    setParameterValue( symbol, 'WOHLER' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MATER', 'QComboBox' )
    setParameterValue( symbol, 'MAT0' )

    #--------------------------------------------------------------------------
    symbol = tr( 'DUREE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'DUREE', 'QLineEdit' )
    setParameterValue( symbol, str( 1. ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'MOMENT_SPEC_0', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'MOMENT_SPEC_0', 'QLineEdit' )
    setParameterValue( symbol, str( 182.5984664 ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'MOMENT_SPEC_2', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'MOMENT_SPEC_2', 'QLineEdit' )
    setParameterValue( symbol, str( 96098024.76 ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForPopupItem("Edit")

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    symbol = tr( 'COMPTAGE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'NIVEAU' )

    #--------------------------------------------------------------------------
    symbol = tr( 'DOMMAGE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'WOHLER' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MATER', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'MAT0' )

    #--------------------------------------------------------------------------
    symbol = tr( 'DUREE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'DUREE', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 1. ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'MOMENT_SPEC_0', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'MOMENT_SPEC_0', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 182.5984664 ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'MOMENT_SPEC_2', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'MOMENT_SPEC_2', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 96098024.76 ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'szlz106a.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file )

    #--------------------------------------------------------------------------
    category = "Other"
    type = "POST_FATI_ALEA"
    name = "TAB1"

    #--------------------------------------------------------------------------
    item = selectItem( "szlz106a", "%s:-5" % category, "%s" % name )
    checkContents( item, name )

    openContextMenu( selectItem( "szlz106a" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "szlz106a" ), 10, 10, 0 )
    popupItem("Graphical Mode")

    item = selectItem( "szlz106a", "%s:-5" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    name = "TAB11"
    populateContents( category, type, name, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
