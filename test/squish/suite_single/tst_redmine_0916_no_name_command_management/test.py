#------------------------------------------------------------------------------
# Issue #916 - Manage names of commands which do not have return value

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContextMenuItem( item, name, focus ):
    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    contextMenu = waitForObject( "{type='QMenu' unnamed='1' visible='1'}" )
    try:
        waitForObjectItem( contextMenu, name, 1000 )
    except LookupError:
        return False
    finally:
        clickItem( waitForObject( ":_QTreeWidget" ), focus, 10, 10, 0, Qt.LeftButton )

    #--------------------------------------------------------------------------
    return True

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    stage = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    addCommandByDialog( "DEBUT", "Deprecated" )
    item = showOBItem( "CurrentCase", "Stage_1", "Deprecated", "DEBUT" )

    treeWidget = waitForObject( ":_QTreeWidget" )
    test.compare( waitForObjectItem( treeWidget, item ).font.italic, True )
    test.verify( checkContextMenuItem( item, "Rename", stage ) == False )

    #--------------------------------------------------------------------------
    addCommandByDialog( "AFFE_MATERIAU", "Material" )
    item = showOBItem( "CurrentCase", "Stage_1", "Material", "AFFE_MATERIAU" )
    test.verify( checkContextMenuItem( item, "Rename", stage ) == True )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
