#------------------------------------------------------------------------------
# Issue #895 - Operations: Duplicate

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    activateMenuItem( "Commands", "Model Definition", "MODI_MODELE" )
    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "model" )

    mouseClick( waitForObject( ":AsterStudy Analysis_ComboBox" ), 51, 13, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( ":AsterStudy Analysis_ComboBox", "CALC\\_MISS" ), 35, 9, 0, Qt.LeftButton )
    checkOBItem( "CurrentCase", "Stage_1", "Analysis", "CALC_MISS" )

    activateMenuItem( "Commands", "Model Definition", "AFFE_MODELE" )
    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "model0" )

    activateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition.MODI_MODELE", "Duplicate" )
    activateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition.MODI_MODELE", "Delete" )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "model" )

    selectObjectBrowserItem( "CurrentCase.Stage_1.Model Definition.model0" )
#    type( ":_QTreeWidget", "<Ctrl+C>" )
    activateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition.model0", "Duplicate" )
    snooze( 1 )
    activateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition.model0", "Rename" )
    setParameterValue( ":_QExpandingLineEdit_2", "model0_orig" )
    type( waitForObject( ":_QExpandingLineEdit_2" ), "<Return>" )
    selectObjectBrowserItem( "CurrentCase.Stage_1.Model Definition.model0" )

#    type( ":_QTreeWidget", "<Delete>" )
    activateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition.model0", "Delete" )
    snooze( 1 )
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )

    activateOBContextMenuItem( "CurrentCase.Stage_1.Analysis.CALC_MISS", "Delete" )
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )
    checkOBItemNotExists( "CurrentCase", "Stage_1", "Analysis" )

    activateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition", "Delete" )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    checkOBItemNotExists( "CurrentCase", "Stage_1", "Model Definition" )

    activateOBContextMenuItem( "CurrentCase.Stage_1", "Delete" )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    checkOBItemNotExists( "CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    pass
