def main():
    source( findFile( "scripts", "common.py" ) )
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    test.xverify( waitForObjectExists( ":AsterStudy *.Show All_QToolButton" ).enabled )
    waitForActivateMenuItem( "Operations", "Add Stage" )

    showOBItem( "CurrentCase", "Stage_1" )
    test.verify( waitForObjectExists( ":AsterStudy *.Show All_QToolButton" ).enabled )

    selectObjectBrowserItem( "CurrentCase" )
    test.verify( waitForObjectExists( ":AsterStudy *.Show All_QToolButton" ).enabled )

    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "Commands", "Show All" )

    grp_name = "{container=':qt_tabwidget_stackedwidget_QWidget' text~='%s.*' type='QToolButton' unnamed='1' visible='1'}"
    grp_item_name = "{aboveWidget=%s container=':qt_tabwidget_stackedwidget_QWidget' type='QListWidget' unnamed='1' visible='1'}"

    for grp in ["Mesh",
                "Model Definition",
                "Material",
                "Functions and Lists",
                "BC and Load",
                "Pre Analysis",
                "Analysis",
                "Post Processing",
                "Fracture and Fatigue",
                "Output",
                "Other",
                "Deprecated"]:
        grpToolButton = grp_name % grp
        aToolButton = waitForObject( grpToolButton )
        test.verify( aToolButton.enabled, aToolButton.text )

    test.xverify( waitForObjectExists( ":OK_QPushButton" ).enabled )
    test.xverify( waitForObjectExists( ":Apply_QPushButton" ).enabled )
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Show All_QToolButton" ) )

    group = "Functions and Lists"; command = "FORMULE"
    setParameterValue( ":_QLineEdit", command )
    test.compare( str( waitForObjectExists( ":_QLineEdit" ).text ), command)

    type(":_QLineEdit", "<Esc>")
    test.compare( str( waitForObjectExists( ":_QLineEdit" ).text ), "")

    setParameterValue( ":_QLineEdit", command )
    test.compare( str( waitForObjectExists( ":_QLineEdit" ).text ), command)
    aboveWidget = grp_name % group
    grpListView =  grp_item_name % aboveWidget
    waitForObjectItem( grpListView, escape( command ) )
    clickItem( grpListView, escape( command ), 10, 10, 0, Qt.LeftButton )
    test.verify( 'FORMULE' in str( waitForObjectExists( ":Command description_QLabel" ).text ) )
    test.verify( 'Définit une formule réelle ou complexe à partir de son expression mathématique' in \
                 str( waitForObjectExists( ":Command description_QLabel" ).text ) )

    test.verify( waitForObjectExists( ":OK_QPushButton" ).enabled )
    test.verify( waitForObjectExists( ":Apply_QPushButton" ).enabled )

    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    test.verify( waitForObject( ":OK_QPushButton" ).enabled )
    test.verify( waitForObject( ":Apply_QPushButton" ).enabled )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    checkOBItem( "CurrentCase", "Stage_1", group, command )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "DEFI_MATERIAU", "Material" )

    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "AFFE_MODELE", "Model Definition" )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    clickButton( waitForObject( ":AsterStudy *.Show All_QToolButton" ) )

    group = "Fracture and Fatigue"; command = "CALC_GP"
    setParameterValue( ":_QLineEdit", command )
    aboveWidget = grp_name % group
    grpListView = grp_item_name % aboveWidget
    waitForObjectItem( grpListView, escape( command ) )
    clickItem( grpListView, escape( command ), 10, 10, 0, Qt.LeftButton )
    test.verify( "CALC_GP" in str( waitForObjectExists( ":Command description_QLabel" ).text ) )
    test.verify( "calcul du parametre de clivage energetique Gp en 2D et en 3D" in \
                 str( waitForObjectExists( ":Command description_QLabel" ).text ) )

    selectObjectBrowserItem( "CurrentCase" ) # Stage is not selected
    doubleClickItem( grpListView, escape( command ), 10, 10, 0, Qt.LeftButton )
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))
    checkOBItemNotExists( "CurrentCase", "Stage_1", group, command )

    selectObjectBrowserItem( "CurrentCase.Stage_1" ) # select Stage
    doubleClickItem( grpListView, escape( command ), 10, 10, 0, Qt.LeftButton )
    checkOBItem( "CurrentCase", "Stage_1", group, command )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Close" ) )
    clickButton( waitForObject( ":Close active study.Save_QPushButton" ) )
    file_path = os.path.normpath( os.path.join( testdir(), "tst_use_case_f.ajs" ) )
    setParameterValue( ":fileNameEdit_QLineEdit", file_path )
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )
    test.compare( findObject( ":AsterStudy *.Save_QToolButton" ).enabled, False )
    test.compare( findObject( ":AsterStudy.Close_QToolButton" ).enabled, False )
    test.verify( os.path.isfile( file_path ) )

    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )

    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1", 50, 9, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1", 50, 9, 0 )
    waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" )
    clickButton( waitForObject( ":AsterStudy *.Show All_QToolButton" ) )

    group = "Functions and Lists"; command = "FORMULE"
    setParameterValue( ":_QLineEdit", command )
    aboveWidget = grp_name % group
    grpListView =  grp_item_name % aboveWidget
    waitForObjectItem( grpListView, escape( command ) )
    clickItem( grpListView, escape( command ), 10, 10, 0, Qt.LeftButton )

    test.verify( waitForObjectExists( ":OK_QPushButton" ).enabled )
    test.verify( waitForObjectExists( ":Apply_QPushButton" ).enabled )

    clickButton( waitForObject( ":Close_QPushButton" ) )

        #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1", 50, 9, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.Stage\\_1", 50, 9, 0 )
    waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" )
    clickButton( waitForObject( ":AsterStudy *.Show All_QToolButton" ) )

    group = "Functions and Lists"; command = "FORMULE"
    setParameterValue( ":_QLineEdit", command )
    aboveWidget = grp_name % group
    grpListView =  grp_item_name % aboveWidget
    waitForObjectItem( grpListView, escape( command ) )
    clickItem( grpListView, escape( command ), 10, 10, 0, Qt.LeftButton )

    test.verify( waitForObjectExists( ":OK_QPushButton" ).enabled )
    test.verify( waitForObjectExists( ":Apply_QPushButton" ).enabled )

    clickButton( waitForObject( ":Close_QPushButton" ) )

        #--------------------------------------------------------------------------
    pass
