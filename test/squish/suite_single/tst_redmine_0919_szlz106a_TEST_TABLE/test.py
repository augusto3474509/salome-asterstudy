#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "szlz106a.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    symbol = tr( 'VALE_CALC', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'VALE_CALC', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QLineEdit' )
    setParameterValue( symbol, str( 3.8517772476578E-07 ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'REFERENCE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'REFERENCE', 'QComboBox' )
    setParameterValue( symbol, 'SOURCE_EXTERNE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'VALE_REFE', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'VALE_REFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QLineEdit' )
    setParameterValue( symbol, str( 3.851827e-07 ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'TABLE', 'QComboBox' )
    setParameterValue( symbol, 'TAB1' )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA', 'QLineEdit' )
    setParameterValue( symbol, 'DOMMAGE' )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    symbol = tr( 'VALE_CALC', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'VALE_CALC', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 3.8517772476578E-07 ) )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'VALE_REFE', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'VALE_REFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), str( 3.851827e-07 ) )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'REFERENCE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'REFERENCE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'SOURCE_EXTERNE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'TABLE', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'TAB1' )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA', 'QLineEdit' )
    test.compare( str( waitForObject( symbol ).text ), 'DOMMAGE' )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'szlz106a.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file )

    #--------------------------------------------------------------------------
    category = "Other"
    type = "TEST_TABLE"
    name = "TEST_TABLE"
    var = "_"

    #--------------------------------------------------------------------------
    item = selectItem( "szlz106a", "%s:-5" % category, "%s" % name )
    checkContents( item, var )

    activateOBContextMenuItem("CurrentCase.szlz106a", "Text Mode")
    waitForActivateOBContextMenuItem("CurrentCase.szlz106a", "Graphical Mode")

    item = selectItem( "szlz106a", "%s:-5" % category, "%s" % name )
    checkContents( item, var )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    populateContents( category, type, var, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, var )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, var )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
