#------------------------------------------------------------------------------
# Issue #1152 - Edition panel: management of Python variables (CCTP 2.3.2)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1152

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )

    #--------------------------------------------------------------------------
    snippet = \
"""
a = 1

b = a + 1

c = 3 * a
"""
    setClipboardText( snippet )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "c" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "3" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "b" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "d" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "2" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    isValid( True, "Stage_1" )
    isValid( True, "Stage_1", "Variables", "a" )
    isValid( True, "Stage_1", "Variables", "d" )
    isValid( True, "Stage_1", "Variables", "c" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "c:8" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "3" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_1" )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )

    snippet = \
"""
a = 1

d = a + 1

c = 3 * a
"""
    actual = str( waitForObject( text_editor ).toPlainText() )
    test.verify( check_text_eq( snippet, actual ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1", "Variables", "a" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "b" )
    test.compare( str( waitForObjectExists( tr( 'Value', 'QLineEdit' ) ).text ), "1" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    isValid( False, "Stage_1" )
    isValid( True, "Stage_1", "Variables", "b" )
    isValid( False, "Stage_1", "Variables", "d" )
    isValid( False, "Stage_1", "Variables", "c" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
