def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem( "Operations", "Add Stage" )

    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 42, 5, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Create Variable"))
    type(waitForObject(":Name_QLineEdit_2"), "x")
    type(waitForObject(":Expression_QLineEdit"), "123")
    clickButton(waitForObject(":OK_QPushButton"))
    
    activateOBContextMenuItem("CurrentCase.Stage_1", "Functions and Lists", "DEFI_FONCTION")
    
    # close without modifications
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 80, 7, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 64, 9, 0, Qt.LeftButton)
     
    # check field, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 54, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":NOM_RESU_QCheckBox"))
    test.compare(True, waitForObject(":NOM_RESU_QCheckBox").checked)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 37, 6, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
     
    # check field, save, close
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 54, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":NOM_RESU_QCheckBox"))
    test.compare(True, waitForObject(":NOM_RESU_QCheckBox").checked)
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 37, 6, 0, Qt.LeftButton)
     
    # uncheck field, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 54, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":NOM_RESU_QCheckBox"))
    test.compare(False, waitForObject(":NOM_RESU_QCheckBox").checked)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 37, 6, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
     
    # uncheck field, save, close
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 54, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":NOM_RESU_QCheckBox"))
    test.compare(False, waitForObject(":NOM_RESU_QCheckBox").checked)
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 37, 6, 0, Qt.LeftButton)
 
    # create comment, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Comment_QToolButton"))
    test.compare("", waitForObject(":text_editor_QPlainTextEdit").plainText)
    type(waitForObject(":text_editor_QPlainTextEdit"), "Sample text 1.")
    clickButton(waitForObject(":OK_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
     
    # create comment, save, close
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Comment_QToolButton"))
    #!!! test.compare("", waitForObject(":text_editor_QPlainTextEdit").plainText)
    type(waitForObject(":text_editor_QPlainTextEdit"), "<Ctrl+A>")
    type(waitForObject(":text_editor_QPlainTextEdit"), "Sample text 2.")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Apply_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
 
    # edit comment, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Comment_QToolButton"))
    test.compare("Sample text 2.", waitForObject(":text_editor_QPlainTextEdit").plainText)
    type(waitForObject(":text_editor_QPlainTextEdit"), "<Ctrl+A>")
    type(waitForObject(":text_editor_QPlainTextEdit"), "Sample text 3.")
    clickButton(waitForObject(":OK_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
 
    # edit comment, save, close
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Comment_QToolButton"))
    #!!! test.compare("Sample text 2.", waitForObject(":text_editor_QPlainTextEdit").plainText)
    type(waitForObject(":text_editor_QPlainTextEdit"), "<Ctrl+A>")
    type(waitForObject(":text_editor_QPlainTextEdit"), "Sample text 4.")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Apply_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
 
    # erase comment, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Edit Comment_QToolButton"))
    test.compare("Sample text 4.", waitForObject(":text_editor_QPlainTextEdit").plainText)
    type(waitForObject(":text_editor_QPlainTextEdit"), "<Ctrl+A>")
    type(waitForObject(":text_editor_QPlainTextEdit"), "<Delete>")
    clickButton(waitForObject(":OK_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
 
    # erase comment, save, close
    #!!! doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    #!!! clickButton(waitForObject(":Edit Comment_QToolButton"))
    #!!! test.compare("Sample text 4.", waitForObject(":text_editor_QPlainTextEdit").plainText)
    #!!! type(waitForObject(":text_editor_QPlainTextEdit"), "<Ctrl+A>")
    #!!! type(waitForObject(":text_editor_QPlainTextEdit"), "<Delete>")
    #!!! clickButton(waitForObject(":OK_QPushButton"))
    #!!! clickButton(waitForObject(":Apply_QPushButton"))
    #!!! clickButton(waitForObject(":Yes_QPushButton"))
    #!!! doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)

    # create variable, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":NOM_RESU_QCheckBox"))
    clickButton(waitForObject(":NOM_RESU-as_ico_text.png_QToolButton"))
    mouseClick(waitForObjectItem(":NOM_RESU_ComboBox", "<Add variable\\.\\.\\.>"), 127, 12, 0, Qt.LeftButton)
    type(waitForObject(":Name_QLineEdit_2"), "a")
    type(waitForObject(":Expression_QLineEdit"), "'a'")
    clickButton(waitForObject(":OK_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))

    # create variable, save, close
    #!!! doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    #!!! clickButton(waitForObject(":NOM_RESU_QCheckBox"))
    #!!! clickButton(waitForObject(":NOM_RESU-as_ico_text.png_QToolButton"))
    #!!! mouseClick(waitForObjectItem(":NOM_RESU_ComboBox", "<Add variable\\.\\.\\.>"), 127, 12, 0, Qt.LeftButton)
    #!!! type(waitForObject(":Name_QLineEdit_2"), "a")
    #!!! type(waitForObject(":Expression_QLineEdit"), "'a'")
    #!!! clickButton(waitForObject(":OK_QPushButton"))
    #!!! clickButton(waitForObject(":Apply_QPushButton"))
    #!!! clickButton(waitForObject(":Yes_QPushButton"))
    #!!! doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)

    # create another variable, don't finish creating, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":NOM_RESU_QCheckBox")) # !!! to be removed
    clickButton(waitForObject(":NOM_RESU-as_ico_text.png_QToolButton")) # !!! to be removed
    mouseClick(waitForObjectItem(":NOM_RESU_ComboBox", "<Add variable\\.\\.\\.>"), 127, 12, 0, Qt.LeftButton)
    type(waitForObject(":Name_QLineEdit_2"), "b")
    type(waitForObject(":Expression_QLineEdit"), "'b'")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))

    # create another variable, save, close
    #!!! doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)
    #!!! mouseClick(waitForObjectItem(":NOM_RESU_ComboBox", "<Add variable\\.\\.\\.>"), 127, 12, 0, Qt.LeftButton)
    #!!! type(waitForObject(":Name_QLineEdit_2"), "b")
    #!!! type(waitForObject(":Expression_QLineEdit"), "'b'")
    #!!! clickButton(waitForObject(":OK_QPushButton"))
    #!!! clickButton(waitForObject(":Apply_QPushButton"))
    #!!! clickButton(waitForObject(":Yes_QPushButton"))
    #!!! doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Variables.x"), 29, 8, 0, Qt.LeftButton)
    
    # one more action, to be sure we are not seeing a dialog now
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 65, 8, 0, Qt.LeftButton)