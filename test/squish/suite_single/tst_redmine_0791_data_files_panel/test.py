#------------------------------------------------------------------------------
# Issue # 791 - L3.1. "Data Files" panel

# TODO Verification points must be fixed since they seem contain screenshots

def main():
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)


def cleanup():
    global_cleanup()


def browseFile( symbol, filename, click_browse=True, save=False ):
    if click_browse:
        clickButton( waitForObject( symbol ) )
    file_path = filename if os.path.isabs( filename ) else os.path.join( casedir(), filename )
    setParameterValue( ":fileNameEdit_QLineEdit", file_path )
    if save:
        clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )
    else:
        clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )


def runCase():
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1" )
    addCommandByDialog( "PRE_GMSH", "Other" )
    test.compare( isItemExists( "Stage_1", "Other", "PRE_GMSH" ), True )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Other", "PRE_GMSH" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    symbol = tr( 'UNITE_GMSH', 'QPushButton' )
    def_file_path = os.path.join( casedir(), 'default.file' )
    browseFile( symbol, def_file_path )

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( def_file_path ) )

    symbol = tr( 'UNITE_MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    def_file2_path = os.path.join( casedir(), 'default.file2' )
    if os.path.exists(def_file2_path):
        os.remove(def_file2_path)
    setParameterValue( ":fileNameEdit_QLineEdit", def_file2_path )
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( def_file2_path ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    test.vp( "DataFiles_graphical_mode_19" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1", 10, 10, 0, Qt.LeftButton )
    test.compare(waitForObjectExists(":DataFilesBase.Add File_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Edit_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Delete_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Go To_QToolButton").enabled, False)

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.19" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.19", 10, 10, 0, Qt.LeftButton )
    test.compare(waitForObjectExists(":DataFilesBase.Add File_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Edit_QToolButton").enabled, True)
    test.compare(waitForObjectExists(":DataFilesBase.Delete_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Go To_QToolButton").enabled, False)

    #--------------------------------------------------------------------------

    item = selectItem( "Stage_1" )
    openContextMenu( item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    #--------------------------------------------------------------------------
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    test.vp( "DataFiles_text_mode_19" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.19" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.19", 10, 10, 0, Qt.LeftButton )
    clickButton(waitForObject(":DataFilesBase.Edit_QToolButton"))
    setParameterValue( ":Unit.Unit_QLineEdit", "33" )
    test.compare( waitForObject( ":Unit.Unit_QLineEdit" ).text, "33" )

    test.compare( waitForObjectExists( ":Mode_QComboBox" ).currentIndex, 0 )
    mouseClick(waitForObject(":Mode_QComboBox"), 65, 8, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":Mode_QComboBox", "in"), 5, 5, 0, Qt.LeftButton)

    clickButton( waitForObject( ":OK_QPushButton" ) )

    test.verify( waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.33" ) )


    test.vp( "DataFiles_text_mode_33" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1", 10, 10, 0, Qt.LeftButton )
    test.compare(waitForObjectExists(":DataFilesBase.Add File_QToolButton").enabled, True)
    test.compare(waitForObjectExists(":DataFilesBase.Edit_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Delete_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Go To_QToolButton").enabled, False)

    #--------------------------------------------------------------------------
    clickButton(waitForObject(":DataFilesBase.Add File_QToolButton"))

    setParameterValue( ":Mode_QComboBox", "in" )
    test.compare( str( waitForObjectExists( ":Mode_QComboBox" ).currentText ), "in" )
    test.xverify( waitForObjectExists( ":OK_QPushButton" ).enabled )

    browseFile( ":Filename.Filename_QToolButton", 'gmsh.file' )
    test.compare( str( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText ), "gmsh.file" )
    test.verify( waitForObjectExists( ":OK_QPushButton" ).enabled )

    test.compare( waitForObject( ":Unit.Unit_QLineEdit" ).text, "2" )
    setParameterValue( ":Unit.Unit_QLineEdit", "44" )
    test.compare( waitForObject( ":Unit.Unit_QLineEdit" ).text, "44" )
    test.verify( waitForObjectExists( ":OK_QPushButton" ).enabled )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    test.verify( waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.33" ) )
    test.verify( waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.20" ) )
    test.verify( waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.44" ) )

    test.vp( "DataFiles_text_mode_33_44_in" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.44" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.44", 10, 10, 0, Qt.LeftButton )
    test.compare(waitForObjectExists(":DataFilesBase.Add File_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":DataFilesBase.Edit_QToolButton").enabled, True)
    test.compare(waitForObjectExists(":DataFilesBase.Delete_QToolButton").enabled, True)
    test.compare(waitForObjectExists(":DataFilesBase.Go To_QToolButton").enabled, False)

    #--------------------------------------------------------------------------
    clickButton(waitForObject(":DataFilesBase.Edit_QToolButton"))
    test.compare( str( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText ), "gmsh.file" )
    test.compare( waitForObject( ":Unit.Unit_QLineEdit" ).text, "44" )
    test.compare( str( waitForObjectExists( ":Mode_QComboBox" ).currentText ), "in" )

    stage = waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    model = stage.model()
    test.compare( model.rowCount( stage ), 3 )

    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1", 10, 10, 0, Qt.LeftButton )
    clickButton(waitForObject(":DataFilesBase.Add File_QToolButton"))

    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.44" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.44", 10, 10, 0, Qt.LeftButton )
    clickButton(waitForObject(":DataFilesBase.Delete_QToolButton"))
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    stage = waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    model = stage.model()
    test.compare( model.rowCount( stage ), 2 )

    test.vp( "DataFiles_text_mode_33" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1", 10, 10, 0, Qt.LeftButton )
    clickButton(waitForObject(":DataFilesBase.Add File_QToolButton"))

    setParameterValue( ":Filename.Filename_QComboBox", 'default.file' )
    setParameterValue( ":Unit.Unit_QLineEdit", "44" )
    setParameterValue( ":Mode_QComboBox", 'out' ) # no crash should be at this point, see issue #1818. 
    setParameterValue( ":Filename.Filename_QComboBox", 'default.file' ) # re-select file name since after previous step it is reset

    clickButton(waitForObject(":OK_QPushButton"))

    test.vp( "DataFiles_text_mode_33_44_out" )

    test.verify( waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.44" ) )

    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.44" )
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.44", 10, 10, 0, Qt.LeftButton )
    clickButton(waitForObject(":DataFilesBase.Edit_QToolButton"))
    test.compare( str( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText ), "default.file" )
    test.compare( waitForObject( ":Unit.Unit_QLineEdit" ).text, "44" )
    test.compare( str( waitForObjectExists( ":Mode_QComboBox" ).currentText ), "out" )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    test.vp( "DataFiles_text_mode_33_44_out" )

    #--------------------------------------------------------------------------

    item = selectItem( "Stage_1" )
    openContextMenu( item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    clickButton(waitForObject(":Undefined files.OK_QPushButton"))
    #--------------------------------------------------------------------------

    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    test.vp( "DataFiles_text_mode_19_undefined_33_44_out" )

    #--------------------------------------------------------------------------
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.19", 10, 10, 0, Qt.LeftButton )
    clickButton(waitForObject(":DataFilesBase.Edit_QToolButton"))

    test.compare( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText, "<19 undefined>" )
    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).checked, False )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, False )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).enabled, False ) # non-existent file can not be embedded

    browseFile( ":Filename.Filename_QToolButton", 'gmsh.file' )

    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).checked, True )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, False )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).enabled, True )

    # file can not be embed if study was not saved
    clickButton( waitForObject( ":Embedded_QCheckBox" ) )

    test.compare(str(waitForObjectExists(":AsterStudy_MessageBox").text), "You should save the study before embedding the file")
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # attempt to embed non-existent file to non-saved study by context menu
    #--------------------------------------------------------------------------
    embed_col = "{column='4' container=':DataFilesView.Stage_1_QModelIndex' occurrence='2' type='QModelIndex'}"
    test.compare( waitForObject( embed_col ).text, "No" )

    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.default\\.file2", 37, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Embedded"))

    test.compare(str(waitForObjectExists(":AsterStudy_MessageBox").text), "You should save the study before embedding the file")
    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))

    test.compare( waitForObject( embed_col ).text, "No" )

    #--------------------------------------------------------------------------
    # save the study
    #--------------------------------------------------------------------------
    study_file = os.path.join(testdir(), "791")
    saveFile(study_file)
    study_files_dir = study_file + "_Files"
    study_embedded_dir = os.path.join( study_files_dir, "Embedded" )

    #--------------------------------------------------------------------------
    # embed valid, but non-existent file to saved study by context menu
    #--------------------------------------------------------------------------
    embed_col = "{column='4' container=':DataFilesView.Stage_1_QModelIndex' occurrence='2' type='QModelIndex'}"
    test.compare( waitForObject( embed_col ).text, "No" )

    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.default\\.file2", 37, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Embedded"))

    test.compare( waitForObject( embed_col ).text, "Yes" )

    #--------------------------------------------------------------------------
    # embed the file
    #--------------------------------------------------------------------------
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.19", 10, 10, 0, Qt.LeftButton )
    clickButton( waitForObject( ":DataFilesBase.Edit_QToolButton" ) )

    test.compare( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText, "<19 undefined>" )
    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).checked, False )
    browseFile( ":Filename.Filename_QToolButton", 'gmsh.file' )
    test.compare( str( waitForObject( ":Filename.Filename_QComboBox" ).currentText ), "gmsh.file" )

    test.compare( waitForObjectExists( ":Unit.Unit_QLineEdit" ).text, "19" )
    test.xverify( waitForObjectExists( ":Unit.Unit_QLineEdit" ).enabled )

    test.compare( str( waitForObjectExists( ":Mode_QComboBox" ).currentText ), "in" )
    test.xverify( waitForObjectExists( ":Mode_QComboBox" ).enabled )

    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).enabled, False )
    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).checked, True )

    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).enabled, True )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, False )

    clickButton( waitForObject( ":Embedded_QCheckBox" ) )

    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, True )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    test.vp( "DataFiles_20_19_embedded1" )
    # external file has been copied to Embedded directory
    test.verify( os.path.isfile( os.path.join( study_embedded_dir, "gmsh.file" ) ) )

    #--------------------------------------------------------------------------
    # unembed the file
    #--------------------------------------------------------------------------
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.gmsh\\.file (embedded)", 10, 10, 0, Qt.LeftButton )
    clickButton( waitForObject( ":DataFilesBase.Edit_QToolButton" ) )

    test.compare( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText, "gmsh.file (embedded)" )
    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).checked, True )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, True )

    clickButton( waitForObject( ":Embedded_QCheckBox" ) )

    test.compare( waitForObject( ":QFileDialog.fileTypeCombo_QComboBox" ).currentText,
                  "All files (*)" )
    file_path = os.path.join( testdir(), 'gmsh.file_ext' )
    browseFile( None, file_path, False, True )

    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, False )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    test.vp( "DataFiles_20_19_ext" )
    # embedded file has been moved to the given external one
    test.verify( not os.path.isfile( os.path.join( study_embedded_dir, "gmsh.file" ) ) )
    test.verify( os.path.isfile( file_path ) )

    #--------------------------------------------------------------------------
    # embed the file again
    #--------------------------------------------------------------------------
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.gmsh\\.file\\_ext", 10, 10, 0, Qt.LeftButton )
    clickButton( waitForObject( ":DataFilesBase.Edit_QToolButton" ) )

    test.compare( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText, "gmsh.file_ext" )

    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).checked, True )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, False )

    clickButton( waitForObject( ":Embedded_QCheckBox" ) )

    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, True )

    clickButton(waitForObject(":OK_QPushButton"))

    test.vp( "DataFiles_20_19_embedded2" )
    test.verify( os.path.isfile( os.path.join( study_embedded_dir, "gmsh.file_ext" ) ) )

    #--------------------------------------------------------------------------
    # set another file instead of embedded one
    #--------------------------------------------------------------------------
    clickItem( ":DataFiles_QTreeView", "Stage\\_1.gmsh\\.file\\_ext (embedded)", 10, 10, 0, Qt.LeftButton )
    clickButton( waitForObject( ":DataFilesBase.Edit_QToolButton" ) )

    test.compare( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText, "gmsh.file_ext (embedded)" )
    test.compare( waitForObjectExists( ":Exists_QCheckBox" ).checked, True )
    test.compare( waitForObjectExists( ":Embedded_QCheckBox" ).checked, True )

    browseFile( ":Filename.Filename_QToolButton", 'gmsh.file' )

    test.compare(waitForObjectExists(":Embedded_QCheckBox").checked, False)

    clickButton( waitForObject( ":OK_QPushButton" ) )

    test.vp( "DataFiles_20_19_ext2" )
    # embedded file has been removed
    test.verify( not os.path.isfile( os.path.join( study_embedded_dir, "gmsh.file_ext" ) ) )

    #--------------------------------------------------------------------------
    # embed the file by context menu
    #--------------------------------------------------------------------------
    embed_col = "{column='4' container=':DataFilesView.Stage_1_QModelIndex' occurrence='2' type='QModelIndex'}"
    test.compare( waitForObject( embed_col ).text, "No" )

    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.gmsh\\.file", 37, 6, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Embedded"))

    test.compare( waitForObject( embed_col ).text, "Yes" )

    # embedded file has been copied to the Embedded directory of the study
    test.verify( os.path.isfile( os.path.join( study_embedded_dir, "gmsh.file" ) ) )

    #--------------------------------------------------------------------------
    # unembed the file by context menu
    #--------------------------------------------------------------------------
    test.compare( waitForObject( embed_col ).text, "Yes" )

    openItemContextMenu(waitForObject(":DataFiles_QTreeView"), "Stage\\_1.gmsh\\.file (embedded)", 66, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Embedded"))

    file_path = os.path.join( testdir(), 'gmsh.file_ext.2' )
    browseFile( None, file_path, False, True )

    test.compare( waitForObject( embed_col ).text, "No" )

    # embedded file has been moved to the given external one
    test.verify( not os.path.isfile( os.path.join( study_embedded_dir, "gmsh.file" ) ) )
    test.verify( os.path.isfile( file_path ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
