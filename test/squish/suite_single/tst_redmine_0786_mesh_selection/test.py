#------------------------------------------------------------------------------
# Checks that selection of MED file as 'UNITE' provides possibility
#     to select corresponding mesh as 'NOM_MED'
#------------------------------------------------------------------------------
def main():
    source( findFile( "scripts", "common.py" ) )
    global_start(noexhook=True)

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    comm_file = os.path.join( casedir(), 'lire_maillage.comm' )
    test.log( "Use '%s' file to create a new stage" % comm_file )

    importStage( comm_file, [":Undefined files.OK_QPushButton"] )
    #--------------------------------------------------------------------------

    showOBItem("CurrentCase", "lire_maillage", "Mesh", "Mesh")
    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.lire\\_maillage.Mesh.Mesh", 50, 9, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    test.compare( waitForObject( ":QFileDialog.fileTypeCombo_QComboBox" ).currentText,
                  "All files (*)" )
    med_file_path = os.path.join( casedir(), findFile( "testdata", "Mesh_recette.med" ) )
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(med_file_path))

    symbol = tr( 'NOM_MED', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'NOM_MED', 'QComboBox' )
    mouseClick(waitForObject(symbol), 127, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(symbol, "Mesh\\_recette"), 77, 11, 0, Qt.LeftButton)

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject( ":_QTreeWidget"), "CurrentCase.lire\\_maillage.Mesh.Mesh", 50, 9, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(med_file_path))

    symbol = tr( 'NOM_MED', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), "Mesh_recette")

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )
