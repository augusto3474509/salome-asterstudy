#------------------------------------------------------------------------------
# Issue #1137 - Manage comments
#
# For more details look at http://salome.redmine.opencascade.com/issues/1137

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    setClipboardText("DEBUT()")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Paste")
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Deprecated.DEBUT", "Edit")

    symbol = tr( 'LANG', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )
    symbol = trex( 'LANG.*', 'QToolButton' )
    clickButton( waitForObjectExists( symbol ) )

    comboBox = waitForObject( tr( 'LANG', 'QComboBox' ) )
    mouseClick( comboBox, 10, 10, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( comboBox, "<Add variable\\.\\.\\.>" ), 57, 11, 0, Qt.LeftButton )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "language" )
    waitForObject( tr( 'Comment', 'QPlainTextEdit' ) ).setPlainText( "language of the stage" )
    setParameterValue( tr( 'Expression', 'QLineEdit' ), "'japanese'" )
    test.compare( str( waitForObject( tr( 'Value', 'QLineEdit' ) ).text ), "'japanese'" )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    clickButton(waitForObject(":Edit Comment_QToolButton"))
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("this is DEBUT command")
    clickButton(waitForObject(":OK_QPushButton"))

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "language of the stage" )
    selectItem( "Stage_1", "Variables", "language" )
    selectItem( "Stage_1", "Deprecated", "this is DEBUT command" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "language of the stage" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "language of the stage" )
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "language" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "language of the stage" )
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Deprecated", "this is DEBUT command" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is DEBUT command" )
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Deprecated", "DEBUT" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is DEBUT command" )
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
