#------------------------------------------------------------------------------
# Issue #945 - Exception when opening Parameters panel if referenced concept is deleted

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    item = showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    text1 = \
"""
table = INFO_EXEC_ASTER(LISTE_INFO=('UNITE_LIBRE', ))
recu1 = RECU_FONCTION(TABLE=table, PARA_X='X', PARA_Y='Y')
recu2 = RECU_FONCTION(TABLE=table, PARA_X='X', PARA_Y='Y', FILTRE=_F(NOM_PARA='X', VALE=0))
table2 = INFO_EXEC_ASTER(LISTE_INFO=('UNITE_LIBRE', ))
"""
    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )
    waitForObject( text_editor ).clear()
    waitForObject( text_editor ).appendPlainText( text1 )
    clickButton( waitForObject( ":OK_QPushButton" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    #--------------------------------------------------------------------------

    selectItem( "Stage_1:2", "Other:-1", "table:6" )
    selectItem( "Stage_1:2", "Output:-2", "recu1:7" )
    selectItem( "Stage_1:2", "Output:-2", "recu2:8" )
    selectItem( "Stage_1:2", "Other:-3", "table2:9" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1:2", "Output:-2", "recu1:7" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    symbol = tr( 'TABLE', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'TABLE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "table" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1:2", "Other:-1", "table:6" )
    openContextMenu( item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1:2", "Output:-1", "recu2:8" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    symbol = tr( 'TABLE', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'TABLE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "<no object selected>" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
