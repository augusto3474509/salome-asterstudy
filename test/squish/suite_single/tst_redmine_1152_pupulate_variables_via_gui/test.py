#------------------------------------------------------------------------------
# Issue #1152 - Edition panel: management of Python variables (CCTP 2.3.2)
#
# For more details look at http://salome.redmine.opencascade.com/issues/1152

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    showOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "Commands" ) )
    activateItem( waitForObjectItem( ":Commands_QMenu", "Create Variable" ) )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "vitesse" )
    setParameterValue( tr( 'Expression', 'QLineEdit' ), "1.e-5" )
    test.compare( str( waitForObject( tr( 'Value', 'QLineEdit' ) ).text ), "1e-05" )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "Commands" ) )
    activateItem( waitForObjectItem( ":Commands_QMenu", "Create Variable" ) )

    setParameterValue( tr( 'Name', 'QLineEdit' ), "t_0" )
    setParameterValue( tr( 'Expression', 'QLineEdit' ), "5.e-2 / ( 8.0 * vitesse )" )
    test.compare( str( waitForObject( tr( 'Value', 'QLineEdit' ) ).text ), "625.0" )

    clickButton( waitForObject( ":OK_QPushButton" ) )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    addCommandByDialog( "DEFI_CONSTANTE", "Functions and Lists" )
    openContextMenu( selectItem( "Stage_1", "Functions and Lists", "DEFI_CONSTANTE:6" ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    setParameterValue( ":Name_QLineEdit", "c1" )

    clickButton( waitForObject( trex( 'VALE.*', 'QToolButton' ) ) )
    comboBox = waitForObject( tr( 'VALE', 'QComboBox' ) )

    # test.verify( comboBox.findText( '<no object selected>' ) )
    test.verify( comboBox.findText( '<Add variable...>' ) != -1 )
    test.verify( comboBox.findText( 'vitesse' ) != -1 )
    test.verify( comboBox.findText( 't_0' ) != -1 )
    test.compare( 4, comboBox.count )

    setParameterValue( tr( 'VALE', 'QComboBox' ), "t_0" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    selectItem( "Stage_1", "Functions and Lists", "c1:6" )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    item = showOBItem( "CurrentCase", "Stage_1" )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    text_editor = tr( 'text_editor', 'QAbstractScrollArea' )

    snippet = \
"""
vitesse = 1.e-5

t_0 = 5.e-2 / (8.0 * vitesse)

c1 = DEFI_CONSTANTE(VALE=t_0)
"""
    actual = str( waitForObject( text_editor ).toPlainText() )
    test.verify( check_text_eq( snippet, actual ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
