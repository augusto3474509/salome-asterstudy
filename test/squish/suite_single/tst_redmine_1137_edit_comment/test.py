#------------------------------------------------------------------------------
# Issue #1137 - Manage comments
#
# For more details look at http://salome.redmine.opencascade.com/issues/1137

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    text = \
"""
# this is variable
a = 10

# this is command
DEBUT()

m = LIRE_MAILLAGE(UNITE=20)
"""
    setClipboardText(text)
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )
    waitForObject( ":_QTreeWidget" ).expandAll()
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "this is variable" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is variable" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("modify comment")
    clickButton(waitForObject(":Apply_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "modify comment" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "this is command" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("aaa aaa aaa")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "aaa aaa aaa" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "this is command" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Variables", "a" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "aaa aaa aaa" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("variable a")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "this is command" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Deprecated", "this is command" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is command" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("ccc ccc ccc")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "ccc ccc ccc" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Deprecated", "DEBUT" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "ccc ccc ccc" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("command DEBUT")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "m" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("command LIRE_MAILLAGE")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "command LIRE_MAILLAGE" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "command LIRE_MAILLAGE" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "command LIRE_MAILLAGE" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("command m")
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "command LIRE_MAILLAGE" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "command LIRE_MAILLAGE" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("command m")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "command m" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "m" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "command m" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "m" )
    checkOBItemNotExists( "CurrentCase", "Stage_1", "Mesh", "command m" )
    checkOBItemNotExists( "CurrentCase", "Stage_1", "Mesh", "" )

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Variables", "variable a" )
    selectItem( "Stage_1", "Variables", "a" )
    selectItem( "Stage_1", "Deprecated", "command DEBUT" )
    selectItem( "Stage_1", "Deprecated", "DEBUT" )
    selectItem( "Stage_1", "Mesh", "m" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
