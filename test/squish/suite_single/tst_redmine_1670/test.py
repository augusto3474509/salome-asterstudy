#------------------------------------------------------------------------------
# Issue #1670 - Show catalogue name in Data Files panel

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # create empty stage in current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add two commands referencing files 
    code = """
stage = AsterStdGui.gui.study().history.current_case[-1]
stage('LIRE_MAILLAGE', 'mesh').init({'UNITE':{20:'aaa.txt'},})
stage('IMPR_RESU').init({'UNITE':{21:'bbb.txt'},})
"""
    exec_in_console(code)

    #--------------------------------------------------------------------------
    # check items
    selectObjectBrowserItem("CurrentCase")
    clickItem(":DataFiles_QTreeView", "Stage\\_1.aaa\\.txt.mesh (LIRE\\_MAILLAGE)", 10, 10, 0, Qt.LeftButton)
    clickItem(":DataFiles_QTreeView", "Stage\\_1.bbb\\.txt.[noname] (IMPR\\_RESU)", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    # switch OFF 'Data Files / 'Show catalog names' preferences option
    activateMenuItem("File", "Preferences...")
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_data_files_QCheckBox"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # check items 
    clickItem(":DataFiles_QTreeView", "Stage\\_1.aaa\\.txt.mesh", 10, 10, 0, Qt.LeftButton)
    clickItem(":DataFiles_QTreeView", "Stage\\_1.bbb\\.txt.[noname]", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    # switch ON 'Data Files / 'Show catalog names' preferences option
    activateMenuItem("File", "Preferences...")
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_show_catalogue_name_data_files_QCheckBox"))
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))
    
    #--------------------------------------------------------------------------
    # check items 
    clickItem(":DataFiles_QTreeView", "Stage\\_1.aaa\\.txt.mesh (LIRE\\_MAILLAGE)", 10, 10, 0, Qt.LeftButton)
    clickItem(":DataFiles_QTreeView", "Stage\\_1.bbb\\.txt.[noname] (IMPR\\_RESU)", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
