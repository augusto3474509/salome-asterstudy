#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populate_DEBUT( name ):
    #--------------------------------------------------------------------------
    addCommandByDialog( "DEBUT", "Deprecated" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Deprecated" )
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Deprecated", 10, 10, 0, Qt.LeftButton )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Deprecated.DEBUT" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), "CurrentCase.Stage\\_1.Deprecated.DEBUT", 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":CODE_QCheckBox" ) )
    clickButton( waitForObject( ":CODE_QPushButton" ) )

    symbol = tr( 'NIV_PUB_WEB', 'QComboBox' )
    setParameterValue( symbol, 'INTERNET' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "INTERNET" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":DEBUG_QCheckBox" ) )
    clickButton( waitForObject( ":DEBUG_QPushButton" ) )

    clickButton( waitForObject( ":SDVERI_QCheckBox" ) )
    waitForObject( ":SDVERI_QCheckBox" ).setChecked( True )

    clickButton( waitForObject( ":SDVERI-value_QCheckBox" ) )
    test.compare( waitForObjectExists( ":SDVERI-value_QCheckBox" ).checked, True )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def check_DEBUT( item, name, imported ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":CODE_QCheckBox" ).checked, True )
    clickButton( waitForObject( ":CODE_QPushButton" ) )

    test.compare( str( waitForObjectExists( ":NIV_PUB_WEB_QComboBox" ).currentText ), "INTERNET" )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":DEBUG_QCheckBox" ).checked, True )
    clickButton( waitForObject( ":DEBUG_QPushButton" ) )

    test.compare( waitForObjectExists( ":SDVERI_QCheckBox" ).checked, True )
    test.compare( waitForObjectExists( ":SDVERI-value_QCheckBox" ).enabled, True )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContens():
    #--------------------------------------------------------------------------
    selectItem( "zzzz289f" )
    selectItem( "zzzz289f", "Mesh" )
    selectItem( "zzzz289f", "Mesh", "MAIL_Q" )
    selectItem( "zzzz289f", "Material" )
    selectItem( "zzzz289f", "Material", "MATER" )
    selectItem( "zzzz289f", "Material" )
    selectItem( "zzzz289f", "Material", "CHMAT_Q" )
    selectItem( "zzzz289f", "Model Definition" )
    selectItem( "zzzz289f", "Model Definition", "MODELUPG" )
    selectItem( "zzzz289f", "Model Definition", "MODELUPQ" )
    selectItem( "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )

    checkContens()

    #--------------------------------------------------------------------------
    item = selectItem( "zzzz289f", "Deprecated", "DEBUT" )
    check_DEBUT( item, name = '_', imported = True )

    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    item = selectItem( "zzzz289f", "Deprecated", "DEBUT" )
    check_DEBUT( item, name = '_', imported = True )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    populate_DEBUT( name = '_' )

    item = selectItem( "Stage_1", "Deprecated", "DEBUT" )
    check_DEBUT( item, name = '_', imported = False )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")

    item = selectItem( "Stage_1", "Deprecated", "DEBUT" )
    check_DEBUT( item, name = '_', imported = True )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
