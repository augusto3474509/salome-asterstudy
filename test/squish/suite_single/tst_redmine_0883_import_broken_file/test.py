#------------------------------------------------------------------------------
# Issue #881 - Operations: Import Stage

#------------------------------------------------------------------------------

def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # Create new study
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    selectObjectBrowserItem( "CurrentCase" )

    #--------------------------------------------------------------------------
    # import broken COMM file
    import os
    comm_file = os.path.join( casedir(), 'broken-zzzz289f.comm' )
    test.log( "Import stage from '%s' file" % comm_file )
    importStage( comm_file )

    #--------------------------------------------------------------------------
    # check that warning is shown that stage cannot be fully converted to graphical mode
    # press 'Cancel' to reject importing COMM file
    test.warning("The file should not be imported in graphnical mode 'strict mode' switched ON!")
    test.verify( "The stage cannot be converted to graphical mode." \
                 in str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ))
    test.compare(waitForObjectExists(":AsterStudy.Show Details..._QPushButton").enabled, True)
    test.compare(waitForObjectExists(":AsterStudy.Yes_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":AsterStudy.No_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":AsterStudy.Cancel_QPushButton").enabled, True)
    clickButton( waitForObject( ":AsterStudy.Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    # check that nothing has been imported
    checkOBItemNotExists( "CurrentCase", "broken-zzzz289f" )

    #--------------------------------------------------------------------------
    # import broken COMM file again
    importStage( comm_file )

    #--------------------------------------------------------------------------
    # check that warning is shown that stage cannot be fully converted to graphical mode
    # press 'No' this time to accept importing COMM file as a text stage
    test.verify( "The stage cannot be converted to graphical mode." \
                 in str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ))
    test.compare(waitForObjectExists(":AsterStudy.Yes_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":AsterStudy.No_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":AsterStudy.Cancel_QPushButton").enabled, True)
    clickButton( waitForObject( ":AsterStudy.No_QPushButton" ) )

    #--------------------------------------------------------------------------
    # check that only one stage item has appeared
    checkOBItem( "CurrentCase", "broken-zzzz289f" )
    checkOBItemNotExists( "CurrentCase", "broken-zzzz289f_1" )

    #--------------------------------------------------------------------------
    # check that stage cannot be converted to graphical mode
    item = showOBItem( "CurrentCase", "broken-zzzz289f" )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    test.compare( str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ),
                  "Cannot convert the stage to graphic mode" )
    clickButton( waitForObject( ":AsterStudy.OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    # undo import stage
    waitForActivateMenuItem( "Edit", "Undo" )
    # check that stage item has been removed
    checkOBItemNotExists( "CurrentCase", "broken-zzzz289f" )
  
    #--------------------------------------------------------------------------
    # import broken COMM file again
    importStage( comm_file )

    #--------------------------------------------------------------------------
    # check that warning is shown that stage cannot be fully converted to graphical mode
    # press 'Yes' this time to accept importing COMM file and splitting it two two parts
    test.verify( "The stage cannot be converted to graphical mode." \
                 in str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ))
    test.compare(waitForObjectExists(":AsterStudy.Yes_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":AsterStudy.No_QPushButton").enabled, True)
    test.compare(waitForObjectExists(":AsterStudy.Cancel_QPushButton").enabled, True)
    clickButton( waitForObject( ":AsterStudy.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    # check that second part cannot be converted to graphical mode
    item = showOBItem( "CurrentCase", "broken-zzzz289f_1" )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    test.compare( str( waitForObjectExists( ":AsterStudy.qt_msgbox_label_QLabel" ).text ),
                  "Cannot convert the stage to graphic mode" )
    clickButton( waitForObject( ":AsterStudy.OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    # convert first part to text mode and then back to graphical mode
    item = showOBItem( "CurrentCase", "broken-zzzz289f" )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    item = showOBItem( "CurrentCase", "broken-zzzz289f" )
    clickItem( ":_QTreeWidget", item, 10, 10, 0, Qt.LeftButton )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    #--------------------------------------------------------------------------
    # exit application
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
