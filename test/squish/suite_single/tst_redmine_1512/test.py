#------------------------------------------------------------------------------
# Issue #1512 - Edition of a command when editing an other command with hide unused checked leads to multiple error message

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add empty stage in current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # add two commands
    setClipboardText("m1 = LIRE_MAILLAGE(UNITE=21)\nm2 = LIRE_MAILLAGE(UNITE=22)\n")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Paste")

    #--------------------------------------------------------------------------
    # open first command for editing
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.m1", "Edit")

    #--------------------------------------------------------------------------
    # open first command for editing
    clickButton(waitForObject(":Hide Unused_QToolButton"))

    #--------------------------------------------------------------------------
    # open second command for editing
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.m2", "Edit")

    #--------------------------------------------------------------------------
    # closing Edit panel
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # exit
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
