#------------------------------------------------------------------------------
# Issue # 1016 - L3 and L1.3 Use case around the file management
#   - crash on reading MED mesh groups

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )

    global_start(noexhook=True)
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    # [project] AsterStudy
    # [scenario] Check 'DEFI_CONTACT' command named 'Cont1'
    # [topic] Checks the command content in the imported stage
    # [tested functionality] 'DEFI_CONTACT' command
    # [summary description] Edit the command and check keyword values
    # [expected results] No errors. No exceptions. Correct validation.

    # [section] New study creation
    test.log( "New study creation" )

    # [step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    # [step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    # [check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    # [step] Import stage
    stage_file_path = os.path.join( casedir(), findFile( "testdata", "Recette_asterstudy.comm" ) )
    importStage( stage_file_path, [":Undefined files.OK_QPushButton"] )

    # [check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkCont1()

    pass

def checkCont1():
    # [section] Check DEFI_CONTACT command named 'Cont1'
    test.log( "Check DEFI_CONTACT command named 'Cont1'" )

    # [step] Double click on 'Cont1' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Recette\\_asterstudy.BC and Load.Cont1", 59, 10, 0, Qt.LeftButton )
    # [check] 'Edit command' panel was opened
    # [check] Command is 'DEFI_CONTACT'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT" )
    # [check] Command name is 'Cont1'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Cont1" )

    # [check] 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Model' )

    # [check] 'CONTINUE' in 'FORMULATION' combobox
    symbol = tr( 'FORMULATION', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'CONTINUE' )

    # [check] 'LISSAGE' checkbox has 'checked' state
    symbol = tr( 'LISSAGE', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    # [check] 'OUI' in 'LISSAGE' checkbox
    symbol = tr( 'LISSAGE-value', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )

    # [check] 'ALGO_RESO_CONT' has 'checked' state
    symbol = tr( 'ALGO_RESO_CONT', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    # [check] 'POINT_FIXE' in 'ALGO_RESO_CONT' combobox
    symbol = tr( 'ALGO_RESO_CONT', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'POINT_FIXE' )

    # [check] 'ITER_CONT_MAXI' checkbox has 'checked' state
    symbol = tr( 'ITER_CONT_MAXI', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    # [check] '15' in 'ITER_CONT_MAXI' field
    symbol = tr( 'ITER_CONT_MAXI', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, '15' )

    # [step] Click 'Edit...' button of 'ZONE' keyword
    clickButton(waitForObject(":ZONE_expand_QToolButton"))
    clickButton(waitForObject(":0_ParameterButton"))

    # [check] Panel of ZONE[0] item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0]" )

    # [check] Check 'GROUP_MA_MAIT' radiobutton has 'checked' state
    symbol = tr( 'GROUP_MA_MAIT', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    # [step] Click 'Cont_mast' button
    symbol = tr( 'GROUP_MA_MAIT', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    # [check] Panel of 'GROUP_MA_MAIT' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0] > GROUP_MA_MAIT" )

    # [check] 'Cont_mast' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Cont_mast" )

    # [step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    # [check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0]" )

    # [check] 'GROUP_MA_ESCL' radiobutton has 'checked' state
    symbol = tr( 'GROUP_MA_ESCL', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    # [step] Click 'Cont_slav' button
    symbol = tr( 'GROUP_MA_ESCL', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    # [check] Panel of 'GROUP_MA_ESCL' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0] > GROUP_MA_ESCL" )

    # [check] 'Cont_slav' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Cont_slav" )

    # [step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    # [check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0]" )

    # [step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    # [check] Panel of 'DEFI_CONTACT' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT" )

    # [step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    # [check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )
    #--------------------------------------------------------------------------
    pass
