#------------------------------------------------------------------------------
# Issue #1609 - Persalys widget

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True) # debug=True is needed to enable Persalys widget

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # add first stage
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # create variables and output command in the first stage
    code = """
stage = AsterStdGui.gui.study().history.current_case[-1]
stage.add_variable('a').update('100')
stage.add_variable('b').update('200')
stage.add_variable('c').update('300')
cmd = stage('IMPR_TABLE')
cmd.init({'FORMAT':'NUMPY','NOM_PARA':'param', 'UNITE':{20:'aaa.txt'},})
"""
    exec_in_console(code)

    #--------------------------------------------------------------------------
    # add second stage
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # create variables and output command in the first stage
    code = """
stage = AsterStdGui.gui.study().history.current_case[-1]
stage.add_variable('x').update('1')
stage.add_variable('y').update('2')
stage.add_variable('z').update('3')
cmd = stage('IMPR_TABLE')
cmd.init({'FORMAT':'NUMPY','NOM_PARA':('p1','p2',), 'UNITE':{30:'bbb.txt'},})
"""
    exec_in_console(code)

    #--------------------------------------------------------------------------
    # activate 'Export to Persalys' command
    activateOBContextMenuItem("CurrentCase", "Export As Parametric")

    test.vp("VP1")
    test.compare(str(waitForObject(":output_cmd_QComboBox").currentText), '')

    #--------------------------------------------------------------------------
    # Make some selection
    waitForObjectItem(":variables_QListWidget", "a")
    clickItem(":variables_QListWidget", "a", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "c")
    clickItem(":variables_QListWidget", "c", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "y")
    clickItem(":variables_QListWidget", "y", 5, 5, 0, Qt.LeftButton)

    combo = waitForObject(":output_cmd_QComboBox")
    combo.setCurrentIndex(0)

    test.vp("VP2")

    #--------------------------------------------------------------------------
    # close Persalys widget, without saving
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # reopen Persalys widget
    activateOBContextMenuItem("CurrentCase", "Export As Parametric")

    test.vp("VP1")
    test.compare(str(waitForObject(":output_cmd_QComboBox").currentText), '')

    #--------------------------------------------------------------------------
    # Make some selection
    waitForObjectItem(":variables_QListWidget", "a")
    clickItem(":variables_QListWidget", "a", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "c")
    clickItem(":variables_QListWidget", "c", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "y")
    clickItem(":variables_QListWidget", "y", 5, 5, 0, Qt.LeftButton)

    combo = waitForObject(":output_cmd_QComboBox")
    combo.setCurrentIndex(0)

    test.vp("VP2")

    #--------------------------------------------------------------------------
    # close Persalys widget, saving user choice
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # reopen Persalys widget
    activateOBContextMenuItem("CurrentCase", "Export As Parametric")

    #--------------------------------------------------------------------------
    # check that data was kept from previous run
    test.vp("VP2")
    test.compare(str(waitForObject(":output_cmd_QComboBox").currentText), 'aaa.txt')

    #--------------------------------------------------------------------------
    # Make another selection
    waitForObjectItem(":variables_QListWidget", "a")
    clickItem(":variables_QListWidget", "a", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "b")
    clickItem(":variables_QListWidget", "b", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "c")
    clickItem(":variables_QListWidget", "c", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "x")
    clickItem(":variables_QListWidget", "x", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "y")
    clickItem(":variables_QListWidget", "y", 5, 5, 0, Qt.LeftButton)
    waitForObjectItem(":variables_QListWidget", "z")
    clickItem(":variables_QListWidget", "z", 5, 5, 0, Qt.LeftButton)

    combo = waitForObject(":output_cmd_QComboBox")
    combo.setCurrentIndex(1)

    test.vp("VP3")

    #--------------------------------------------------------------------------
    # close Persalys widget, saving user choice
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # reopen Persalys widget
    activateOBContextMenuItem("CurrentCase", "Export As Parametric")

    #--------------------------------------------------------------------------
    # check that data was kept from previous run
    test.vp("VP3")
    test.compare(str(waitForObject(":output_cmd_QComboBox").currentText), 'bbb.txt')

    #--------------------------------------------------------------------------
    # close Persalys widget
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # undo last action
    activateMenuItem("Edit", "Undo")

    #--------------------------------------------------------------------------
    # reopen Persalys widget
    activateOBContextMenuItem("CurrentCase", "Export As Parametric")

    #--------------------------------------------------------------------------
    # check that data was kept as after first invokation
    test.vp("VP2")
    test.compare(str(waitForObject(":output_cmd_QComboBox").currentText), 'aaa.txt')

    #--------------------------------------------------------------------------
    # close Persalys widget
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # undo last action
    activateMenuItem("Edit", "Undo")

    #--------------------------------------------------------------------------
    # reopen Persalys widget
    activateOBContextMenuItem("CurrentCase", "Export As Parametric")

    #--------------------------------------------------------------------------
    # check that data was kept as after first invokation
    test.vp("VP1")
    test.compare(str(waitForObject(":output_cmd_QComboBox").currentText), '')

    #--------------------------------------------------------------------------
    # close Persalys widget
    clickButton(waitForObject(":Cancel_QPushButton"))

    #--------------------------------------------------------------------------
    # exit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
