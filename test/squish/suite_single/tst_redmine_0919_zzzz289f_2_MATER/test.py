#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'ECRO_LINE' )

    symbol = tr( 'ECRO_LINE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'ECRO_LINE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'SY', 'QLineEdit' )
    setParameterValue( symbol, str( 3.0 ) )

    symbol = tr( 'D_SIGM_EPSI', 'QLineEdit' )
    setParameterValue( symbol, str( -1950.0 ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'ELAS' )

    symbol = tr( 'ELAS', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'ELAS', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'E', 'QLineEdit' )
    setParameterValue( symbol, str( 30000.0 ) )

    symbol = tr( 'NU', 'QLineEdit' )
    setParameterValue( symbol, str( 0.2 ) )

    symbol = tr( 'RHO', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'RHO', 'QLineEdit' )
    setParameterValue( symbol, str( 2764.0 ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'ECRO_LINE' )

    symbol = tr( 'ECRO_LINE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'ECRO_LINE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'SY', 'QLineEdit' )
    test.compare( float( str( waitForObject( symbol ).text ) ), 3.0 )

    symbol = tr( 'D_SIGM_EPSI', 'QLineEdit' )
    test.compare( float( str( waitForObject( symbol ).text ) ), -1950.0 )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":_QLineEdit", 'ELAS' )

    symbol = tr( 'ELAS', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'ELAS', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'E', 'QLineEdit' )
    test.compare( float( str( waitForObject( symbol ).text ) ), 30000.0 )

    symbol = tr( 'NU', 'QLineEdit' )
    test.compare( float( str( waitForObject( symbol ).text ) ), 0.2 )

    symbol = tr( 'RHO', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'RHO', 'QLineEdit' )
    test.compare( float( str( waitForObject( symbol ).text ) ), 2764.0 )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    item = selectItem( "zzzz289f", "Material", "MATER" )
    checkContents( item, name = 'MATER' )

    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    item = selectItem( "zzzz289f", "Material", "MATER" )
    checkContents( item, name = 'MATER' )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    populateContents( "Material", "DEFI_MATERIAU", "MATER", "Stage_1", "Material", "DEFI_MATERIAU" )

    item = selectItem( "Stage_1", "Material", "MATER" )
    checkContents( item, name = 'MATER' )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")

    item = selectItem( "Stage_1", "Material", "MATER" )
    checkContents( item, name = 'MATER' )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
