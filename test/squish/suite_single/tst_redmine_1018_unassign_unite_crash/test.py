#------------------------------------------------------------------------------
# Issue #1018 - Unassigning file from a UNITE leads to crash

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1" )
    addCommandByDialog( "PRE_GMSH", "Other" )
    test.compare( isItemExists( "Stage_1", "Other", "PRE_GMSH" ), True )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Other", "PRE_GMSH" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_GMSH', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    gmsh_file = os.path.join( casedir(), 'gmsh.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", gmsh_file )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( gmsh_file ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    maillage_file = os.path.join( casedir(), 'gmsh.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", maillage_file )
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )
    clickButton( waitForObject( ":Select_File_Yes_QPushButton" ) )

    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( maillage_file ) )

    clickButton( waitForObject( ":Apply_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
