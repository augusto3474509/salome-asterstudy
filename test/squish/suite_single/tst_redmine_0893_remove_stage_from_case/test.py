#------------------------------------------------------------------------------
# Issue #881 - Operations: Import Stage

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

def checkStageContent( stage, categories ):
    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", stage )

    for category in categories:
        showOBItem( "CurrentCase", stage, category )

    checkValidity( True, "CurrentCase", stage )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    selectObjectBrowserItem( "CurrentCase" )

    #--------------------------------------------------------------------------
    import os
    stagename = 'rccm01b'
    filename = os.path.join( casedir(), "%s.comm" % stagename )
    test.log( "Use '%s' file to create a new stage" % filename )

    importStage( filename, [":Undefined files.OK_QPushButton"] )

    activateOBContextMenuItem("CurrentCase."+stagename, "Rename")
    stagename += "_m"
    type(waitForObject(":_QExpandingLineEdit"), stagename)
    type(waitForObject(":_QExpandingLineEdit"), "<Return>")

    checkStageContent( stagename, ['Functions and Lists', 'Material'] )

    #--------------------------------------------------------------------------
    import os
    for id in [0, 1, 2, 3]:
        stagename = 'rccm01b'
        filename = os.path.join( casedir(), "%s.com%d" % (stagename, id) )
        test.log( "Use '%s' file to create a new stage" % filename )

        importStage( filename, [":Undefined files.OK_QPushButton"] ) # if id == 3 else []

        activateOBContextMenuItem("CurrentCase."+stagename, "Rename")
        stagename += "_%d"%id
        lineedit = "{columnIndex='0' container=':_QTreeWidget' rowIndex='%d' type='QExpandingLineEdit' unnamed='1' visible='1'}"%(id+1)
        type(waitForObject(lineedit), stagename)
        type(waitForObject(lineedit), "<Return>")
        checkStageContent( stagename, [] )

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase" )

    showOBIndex( "CurrentCase", "rccm01b_2" )
    activateOBContextMenuItem("CurrentCase.rccm01b_2", "Delete")
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )
    clickButton( waitForObject(":Delete.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkOBItemNotExists( "CurrentCase", "rccm01b_3" )
    checkOBItemNotExists( "CurrentCase", "rccm01b_2" )
    checkValidity( True, "CurrentCase", "rccm01b_1" )
    checkValidity( True, "CurrentCase", "rccm01b_m" )
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
