def main():
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "EXEC_LOGICIEL", "Other" )
    showOBItem( "CurrentCase", "Stage_1", "Other", "EXEC_LOGICIEL" )

    # Edit of command --------------------------------------------------------------------------
    waitForObjectItem(":_QTreeWidget", "CurrentCase.Stage\\_1.Other.EXEC\\_LOGICIEL")
    doubleClickItem(":_QTreeWidget", "CurrentCase.Stage\\_1.Other.EXEC\\_LOGICIEL", 59, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":Close_QPushButton"))

    # Collapse and expand of category --------------------------------------------------------------------------
    waitForObjectItem(":_QTreeWidget", "CurrentCase.Stage\\_1.Other")
    doubleClickItem(":_QTreeWidget", "CurrentCase.Stage\\_1.Other", 36, 7, 0, Qt.LeftButton)
    doubleClickItem(":_QTreeWidget", "CurrentCase.Stage\\_1.Other", 36, 7, 0, Qt.LeftButton)
    test.verify(isItemExpanded("CurrentCase:1", "Stage_1:2", "Other:-1"))

    # Rename of stage --------------------------------------------------------------------------
    waitForObjectItem(":_QTreeWidget", "CurrentCase.Stage\\_1")
    doubleClickItem(":_QTreeWidget", "CurrentCase.Stage\\_1", 35, 8, 0, Qt.LeftButton)
    doubleClickItem(":_QTreeWidget", "CurrentCase.Stage\\_1", 35, 8, 0, Qt.LeftButton)
    test.verify(isItemExpanded("CurrentCase:1", "Stage_1:2"))

    # Collapse of case --------------------------------------------------------------------------
    waitForObjectItem(":_QTreeWidget", "CurrentCase")
    doubleClickItem(":_QTreeWidget", "CurrentCase", 41, 6, 0, Qt.LeftButton)
    test.xverify(isItemExpanded("CurrentCase:1"))

    # Expand of case --------------------------------------------------------------------------
    waitForObjectItem(":_QTreeWidget", "CurrentCase")
    doubleClickItem(":_QTreeWidget", "CurrentCase", 41, 6, 0, Qt.LeftButton)
    test.verify(isItemExpanded("CurrentCase:1"))

    # Switch to text mode --------------------------------------------------------------------------
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_1", 35, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Text Mode"))
    clickButton(waitForObject(":AsterStudy.Yes_QPushButton"))

    # Edit of stage in text editor --------------------------------------------------------------------------
    waitForObjectItem(":_QTreeWidget", "CurrentCase.Stage\\_1")
    doubleClickItem(":_QTreeWidget", "CurrentCase.Stage\\_1", 46, 11, 0, Qt.LeftButton)
    clickButton(waitForObject(":Close_QPushButton"))

    # Collapse of case --------------------------------------------------------------------------
    waitForObjectItem(":_QTreeWidget", "CurrentCase")
    doubleClickItem(":_QTreeWidget", "CurrentCase", 41, 6, 0, Qt.LeftButton)
    test.xverify(isItemExpanded("CurrentCase:1"))
