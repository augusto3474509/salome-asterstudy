#------------------------------------------------------------------------------
# Issue #1192 - Automatically select single choice for mandatory keyword as default value
# - (bug) Issue #1279 - Automatically selected combobox item (into case) when only one choice available.

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start()

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # add empty stage in current case
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    # add 'POST_K_TRANS' command
    activateOBContextMenuItem("CurrentCase.Stage_1", "Fracture and Fatigue", "POST_K_TRANS")
    # check keyword's choices: '<no object selected>' (no choices)
    check_keyword_cmd('<no object selected>')

    #--------------------------------------------------------------------------
    # add 'DEFI_FOND_FISS' commands
    activateOBContextMenuItem("CurrentCase.Stage_1", "Fracture and Fatigue", "DEFI_FOND_FISS")
    # check keyword's choices: 'crack' - first suitable item (the only available)
    check_keyword_cmd('crack')

    #--------------------------------------------------------------------------
    # add another 'DEFI_FOND_FISS' commands
    activateOBContextMenuItem("CurrentCase.Stage_1", "Fracture and Fatigue", "DEFI_FOND_FISS")
    # check keyword's choices: '<no object selected>' - there are two items
    check_keyword_cmd('<no object selected>')

    #--------------------------------------------------------------------------
    # delete one 'DEFI_FOND_FISS' command
    activateOBContextMenuItem("CurrentCase.Stage_1.Fracture and Fatigue.crack0", "Delete")
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    # check keyword's choices: 'crack' - first suitable item (the only available)
    check_keyword_cmd('crack')

    #--------------------------------------------------------------------------
    # undo delete 
    waitForActivateMenuItem("Edit", "Undo")
    # check keyword's choices: '<no object selected>' - there are two items
    check_keyword_cmd('<no object selected>')
    
    #--------------------------------------------------------------------------
    # delete another 'DEFI_FOND_FISS' command
    activateOBContextMenuItem("CurrentCase.Stage_1.Fracture and Fatigue.crack", "Delete")
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    # check keyword's choices: 'crack' - first suitable item (the only available)
    check_keyword_cmd('crack0')

    #--------------------------------------------------------------------------
    # delete remaining 'DEFI_FOND_FISS' command
    activateOBContextMenuItem("CurrentCase.Stage_1.Fracture and Fatigue.crack0", "Delete")
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    # check keyword's choices: '<no object selected>' (no choices)
    check_keyword_cmd('<no object selected>')

    #--------------------------------------------------------------------------
    # add 'AFFE_MODELE' command
    activateOBContextMenuItem("CurrentCase.Stage_1", "Model Definition", "AFFE_MODELE")
    check_keyword_simple("OUI")

    #--------------------------------------------------------------------------
    # quit application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass

def check_keyword_cmd(value):
    #--------------------------------------------------------------------------
    # open 'POST_K_TRANS' command for editing
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Fracture and Fatigue.table", "Edit")
    # check that keyword's value selector shows expected value
    clickButton( waitForObject( tr( 'K_MODAL', 'QPushButton' ) ) )
    clickButton( waitForObject( tr( 'FOND_FISS', 'QRadioButton' ) ) )
    test.compare( str( waitForObjectExists( tr( 'FOND_FISS', 'QComboBox' ) ).currentText ), value )
    # close Edit panel
    clickButton(waitForObject(":Abort_QPushButton"))
    clickButton(waitForObject(":Abort.Yes_QPushButton"))
    #--------------------------------------------------------------------------
    pass

def check_keyword_simple(value):
    #--------------------------------------------------------------------------
    # open 'AFFE_MODELE' command for editing
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Model Definition.model", "Edit")
    clickButton( waitForObject( tr( 'AFFE', 'QCheckBox' ) ) )
    # check that keyword's value selector shows expected value
    clickButton(waitForObject(":AFFE_add_QToolButton"))
    clickButton(waitForObject(":0_ParameterButton"))
    test.compare( str( waitForObjectExists( tr( 'TOUT', 'QComboBox' ) ).currentText ), "OUI" )
    # close Edit panel
    clickButton(waitForObject(":Abort_QPushButton"))
    clickButton(waitForObject(":Abort.Yes_QPushButton"))
    #--------------------------------------------------------------------------
    pass
