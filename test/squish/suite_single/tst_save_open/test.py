def main():
    source( findFile( "scripts", "common.py" ) )
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    activateMenuItem( "Commands", "Fracture and Fatigue", "CALC_G" )
    showOBItem( "CurrentCase", "Stage_1", "Fracture and Fatigue", "table" )

    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    activateMenuItem( "Commands", "Analysis", "Dynamics", "DYNA_VIBRA" )
    showOBItem( "CurrentCase", "Stage_1", "Analysis", "resharm" )

    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "MACR_RECAL", "Other" )
    showOBItem( "CurrentCase", "Stage_1", "Other", "listr" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_2", 10, 10, 0, Qt.LeftButton )
    activateMenuItem( "Commands", "Material", "DEFI_MATERIAU" )
    showOBItem( "CurrentCase", "Stage_2", "Material", "mater" )

    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_2", 10, 10, 0, Qt.LeftButton )
    activateMenuItem( "Commands", "Functions and Lists", "DEFI_FONCTION" )
    showOBItem( "CurrentCase", "Stage_2", "Functions and Lists", "func" )

    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_2", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "MACR_SPECTRE", "Other" )
    showOBItem( "CurrentCase", "Stage_2", "Other", "table0" )

    #--------------------------------------------------------------------------
    filename = "tst_save_open"
    filepath = saveFile( filename )
    test.log( 'Saving %s' % filepath )
    waitFor( "object.exists(':AsterStudy *.Save_QToolButton')", 20000 )
    test.compare( findObject( ":AsterStudy *.Save_QToolButton" ).enabled, False )

    #--------------------------------------------------------------------------
    filepath += '.ajs'
    test.log( 'Restoring %s' % filepath )

    closeFile()
    openFile( filepath )

    clickButton(waitForObject(":AsterStudy.OK_QPushButton"))
    #clickButton(waitForObject(":Missing study directory.OK_QPushButton"))
    #clickButton(waitForObject(":Inconsistent study directory.OK_QPushButton"))

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase" )

    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "Stage_1" )
    showOBItem( "CurrentCase", "Stage_1", "Fracture and Fatigue", "table" )
    showOBItem( "CurrentCase", "Stage_1", "Analysis", "resharm" )
    showOBItem( "CurrentCase", "Stage_1", "Other", "listr" )

    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "Stage_2" )
    showOBItem( "CurrentCase", "Stage_2", "Material", "mater" )
    showOBItem( "CurrentCase", "Stage_2", "Functions and Lists", "func" )
    showOBItem( "CurrentCase", "Stage_2", "Other", "table0" )

    #--------------------------------------------------------------------------
    pass
