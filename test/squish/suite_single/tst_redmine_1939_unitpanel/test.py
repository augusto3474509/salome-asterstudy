from os.path import dirname

def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem( "Operations", "Add Stage" )

    mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 73, 6, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Mesh.mesh"), 7, 12, 0, Qt.LeftButton)
    clickButton(waitForObject(":UNITE_QPushButton"))
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + "/1.txt")
    type(waitForObject(":fileNameEdit_QLineEdit"), "<Return>")
    clickButton(waitForObject(":OK_QPushButton"))

    mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 73, 6, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Mesh.mesh0"), 7, 12, 0, Qt.LeftButton)
    clickButton(waitForObject(":UNITE_QPushButton"))
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + "/2.txt")
    type(waitForObject(":fileNameEdit_QLineEdit"), "<Return>")
    clickButton(waitForObject(":OK_QPushButton"))

    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    
    # leave unmodified, close
    doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 52, 9, 0, Qt.LeftButton)
    
    # replace file, try closing
    doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
    clickButton(waitForObject(":Filename.Filename_QToolButton"))
    type(waitForObject(":fileNameEdit_QLineEdit"), "<Ctrl+A>")
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + "/3.txt")
    type(waitForObject(":_QListView"), "<Return>")
    doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 52, 9, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
    
    # replace file, replace it back, close
    doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
    clickButton(waitForObject(":Filename.Filename_QToolButton"))
    type(waitForObject(":fileNameEdit_QLineEdit"), "<Ctrl+A>")
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + "/3.txt")
    type(waitForObject(":_QListView"), "<Return>")
    clickButton(waitForObject(":Filename.Filename_QToolButton"))
    type(waitForObject(":fileNameEdit_QLineEdit"), "<Ctrl+A>")
    type(waitForObject(":fileNameEdit_QLineEdit"), dirname(__file__) + "/2.txt")
    type(waitForObject(":_QListView"), "<Return>")
    doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.1\\.txt"), 52, 9, 0, Qt.LeftButton)
    
    # one more action, to be sure we are not seeing a dialog now
    doubleClick(waitForObjectItem(":DataFiles_QTreeView", "Stage\\_1.2\\.txt"), 46, 9, 0, Qt.LeftButton)
