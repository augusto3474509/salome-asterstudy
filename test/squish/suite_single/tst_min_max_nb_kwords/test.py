def main():
    source( findFile( "scripts", "common.py" ) )
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateMenuItem("Operations", "Add Stage")
    checkOBItem( "CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    activateMenuItem("Commands", "Model Definition", "AFFE_CARA_ELEM")
    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "AFFE_CARA_ELEM")
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Model Definition.AFFE_CARA_ELEM", "Edit")

    #--------------------------------------------------------------------------
    #    max=2
    #--------------------------------------------------------------------------
    clickButton(waitForObject(":RIGI_PARASOL_QCheckBox"))
    clickButton(waitForObject(":RIGI_PARASOL_QPushButton"))
    clickButton(waitForObject(":Add item_QPushButton"))
    clickButton(waitForObject(":0_QPushButton"))
    clickButton(waitForObject(":CARA_QPushButton"))
    clickButton(waitForObject(":Add item_QPushButton"))
    clickButton(waitForObject(":Add item_QPushButton"))
    test.compare(waitForObjectExists(":Add item_QPushButton").enabled, False, )
    test.compare(waitForObjectExists(":Remove_Item_QToolButton").enabled, True)
    clickButton(waitForObject(":Remove_Item_QToolButton"))
    test.compare(waitForObjectExists(":Add item_QPushButton").enabled, True)
    clickButton(waitForObject(":Cancel_QPushButton"))

    clickButton(waitForObject(":Close.Yes_QPushButton"))
    #--------------------------------------------------------------------------
    #    min=2, max=3
    #--------------------------------------------------------------------------

    clickButton( waitForObject( tr( 'COOR_CENTRE', 'QRadioButton' ) ) )
    clickButton(waitForObject(":COOR_CENTRE_QPushButton"))
    test.compare(waitForObjectExists(":Remove_Item_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":Remove_Item_QToolButton_2").enabled, False)
    test.compare(waitForObjectExists(":Add item_QPushButton").enabled, True)
    clickButton(waitForObject(":Add item_QPushButton"))
    test.compare(waitForObjectExists(":Add item_QPushButton").enabled, False)
    test.compare(waitForObjectExists(":Remove_Item_QToolButton").enabled, True)
    test.compare(waitForObjectExists(":Remove_Item_QToolButton_2").enabled, True)
    test.compare(waitForObjectExists(":Remove_Item_QToolButton_3").enabled, True)
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    #    min=3, max=3
    #--------------------------------------------------------------------------

    clickButton(waitForObject(":COQUE_QCheckBox"))
    clickButton(waitForObject(":COQUE_QPushButton"))
    clickButton(waitForObject(":Add item_QPushButton"))
    clickButton(waitForObject(":0_QPushButton"))
    clickButton(waitForObject(":VECTEUR_QCheckBox"))
    clickButton(waitForObject(":VECTEUR_QPushButton"))
    test.compare(waitForObjectExists(":Add item_QPushButton").enabled, False)
    test.compare(waitForObjectExists(":Remove_Item_QToolButton").enabled, False)
    test.compare(waitForObjectExists(":Remove_Item_QToolButton_2").enabled, False)
    test.compare(waitForObjectExists(":Remove_Item_QToolButton_3").enabled, False)

    test.compare(waitForObject(":_ParameterTitle").text, "AFFE_CARA_ELEM > COQUE > [0] > VECTEUR")

    for i in range(3):
        setInputFieldValue("{container=':qt_tabwidget_stackedwidget_QWidget' name='%d' type='QLineEdit' visible='1'}" % i, "%d" % i)

    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))

    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))

    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))

    #--------------------------------------------------------------------------
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Model Definition.AFFE_CARA_ELEM", "Edit")

    test.verify(waitForObject(":COQUE_QCheckBox").checked)
    clickButton(waitForObject(":COQUE_QPushButton"))
    clickButton(waitForObject(":0_QPushButton"))
    test.verify(waitForObject(":VECTEUR_QCheckBox").checked)
    clickButton(waitForObject(":VECTEUR_QPushButton"))
    test.compare(waitForObject(":_ParameterTitle").text, "AFFE_CARA_ELEM > COQUE > [0] > VECTEUR")
    for i in range(3):
        text = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' name='%d' type='QLineEdit' visible='1'}" % i).text
        test.compare(text.toFloat(), i)
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Cancel_QPushButton"))

    test.xverify(waitForObject(":RIGI_PARASOL_QCheckBox").checked)
    waitForObject(":RIGI_PARASOL_QCheckBox").setChecked(True)
    clickButton(waitForObject(":RIGI_PARASOL_QPushButton"))
    test.verify(object.exists(":Add item_QPushButton"))
    test.xverify(object.exists(":0_QPushButton"))

    clickButton(waitForObject(":Cancel_QPushButton"))
    clickButton(waitForObject(":Close_QPushButton"))
    clickButton(waitForObject(":Close.Yes_QPushButton"))

    #--------------------------------------------------------------------------

    activateItem(waitForObjectItem(":AsterStudy_QMenuBar", "File"))
    activateItem(waitForObjectItem(":File_QMenu", "Exit"))
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))
