#------------------------------------------------------------------------------
# Issue #892 - Remove Command in use from a Stage

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContens():
    #--------------------------------------------------------------------------
    showOBItem( "CurrentCase", "zzzz289f" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh" )
    showOBItem( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "MATER" )
    showOBItem( "CurrentCase", "zzzz289f", "Material" )
    showOBItem( "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    showOBItem( "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    selectObjectBrowserItem( "CurrentCase" )

    #--------------------------------------------------------------------------
    import os
    comm_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % comm_file )

    #--------------------------------------------------------------------------
    importStage( comm_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    checkContens()

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "MATER" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    item, index = showOBIndex( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton(waitForObject(":Delete.Yes_QPushButton"))

    #--------------------------------------------------------------------------
    checkOBItemNotExists( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "MATER" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Undo_QToolButton" ) )

    checkValidity( True, "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "MATER" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Redo_QToolButton" ) )

    checkOBItemNotExists( "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "MATER" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
