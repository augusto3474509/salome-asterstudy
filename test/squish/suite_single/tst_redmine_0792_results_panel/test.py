#------------------------------------------------------------------------------
# Issue # 792 - L3.1. "Results" panel

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1" )
    addCommandByDialog( "PRE_GMSH", "Other" )
    test.compare( isItemExists( "Stage_1", "Other", "PRE_GMSH" ), True )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Other", "PRE_GMSH" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_GMSH', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    gmsh_file = os.path.join( casedir(), 'gmsh.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", gmsh_file )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE_GMSH', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( gmsh_file ) )

    clickButton( waitForObject( ":Apply_QPushButton" ) )

    clickButton(waitForObject(":Yes_QPushButton"))

    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp( "DataFiles" )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    maillage_file = os.path.join( casedir(), 'maillage.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", maillage_file )
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )
    clickButton( waitForObject( ":Select_File_Yes_QPushButton" ) )
    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( maillage_file ) )

    clickButton( waitForObject( ":Apply_QPushButton" ) )

    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp( "DataFiles-1" )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_MAILLAGE', 'QComboBox' )
    setParameterValue( symbol, os.path.basename( gmsh_file ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp( "DataFiles-4" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.19" )
    doubleClickItem( ":DataFiles_QTreeView", "Stage\\_1.19", 10, 10, 0, Qt.LeftButton )

    test.vp( "EditDialog" )

    test.compare( str( waitForObjectExists( ":Filename.Filename_QComboBox" ).currentText ), "gmsh.file" )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1.19" )
    doubleClickItem( ":DataFiles_QTreeView", "Stage\\_1.19", 10, 10, 0, Qt.LeftButton )

    test.vp( "EditDialog-2" )

    clickButton( waitForObject( ":Filename.Filename_QToolButton" ) )

    maillage_file = os.path.join( casedir(), 'maillage.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", maillage_file )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    test.vp( "EditDialog-3" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp( "DataFiles-6" )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )

    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp( "DataFiles-7" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
