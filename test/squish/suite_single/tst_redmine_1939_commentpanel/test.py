def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem( "Operations", "Add Stage" )
    
    activateOBContextMenuItem("CurrentCase.Stage_1", "Functions and Lists", "DEFI_FONCTION")
    
    # leave empty, close
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 90, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 63, 12, 0, Qt.LeftButton)
    
    # enter comment, try closing
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 90, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))
    type(waitForObject(":text_editor_QPlainTextEdit"), "Aaa.")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 63, 12, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
    
    # enter comment, save, close
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 90, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))
    type(waitForObject(":text_editor_QPlainTextEdit"), "Aaa.")
    clickButton(waitForObject(":Apply_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 63, 12, 0, Qt.LeftButton)
    
    # leave unmodified, close
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 90, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 63, 12, 0, Qt.LeftButton)
    
    # edit comment, try closing
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 90, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))
    type(waitForObject(":text_editor_QPlainTextEdit"), "Bbb.")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 63, 12, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
    
    # edit comment, save, close
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 90, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))
    type(waitForObject(":text_editor_QPlainTextEdit"), "Bbb.")
    clickButton(waitForObject(":Apply_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 63, 12, 0, Qt.LeftButton)
    
    # one more action, to be sure we are not seeing a dialog now
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Functions and Lists.func"), 90, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))
