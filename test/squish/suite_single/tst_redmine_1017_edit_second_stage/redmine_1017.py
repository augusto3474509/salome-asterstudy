from asterstudy.gui.asterstdgui import AsterStdGui

gui = AsterStdGui.gui

if gui.study() is None:
    gui.newStudy()

s1 = gui.study().history.current_case.create_stage("Stage 1")
s2 = gui.study().history.current_case.create_stage("Stage 2")

s2.use_text_mode()
s1.use_text_mode()

gui.update()
