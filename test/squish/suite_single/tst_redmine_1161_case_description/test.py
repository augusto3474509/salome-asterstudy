#------------------------------------------------------------------------------
# Issue # 1161 - History view: back-up/restore Current Case
# Check Edit Description feature

def main():
    source(findFile("scripts", "common.py"))
    global_start()


def cleanup():
    global_cleanup()


def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem("File", "New")
    activateMenuItem("Operations", "Back Up")
    activateMenuItem("Operations", "Back Up")

    #--------------------------------------------------------------------------
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1", 10, 10, 0, Qt.LeftButton)
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_2", 10, 10, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Edit Description"))
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("this is backup case 1")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_2", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Edit Description"))
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("and this is backup case 2")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Edit Description"))
    test.verify(waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is backup case 1")
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_2", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Edit Description"))
    test.verify(waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "and this is backup case 2")
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "Backup Cases.BackupCase\\_1", 10, 10, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Backup Cases.BackupCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "View Case (read-only)"))
    activateOBContextMenuItem("BackupCase_1", "Edit Description")
    test.verify(waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is backup case 1")
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("backup case number 1")
    clickButton(waitForObject(":OK_QPushButton"))
    activateOBContextMenuItem("BackupCase_1", "Edit Description")
    test.verify(waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "backup case number 1")
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    doubleClickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    waitForActivateMenuItem("File", "Save")
    import os
    study = os.path.join(testdir(), "tst_redmine_1161_case_description.ajs" )
    setParameterValue(":fileNameEdit_QLineEdit", study)
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 10, 10, 0, Qt.LeftButton)
    use_option('Stage_1', Execute)
    clickButton(waitForObject(":Run_QPushButton_2"))
#     clickButton(waitForObject(":DashboardRunDialog.run_QPushButton"))
    snooze(1)

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Edit Description"))
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("this is run case 1")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Edit Description"))
    test.verify(waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is run case 1")
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    activateMenuItem("File", "Exit")

    #--------------------------------------------------------------------------
    pass
