#------------------------------------------------------------------------------
# Issue #1149 - Show All dialog: automatically activate Edit operation when command is added

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(nativenames=False, basicnaming=False)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # [section] Create New study
    # [step] Menu File - New
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # [section] Create empty stage in current case
    # [step] Switch to current case
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    # [step] Select CurrentCase object in Data Settings panel, invoke "Add Stage" item from popup menu
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    # [section] Add command from Show All dialog with "auto-edit" feature switched OFF
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Show All" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Show All")
    # [step] Show All dialog: select "Mesh" / "Read a mesh" item
    waitForObjectItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)")
    clickItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)", 103, 7, 0, Qt.LeftButton)
    # [step] Show All dialog: press Apply button.
    # [step] Check that dialog is not closed.
    clickButton(waitForObject(":Apply_QPushButton"))
    # [step] Show All dialog: press OK button.
    # [step] Check that dialog is closed but Edit operation is not activated.
    clickButton(waitForObject(":OK_QPushButton"))
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Show All" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Show All")
    # [step] Show All dialog: double-click "Mesh" / "Read a mesh" item.
    # [step] Check that dialog is closed but Edit operation is not activated.
    waitForObjectItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)")
    doubleClickItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)", 84, 7, 0, Qt.LeftButton)

    #--------------------------------------------------------------------------
    # [section] Activate auto-edit feature
    # [step] Menu Edit - Preferences
    activateMenuItem("File", "Preferences...")
    # [step] Switch ON "Automatically activate command edition" check box
    clickTab(waitForObject(":Preferences.Dialog_tab_QTabWidget"), "Interface")
    clickButton(waitForObject(":Preferences_Interface.Preferences_check_auto_edit_QCheckBox"))
    # [step] Preferences dialog: press OK button
    clickButton(waitForObject(":Preferences.Dialog_ok_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Add command from Show All dialog with "auto-edit" feature switched ON
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Show All" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Show All")
    # [step] Show All dialog: select "Mesh" / "Read a mesh" item
    waitForObjectItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)")
    clickItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)", 103, 7, 0, Qt.LeftButton)
    # [step] Show All dialog: press Apply button.
    # [step] Check that dialog is not closed.
    clickButton(waitForObject(":Apply_QPushButton"))
    # [step] Show All dialog: press OK button.
    # [step] Check that dialog is closed and Edit operation is automatically activated.
    clickButton(waitForObject(":OK_QPushButton"))
    # [step] Edit command dialog: press Close button.
    clickButton(waitForObject(":Close_QPushButton"))
    # [step] Select Stage_1 object in Data Settings panel, invoke popup menu, choose "Show All" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Show All")
    # [step] Show All dialog: double-click "Mesh" / "Read a mesh" item.
    # [step] Check that dialog is closed and Edit operation is automatically activated.
    waitForObjectItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)")
    doubleClickItem(":Mesh [5 items]_QListWidget", "Read a mesh (LIRE\\_MAILLAGE)", 84, 7, 0, Qt.LeftButton)
    # [step] Edit command dialog: press Close button.
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Quit application
    # [step] Menu File - Exit
    activateMenuItem("File", "Exit")
    # [step] Press "Discard" button in confirmation dialog
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
