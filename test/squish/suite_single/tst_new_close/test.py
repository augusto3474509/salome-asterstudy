
def main():
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, basicnaming=False)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    checkState()
    clickButton( waitForObject( ":AsterStudy.Close_QToolButton" ) )

    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    checkState()
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )
    activateMenuItem( "File", "Close" )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    waitForActivateMenuItem( "File", "New" )
    checkState()
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    checkState()
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )
    clickButton( waitForObject( ":AsterStudy.Close_QToolButton" ) )
    clickButton( waitForObject( ":Close active study.Save_QPushButton" ) )
    clickButton( waitForObject( ":QFileDialog.Cancel_QPushButton" ) )
    clickButton( waitForObject( ":AsterStudy.Close_QToolButton" ) )
    clickButton( waitForObject( ":AsterStudy.Cancel_QPushButton" ) )
    clickButton( waitForObject( ":AsterStudy.Close_QToolButton" ) )

    clickButton( waitForObject( ":Close active study.Save_QPushButton" ) )
    filepath = os.path.normpath( os.path.join( testdir(), "tst_new_close.ajs" ) )
    setInputFieldValue(":fileNameEdit_QLineEdit", filepath)
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    snooze( 1 )
    test.xverify( findObject( ":AsterStudy *.Save_QToolButton" ).enabled )
    test.xverify( findObject( ":AsterStudy.Close_QToolButton" ).enabled )

def checkState():
    test.verify( waitForObject( ":qt_tabwidget_tabbar.Case View_TabItem" ).enabled )
    test.xverify( findObject( ":AsterStudy *.Save_QToolButton" ).enabled, False )
    test.verify( findObject( ":AsterStudy.Close_QToolButton" ).enabled )
