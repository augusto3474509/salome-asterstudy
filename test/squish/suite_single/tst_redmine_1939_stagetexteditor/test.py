def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    waitForActivateMenuItem( "File", "New" )
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem( "Operations", "Add Stage" )
    activateMenuItem( "Operations", "Add Stage" )
    
    # prepare variables
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.2"), 21, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Create Variable"))
    setParameterValue(":Name_QLineEdit_2", "a")
    setParameterValue(":Expression_QLineEdit", "'a'")
    clickButton(waitForObject(":OK_QPushButton"))
    openContextMenu(waitForObjectItem(":_SettingsView", "CurrentCase.2"), 21, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Create Variable"))
    setParameterValue(":Name_QLineEdit_2", "b")
    setParameterValue(":Expression_QLineEdit", "'b'")
    clickButton(waitForObject(":OK_QPushButton"))
    
    # switch to text mode
    mouseClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 65, 4, 0, Qt.LeftButton)
    clickButton(waitForObject(":AsterStudy *.Text Mode_QToolButton"))
    mouseClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2"), 65, 4, 0, Qt.LeftButton)
    clickButton(waitForObject(":AsterStudy *.Text Mode_QToolButton"))
    
    # leave everything unmodified, close
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 67, 10, 0, Qt.LeftButton)
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2"), 67, 10, 0, Qt.LeftButton)
    
    # edit text, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 67, 10, 0, Qt.LeftButton)
    type(waitForObject(":text_editor_PyEditor_Editor"), "<Ctrl+End>")
    type(waitForObject(":text_editor_PyEditor_Editor"), "<Return>")
    type(waitForObject(":text_editor_PyEditor_Editor"), "# something something")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2"), 67, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
    
    # edit text, save, close
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 67, 10, 0, Qt.LeftButton)
    type(waitForObject(":text_editor_PyEditor_Editor"), "<Ctrl+End>")
    type(waitForObject(":text_editor_PyEditor_Editor"), "<Return>")
    type(waitForObject(":text_editor_PyEditor_Editor"), "# something something")
    clickButton(waitForObject(":Apply_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2"), 67, 10, 0, Qt.LeftButton)
    
    # add variable, try closing
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 67, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":Add concept_QToolButton"))
    doubleClick(waitForObjectItem(":concept_editor_add_table_Table", "2/0"), 57, 10, 0, Qt.LeftButton)
    setParameterValue(":concept_editor_add_table.name_delegate_editor_QExpandingLineEdit", "newvar")
    doubleClick(waitForObjectItem(":concept_editor_add_table_Table", "2/1"), 57, 10, 0, Qt.LeftButton)
    setParameterValue(":concept_editor_add_table.command_delegate_editor_QComboBox_2", "Variable")
    doubleClick(waitForObjectItem(":concept_editor_add_table_Table", "2/2"), 57, 10, 0, Qt.LeftButton)
    setParameterValue(":concept_editor_add_table.type_delegate_editor_QComboBox", "Text")
    type(waitForObject(":concept_editor_add_table.type_delegate_editor_QComboBox"), "<Enter>")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2"), 67, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":Operation performed.Yes_QPushButton"))
    
    # add variable, save, close
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 67, 10, 0, Qt.LeftButton)
    clickButton(waitForObject(":Add concept_QToolButton"))
    doubleClick(waitForObjectItem(":concept_editor_add_table_Table", "2/0"), 57, 10, 0, Qt.LeftButton)
    setParameterValue(":concept_editor_add_table.name_delegate_editor_QExpandingLineEdit", "newvar")
    doubleClick(waitForObjectItem(":concept_editor_add_table_Table", "2/1"), 57, 10, 0, Qt.LeftButton)
    setParameterValue(":concept_editor_add_table.command_delegate_editor_QComboBox_2", "Variable")
    doubleClick(waitForObjectItem(":concept_editor_add_table_Table", "2/2"), 57, 10, 0, Qt.LeftButton)
    setParameterValue(":concept_editor_add_table.type_delegate_editor_QComboBox", "Text")
    type(waitForObject(":concept_editor_add_table.type_delegate_editor_QComboBox"), "<Enter>")
    clickButton(waitForObject(":Apply_QPushButton"))
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2"), 67, 10, 0, Qt.LeftButton)
    
    # one more action, to be sure we are not seeing a dialog now
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1"), 67, 10, 0, Qt.LeftButton)
