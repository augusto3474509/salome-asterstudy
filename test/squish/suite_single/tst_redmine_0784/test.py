#------------------------------------------------------------------------------
# Issue #784 - L2.1. "Parameters" panel
#  - reproducing the following "comm" file content
"""
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'), DEBUG=_F(SDVERI='OUI'))

MAYA = LIRE_MAILLAGE(FORMAT='MED', UNITE=20)

MODELE = AFFE_MODELE(
    AFFE=(
        _F(GROUP_MA='EFLUIDE', MODELISATION='2D_FLUIDE', PHENOMENE='MECANIQUE'),
        _F(
            GROUP_MA=('EFS_P_IN', 'EFS_PIST', 'EFS_P_OU'),
            MODELISATION='2D_FLUI_STRU',
            PHENOMENE='MECANIQUE'
        ), _F(
            GROUP_MA=('E_PISTON', 'E_P_IN', 'ES_P_IN', 'E_P_OU'),
            MODELISATION='D_PLAN',
            PHENOMENE='MECANIQUE'
        ), _F(
            GROUP_MA='AMORPONC', MODELISATION='2D_DIS_T', PHENOMENE='MECANIQUE'
        ), _F(
            GROUP_MA=('MASSPONC', ),
            MODELISATION='2D_DIS_T',
            PHENOMENE='MECANIQUE'
        )
    ),
    INFO=2,
    MAILLAGE=MAYA
)

FIN()
"""
#------------------------------------------------------------------------------
def populate_DEBUT():
    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "DEBUT", "Deprecated" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Deprecated" )
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Deprecated", 10, 10, 0, Qt.LeftButton )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Deprecated.DEBUT" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), "CurrentCase.Stage\\_1.Deprecated.DEBUT", 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":CODE_QCheckBox" ) )
    clickButton( waitForObject( ":CODE_QPushButton" ) )

    symbol = tr( 'NIV_PUB_WEB', 'QComboBox' )
    setParameterValue( symbol, 'INTERNET' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "INTERNET" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":DEBUG_QCheckBox" ) )
    clickButton( waitForObject( ":DEBUG_QPushButton" ) )

    clickButton( waitForObject( ":SDVERI_QCheckBox" ) )
    waitForObject( ":SDVERI_QCheckBox" ).setChecked( True )

    clickButton( waitForObject( ":SDVERI-value_QCheckBox" ) )
    test.compare( waitForObjectExists( ":SDVERI-value_QCheckBox" ).checked, True )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def check_DEBUT():
    #--------------------------------------------------------------------------
    waitForObjectItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Deprecated.DEBUT" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), "CurrentCase.Stage\\_1.Deprecated.DEBUT", 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":CODE_QCheckBox" ).checked, True )
    clickButton( waitForObject( ":CODE_QPushButton" ) )

    test.compare( str( waitForObjectExists( ":NIV_PUB_WEB_QComboBox" ).currentText ), "INTERNET" )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":DEBUG_QCheckBox" ).checked, True )
    clickButton( waitForObject( ":DEBUG_QPushButton" ) )

    test.compare( waitForObjectExists( ":SDVERI_QCheckBox" ).checked, True )
    test.compare( waitForObjectExists( ":SDVERI-value_QCheckBox" ).enabled, True )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populate_LIRE_MAILLAGE():
    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )

    activateMenuItem( "Commands", "Mesh", "LIRE_MAILLAGE" )
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "LIRE_MAILLAGE" )
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.LIRE_MAILLAGE", "Edit" )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", "MAYA" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":FORMAT_QCheckBox" ) )
    setParameterValue( ":FORMAT_QComboBox", "MED" )

    #--------------------------------------------------------------------------
    #[step] Click '...' browse button
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Open 'Mesh_recette.med' file
    med_file_path = os.path.join( casedir(), findFile( "testdata", "zzzz121b.med" ) )
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    #[check] 'Mesh_recette.med' file name was added to combobox and activated
    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(med_file_path))

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def check_LIRE_MAILLAGE():
    #--------------------------------------------------------------------------
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "MAYA" )
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.MAYA", "Edit" )

    #--------------------------------------------------------------------------
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), "MAYA" )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":FORMAT_QCheckBox" ).checked, True )
    test.compare( str( waitForObjectExists( ":FORMAT_QComboBox" ).currentText ), "MED" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populate_AFFE_MODELE():
    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )

    activateMenuItem( "Commands", "Model Definition", "AFFE_MODELE" )
    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "AFFE_MODELE" )
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition.AFFE_MODELE", "Edit" )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", "MODELE" )

    #--------------------------------------------------------------------------
    waitForObject( ":AFFE_QCheckBox" ).setChecked( True )
    clickButton( waitForObject( ":AFFE_QPushButton" ) )

    # --- AFFE 0 --------------------------------------------------------------
    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    setParameterValue( symbol, "EFLUIDE" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QComboBox' )
    setParameterValue( symbol, "2D_FLUIDE" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    # --- AFFE 1 --------------------------------------------------------------
    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '1', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    setParameterValue( symbol, "EFS_P_IN,EFS_PIST,EFS_P_OU" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QComboBox' )
    setParameterValue( symbol, "2D_FLUI_STRU" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    # --- AFFE 2 --------------------------------------------------------------
    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '2', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    setParameterValue( symbol, "E_PISTON,E_P_IN,ES_P_IN,E_P_OU" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QComboBox' )
    setParameterValue( symbol, "D_PLAN" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    # --- AFFE 3 --------------------------------------------------------------
    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '3', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    setParameterValue( symbol, "AMORPONC" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QComboBox' )
    setParameterValue( symbol, "2D_DIS_T" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    # --- AFFE 4 --------------------------------------------------------------
    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '4', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    setParameterValue( symbol, "MASSPONC" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Add item_QPushButton" ) )

    symbol = tr( '0', 'QComboBox' )
    setParameterValue( symbol, "2D_DIS_T" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'INFO', 'QCheckBox' )
    waitForObject( symbol ).setChecked( True )

    symbol = tr( 'INFO', 'QComboBox' )
    setParameterValue( symbol, "2" )

    #--------------------------------------------------------------------------
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    setParameterValue( symbol, "MAYA" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def check_AFFE_MODELE():
    #--------------------------------------------------------------------------
    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "MODELE" )
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Model Definition.MODELE", "Edit" )

    #--------------------------------------------------------------------------
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), "MODELE" )

    #--------------------------------------------------------------------------
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "MAYA" )

    #--------------------------------------------------------------------------
    symbol = tr( 'INFO', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'INFO', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "2" )

    #--------------------------------------------------------------------------
    symbol = tr( 'AFFE', 'QPushButton' )
    waitForObject( symbol ).setChecked( True )

    symbol = tr( 'INFO', 'QComboBox' )
    setParameterValue( symbol, "2" )

    #--------------------------------------------------------------------------
    symbol = tr( 'AFFE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    # --- AFFE 0 --------------------------------------------------------------
    symbol = tr( '0', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), 'EFLUIDE' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), '2D_FLUIDE' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    # --- AFFE 1 --------------------------------------------------------------
    symbol = tr( '1', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), 'EFS_P_IN,EFS_PIST,EFS_P_OU' )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), '2D_FLUI_STRU' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    # --- AFFE 2 --------------------------------------------------------------
    symbol = tr( '2', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), 'E_PISTON,E_P_IN,ES_P_IN,E_P_OU' )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), 'D_PLAN' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    # --- AFFE 3 --------------------------------------------------------------
    symbol = tr( '3', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), 'AMORPONC' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), '2D_DIS_T' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    # --- AFFE 4 --------------------------------------------------------------
    symbol = tr( '4', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( str( waitForObjectExists( symbol ).text ), 'MASSPONC' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )

    #--------------------------------------------------------------------------
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), '2D_DIS_T' )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populate_DEFI_MATERIAU():
    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )

    activateMenuItem( "Commands", "Material", "DEFI_MATERIAU" )
    checkOBItem( "CurrentCase", "Stage_1", "Material", "DEFI_MATERIAU" )
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Material.DEFI_MATERIAU", "Edit" )

    #--------------------------------------------------------------------------
    waitForObject( ":COMP_THM_QCheckBox" ).setChecked( True )
    setParameterValue( ":COMP_THM_QComboBox", "GAZ" )

    #--------------------------------------------------------------------------
    waitForObject( ":ELAS_DHRC_QCheckBox" ).setChecked( True )
    clickButton( waitForObject( ":ELAS_DHRC_QPushButton" ) )

    waitForObject( ":RHO_QCheckBox" ).setChecked( True )
    setParameterValue( ":RHO_QLineEdit", "9" )

    clickButton(waitForObject(":A0_QPushButton"))
    for i in range(21):
        symbol = tr( str(i), "QLineEdit")
        setParameterValue( symbol, str( i + 1 ) )

    clickButton(waitForObject(":OK_QPushButton"))

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton(waitForObject(":Yes_QPushButton"))

    pass

#------------------------------------------------------------------------------
def check_DEFI_MATERIAU():
    #--------------------------------------------------------------------------
    checkOBItem( "CurrentCase", "Stage_1", "Material", "DEFI_MATERIAU" )
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Material.DEFI_MATERIAU", "Edit" )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":COMP_THM_QCheckBox" ).checked, True )
    test.compare( str( waitForObjectExists( ":COMP_THM_QComboBox" ).currentText ), "GAZ" )

    #--------------------------------------------------------------------------
    test.compare( waitForObjectExists( ":ELAS_DHRC_QCheckBox" ).checked, True )
    clickButton( waitForObject( ":ELAS_DHRC_QPushButton" ) )

    test.compare( waitForObjectExists( ":RHO_QCheckBox" ).checked, True )
    test.compare( str( waitForObjectExists( ":RHO_QLineEdit" ).text ), "9.0" )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    populate_DEBUT()
    check_DEBUT()

    #--------------------------------------------------------------------------
    populate_LIRE_MAILLAGE()
    check_LIRE_MAILLAGE()

    #--------------------------------------------------------------------------
    populate_AFFE_MODELE()
    check_AFFE_MODELE()

    #--------------------------------------------------------------------------
    populate_DEFI_MATERIAU()
    check_DEFI_MATERIAU()

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
