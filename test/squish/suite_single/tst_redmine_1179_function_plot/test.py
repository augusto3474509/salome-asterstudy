def main():
    source(findFile("scripts", "common.py"))
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))

    #--------------------------------------------------------------------------
    clickItem(":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton)
    addCommandByDialog("DEFI_FONCTION", "Functions and Lists")
    showOBItem("CurrentCase", "Stage_1", "Functions and Lists", "DEFI_FONCTION")

    #--------------------------------------------------------------------------
    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_1.Functions and Lists.DEFI\\_FONCTION", 50, 9, 0)
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    #--------------------------------------------------------------------------
    # plot 1D curve
    #--------------------------------------------------------------------------

    symbol = tr('ABSCISSE', 'QRadioButton')
    clickButton(waitForObject(symbol))

    symbol = tr('ABSCISSE', 'QPushButton')
    clickButton(waitForObject(symbol))

    clickButton(waitForObject(":Import table_QToolButton"))
    data_file_path = os.path.join(casedir(), '1d.dat')
    setParameterValue(":fileNameEdit_QLineEdit", data_file_path)
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))

    data = [ 32000.0, 12860.09, 5949.899, 3051.76 ]

    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")

    for row in range(len(data)):
        for column in range(tableWidget.columnCount):
            tableItem = tableWidget.item(row, column)
            text = str(tableItem.text())
            value = float(text)
            test.compare(value, data[row], "(%d, %d)" % (row, column))

    clickButton(waitForObject(":Plot function_QToolButton"))
    test.verify(waitForObjectExists(":_PlotCanvas").enabled)
    #test.vp("VP11")

    #--------------------------------------------------------------------------
    # row removing

    nb_rows = tableWidget.rowCount
    waitForObjectItem(":_FunctionTable", "2/0")
    clickItem(":_FunctionTable", "2/0", 52, 17, 0, Qt.LeftButton)

    clickButton(waitForObject(":Remove rows_QToolButton"))

    test.compare(nb_rows - 1, tableWidget.rowCount)

    tableItem = tableWidget.item(2, 0)
    text = str(tableItem.text())
    value = float(text)
    test.compare(value, data[3], "(2, 0)")

    #test.vp("VP12")

    #--------------------------------------------------------------------------
    # row addition

    nb_rows = tableWidget.rowCount

    clickButton(waitForObject(":Append row_QToolButton"))
    test.compare(nb_rows + 1, tableWidget.rowCount)
    tableWidget.scrollToBottom()

    waitForObjectItem(":_FunctionTable", "35/0")
    doubleClickItem(":_FunctionTable", "35/0", 39, 13, 0, Qt.LeftButton)
    cell_35_0 = "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    type(waitForObject(cell_35_0), "0.8")
    type(waitForObject(cell_35_0), "<Return>")
    test.compare(nb_rows + 1, tableWidget.rowCount)

    #test.vp("VP13")

    #--------------------------------------------------------------------------
    # row inserting

    nb_rows = tableWidget.rowCount
    waitForObjectItem(":_FunctionTable", "0/0")
    clickItem(":_FunctionTable", "0/0", 53, 15, 0, Qt.LeftButton)

    clickButton(waitForObject(":Insert row_QPushButton"))
    test.compare(nb_rows + 1, tableWidget.rowCount)

    waitForObjectItem(":_FunctionTable", "0/0")
    doubleClickItem(":_FunctionTable", "0/0", 39, 13, 0, Qt.LeftButton)
    cell_0_0 = "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    type(waitForObject(cell_0_0), "1.E+8")
    type(waitForObject(cell_0_0), "<Return>")

    #test.vp("VP14")

    #--------------------------------------------------------------------------
    # saving and check

    clickButton(waitForObject(":OK_QPushButton"))

    symbol = tr('ABSCISSE', 'QPushButton')
    clickButton(waitForObject(symbol))

    clickButton(waitForObject(":Plot function_QToolButton"))
    test.verify(waitForObjectExists(":_PlotCanvas").enabled)
    #test.vp("VP14")

    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # read 2-columns file into 2-columns table
    #--------------------------------------------------------------------------
    symbol = tr('VALE', 'QRadioButton')
    clickButton(waitForObject(symbol))

    symbol = tr('VALE', 'QPushButton')
    clickButton(waitForObjectExists(symbol))

    clickButton(waitForObject(":Plot function_QToolButton"))
    test.verify(waitForObjectExists(":_PlotCanvas").enabled)
    #test.vp("VP21")

    clickButton(waitForObject(":Import table_QToolButton"))
    data_file_path = os.path.join(casedir(), '2d.dat')
    setParameterValue(":fileNameEdit_QLineEdit", data_file_path)
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))

    data = [ [1.0, 3.125e+11], [2.0, 9765625000.0], [5.0, 100000000.0], [25.0, 32000.0] ]

    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")

    for row in range(len(data)):
        for column in range(tableWidget.columnCount):
            tableItem = tableWidget.item(row, column)
            text = str(tableItem.text())
            value = float(text)
            test.compare(value, data[row][column], "(%d, %d)" % (row, column))

    #test.vp("VP22")

    clickButton(waitForObject(":Plot function_QToolButton"))
    clickButton(waitForObject(":OK_QPushButton"))
