#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForPopupItem("Edit")

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    symbol = tr( 'MATR_ELEM', 'QComboBox' )
    setParameterValue( symbol, "MATMUPG" )

    #--------------------------------------------------------------------------
    symbol = tr( 'NUME_DDL', 'QComboBox' )
    setParameterValue( symbol, "NUMEUPG" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name, imported ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForPopupItem("Edit")

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    symbol = tr( 'MATR_ELEM', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "MATMUPG" )

    #--------------------------------------------------------------------------
    symbol = tr( 'NUME_DDL', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "NUMEUPG" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )
    #--------------------------------------------------------------------------
    importStage( good_file, [":Undefined files.OK_QPushButton"] )
    #--------------------------------------------------------------------------
    category = "Other"
    type = "ASSE_MATRICE"
    name = "MAMASUPG"
    #--------------------------------------------------------------------------
    item = selectItem( "zzzz289f", "%s:-5" % category, "%s" % name )
    checkContents( item, name, imported = True )

    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "zzzz289f" ), 10, 10, 0 )
    popupItem("Graphical Mode")
    clickButton(waitForObject(":Undefined files.OK_QPushButton"))

    item = selectItem( "zzzz289f", "%s:-5" % category, "%s" % name )
    checkContents( item, name, imported = True )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    name = "MAMASUP1"
    populateContents( category, type, name, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name, imported = False )

    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Text Mode")
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    popupItem("Graphical Mode")

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name, imported = True )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
