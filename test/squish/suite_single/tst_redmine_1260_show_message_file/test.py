def main():
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )


def cleanup():
    global_cleanup()


def createMsgFile( files_dir, run_case, stages_list ):
    # create 'message' file in run_case for the given stages
    case_dir = os.path.join( files_dir, run_case )
    i = 0
    while not os.path.isdir( case_dir ):
        if i > 10:
            breakTest( "'%s' directory does not exist."%run_case )
        i += 0.1
        snooze(0.1)

    for stage in stages_list:
        msg_dir = os.path.join( case_dir, "Result-%s" % stage )
        os.makedirs( msg_dir )
        msg_file = open( os.path.join( msg_dir, "message" ), 'w' )
        msg_file.write("%s_test_message" % stage)
        msg_file.close()

def runCase():
    #--------------------------------------------------------------------------
    createNewStudy()

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    file_path = os.path.join( testdir(), "%s.ajs"%(os.path.basename(casedir())) )
    saveFile( file_path )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    # run one stage 'message' file in stage.
    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )

    files_dir = open_run_dialog()

    execute_case()

    createMsgFile( files_dir, 'RunCase_1', ['Stage_1'] )
    wait_for_case( 'RunCase_1' )

    waitForObjectItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1" )
    clickItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 39, 8, 0, Qt.LeftButton )

    snooze(3)
    test.verify( waitForObjectExists( ":Show message file_QPushButton" ).enabled )
    test.verify( not waitForObject( ":Show message file_QPushButton" ).menu().isEmpty() )

    sendEvent("QMouseEvent", waitForObject(":Show message file_QPushButton"), QEvent.MouseButtonPress, 79, 11, Qt.LeftButton, 1, 0)
    activateItem(waitForObjectItem(":AsterStudy *.StandardToolbar_QMenu", "Stage\\_1 'message'"))

    case_dir = os.path.join( files_dir, "RunCase_1" )
    msg_file = open( os.path.join( case_dir, "Result-Stage_1", "message" ) )
    msgs = msg_file.read()
    msg_file.close()
    test.compare( str( waitForObjectExists( ":text_file_dlg.text_file_dlg_editor_QTextEdit" ).plainText ), msgs )

    clickButton(waitForObject(":text_file_dlg.Close_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    # run two stages. 'message' file in second stage.
    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )
    use_option( 'Stage_2', Execute )

    files_dir = open_run_dialog()

    execute_case()

    createMsgFile( files_dir, 'RunCase_2', ['Stage_2'] )
    wait_for_case( 'RunCase_2' )

    waitForObjectItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_2" )
    clickItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_2", 39, 8, 0, Qt.LeftButton )

    test.verify( waitForObjectExists( ":Show message file_QPushButton" ).enabled )
    test.verify( not waitForObject( ":Show message file_QPushButton" ).menu().isEmpty() )

    sendEvent( "QMouseEvent", waitForObject( ":Show message file_QPushButton" ), QEvent.MouseButtonPress, 79, 11, Qt.LeftButton, 1, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.StandardToolbar_QMenu", "Stage\\_2 'message'" ) )

    case_dir = os.path.join( files_dir, "RunCase_2" )
    msg_file = open( os.path.join( case_dir, "Result-Stage_2", "message" ) )
    msgs = msg_file.read()
    msg_file.close()
    test.compare( str( waitForObjectExists( ":text_file_dlg.text_file_dlg_editor_QTextEdit" ).plainText ), msgs )

    clickButton(waitForObject(":text_file_dlg.Close_QPushButton"))

    #--------------------------------------------------------------------------
    # run two stages. No 'message' file in stages. 
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )
    use_option( 'Stage_2', Execute )

    run_case( 'RunCase_3' )

    waitForObjectItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_3" )
    clickItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_3", 39, 8, 0, Qt.LeftButton )

    test.verify( not waitForObjectExists( ":Show message file_QPushButton" ).enabled )
    test.verify( waitForObjectExists( ":Show message file_QPushButton" ).menu().isEmpty() )

    clickItem( ":CasesTreeView_QTreeView", "Run Cases.RunCase\\_2", 39, 8, 0, Qt.LeftButton )

    test.verify( waitForObjectExists( ":Show message file_QPushButton" ).enabled )
    test.verify( not waitForObjectExists( ":Show message file_QPushButton" ).menu().isEmpty() )
