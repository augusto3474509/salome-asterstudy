def main():
    source(findFile("scripts", "common.py"))
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    clickButton(waitForObject(":AsterStudy.New_QToolButton"))
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))
    selectObjectBrowserItem("CurrentCase.Stage\\_1")
    activateMenuItem("Commands", "Material", "DEFI_MATERIAU")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_1.Material.mater"), 54, 12, Qt.NoModifier, Qt.LeftButton)
    clickButton(waitForObject(tr( 'ELAS', 'QCheckBox' )))
    clickButton(waitForObject(tr( 'ELAS', 'QPushButton' )))
    setParameterValue("{name='E' type='QLineEdit' visible='1'}", "2000")
    setParameterValue("{name='NU' type='QLineEdit' visible='1'}", "0.3")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":OK_QPushButton"))

    clickButton(waitForObject(":AsterStudy *.Add Stage_QToolButton"))
    selectObjectBrowserItem("CurrentCase.Stage\\_2")
    activateMenuItem("Commands", "Material", "DEFI_MATERIAU")
    doubleClick(waitForObjectItem(":_SettingsView", "CurrentCase.Stage\\_2.Material.mater0"), 54, 12, Qt.NoModifier, Qt.LeftButton)
    clickButton(waitForObject(tr( 'ELAS', 'QCheckBox' )))
    clickButton(waitForObject(tr( 'ELAS', 'QPushButton' )))
    setParameterValue("{name='E' type='QLineEdit' visible='1'}", "2000")
    setParameterValue("{name='NU' type='QLineEdit' visible='1'}", "0.3")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":OK_QPushButton"))

    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_2", 50, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Text Mode"))

    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_1", 44, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Text Mode"))

    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_1", 51, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode"))

    openItemContextMenu(waitForObject(":_QTreeWidget"), "CurrentCase.Stage\\_2", 43, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode"))

    test.log("End of test")
