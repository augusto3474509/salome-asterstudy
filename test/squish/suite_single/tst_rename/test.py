def main():
    source(findFile("scripts", "common.py"))
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    activateMenuItem("Operations", "Add Stage")
    checkOBItem( "CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.Stage_1", "Rename")
    type(waitForObject(":_QExpandingLineEdit"), "New_stage")
    type(waitForObject(":_QExpandingLineEdit"), "<Return>")

    activateOBContextMenuItem("CurrentCase.New_stage", "Rename")
    type(waitForObject(":_QExpandingLineEdit"), "My_stage")
    type(waitForObject(":_QExpandingLineEdit"), "<Return>")
    checkOBItem( "CurrentCase", "My_stage")

    #--------------------------------------------------------------------------
    selectObjectBrowserItem("CurrentCase.My_stage")
    activateMenuItem("Commands", "Material", "DEFI_MATERIAU")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.My_stage.Material.DEFI_MATERIAU", "Rename")
    type(waitForObject(":_QExpandingLineEdit"), "MY_MATERIAL")
    type(waitForObject(":_QExpandingLineEdit"), "<Return>")
    checkOBItem( "CurrentCase", "My_stage", "Material", "MY_MATERIAL")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.My_stage.Material.MY_MATERIAL", "Duplicate")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.My_stage.Material.MY_MATERIAL", "Delete")
    clickButton(waitForObject(":Delete.Yes_QPushButton"))
    checkOBItem( "CurrentCase", "My_stage", "Material", "MY_MATERIAL")

    #--------------------------------------------------------------------------
    waitForActivateOBContextMenuItem("CurrentCase.My_stage.Material.MY_MATERIAL", "Edit")
    test.compare(str(waitForObjectExists(":_ParameterTitle").displayText), "DEFI_MATERIAU")
    test.compare(str(waitForObjectExists(":Name_QLineEdit").displayText), "MY_MATERIAL")
    openContextMenu(waitForObject(":Name_QLineEdit"), 101, 9, 0)
    activateItem(waitForObjectItem(":AsterStudy *.qt_edit_menu_QMenu", "Select All"))
    type(waitForObject(":Name_QLineEdit"), "RENAMED")
    clickButton(waitForObject(":OK_QPushButton"))
    clickButton(waitForObject(":Yes_QPushButton"))
    checkOBItem( "CurrentCase", "My_stage", "Material", "RENAMED")

    #--------------------------------------------------------------------------
    filepath = saveFile("renaming.ajs")
    closeFile()
    openFile(filepath)
    clickButton(waitForObject("{text='OK' type='QPushButton' unnamed='1' visible='1' window=':Close active study_MessageBox'}"))

#    clickButton(waitForObject(":Missing study directory.OK_QPushButton"))
#    clickButton(waitForObject(":Inconsistent study directory.OK_QPushButton"))

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    checkOBItem( "CurrentCase")
    checkOBItem( "CurrentCase", "My_stage")
    checkOBItem( "CurrentCase", "My_stage", "Material", "RENAMED")
