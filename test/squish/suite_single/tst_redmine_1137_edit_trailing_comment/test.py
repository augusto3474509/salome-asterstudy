#------------------------------------------------------------------------------
# Issue #1137 - Manage comments
# Issue #1219 - Can't add comment to command if there's trailing comment in stage
#
# For more details look at
# - http://salome.redmine.opencascade.com/issues/1137
# - http://salome.redmine.opencascade.com/issues/1219

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")

    #--------------------------------------------------------------------------
    text = \
"""
m = LIRE_MAILLAGE(UNITE=20)
# this is trailing comment
"""
    setClipboardText(text)
    openContextMenu( selectItem( "Stage_1" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Paste" ) )
    waitForObject( ":_QTreeWidget" ).expandAll()
    checkValidity( True, "CurrentCase" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "m" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("command LIRE_MAILLAGE")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Mesh", "command LIRE_MAILLAGE" )
    selectItem( "Stage_1", "Mesh", "m" )
    selectItem( "Stage_1", "Mesh", "this is trailing comment" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "command LIRE_MAILLAGE" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "command LIRE_MAILLAGE" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("command m")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Mesh", "command m" )
    selectItem( "Stage_1", "Mesh", "m" )
    selectItem( "Stage_1", "Mesh", "this is trailing comment" )

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "m" )
    openContextMenu( item, 10, 10, 0 )
    activateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit Comment"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "command m" )
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    item = selectItem( "Stage_1", "Mesh", "this is trailing comment" )
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem(waitForObjectItem(":AsterStudy *.CommandsToolbar_QMenu", "Edit"))

    test.verify( waitForObject(":text_editor_QPlainTextEdit").toPlainText() == "this is trailing comment" )
    waitForObject(":text_editor_QPlainTextEdit").setPlainText("unnecessary comment")
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    selectItem( "Stage_1", "Mesh", "command m" )
    selectItem( "Stage_1", "Mesh", "m" )
    selectItem( "Stage_1", "Mesh", "unnecessary comment" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
