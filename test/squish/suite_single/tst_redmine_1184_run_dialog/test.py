def main():
    source( findFile( "scripts", "common.py" ) )
    global_start( debug=True, noexhook=True )


def cleanup():
    global_cleanup()


def check_default_run_params():
    tab_widget = waitForObject( "{container=':qt_tabwidget_stackedwidget_QWidget' type='QTabWidget' unnamed='1' visible='1'}" )
    idx = tab_widget.currentIndex
    tab_name = tab_widget.tabText( idx )
    test.compare( tab_name, "Basic" )
    clickTab( waitForObject(":Data Settings_QTabWidget" ), "Basic" )
    test.vp( "VP1" )
    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Advanced" )
    test.vp( "VP2" )
    clickTab( waitForObject(":Data Settings_QTabWidget" ), "Basic" )


def runCase():
    #--------------------------------------------------------------------------
    createNewStudy()
    test.verify( waitForObjectExists( "{column='0' container=':CasesTreeView_QTreeView' text='CurrentCase' type='QModelIndex'}" ).selected )

    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    test.verify( waitForObjectExists( "{column='0' container=':Data Settings_SettingsView' text='CurrentCase' type='QModelIndex'}" ).selected )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    saveFile( os.path.join( testdir(), "Noname1.ajs" ) )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    # run with user's parameters
    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )

    open_run_dialog()

    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Basic" )

    check_default_run_params()

    setInputFieldValue( ":Memory_QLineEdit", "512" )
    setInputFieldValue( ":_QTextEdit", "test run dialog" )

    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Basic" )

    mouseClick( waitForObject( ":Version of code_aster_QComboBox" ), 58, 12, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( ":Version of code_aster_QComboBox", "testing" ), 58, 12, 0, Qt.LeftButton )

    execute_case()
    wait_for_case( 'RunCase_1' )
    test.verify( waitForObjectExists( "{column='0' container=':CasesTreeView.Run Cases_QModelIndex' text='RunCase_1' type='QModelIndex'}" ).selected )

    #--------------------------------------------------------------------------
    # check that user's parameters were saved after Run
    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )

    open_run_dialog()

    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Basic" )

    test.compare( waitForObject( ":Memory_QLineEdit" ).text, "512" )
    test.compare( waitForObject( ":_QTextEdit" ).toPlainText(), "test run dialog" )

#     clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Advanced" )

    test.compare( waitForObject( ":Version of code_aster_QComboBox" ).currentText, "testing" )

    execute_case()
    wait_for_case( 'RunCase_2' )
    snooze(1)
    test.verify( waitForObjectExists( "{column='0' container=':CasesTreeView.Run Cases_QModelIndex' text='RunCase_2' type='QModelIndex'}" ).selected )

    closeFile()
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    # check that user's parameters are kept for new study
    #--------------------------------------------------------------------------
    createNewStudy()
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    saveFile( os.path.join( testdir(), "Noname2.ajs" ) )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )

    open_run_dialog()

    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Basic" )

    test.compare( waitForObject( ":Memory_QLineEdit" ).text, "512" )

    test.compare( waitForObject( ":_QTextEdit" ).toPlainText(), "test run dialog" )

    test.compare( waitForObject( ":Version of code_aster_QComboBox" ).currentText, "testing" )

    execute_case()
    wait_for_case( 'RunCase_1' )
    snooze(1)

    #--------------------------------------------------------------------------
    # check that default parameters were set for new session (after restart application)
    #--------------------------------------------------------------------------
    activateMenuItem( "File", "Exit" )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    global_start()

    #--------------------------------------------------------------------------
    createNewStudy()
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    saveFile( os.path.join( testdir(), "Noname3.ajs" ) )

    #--------------------------------------------------------------------------
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "History View" )

    #--------------------------------------------------------------------------
    waitForObjectItem( ":CasesTreeView_QTreeView", "CurrentCase" )
    clickItem( ":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton )

    use_option( 'Stage_1', Execute )

    open_run_dialog()

    clickTab( waitForObject( ":Data Settings_QTabWidget" ), "Basic" )

    check_default_run_params()

#     clickButton( waitForObject( ":Cancel_QPushButton" ) )

    activateMenuItem( "File", "Exit" )
