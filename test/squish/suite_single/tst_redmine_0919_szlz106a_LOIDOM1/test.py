#------------------------------------------------------------------------------
# Issue #991 - Create several GUI use cases to test edition of commands via Edition panel
#   - population of "zzzz289f.comm" referenced file

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def populateContents( *args ):
    #--------------------------------------------------------------------------
    category = args[0]
    command = args[1]
    name = args[2]
    path = args[3:]

    #--------------------------------------------------------------------------
    addCommandByDialog( command, category )

    #--------------------------------------------------------------------------
    openContextMenu( selectItem( *path ), 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    #--------------------------------------------------------------------------
    setParameterValue( ":Name_QLineEdit", name )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    setParameterValue( symbol, "SIGM" )

    #--------------------------------------------------------------------------
    symbol = tr( 'VALE', 'QRadioButton' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton( waitForObject( ":Import table_QToolButton" ) )
    data_file_path = os.path.join( casedir(), 'szlz106a.dat' )
    setParameterValue( ":fileNameEdit_QLineEdit", data_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'INTERPOL', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'INTERPOL', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    clickButton(waitForObject(":Add item_QPushButton"))

    symbol = tr( '0', 'QComboBox' )
    setParameterValue( symbol, "LOG" )

    clickButton( waitForObject( ":Add item_QPushButton" ) )
    symbol = tr( '1', 'QComboBox' )
    setParameterValue( symbol, "LOG" )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    setParameterValue( symbol, "LINEAIRE" )

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    setParameterValue( symbol, "LINEAIRE" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":OK_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def checkContents( item, name ):
    #--------------------------------------------------------------------------
    openContextMenu( item, 10, 10, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).text ), name )

    #--------------------------------------------------------------------------
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "SIGM" )

    #--------------------------------------------------------------------------
    symbol = tr( 'VALE', 'QRadioButton' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    data = [ [1.0, 3.125e+11], [2.0, 9765625000.0], [5.0, 100000000.0], [25.0, 32000.0] ]

    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")

    for row in range(4 if tableWidget.rowCount >= 4 else tableWidget.rowCount):
        for column in range(tableWidget.columnCount):
            tableItem = tableWidget.item(row, column)
            text = str(tableItem.text())
            value = float(text)
            test.compare(value, data[row][column])
            test.log("(%d, %d) '%s'" % (row, column, text))

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'INTERPOL', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'INTERPOL', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( '0', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "LOG" )

    symbol = tr( '1', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "LOG" )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_GAUCHE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_GAUCHE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "LINEAIRE" )

    #--------------------------------------------------------------------------
    symbol = tr( 'PROL_DROITE', 'QCheckBox' )
    test.compare( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'PROL_DROITE', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), "LINEAIRE" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    import os
    good_file = os.path.join( os.getcwd(), 'szlz106a.comm' )
    test.log( "Use '%s' file to create a new stage" % good_file )

    #--------------------------------------------------------------------------
    importStage( good_file )

    #--------------------------------------------------------------------------
    category = "Functions and Lists"
    type = "DEFI_FONCTION"
    name = "LOIDOM1"

    #--------------------------------------------------------------------------
    item = selectItem( "szlz106a", "%s" % category, "%s" % name )
    checkContents( item, name )

    openContextMenu( selectItem( "szlz106a" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Text Mode" ) )
    openContextMenu( selectItem( "szlz106a" ), 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Graphical Mode" ) )

    item = selectItem( "szlz106a", "%s" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateMenuItem( "Operations", "Add Stage" )
    checkOBItem( "CurrentCase", "Stage_1" )
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #--------------------------------------------------------------------------
    name = "LOIDOM11"
    populateContents( category, type, name, "Stage_1", "%s" % category, "%s" % type )

    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    activateOBContextMenuItem("CurrentCase.Stage_1", "Text Mode")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Graphical Mode")
    
    item = selectItem( "Stage_1", "%s" % category, "%s" % name )
    checkContents( item, name )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
