#------------------------------------------------------------------------------
# Issue #1257 - Information view: preview command

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    activateMenuItem("Operations", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.Stage_1", "Mesh", "LIRE_MAILLAGE")
    expected = \
"""
mesh = LIRE_MAILLAGE()
"""
    actual = str(waitForObject(tr('information_view', 'QTextEdit')).plainText)
    test.verify(check_text_eq(expected, actual))

    #--------------------------------------------------------------------------
    waitForActivateOBContextMenuItem("CurrentCase.Stage_1.Mesh.mesh", "Edit")

    clickButton(waitForObject(tr('FORMAT', 'QCheckBox')))
    setParameterValue(tr('FORMAT', 'QComboBox'), 'MED')

    clickButton(waitForObject(tr('UNITE', 'QPushButton')))
    med_file_path = os.path.join(casedir(), findFile("testdata", "Mesh_recette.med"))
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton(waitForObject(":QFileDialog.Open_QPushButton"))

    clickButton(waitForObject(tr('NOM_MED', 'QCheckBox')))
    setParameterValue(tr('NOM_MED', 'QComboBox'), 'Mesh_recette')

    clickButton(waitForObject(":OK_QPushButton"))

    expected = \
"""
mesh = LIRE_MAILLAGE(
  FORMAT='MED', 
  NOM_MED='Mesh_recette', 
  UNITE=20
)
"""
    actual = str(waitForObject(tr('information_view', 'QTextEdit')).plainText)
    test.verify(check_text_eq(expected, actual))

    #--------------------------------------------------------------------------
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    pass
