#------------------------------------------------------------------------------
# Issue #893 - Remove Stage in use from a Case

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source( findFile( "scripts", "common.py" ) )
    global_start(debug=True, noexhook=True)

    #--------------------------------------------------------------------------
    pass

#------------------------------------------------------------------------------
def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()

    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    waitForActivateMenuItem( "File", "New" )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    selectObjectBrowserItem( "CurrentCase" )

    #--------------------------------------------------------------------------
    import os
    comm_file = os.path.join( os.getcwd(), 'zzzz289f.comm' )
    test.log( "Use '%s' file to create a new stage" % comm_file )

    importStage( comm_file, [":Undefined files.OK_QPushButton"] )

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase" )

    checkValidity( True, "CurrentCase", "zzzz289f" )

    checkValidity( True, "CurrentCase", "zzzz289f", "Mesh" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Mesh", "MAIL_Q" )

    checkValidity( True, "CurrentCase", "zzzz289f", "Material" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "MATER" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Material", "CHMAT_Q" )

    checkValidity( True, "CurrentCase", "zzzz289f", "Deprecated" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Deprecated", "DEBUT" )

    #--------------------------------------------------------------------------
    activateOBContextMenuItem("CurrentCase.zzzz289f", "Rename")
    type(waitForObject(":_QExpandingLineEdit"), "zzzz289f_1")
    type(waitForObject(":_QExpandingLineEdit"), "<Return>")

    #--------------------------------------------------------------------------
    import os
    comm_file = os.path.join( os.getcwd(), 'zzzz289f.com1' )
    test.log( "Use '%s' file to create a new stage" % comm_file )

    importStage( comm_file )

    #--------------------------------------------------------------------------
    checkValidity( True, "CurrentCase" )

    checkValidity( True, "CurrentCase", "zzzz289f" )

    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    checkValidity( True, "CurrentCase", "zzzz289f", "Other" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Other", "MATMUPG" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Other", "MATMUPQ" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Other", "MATMUPL" )

    #--------------------------------------------------------------------------
    item, index = showOBIndex( "CurrentCase", "zzzz289f_1", "Mesh", "MAIL_Q" )
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), item, 10, 10, 0 )
    activateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Delete" ) )
    clickButton( waitForObject( ":Delete.Yes_QPushButton" ) )

    #--------------------------------------------------------------------------
    checkValidity( False, "CurrentCase" )

    checkValidity( False, "CurrentCase", "zzzz289f_1" )
    checkOBItemNotExists( "CurrentCase", "zzzz289f_1", "Mesh" )
    checkOBItemNotExists( "CurrentCase", "zzzz289f_1", "Mesh", "MAIL_Q" )

    checkValidity( False, "CurrentCase", "zzzz289f_1", "Material" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Material", "MATER" )
    checkValidity( False, "CurrentCase", "zzzz289f_1", "Material", "CHMAT_Q" )

    checkValidity( True, "CurrentCase", "zzzz289f_1", "Deprecated" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Deprecated", "DEBUT" )

    checkValidity( False, "CurrentCase", "zzzz289f" )

    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    checkValidity( False, "CurrentCase", "zzzz289f", "Other" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Other", "MATMUPG" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Other", "MATMUPQ" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Other", "MATMUPL" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Undo_QToolButton" ) )

    checkValidity( True, "CurrentCase" )

    checkValidity( True, "CurrentCase", "zzzz289f_1" )

    checkValidity( True, "CurrentCase", "zzzz289f_1", "Mesh" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Mesh", "MAIL_Q" )

    checkValidity( True, "CurrentCase", "zzzz289f_1", "Material" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Material", "MATER" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Material", "CHMAT_Q" )

    checkValidity( True, "CurrentCase", "zzzz289f_1", "Deprecated" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Deprecated", "DEBUT" )

    checkValidity( True, "CurrentCase", "zzzz289f" )

    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    checkValidity( True, "CurrentCase", "zzzz289f", "Other" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Other", "MATMUPG" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Other", "MATMUPQ" )
    checkValidity( True, "CurrentCase", "zzzz289f", "Other", "MATMUPL" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Redo_QToolButton" ) )

    checkValidity( False, "CurrentCase" )

    checkValidity( False, "CurrentCase", "zzzz289f_1" )
    checkOBItemNotExists( "CurrentCase", "zzzz289f_1", "Mesh" )
    checkOBItemNotExists( "CurrentCase", "zzzz289f_1", "Mesh", "MAIL_Q" )

    checkValidity( False, "CurrentCase", "zzzz289f_1", "Material" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Material", "MATER" )
    checkValidity( False, "CurrentCase", "zzzz289f_1", "Material", "CHMAT_Q" )

    checkValidity( True, "CurrentCase", "zzzz289f_1", "Deprecated" )
    checkValidity( True, "CurrentCase", "zzzz289f_1", "Deprecated", "DEBUT" )

    checkValidity( False, "CurrentCase", "zzzz289f" )

    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPG" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPQ" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Model Definition", "MODELUPL" )

    checkValidity( False, "CurrentCase", "zzzz289f", "Other" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Other", "MATMUPG" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Other", "MATMUPQ" )
    checkValidity( False, "CurrentCase", "zzzz289f", "Other", "MATMUPL" )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )

    #--------------------------------------------------------------------------
    pass
