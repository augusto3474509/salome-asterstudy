def main():
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

def cleanup():
    global_cleanup()

def runCase():
    activateMenuItem("Test", "Create sample study")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    showOBItem( "CurrentCase", "Stage_1", "Material", "material_1")
    showOBItem( "CurrentCase", "Stage_1", "Material", "material_2")
    showOBItem( "CurrentCase", "Stage_1", "Material", "material_3")
    showOBItem( "CurrentCase", "Stage_1", "Model Definition", "model")
    showOBItem( "CurrentCase", "Stage_2")
    activateItem(waitForObjectItem(":AsterStudy_QMenuBar", "File"))
    activateItem(waitForObjectItem(":File_QMenu", "Exit"))
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )
