#------------------------------------------------------------------------------
# Issue #1142 - Explicitly highlight read-only view mode of the Case view

#------------------------------------------------------------------------------

def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True, nativenames=False, basicnaming=False)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # [section] Create New study
    # [step] Menu File - New
    waitForActivateMenuItem("File", "New")

    #--------------------------------------------------------------------------
    # [section] Create empty stage in current case
    # [step] Switch to "Case View" tab
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")
    # [step] Select "CurrentCase" object in "Data Settings" panel, invoke "Add Stage" item from popup menu
    activateOBContextMenuItem("CurrentCase", "Add Stage")
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")

    #--------------------------------------------------------------------------
    # [section] Add command to the stage
    # [step] Select "Stage_1" object in "Data Settings" panel, invoke popup menu, choose "Show All" item
    activateOBContextMenuItem("CurrentCase.Stage_1", "Show All")
    # [step] Show All dialog: type "DEBUT" in "Search box"
    type(waitForObject(":searcher_LineEdit"), "DEBUT")
    # [step] Show All dialog: press OK button.
    clickButton(waitForObject(":OK_QPushButton"))

    #--------------------------------------------------------------------------
    # [section] Save study
    # [step] Menu File - Save
    activateMenuItem("File", "Save")
    import os
    # [step] Enter a path to the saved study
    study = os.path.join(testdir(), "tst_redmine_1142.ajs" )
    setParameterValue(":fileNameEdit_QLineEdit", study)
    # [step] "Save study" dialog: press "Save"
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    #--------------------------------------------------------------------------
    # [section] Run case
    # [step] Switch to "History View" tab
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    # [step] Select 'CurrentCase' object in Cases view
    waitForObjectItem(":CasesTreeView_QTreeView", "CurrentCase")
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 39, 8, 0, Qt.LeftButton)
    # [step] Dashboard window: press the button near "Stage_1" label
    use_option('Stage_1', Execute)
    # [step] Dashboard window: Press "Run" button
    clickButton(waitForObject("{type='QPushButton' name='Run'}"))
    # [step] "Run..." dialog: press "Run" button
#     clickButton(waitForObject(":DashboardRunDialog.run_QPushButton"))
    wait_for_case("RunCase_1")

    #--------------------------------------------------------------------------
    # [section] Activate Run case
    # [step] Select "RunCase_1" object in "Cases" view, invoke "View Case (read-only)" item from popup menu
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 47, 11, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "View Case (read-only)"))
    # [step] Check that Run case is activated in the "Case View" and "Read-only" banner is visible in the top area of the application window
    # [step] Click a link in "Read-only" banner
    mouseClick(waitForObject(":read_only_banner_QLabel"), 516, 6, 0, Qt.LeftButton)
    # [step] Check that Current case is activated in the "Case View" and "Read-only" banner is not visible

    #--------------------------------------------------------------------------
    # [section] Re-activate Run case
    # [step] Switch to "History View" tab
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    # [step] Select "RunCase_1" object in "Cases" view, invoke "View Case (read-only)" item from popup menu
    clickItem(":CasesTreeView_QTreeView", "Run Cases.RunCase\\_1", 47, 11, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "Run Cases.RunCase\\_1", 47, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "View Case (read-only)"))
    # [step] Check that Run case is activated in the "Case View" and "Read-only" banner is visible

    #--------------------------------------------------------------------------
    # [section] Activate Current case
    # [step] Switch to "History View" tab
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "History View")
    # [step] Select "CurrentCase" object in "Cases" view, invoke "Edit Case" item from popup menu
    clickItem(":CasesTreeView_QTreeView", "CurrentCase", 47, 11, 0, Qt.LeftButton)
    openItemContextMenu(waitForObject(":CasesTreeView_QTreeView"), "CurrentCase", 66, 11, 0)
    activateItem(waitForObjectItem(":AsterStudy *.OperationsToolbar_QMenu", "Edit Case"))
    # [step] Check that Current case is activated in the "Case View" and "Read-only" banner is not visible

    #--------------------------------------------------------------------------
    # [section] Quit application
    # [step] Menu File - Exit
    activateMenuItem("File", "Exit")

    #--------------------------------------------------------------------------
    pass
