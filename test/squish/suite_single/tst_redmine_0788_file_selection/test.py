def main():
    source( findFile( "scripts", "common.py" ) )
    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )

    #--------------------------------------------------------------------------
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )

    #--------------------------------------------------------------------------
    clickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1", 10, 10, 0, Qt.LeftButton )
    addCommandByDialog( "EXEC_LOGICIEL", "Other" )
    showOBItem( "CurrentCase", "Stage_1", "Other", "EXEC_LOGICIEL" )

    #--------------------------------------------------------------------------
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    stage = waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    model = stage.model()
    test.compare( model.rowCount( stage ), 0 )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), "CurrentCase.Stage\\_1.Other.EXEC\\_LOGICIEL", 50, 9, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    symbol = tr( 'MAILLAGE', 'QCheckBox' )
    waitForObjectExists( symbol ).setChecked( True )

    symbol = tr( 'MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'FORMAT', 'QComboBox' )
    setParameterValue( symbol, 'GMSH' )

    symbol = tr( 'UNITE_GEOM', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    test.compare( waitForObject( ":QFileDialog.fileTypeCombo_QComboBox" ).currentText,
                  "All files (*)" )
    def_file_path = os.path.join( casedir(), 'default.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", def_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    symbol = tr( 'UNITE_GEOM', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( def_file_path ) )

    #--------------------------------------------------------------------------
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    stage = waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    model = stage.model()
    test.compare( model.rowCount( stage ), 0 )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    stage = waitForObjectItem( ":DataFiles_QTreeView", "Stage\\_1" )
    model = stage.model()
    test.compare( model.rowCount( stage ), 0 )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    waitForObject( ":_QTreeWidget" ).expandAll()
    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    
    test.vp( "DataFiles" )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), "CurrentCase.Stage\\_1.Other.\\_", 50, 9, Qt.RightButton )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    symbol = tr( 'MAILLAGE', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'UNITE_GEOM', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    anot_file_path = os.path.join( casedir(), 'another.file' )
    setParameterValue( ":fileNameEdit_QLineEdit", anot_file_path )
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )

    #--------------------------------------------------------------------------
    mouseClick(waitForObjectItem(":Data Settings_SettingsView", "CurrentCase"), 63, 8, 0, Qt.LeftButton)
    symbol = tr( 'UNITE_GEOM', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( anot_file_path ) )

    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp( "DataFiles_scrollbar" )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_GEOM', 'QComboBox' )

    mouseClick( waitForObject( symbol ), 109, 12, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( symbol, escape( os.path.basename( def_file_path ) ) ), 70, 14, 0, Qt.LeftButton )

    #--------------------------------------------------------------------------
    symbol = tr( 'UNITE_GEOM', 'QComboBox' )

    mouseClick( waitForObject( symbol ), 108, 13, 0, Qt.LeftButton )
    mouseClick( waitForObjectItem( symbol, escape( os.path.basename( anot_file_path ) ) ), 91, 9, 0, Qt.LeftButton )

    symbol = tr( 'UNITE_GEOM', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( anot_file_path ) )

    clickButton( waitForObject( ":OK_QPushButton" ) )

    waitForObject( ":DataFiles_QTreeView" ).expandAll()
    test.vp( "DataFiles_scrollbar" )

    clickButton(waitForObject(":Apply_QPushButton"))

    waitForObject( ":DataFiles_QTreeView" ).expandAll()

    test.vp( "DataFiles-2_scrollbar" )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    openItemContextMenu( waitForObject( ":_QTreeWidget" ), "CurrentCase.Stage\\_1.Other.\\_", 50, 9, 0 )
    waitForActivateItem( waitForObjectItem( ":AsterStudy *.CommandsToolbar_QMenu", "Edit" ) )

    symbol = tr( 'MAILLAGE', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).checked, True )

    symbol = tr( 'MAILLAGE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )

    symbol = tr( 'UNITE_GEOM', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled, True )

    symbol = tr( 'UNITE_GEOM', 'QComboBox' )
    test.compare( str( waitForObjectExists( symbol ).currentText ), os.path.basename( anot_file_path ) )

    clickButton( waitForObject( ":Cancel_QPushButton" ) )

    clickButton( waitForObject( ":Close_QPushButton" ) )

    #--------------------------------------------------------------------------
    activateItem( waitForObjectItem( ":AsterStudy_QMenuBar", "File" ) )
    activateItem( waitForObjectItem( ":File_QMenu", "Exit" ) )
    clickButton( waitForObject( ":Close active study.Discard_QPushButton" ) )
