#------------------------------------------------------------------------------
# Issue #1617 - Favorites category

#------------------------------------------------------------------------------
def main():
    #--------------------------------------------------------------------------
    source(findFile("scripts", "common.py"))
    global_start(debug=True)

    #--------------------------------------------------------------------------
    pass

def cleanup():
    #--------------------------------------------------------------------------
    global_cleanup()
    #--------------------------------------------------------------------------
    pass

def runCase():
    #--------------------------------------------------------------------------
    # create new study
    activateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # create an empty stage in the current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # display the "Show All" panel
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Show All")

    #--------------------------------------------------------------------------
    # add several commands to the Favorites category
    contextMenuActionForCommand("CREA_MAILLAGE", "Mesh", "Add to Favorites")
    contextMenuActionForCommand("LIRE_MAILLAGE", "Mesh", "Add to Favorites")
    contextMenuActionForCommand("AFFE_CARA_ELEM", "Model Definition", "Add to Favorites")
    contextMenuActionForCommand("AFFE_MATERIAU", "Material", "Add to Favorites")

    #--------------------------------------------------------------------------
    # check that the Favorites category is appeared
    check_hasFavorites("true")

    #--------------------------------------------------------------------------
    # remove a command from the Favorites category
    contextMenuActionForCommand("LIRE_MAILLAGE", "Favorites", "Remove from Favorites")

    #--------------------------------------------------------------------------
    # remove a command from the Favorites category by selecting the item in its original category 
    contextMenuActionForCommand("AFFE_CARA_ELEM", "Model Definition", "Remove from Favorites")

    #--------------------------------------------------------------------------
    # close the "Show All" panel
    clickButton(waitForObject(":Close_QPushButton"))

    #--------------------------------------------------------------------------
    # create an instance of command from the Favorites category
    # (using the main menu, category button and stage context menu)

    activateMenuItem("Commands", "Favorites", escape("CREA_MAILLAGE"))

    # actually useless (action CREA_MAILLAGE exists anyway)
    #mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 10, 10, 0, Qt.LeftButton)
    #mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", escape("CREA_MAILLAGE")), 10, 10, 0, Qt.LeftButton)

    selectObjectBrowserItem("CurrentCase.Stage_1")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Favorites", "CREA_MAILLAGE")

    #--------------------------------------------------------------------------
    # exit the application
    activateMenuItem("File", "Exit")
    clickButton(waitForObject(":Close active study.Discard_QPushButton"))

    #--------------------------------------------------------------------------
    # restart the application
    global_start()

    #--------------------------------------------------------------------------
    # create new study
    activateMenuItem("File", "New")
    clickTab(waitForObject(":AsterStudy *.Case View_QTabWidget"), "Case View")

    #--------------------------------------------------------------------------
    # create an empty stage in the current case
    activateOBContextMenuItem("CurrentCase", "Add Stage")

    #--------------------------------------------------------------------------
    # check that the stage context menu still contains the Favorites sub-menu 
    checkOBItem("CurrentCase", "Stage_1")
    selectObjectBrowserItem("CurrentCase.Stage_1")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Favorites", "CREA_MAILLAGE")

    #--------------------------------------------------------------------------
    # display the "Show All" panel
    selectObjectBrowserItem("CurrentCase.Stage_1")
    activateOBContextMenuItem("CurrentCase.Stage_1", "Show All")

    #--------------------------------------------------------------------------
    # remove all remaining commands the Favorites category
    contextMenuActionForCommand("CREA_MAILLAGE", "Favorites", "Remove from Favorites")
    contextMenuActionForCommand("AFFE_MATERIAU", "Favorites", "Remove from Favorites")

    #--------------------------------------------------------------------------
    # check that the Favorites category is disappeared
    check_hasFavorites("false")

    #--------------------------------------------------------------------------
    pass

def checkCommandInShowAllDialog(command, group):
    aboveWidget = "{container=':qt_tabwidget_stackedwidget_QWidget' text~='%s.*' type='QToolButton' unnamed='1' visible='1'}" % group
    grpListView = "{aboveWidget=%s container=':qt_tabwidget_stackedwidget_QWidget' type='QListWidget' unnamed='1' visible='1'}" % aboveWidget
    waitForObjectItem(grpListView, escape(command))
    clickItem(grpListView, escape(command), 10, 10, 0, Qt.LeftButton)

def contextMenuActionForCommand(command, group, action):
    checkCommandInShowAllDialog(command, group)
    aboveWidget = "{container=':qt_tabwidget_stackedwidget_QWidget' text~='%s.*' type='QToolButton' unnamed='1' visible='1'}" % group
    grpListView = "{aboveWidget=%s container=':qt_tabwidget_stackedwidget_QWidget' type='QListWidget' unnamed='1' visible='1'}" % aboveWidget
    waitForObjectItem(grpListView, escape(command))
    openContextMenu(waitForObjectItem(grpListView, escape(command)), 10, 10, 0)
    activateItem(waitForObjectItem("{type='QMenu' unnamed='1' visible='1'}", action))

def check_hasFavorites(flag):
    openContextMenu(waitForObject(":AsterStudy *.DebugWidget_DebugWidget"), 10, 10, 0)
    activateItem(waitForObjectItem(":AsterStudy *.StandardToolbar_QMenu", "has favorites"))
    test.compare(str(waitForObject(":AsterStudy *.DebugWidget_DebugWidget").text), flag)
