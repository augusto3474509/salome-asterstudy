#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'DEFI_LIST_INST' command named 'List_i'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'DEFI_LIST_INST' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkList_i()

    pass

def checkList_i():
    #[section] Check DEFI_LIST_INST command named 'List_i'
    test.log( "Check DEFI_LIST_INST command named 'List_i'" )

    #[step] Double click on 'List_i' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Functions and Lists", "List_i" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_LIST_INST'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST" )
    #[check] Command name is 'List_i'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "List_i" )
    #[check] 'METHODE' combobox is enable
    symbol = tr( 'METHODE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'METHODE' combobox value is 'MANUEL'
    test.compare( waitForObjectExists( symbol ).currentText, 'MANUEL' )

    #[step] Click 'Edit...' button of 'DEFI_LIST' keyword
    symbol = tr( 'DEFI_LIST', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] 'DEFI_LIST' panel was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > DEFI_LIST" )

    #[check] 'LIST_INST' radiobutton has 'checked' state
    symbol = tr( 'LIST_INST', 'QRadioButton' )
    test.verify( waitForObjectExists( symbol ).checked )
    #[check] 'List_r' in 'LIST_INST' combobox
    symbol = tr( 'LIST_INST', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'List_r' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST" )

    #[check] 'ECHEC' checkbox has 'checked' state
    symbol = tr( 'ECHEC', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'ECHEC', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'ECHEC' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > ECHEC" )

    #[check] Row with 'Edit...' and 'Remove' buttons is in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > ECHEC > [0]" )

    #[check] 'EVENEMENT' checkbox has 'checked' state
    symbol = tr( 'EVENEMENT', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).checked )
    #[check] 'EVENEMENT' combobox value is 'ERREUR'
    symbol = tr( 'EVENEMENT', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'ERREUR' )

    #[check] 'ACTION' checkbox has 'checked' state
    symbol = tr( 'ACTION', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).checked )
    #[check] 'ACTION' combobox value is 'DECOUPE'
    symbol = tr( 'ACTION', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'DECOUPE' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'ECHEC' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > ECHEC" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
