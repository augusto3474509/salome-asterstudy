#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'DEFI_MATERIAU' command named 'Mat02'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'DEFI_MATERIAU' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkMat02()

    pass

def checkMat02():
    #[section] Check DEFI_MATERIAU command named 'Mat02'
    test.log( "Check DEFI_MATERIAU command named 'Mat02'" )

    #[step] Double click on 'Mat02' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Material", "Mat02" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_MATERIAU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )
    #[check] Command name is 'Mat02'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Mat02" )

    #[check] 'ELAS' checkbox has 'checked' state
    symbol = tr( 'ELAS', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ELAS', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU > ELAS" )

    #[check] '2000.0' in 'E' field
    symbol = tr( 'E', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, '2000.0' )
    #[check] '0.0' in 'NU' field
    symbol = tr( 'NU', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, '0.0' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'DEFI_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
