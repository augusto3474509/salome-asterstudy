#------------------------------------------------------------------------------
#  Checks creation of different commands in order to check all widgets that were required for LOT1 and LOT2.
#------------------------------------------------------------------------------
import filecmp

global DEBUG_FUNC
DEBUG_FUNC = "" # Set the desirable function name to debug or "ALL" or "" otherwise

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def newStudy():
    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    createNewStudy()
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #--------------------------------------------------------------------------
def newStage():

    #[section] Stage creation and deletion
    test.log( "Stage creation and deletion" )

    #[step] Create a new stage by toolbar button
    clickButton( waitForObject( ":AsterStudy *.Add Stage_QToolButton" ) )
    #[check] 'Stage' was added to tree
    checkOBItem( "CurrentCase", "Stage_1" )

    #--------------------------------------------------------------------------
def cell(row, col):
    # Currently row and col are not used
    return "{container=':_FunctionTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"

def saveStage( func_name_to_debug ):
    file_name = 'before%s.comm'%func_name_to_debug
    file_path = os.path.join( testdir(), file_name )
    activateOBContextMenuItem( "CurrentCase.Stage_1", "Export Command File" )
    setParameterValue( ":fileNameEdit_QLineEdit", file_path )
    clickButton( waitForObject( ":QFileDialog.Save_QPushButton" ) )

    waitUntilObjectExists( ":QFileDialog.Save_QPushButton" )

    ref_file = os.path.join( casedir(), "ref", file_name )
    if not filecmp.cmp( ref_file, file_path, shallow=False ):
        test.fail("Comparison of reference '%s' and current '%s' stages in text mode." % ( ref_file, file_path ) )

def loadStage( func_name_to_debug = DEBUG_FUNC ):
    file_path = os.path.join( casedir(), 'ref', 'before%s.comm'%func_name_to_debug )
    importStage(file_path, [":Undefined files.OK_QPushButton"])

    activateOBContextMenuItem("CurrentCase.%s" % os.path.splitext(os.path.basename(file_path))[0], "Rename")
    type(waitForObject(":_QExpandingLineEdit"), "Stage_1")
    type(waitForObject(":_QExpandingLineEdit"), "<Return>")

    eval(func_name_to_debug)()

def runCase():
    #[project] AsterStudy
    #[scenario] LOT1 and LOT2: commands
    #[topic] Acceptance LOT1 and LOT2
    #[tested functionality] Commands creation
    #[summary description] Creation of commands using all required types of widgets
    #[expected results] No errors. No exceptions. Correct validation.

    newStudy()

    if not DEBUG_FUNC or DEBUG_FUNC == "ALL":
        newStage()
        _1_Mesh()

        saveStage("_2_Mesh")
        _2_Mesh()

        saveStage("Model")
        Model()

        saveStage("Mat01")
        Mat01()

        saveStage("Mat02")
        Mat02()

        saveStage("MatF")
        MatF()

        saveStage("Load1")
        Load1()

        saveStage("Load2")
        Load2()

        saveStage("Cont1")
        Cont1()

        saveStage("List_r")
        List_r()

        saveStage("List_i")
        List_i()

        saveStage("F_mult")
        F_mult()

        saveStage("_1_NL_res")
        _1_NL_res()

        saveStage("_2_NL_res")
        _2_NL_res()

        saveStage("Displ")
        Displ()

        saveStage("_3_Mesh")
        _3_Mesh()

        saveStage("IMPR_RESU")
        IMPR_RESU()

        saveStage("TEST_RESU")
        TEST_RESU()

        saveStage("END")
    else:
        loadStage() # provide function name to debug

    #--------------------------------------------------------------------------
def _1_Mesh():

    #[section] Creation LIRE_MAILLAGE command named 'Mesh'
    test.log( "Creation LIRE_MAILLAGE command named 'Mesh'" )

    #[step] Select 'Stage' in tree
    selectObjectBrowserItem( "CurrentCase.Stage_1" )

    #[step] Click 'Mesh' combobox in toolbar and select 'LIRE_MAILLAGE' item
    mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 59, 15, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "LIRE\\_MAILLAGE"), 51, 11, 0, Qt.LeftButton)

    #[step] Click 'Rename' in context menu
    activateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.LIRE_MAILLAGE", "Rename" )
    #[step] Type 'Mesh' and press <Enter>
    setParameterValue( ":_QExpandingLineEdit", "Mesh" )
    type( waitForObject( ":_QExpandingLineEdit" ), "<Return>" )
    #[check] 'LIRE_MAILLAGE' was renamed to 'Mesh'
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "Mesh" )

    #[step] Click 'Edit' in context menu
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Mesh.Mesh", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'LIRE_MAILLAGE'
    test.compare(str(waitForObjectExists(":_ParameterTitle").displayText), "LIRE_MAILLAGE")
    #[check] Command name is 'Mesh'
    test.compare(str(waitForObjectExists(":Name_QLineEdit").displayText), "Mesh")

    #[step] Check 'FORMAT' checkbox
    symbol = tr( 'FORMAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'MED' in 'FORMAT' combobox
    symbol = tr( 'FORMAT', 'QComboBox' )
    mouseClick(waitForObject(symbol), 75, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(symbol, "MED"), 45, 11, 0, Qt.LeftButton)

#    #[step] Check 'Input mesh file' checkbox
#    symbol = tr( 'UNITE', 'QCheckBox' )
#   clickButton( waitForObject( symbol ) )
    #[step] Click '...' browse button
    symbol = tr( 'UNITE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Open 'Mesh_recette.med' file
    med_file_path = os.path.join( casedir(), findFile( "testdata", "Mesh_recette.med" ) )
    setParameterValue(":fileNameEdit_QLineEdit", med_file_path)
    clickButton( waitForObject( ":QFileDialog.Open_QPushButton" ) )
    #[check] 'Mesh_recette.med' file name was added to combobox and activated
    symbol = tr( 'UNITE', 'QComboBox' )
    test.compare(str(waitForObjectExists(symbol).currentText), os.path.basename(med_file_path))
    #[step] Check 'NOM_MED' checkbox
    symbol = tr( 'NOM_MED', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'Mesh_recette' in 'NOM_MED' combobox
    symbol = tr( 'NOM_MED', 'QComboBox' )
    mouseClick(waitForObject(symbol), 10, 10, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(symbol, "Mesh\\_recette"), 70, 8, 0, Qt.LeftButton)

    #[step] Click OK button to accept the changes
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #--------------------------------------------------------------------------
def _2_Mesh():

    #[section] Creation MODI_MAILLAGE command named 'Mesh'
    test.log( "Creation MODI_MAILLAGE command named 'Mesh'" )

    #[step] Click 'Mesh' combobox in toolbar and select 'MODI_MAILLAGE' item
    mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 59, 15, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "MODI\\_MAILLAGE"), 51, 11, 0, Qt.LeftButton)

    #[step] Double click on 'MODI_MAILLAGE' item in tree
    waitForDoubleClick( selectItem( "Stage_1", "Mesh", "MODI_MAILLAGE" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )
    #[check] Command name is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "automatic" )

    #[step] Type 'Mesh' in 'Name' input field instead of 'MODI_MAILLAGE'
    setParameterValue(":Name_QLineEdit", "Mesh")

    #[step] Select 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    setParameterValue( symbol, 'Mesh' )

    #[step] Check 'ORIE_PEAU_2D' checkbox
    symbol = tr( 'ORIE_PEAU_2D', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ORIE_PEAU_2D', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D" )
    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0]" )
    #[step] Click 'Edit...' button of 'GROUP_MA' keyword
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] 'GROUP_MA' panel was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0] > GROUP_MA" )
    #[check] 'Mesh' mesh is set in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Mesh' )

    ##[step] Select 'Cont_mast' and 'Cont_slav' groups in list
    waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_mast")
    clickItem(":_QTreeWidget", "1D elements.Cont\\_mast", 10, 10, 0, Qt.LeftButton)
    waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_slav")
    clickItem(":_QTreeWidget", "1D elements.Cont\\_slav", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0]" )
    #[check] 'Edit...' button of 'GROUP_MA' keyword was renamed to selected entities
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Command's panel was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'MODI_MAILLAGE' was renamed to 'Mesh'
    checkOBItem( "CurrentCase", "Stage_1", "Mesh", "Mesh" )

    #--------------------------------------------------------------------------
def Model():

    #[section] Creation AFFE_MODELE command named 'Model'
    test.log( "Creation AFFE_MODELE command named 'Model'" )

    #[step] Click 'Show All' toolbar button
    #[step] Type 'AFFE_MODELE' in search field
    #[step] Select 'AFFE_MODELE' in list
    #[step] Click OK
    addCommandByDialog("AFFE_MODELE", "Model Definition")

    #[step] Double click on 'AFFE_MODELE' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Model Definition.AFFE\\_MODELE", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_MODELE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE" )
    #[check] Command name is 'AFFE_MODELE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "AFFE_MODELE" )

    #[step] Type 'Model' in 'Name' input field instead of 'AFFE_MODELE'
    setParameterValue(":Name_QLineEdit", "Model")

    #[step] Select 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    #[step] Check 'AFFE' checkbox
    symbol = tr( 'AFFE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE" )
    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0]" )
    #[step] Check 'TOUT' radiobutton
    symbol = tr( 'TOUT', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[check] 'TOUT' combobox value is 'OUI'
    symbol = tr( 'TOUT', 'QComboBox' )
    setParameterValue( symbol, 'OUI' )
    #[check] 'PHENOMENE' combobox value is 'MECANIQUE'
    symbol = tr( 'PHENOMENE', 'QComboBox' )
    setParameterValue( symbol, 'MECANIQUE' )
    #[step] Click 'Edit...' button of 'MODELISATION' keyword
    symbol = tr( 'MODELISATION', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0] > MODELISATION" )
    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with combobox and 'Remove' buttons was added to list
    symbol = tr( '0', 'QComboBox' )
    test.verify( waitForObject( symbol ) )
    #[step] Select 'C_PLAN' in the combobox
    setParameterValue( symbol, 'C_PLAN' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE[0]' list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE > [0]" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE > AFFE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE_MODELE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MODELE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'AFFE_MODELE' was renamed to 'Model'
    checkOBItem( "CurrentCase", "Stage_1", "Model Definition", "Model" )

    #--------------------------------------------------------------------------
def Mat01():

    #[section] Creation DEFI_MATERIAU command named 'Mat01'
    test.log( "Creation DEFI_MATERIAU command named 'Mat01'" )

    #[step] Add 'DEFI_MATERIAU' command by 'Show All' dialog
    addCommandByDialog("DEFI_MATERIAU", "Material")

    #[step] Double click on 'DEFI_MATERIAU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Material.DEFI\\_MATERIAU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_MATERIAU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )
    #[check] Command name is 'DEFI_MATERIAU'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_MATERIAU" )

    #[step] Type 'Mat01' in 'Name' input field instead of 'DEFI_MATERIAU'
    setParameterValue(":Name_QLineEdit", "Mat01")

    #[step] Check 'ELAS' checkbox
    symbol = tr( 'ELAS', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ELAS', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU > ELAS" )

    #[step] Type '2000' in 'E' field
    symbol = tr( 'E', 'QLineEdit' )
    setParameterValue( symbol, '2000' )
    #[step] Type '0.3' in 'NU' field
    symbol = tr( 'NU', 'QLineEdit' )
    setParameterValue( symbol, '0.3' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_MATERIAU' was renamed to 'Mat01'
    checkOBItem( "CurrentCase", "Stage_1", "Material", "Mat01" )

    #--------------------------------------------------------------------------
def Mat02():

    #[section] Creation DEFI_MATERIAU command named 'Mat02'
    test.log( "Creation DEFI_MATERIAU command named 'Mat02'" )

    #[step] Add 'DEFI_MATERIAU' command by 'Show All' dialog
    addCommandByDialog("DEFI_MATERIAU", "Material")

    #[step] Double click on 'DEFI_MATERIAU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Material.DEFI\\_MATERIAU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_MATERIAU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )
    #[check] Command name is 'DEFI_MATERIAU'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_MATERIAU" )

    #[step] Type 'Mat02' in 'Name' input field instead of 'DEFI_MATERIAU'
    setParameterValue(":Name_QLineEdit", "Mat02")

    #[step] Check 'ELAS' checkbox
    symbol = tr( 'ELAS', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ELAS', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU > ELAS" )

    #[step] Type '2000' in 'E' field
    symbol = tr( 'E', 'QLineEdit' )
    setParameterValue( symbol, '2000' )
    #[step] Type '0.0' in 'NU' field
    symbol = tr( 'NU', 'QLineEdit' )
    setParameterValue( symbol, '0.0' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_MATERIAU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_MATERIAU' was renamed to 'Mat02'
    checkOBItem( "CurrentCase", "Stage_1", "Material", "Mat02" )

    #--------------------------------------------------------------------------
def MatF():

    #[section] Creation AFFE_MATERIAU command named 'MatF'
    test.log( "Creation AFFE_MATERIAU command named 'MatF'" )

    #[step] Add 'AFFE_MATERIAU' command by 'Show All' dialog
    addCommandByDialog("AFFE_MATERIAU", "Material")

    #[step] Double click on 'AFFE_MATERIAU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Material.AFFE\\_MATERIAU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_MATERIAU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU" )
    #[check] Command name is 'AFFE_MATERIAU'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "AFFE_MATERIAU" )

    #[step] Type 'MatF' in 'Name' input field instead of 'AFFE_MATERIAU'
    setParameterValue(":Name_QLineEdit", "MatF")

    #[step] Check 'MAILLAGE' checkbox
    symbol = tr( 'MAILLAGE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    #[step] Check 'MODELE' checkbox
    symbol = tr( 'MODELE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    setParameterValue( symbol, "Model" )

    #[step] Click 'Edit...' button of 'AFFE' keyword
    symbol = tr( 'AFFE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'AFFE' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[step] Check 'GROUP_MA' radiobutton
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0] > GROUP_MA" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'Upper' group in list
    waitForObjectItem(":_QTreeWidget", "2D elements.Upper")
    clickItem(":_QTreeWidget", "2D elements.Upper", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[check] 'Edit...' button of 'GROUP_MA' keyword was renamed to 'Upper'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Click 'Edit...' button of 'MATER' keyword
    symbol = tr( 'MATER', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0] > MATER" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with combobox and 'Remove' buttons was added to list
    symbol = tr( '0', 'QComboBox' )
#    test.verify( waitForObject( symbol ) )
    test.verify( waitForObject( symbol ) )
    #[step] Select 'Mat01' in the combobox
    setParameterValue( symbol, 'Mat01' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [0]" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '1', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click second 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1]" )

    #[step] Check 'GROUP_MA' radiobutton
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1] > GROUP_MA" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'Lower' group in list
    waitForObjectItem(":_QTreeWidget", "2D elements.Lower")
    clickItem(":_QTreeWidget", "2D elements.Lower", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1]" )

    #[check] 'Edit...' button of 'GROUP_MA' keyword was renamed to 'Lower'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Click 'Edit...' button of 'MATER' keyword
    symbol = tr( 'MATER', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1] > MATER" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with combobox and 'Remove' buttons was added to list
    symbol = tr( '0', 'QComboBox' )
    test.verify( waitForObject( symbol ) )
    #[step] Select 'Mat02' in the combobox
    setParameterValue( symbol, 'Mat02' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE > [1]" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU > AFFE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE_MATERIAU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_MATERIAU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'AFFE_MATERIAU' was renamed to 'MatF'
    checkOBItem( "CurrentCase", "Stage_1", "Material", "MatF" )

    #--------------------------------------------------------------------------
def Load1():

    #[section] Creation AFFE_CHAR_CINE command named 'Load1'
    test.log( "Creation AFFE_CHAR_CINE command named 'Load1'" )

    #[step] Add 'AFFE_CHAR_CINE' command by 'Show All' dialog
    addCommandByDialog("AFFE_CHAR_CINE", "BC and Load")

    #[step] Double click on 'AFFE_CHAR_CINE' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.BC and Load.AFFE\\_CHAR\\_CINE", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_CHAR_CINE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE" )
    #[check] Command name is 'AFFE_CHAR_CINE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "AFFE_CHAR_CINE" )

    #[step] Type 'Load1' in 'Name' input field instead of 'AFFE_CHAR_CINE'
    setParameterValue(":Name_QLineEdit", "Load1")

    #[step] Select 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    setParameterValue( symbol, "Model" )

    #[step] Check 'MECA_IMPO' radiobutton
    symbol = tr( 'MECA_IMPO', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'MECA_IMPO', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'MECA_IMPO' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of MECA_IMPO[0] item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [0]" )

    #[step] Check 'GROUP_MA' radiobutton
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [0] > GROUP_MA" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'Sym_x' group in list
    waitForObjectItem(":_QTreeWidget", "1D elements.Sym\\_x")
    clickItem(":_QTreeWidget", "1D elements.Sym\\_x", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [0]" )

    #[check] 'Edit...' button of 'GROUP_MA' keyword was renamed to 'Sym_x'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Check 'DX' checkbox
    symbol = tr( 'DX', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '0.0' in 'DX' field
    symbol = tr( 'DX', 'QLineEdit' )
    setParameterValue( symbol, '0.0' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'MECA_IMPO' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '1', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click second 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of MECA_IMPO[1] item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [1]" )

    #[step] Check 'GROUP_MA' radiobutton
    symbol = tr( 'GROUP_MA', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [1] > GROUP_MA" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'Sym_y' group in list
    waitForObjectItem(":_QTreeWidget", "1D elements.Sym\\_y")
    clickItem(":_QTreeWidget", "1D elements.Sym\\_y", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO > [1]" )

    #[check] 'Edit...' button of 'GROUP_MA' keyword was renamed to 'Sym_y'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Check 'DY' checkbox
    symbol = tr( 'DY', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '0.0' in 'DY' field
    symbol = tr( 'DY', 'QLineEdit' )
    setParameterValue( symbol, '0.0' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'MECA_IMPO' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE > MECA_IMPO" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE_CHAR_CINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_CINE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'AFFE_CHAR_CINE' was renamed to 'Load1'
    checkOBItem( "CurrentCase", "Stage_1", "BC and Load", "Load1" )

    #--------------------------------------------------------------------------
def Load2():

    #[section] Creation AFFE_CHAR_MECA command named 'Load2'
    test.log( "Creation AFFE_CHAR_MECA command named 'Load2'" )

    #[step] Add 'AFFE_CHAR_MECA' command by 'Show All' dialog
    addCommandByDialog("AFFE_CHAR_MECA", "BC and Load")

    #[step] Double click on 'AFFE_CHAR_MECA' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.BC and Load.AFFE\\_CHAR\\_MECA", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_CHAR_MECA'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA" )
    #[check] Command name is 'AFFE_CHAR_MECA'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "AFFE_CHAR_MECA" )

    #[step] Type 'Load2' in 'Name' input field instead of 'AFFE_CHAR_MECA'
    setParameterValue(":Name_QLineEdit", "Load2")

    #[step] Select 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    setParameterValue( symbol, "Model" )

    #[step] Type 'PRES' in search field
    symbol = tr( 'searcher', 'QLineEdit' )
    setParameterValue( symbol, "PRES" )

    #[step] Check 'PRES_REP' checkbox
    symbol = tr( 'PRES_REP', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'PRES_REP', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'PRES_REP' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP > [0]" )

    #[step] Check 'GROUP_MA' checkbox
    symbol = tr( 'GROUP_MA', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP > [0] > GROUP_MA" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'Upper_face' group in list
    waitForObjectItem(":_QTreeWidget", "1D elements.Upper\\_face")
    clickItem(":_QTreeWidget", "1D elements.Upper\\_face", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP > [0]" )

    #[check] 'Edit...' button of 'GROUP_MA' keyword was renamed to 'Upper_face'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Check 'PRES' checkbox
    symbol = tr( 'PRES', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '25.0' in 'PRES' field
    symbol = tr( 'PRES', 'QLineEdit' )
    setParameterValue( symbol, '25.0' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'PRES_REP' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'AFFE_CHAR_MECA' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'AFFE_CHAR_MECA' was renamed to 'Load2'
    checkOBItem( "CurrentCase", "Stage_1", "BC and Load", "Load2" )

    #--------------------------------------------------------------------------
def Cont1():

    #[section] Creation DEFI_CONTACT command named 'Cont1'
    test.log( "Creation DEFI_CONTACT command named 'Cont1'" )

    #[step] Add 'DEFI_CONTACT' command by 'Show All' dialog
    addCommandByDialog("DEFI_CONTACT", "BC and Load")

    #[step] Double click on 'DEFI_CONTACT' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.BC and Load.DEFI\\_CONTACT", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_CONTACT'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT" )
    #[check] Command name is 'DEFI_CONTACT'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_CONTACT" )

    #[step] Type 'Cont1' in 'Name' input field instead of 'DEFI_CONTACT'
    setParameterValue(":Name_QLineEdit", "Cont1")

    #[step] Select 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    setParameterValue( symbol, "Model" )

    #[step] Check 'LISSAGE' checkbox
    symbol = tr( 'FORMULATION', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'CONTINUE' in 'FORMULATION' combobox
    symbol = tr( 'FORMULATION', 'QComboBox' )
    setParameterValue( symbol, 'CONTINUE' )

    #[step] Check 'LISSAGE' checkbox
    symbol = tr( 'LISSAGE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Set 'OUI' in 'LISSAGE' checkbox
    symbol = tr( 'LISSAGE-value', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )

    #[step] Check 'ALGO_RESO_CONT' checkbox
    symbol = tr( 'ALGO_RESO_CONT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'POINT_FIXE' in 'ALGO_RESO_CONT' combobox
    symbol = tr( 'ALGO_RESO_CONT', 'QComboBox' )
    setParameterValue( symbol, 'POINT_FIXE' )

    #[step] Check 'ITER_CONT_MAXI' checkbox
    symbol = tr( 'ITER_CONT_MAXI', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '15' in 'ITER_CONT_MAXI' field
    symbol = tr( 'ITER_CONT_MAXI', 'QLineEdit' )
    setParameterValue( symbol, '15' )

    #[step] Click 'Edit...' button of 'ZONE' keyword
    symbol = tr( 'ZONE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of ZONE[0] item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0]" )

    #[step] Check 'GROUP_MA_MAIT' radiobutton
    symbol = tr( 'GROUP_MA_MAIT', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA_MAIT', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA_MAIT' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0] > GROUP_MA_MAIT" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'Cont_mast' group in list
    waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_mast")
    clickItem(":_QTreeWidget", "1D elements.Cont\\_mast", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0]" )
    #[check] 'Edit...' button of 'GROUP_MA_MAIT' keyword was renamed to 'Cont_mast'
    symbol = tr( 'GROUP_MA_MAIT', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Check 'GROUP_MA_ESCL' radiobutton
    symbol = tr( 'GROUP_MA_ESCL', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'GROUP_MA_ESCL', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA_ESCL' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0] > GROUP_MA_ESCL" )

    ##[step] Select second 'Mesh' item in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    waitForObject( symbol ).setCurrentIndex( 1 )

    ##[step] Select 'Cont_slav' group in list
    waitForObjectItem(":_QTreeWidget", "1D elements.Cont\\_slav")
    clickItem(":_QTreeWidget", "1D elements.Cont\\_slav", 10, 10, 0, Qt.LeftButton)

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE > [0]" )
    #[check] 'Edit...' button of 'GROUP_MA_ESCL' keyword was renamed to 'Cont_slav'
    symbol = tr( 'GROUP_MA_ESCL', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'ZONE' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT > ZONE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_CONTACT' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_CONTACT" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_CONTACT' was renamed to 'Cont1'
    checkOBItem( "CurrentCase", "Stage_1", "BC and Load", "Cont1" )

    #--------------------------------------------------------------------------
def List_r():

    #[section] Creation DEFI_LIST_REEL command named 'List_r'
    test.log( "Creation DEFI_LIST_REEL command named 'List_r'" )

    #[step] Add 'DEFI_LIST_REEL' command by 'Show All' dialog
    addCommandByDialog("DEFI_LIST_REEL", "Functions and Lists")

    #[step] Double click on 'DEFI_LIST_REEL' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Functions and Lists.DEFI\\_LIST\\_REEL", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_LIST_REEL'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL" )
    #[check] Command name is 'DEFI_LIST_REEL'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_LIST_REEL" )

    #[step] Type 'List_r' in 'Name' input field instead of 'DEFI_LIST_REEL'
    setParameterValue(":Name_QLineEdit", "List_r")

    #[step] Check 'DEBUT' radiobutton
    symbol = tr( 'DEBUT', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '0.0' as value
    symbol = tr( 'DEBUT', 'QLineEdit' )
    setParameterValue( symbol, "0.0")

    #[check] 'INTERVALLE' checkbox was checked automatically
    symbol = tr( 'INTERVALLE', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).checked, True )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'INTERVALLE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL > INTERVALLE" )

    #[step] Enter '1.0' value as first interval's 'Until' parameter
    waitForObjectItem(":_IntervalTable", "0/0")
    doubleClickItem(":_IntervalTable", "0/0", 5, 5, 0, Qt.LeftButton)
    cell_0_0 = "{container=':_IntervalTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    type(waitForObject(cell_0_0), "1.0")
    type(waitForObject(cell_0_0), "<Return>")
    
    #[step] Choose 'Step length' interval type
    waitForObjectItem(":_IntervalTable", "0/1")
    doubleClickItem(":_IntervalTable", "0/1", 5, 5, 0, Qt.LeftButton)
    setParameterValue(":_QComboBox", 'Step length' )
    type(waitForObject(":_QComboBox"), "<Return>")

    #[step] Enter '1.0' value as first interval's 'Step value' parameter
    waitForObjectItem(":_IntervalTable", "0/2")
    doubleClickItem(":_IntervalTable", "0/2", 5, 5, 0, Qt.LeftButton)
    cell_0_2 = "{container=':_IntervalTable' name='ValueOrVariableEditor_LineEdit' type='QLineEdit' visible='1'}"
    type(waitForObject(cell_0_2), "1.0")
    type(waitForObject(cell_0_2), "<Return>")

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_REEL' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_LIST_REEL' was renamed to 'List_r'
    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "List_r" )

    #--------------------------------------------------------------------------
def List_i():

    #[section] Creation DEFI_LIST_INST command named 'List_i'
    test.log( "Creation DEFI_LIST_INST command named 'List_i'" )

    #[step] Add 'DEFI_LIST_INST' command by 'Show All' dialog
    addCommandByDialog("DEFI_LIST_INST", "Functions and Lists")

    #[step] Double click on 'DEFI_LIST_INST' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Functions and Lists.DEFI\\_LIST\\_INST", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_LIST_INST'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST" )
    #[check] Command name is 'DEFI_LIST_INST'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_LIST_INST" )

    #[step] Type 'List_i' in 'Name' input field instead of 'DEFI_LIST_INST'
    setParameterValue(":Name_QLineEdit", "List_i")

    #[check] Click 'METHODE' check box
    symbol = tr( 'METHODE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )

    #[check] 'METHODE' combobox is enable
    symbol = tr( 'METHODE', 'QComboBox' )
    #[check] 'METHODE' combobox value is 'MANUEL'
    test.compare( waitForObjectExists( symbol ).currentText, 'MANUEL' )

    #[step] Click 'Edit...' button of 'DEFI_LIST' keyword
    symbol = tr( 'DEFI_LIST', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] 'DEFI_LIST' panel was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > DEFI_LIST" )

    #[step] Check 'LIST_INST' radiobutton
    symbol = tr( 'LIST_INST', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'List_r' in 'LIST_INST' combobox
    symbol = tr( 'LIST_INST', 'QComboBox' )
    setParameterValue( symbol, 'List_r' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST" )

    #[step] Check 'ECHEC' checkbox
    symbol = tr( 'ECHEC', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ECHEC', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'ECHEC' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > ECHEC" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > ECHEC > [0]" )

    #[step] Check 'EVENEMENT' checkbox
    symbol = tr( 'EVENEMENT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[check] 'EVENEMENT' combobox value is 'ERREUR'
    symbol = tr( 'EVENEMENT', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'ERREUR' )

    #[step] Check 'ACTION' checkbox
    symbol = tr( 'ACTION', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[check] 'ACTION' combobox value is 'DECOUPE'
    symbol = tr( 'ACTION', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'DECOUPE' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'ECHEC' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST > ECHEC" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_INST" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_LIST_INST' was renamed to 'List_i'
    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "List_i" )

    #--------------------------------------------------------------------------
def F_mult():

    #[section] Creation DEFI_FONCTION command named 'F_mult'
    test.log( "Creation DEFI_FONCTION command named 'F_mult'" )

    #[step] Add 'DEFI_FONCTION' command by 'Show All' dialog
    addCommandByDialog("DEFI_FONCTION", "Functions and Lists")

    #[step] Call context menu on 'DEFI_FONCTION' item in tree
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Functions and Lists.DEFI_FONCTION", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_FONCTION'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )
    #[check] Command name is 'DEFI_FONCTION'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "DEFI_FONCTION" )

    #[step] Type 'F_mult' in 'Name' input field instead of 'DEFI_FONCTION'
    setParameterValue(":Name_QLineEdit", "F_mult")

    #[check] 'NOM_PARA' combobox is enable
    symbol = tr( 'NOM_PARA', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'INST' value in 'NOM_PARA' combobox
    setParameterValue( symbol, 'INST' )

    #[step] Check 'VALE' radiobutton
    symbol = tr( 'VALE', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'VALE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of table was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION > VALE" )

    #[check] Tab has 1 row and 2 columns
    tableWidget = waitForObject("{container=':qt_tabwidget_stackedwidget_QWidget' " +
                                "type='QTableWidget' unnamed='1' visible='1'}")
    test.compare( tableWidget.rowCount, 1 )

    waitForObjectItem(":_FunctionTable", "0/0")
    #[step] Do double click in cell (0, 0)
    doubleClickItem(":_FunctionTable", "0/0", 39, 13, 0, Qt.LeftButton)
    #[step] Type '0.0' in  cell (0, 0) and press <Tab>
    type(waitForObject(cell(0, 0)), "0.0")
    type(waitForObject(cell(0, 0)), "<Tab>")

    #[step] Type '0.0' in  cell (0, 1) and press <Tab>
    type(waitForObject(cell(0, 1)), "0.0")
    type(waitForObject(cell(0, 1)), "<Tab>")

    #[check] A second row was added
    test.compare( tableWidget.rowCount, 2 )

    #[step] Type '1.0' in  cell (1, 0) and press <Tab>
    type(waitForObject(cell(1, 0)), "1.0")
    type(waitForObject(cell(1, 0)), "<Tab>")

    #[step] Type '1.0' in  cell (1, 1) and press <Tab>
    type(waitForObject(cell(1, 1)), "1.0")
    type(waitForObject(cell(1, 1)), "<Return>")

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_INST' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_FONCTION" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'DEFI_FONCTION' was renamed to 'F_mult'
    checkOBItem( "CurrentCase", "Stage_1", "Functions and Lists", "F_mult" )

    #--------------------------------------------------------------------------
def _1_NL_res():

    #[section] Creation STAT_NON_LINE command named 'NL_res'
    test.log( "Creation STAT_NON_LINE command named 'NL_res'" )

    #[step] Add 'STAT_NON_LINE' command by 'Show All' dialog
    addCommandByDialog("STAT_NON_LINE", "Analysis")

    #[step] Double click on 'DEFI_LIST_INST' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Analysis.STAT\\_NON\\_LINE", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'STAT_NON_LINE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )
    #[check] Command name is 'STAT_NON_LINE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "STAT_NON_LINE" )

    #[step] Type 'NL_res' in 'Name' input field instead of 'STAT_NON_LINE'
    setParameterValue(":Name_QLineEdit", "NL_res")

    #[check] 'MODELE' combobox is enable
    symbol = tr( 'MODELE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'Model' value in 'MODELE' combobox
    setParameterValue( symbol, 'Model' )

    #[check] 'CHAM_MATER' combobox is enable
    symbol = tr( 'CHAM_MATER', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'MatF' value in 'CHAM_MATER' combobox
    setParameterValue( symbol, 'MatF' )

    #[check] 'Edit...' button of 'EXCIT' is enable
    symbol = tr( 'EXCIT', 'QPushButton' )
    test.verify( waitForObject( symbol ).enabled )
    #[step] Click the enabled 'Edit...' button
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT > [0]" )

    #[check] 'CHARGE' combobox is enable
    symbol = tr( 'CHARGE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'Load1' value in 'CHARGE' combobox
    setParameterValue( symbol, 'Load1' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '1', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT > [1]" )

    #[check] 'CHARGE' combobox is enable
    symbol = tr( 'CHARGE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'Load1' value in 'CHARGE' combobox
    setParameterValue( symbol, 'Load2' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'STAT_NON_LINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )

    #[step] Check 'CONTACT' checkbox
    symbol = tr( 'CONTACT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'Cont1' value in 'CONTACT' combobox
    symbol = tr( 'CONTACT', 'QComboBox' )
    setParameterValue( symbol, 'Cont1' )

    #[check] 'Edit...' button of 'INCREMENT' is enable
    symbol = tr( 'INCREMENT', 'QPushButton' )
    test.verify( waitForObject( symbol ).enabled )
    #[step] Click the enabled 'Edit...' button
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'INCREMENT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > INCREMENT" )

    #[check] 'LIST_INST' combobox is enable
    symbol = tr( 'LIST_INST', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'List_i' value in 'LIST_INST' combobox
    setParameterValue( symbol, 'List_i' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'STAT_NON_LINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )

    #[step] Check 'NEWTON' checkbox
    symbol = tr( 'NEWTON', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'NEWTON', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'NEWTON' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > NEWTON" )

    #[step] Check 'REAC_ITER' checkbox
    symbol = tr( 'REAC_ITER', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[check] Check 'REAC_ITER' value is '1'
    symbol = tr( 'REAC_ITER', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "1" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'STAT_NON_LINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'STAT_NON_LINE' was renamed to 'NL_res'
    checkOBItem( "CurrentCase", "Stage_1", "Analysis", "NL_res" )

    #--------------------------------------------------------------------------
def _2_NL_res():

    #[section] Creation CALC_CHAMP command named 'NL_res'
    test.log( "Creation CALC_CHAMP command named 'NL_res'" )

    #[step] Add 'CALC_CHAMP' command by 'Show All' dialog
    addCommandByDialog("CALC_CHAMP", "Post Processing")

    #[step] Call context menu on 'CALC_CHAMP' item in tree
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Post Processing.CALC_CHAMP", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'CALC_CHAMP'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CALC_CHAMP" )
    #[check] Command name is 'CALC_CHAMP'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "CALC_CHAMP" )

    #[step] Type 'NL_res' in 'Name' input field instead of 'CALC_CHAMP'
    setParameterValue(":Name_QLineEdit", "NL_res")

    #[check] 'RESULTAT' combobox is enable
    symbol = tr( 'RESULTAT', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'NL_res' value in 'RESULTAT' combobox
    setParameterValue( symbol, 'NL_res' )

    #[step] Type 'CONTRAINTE' in search field
    symbol = tr( 'searcher', 'QLineEdit' )
    setParameterValue( symbol, "CONTRAINTE" )

    #[step] Check 'CONTRAINTE' checkbox
    symbol = tr( 'CONTRAINTE', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'CONTRAINTE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'CONTRAINTE' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CALC_CHAMP > CONTRAINTE" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with combobox and 'Remove' button was added to list
    symbol = tr( '0', 'QComboBox' )
    test.verify( waitForObject( symbol ) )
    #[step] Select 'SIEF_NOEU' in the combobox
    setParameterValue( symbol, 'SIEF_NOEU' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'CALC_CHAMP' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CALC_CHAMP" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'CALC_CHAMP' was renamed to 'NL_res'
    checkOBItem( "CurrentCase", "Stage_1", "Post Processing", "NL_res" )

    #--------------------------------------------------------------------------
def Displ():

    #[section] Creation CREA_CHAMP command named 'Displ'
    test.log( "Creation CREA_CHAMP command named 'Displ'" )

    #[step] Add 'CREA_CHAMP' command by 'Show All' dialog
    addCommandByDialog("CREA_CHAMP", "Post Processing")

    #[step] Call context menu on 'CREA_CHAMP' item in tree
    waitForActivateOBContextMenuItem( "CurrentCase.Stage_1.Post Processing.CREA_CHAMP", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'CREA_CHAMP'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "CREA_CHAMP" )
    #[check] Command name is 'CREA_CHAMP'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "CREA_CHAMP" )

    #[step] Type 'Displ' in 'Name' input field instead of 'CREA_CHAMP'
    setParameterValue(":Name_QLineEdit", "Displ")

    #[check] 'TYPE_CHAM' combobox is enable
    symbol = tr( 'TYPE_CHAM', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'NOEU_DEPL_R' value in 'TYPE_CHAM' combobox
    setParameterValue( symbol, 'NOEU_DEPL_R' )

    #[check] 'OPERATION' combobox is enable
    symbol = tr( 'OPERATION', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'EXTR' value in 'OPERATION' combobox
    setParameterValue( symbol, 'EXTR' )

    #[step] Check 'RESULTAT' checkbox
    symbol = tr( 'RESULTAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'NL_res' value in 'RESULTAT' combobox
    symbol = tr( 'RESULTAT', 'QComboBox' )
    setParameterValue( symbol, "NL_res" )

    #[check] 'NOM_CHAM' combobox is enable
    symbol = tr( 'NOM_CHAM', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'DEPL' value in 'NOM_CHAM' combobox
    setParameterValue( symbol, 'DEPL' )

    #[step] Check 'INST' checkbox
    symbol = tr( 'INST', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Enter '1.0' in the 'INST' input field
    symbol = tr( 'INST', 'QLineEdit' )
    test.verify( waitForObjectExists( symbol ).enabled )
    setParameterValue( symbol, '1.0' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'CREA_CHAMP' was renamed to 'Displ'
    checkOBItem( "CurrentCase", "Stage_1", "Post Processing", "Displ" )

    #--------------------------------------------------------------------------
def _3_Mesh():

    #[section] Creation MODI_MAILLAGE command named 'Mesh'
    test.log( "Creation MODI_MAILLAGE command named 'Mesh'" )

    #[step] Click 'Mesh' combobox in toolbar and select 'MODI_MAILLAGE' item
    mouseClick(waitForObject(":AsterStudy *_ShrinkingComboBox"), 59, 15, 0, Qt.LeftButton)
    mouseClick(waitForObjectItem(":AsterStudy *_ShrinkingComboBox", "MODI\\_MAILLAGE"), 51, 11, 0, Qt.LeftButton)

    #[step] Double click on 'MODI_MAILLAGE' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Mesh.MODI\\_MAILLAGE", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )
    #[check] Command name is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "automatic" )

    #[step] Type 'Mesh' in 'Name' input field instead of 'MODI_MAILLAGE'
    setParameterValue(":Name_QLineEdit", "Mesh")

    #[check] 'MAILLAGE' combobox is enable
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'Mesh' in 'MAILLAGE' combobox
    waitForObject( symbol ).setCurrentIndex( 1 )

    #[step] Check 'DEFORME' checkbox
    symbol = tr( 'DEFORME', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'DEFORME', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'DEFORME' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > DEFORME" )

    #[check] 'OPTION' combobox is enable
    symbol = tr( 'OPTION', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'OPTION' combobox value is 'TRAN'
    setParameterValue( symbol, 'TRAN' )

    #[step] Check 'DEPL' radiobutton
    symbol = tr( 'DEPL', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'Displ' in 'DEPL' combobox
    symbol = tr( 'DEPL', 'QComboBox' )
    setParameterValue( symbol, 'Displ' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Command's panel was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #[check] 'MODI_MAILLAGE' was renamed to 'Mesh'
    test.verify( isItemSelected( "CurrentCase:1", "Stage_1:2", "Mesh:-8", "Mesh" ) )

    #--------------------------------------------------------------------------
def IMPR_RESU():

    #[section] Creation IMPR_RESU command named 'IMPR_RESU'
    test.log( "Creation IMPR_RESU command named 'IMPR_RESU'" )

    #[step] Add 'IMPR_RESU' command by 'Show All' dialog
    addCommandByDialog("IMPR_RESU", "Output")

    #[step] Double click on 'IMPR_RESU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Output.IMPR\\_RESU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'IMPR_RESU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU" )
    #[check] Command name is '_' (empty)
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "_" )

    #[step] Check 'FORMAT' checkbox
    symbol = tr( 'FORMAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'MED' value in 'FORMAT' combobox
    symbol = tr( 'FORMAT', 'QComboBox' )
    setParameterValue( symbol, 'MED' )

    #[check] 'UNITE' combobox is enable
    symbol = tr( 'UNITE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'Mesh_recette.med' in 'UNITE' combobox
    waitForObjectExists( symbol ).setCurrentIndex( 0 )

    #[step] Click the 'Edit...' button for 'RESU'
    symbol = tr( 'RESU', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'RESU' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU > [0]" )

    #[step] Check 'RESULTAT' checkbox
    symbol = tr( 'RESULTAT', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select first 'NL_res' value in 'RESULTAT' combobox
    symbol = tr( 'RESULTAT', 'QComboBox' )
    setParameterValue( symbol, "NL_res" )

    #[step] Check 'NOM_CHAM' checkbox
    symbol = tr( 'NOM_CHAM', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'NOM_CHAM', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'NOM_CHAM' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU > [0] > NOM_CHAM" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with combobox and 'Remove' buttons was added to list
    symbol = tr( '0', 'QComboBox' )
    test.verify( waitForObject( symbol ) )
    #[step] Select 'DEPL' in the combobox
    setParameterValue( symbol, 'DEPL' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'RESU[0]' item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU > [0]" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'RESU' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU > RESU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'IMPR_RESU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "IMPR_RESU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

    #--------------------------------------------------------------------------
def TEST_RESU():

    #[section] Creation TEST_RESU command named 'TEST_RESU'
    test.log( "Creation TEST_RESU command named 'TEST_RESU'" )

    #[step] Add 'TEST_RESU' command by 'Show All' dialog
    addCommandByDialog("TEST_RESU", "Other")

    #[step] Double click on 'TEST_RESU' item in tree
    doubleClickItem( ":_QTreeWidget", "CurrentCase.Stage\\_1.Other.TEST\\_RESU", 59, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'TEST_RESU'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU" )
    #[check] Command name is '_' (empty)
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "_" )

    #[step] Check 'RESU' checkbox
    symbol = tr( 'RESU', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'RESU', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'RESU' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with 'Edit...' and 'Remove' buttons was added to list
    symbol = tr( '0', 'QPushButton' )
    test.verify( waitForObject( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU > [0]" )

    #[check] 'RESULTAT' combobox is enable
    symbol = tr( 'RESULTAT', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[step] Select 'NL_res' value in 'RESULTAT' combobox
    setParameterValue( symbol, "NL_res" )

    #[step] Check 'INST' radiobutton
    symbol = tr( 'INST', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Type '1.0' value in 'INST' input field
    symbol = tr( 'INST', 'QLineEdit' )
    setParameterValue( symbol, "1.0" )

    #[step] Check 'NOM_CHAM' radiobutton
    symbol = tr( 'NOM_CHAM', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'SIEF_NOEU' value in 'NOM_CHAM' combobox
    symbol = tr( 'NOM_CHAM', 'QComboBox' )
    setParameterValue( symbol, 'SIEF_NOEU' )

    #[step] Check 'NOM_CMP' checkbox
    symbol = tr( 'NOM_CMP', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Type 'SIYY' in 'NOM_CMP' field
    symbol = tr( 'NOM_CMP', 'QLineEdit' )
    setParameterValue( symbol, 'SIYY' )

    #[step] Check 'TYPE_TEST' checkbox
    symbol = tr( 'TYPE_TEST', 'QCheckBox' )
    clickButton( waitForObject( symbol ) )
    #[step] Select 'MIN' value in 'TYPE_TEST' combobox
    symbol = tr( 'TYPE_TEST', 'QComboBox' )
    setParameterValue( symbol, 'MIN' )

    #[step] Check 'VALE_CALC' radiobutton
    symbol = tr( 'VALE_CALC', 'QRadioButton' )
    clickButton( waitForObject( symbol ) )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'VALE_CALC', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'VALE_CALC' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU > [0] > VALE_CALC" )

    #[step] Click 'Add item' button
    clickButton( waitForObject( ":Add item_QPushButton" ) )
    #[check] Row with editable field and 'Remove' button was added to list
    symbol = tr( '0', 'QLineEdit' )
    test.verify( waitForObject( symbol ) )
    #[step] Type '-31.2006807436' in the input field
    setParameterValue( symbol, '-31.2006807436' )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'RESU[0]' item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU > [0]" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'RESU' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU > RESU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] Panel of 'TEST_RESU' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "TEST_RESU" )

    #[step] Click OK
    clickButton( waitForObject( ":OK_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.verify( waitUntilObjectExists( ":qt_splithandle__EditionPanel" ) )

def END():
    pass
