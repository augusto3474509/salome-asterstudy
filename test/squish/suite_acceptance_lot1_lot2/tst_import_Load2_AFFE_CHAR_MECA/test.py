#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'AFFE_CHAR_MECA' command named 'Load2'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'AFFE_CHAR_MECA' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkLoad2()

    pass

def checkLoad2():
    #[section] Check AFFE_CHAR_MECA command named 'Load2'
    test.log( "Check AFFE_CHAR_MECA command named 'Load2'" )

    #[step] Double click on 'Load2' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "BC and Load", "Load2" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'AFFE_CHAR_MECA'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA" )
    #[check] Command name is 'Load2'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Load2" )

    #[check] 'Model' in 'MODELE' combobox
    symbol = tr( 'MODELE', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Model' )

    #[check] 'PRES_REP' checkbox has 'checked' state
    symbol = tr( 'PRES_REP', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'PRES_REP', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'PRES_REP' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP" )

    #[check] Row with 'Edit...' and 'Remove' buttons is in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP > [0]" )

    #[check] 'GROUP_MA' checkbox has 'checked' state
    symbol = tr( 'GROUP_MA', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Upper_face' button
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'GROUP_MA' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP > [0] > GROUP_MA" )

    test.warning("As currently function to get mesh groups is not implemented yet (only a dummy stub is used), it is only possible to enter group names by manual typing in line edit field." )
    ##[check] 'Upper_face' group in list
    # TODO: check of groups should be added
    # workaround:
    #[check] 'Upper_face' in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Upper_face" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP > [0]" )

    #[check] Button of 'GROUP_MA' keyword has name 'Upper_face'
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[check] 'PRES' checkbox has 'checked' state
    symbol = tr( 'PRES', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] '25.0' in 'PRES' field
    symbol = tr( 'PRES', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, '25.0' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'PRES_REP' list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA > PRES_REP" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'AFFE_CHAR_MECA' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "AFFE_CHAR_MECA" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
