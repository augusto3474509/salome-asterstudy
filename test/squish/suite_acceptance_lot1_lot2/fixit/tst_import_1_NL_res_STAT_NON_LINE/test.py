#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'STAT_NON_LINE' command named 'NL_res'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'STAT_NON_LINE' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    check_1_NL_res()

    pass

def check_1_NL_res():
    #[section] Check STAT_NON_LINE command named 'NL_res'
    test.log( "Check STAT_NON_LINE command named 'NL_res'" )

    #[step] Double click on 'NL_res' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Analysis", "NL_res" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'STAT_NON_LINE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )
    #[check] Command name is 'NL_res'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "NL_res" )

    #[check] 'MODELE' combobox is mandatory
    symbol = tr( 'MODELE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'Model' value in 'MODELE' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'Model' )

    #[check] 'CHAM_MATER' combobox is mandatory
    symbol = tr( 'CHAM_MATER', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'MatF' value in 'CHAM_MATER' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'MatF' )

    #[check] 'Edit...' button of 'EXCIT' is enable
    symbol = tr( 'EXCIT', 'QPushButton' )
    test.verify( waitForObject( symbol ).enabled )
    #[step] Click 'Edit...' button
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT" )

    #[check] Two rows with 'Edit...' and 'Remove' buttons are in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    symbol = tr( '1', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click first 'Edit...' button in the list
    symbol = tr( '0', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT > [0]" )

    #[check] 'CHARGE' combobox is mandatory
    symbol = tr( 'CHARGE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'Load1' value in 'CHARGE' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'Load1' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT" )

    #[step] Click second 'Edit...' button in the list
    symbol = tr( '1', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT > [1]" )

    #[check] 'CHARGE' combobox is mandatory
    symbol = tr( 'CHARGE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'Load1' value in 'CHARGE' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'Load2' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'EXCIT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > EXCIT" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'STAT_NON_LINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )

    #[check] 'CONTACT' checkbox has 'checked' state
    symbol = tr( 'CONTACT', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'Cont1' value in 'CONTACT' combobox
    symbol = tr( 'CONTACT', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'Cont1' )

    #[check] 'Edit...' button of 'INCREMENT' is enable
    symbol = tr( 'INCREMENT', 'QPushButton' )
    test.verify( waitForObject( symbol ).enabled )
    #[step] Click 'Edit...' button
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'INCREMENT' list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > INCREMENT" )

    #[check] 'LIST_INST' combobox is enable
    symbol = tr( 'LIST_INST', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'List_i' value in 'LIST_INST' combobox
    test.compare( waitForObjectExists( symbol ).currentText, 'List_i' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'STAT_NON_LINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )

    #[check] 'NEWTON' checkbox has 'checked' state
    symbol = tr( 'NEWTON', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'NEWTON', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'NEWTON' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE > NEWTON" )

    #[check] 'REAC_ITER' checkbox has 'checked' state
    symbol = tr( 'REAC_ITER', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'REAC_ITER' value is '1'
    symbol = tr( 'REAC_ITER', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "1" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'STAT_NON_LINE' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "STAT_NON_LINE" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
