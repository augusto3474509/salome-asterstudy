#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'MODI_MAILLAGE' command named 'Mesh'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'MODI_MAILLAGE' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    check_3_Mesh()

    pass

def check_3_Mesh():
    #[section] Check MODI_MAILLAGE command named 'Mesh'
    test.log( "Check MODI_MAILLAGE command named 'Mesh'" )

    #[step] Select 3rd 'Mesh' item in second 'Mesh' category
    selectItem( "Recette_asterstudy", "Mesh:-8", "Mesh" )
    #[step] Click 'Edit - Edit' in main menu
    waitForActivateMenuItem( "Edit", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )
    #[check] Command name is 'Mesh'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Mesh" )

    #[check] 'MAILLAGE' combobox is enable
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'Mesh' in 'MAILLAGE' combobox
    test.compare( waitForObject( symbol ).currentText, 'Mesh' )

    #[check] 'DEFORME' checkbox has 'checked' state
    symbol = tr( 'DEFORME', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click 'Edit...' button
    symbol = tr( 'DEFORME', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of 'DEFORME' was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > DEFORME" )

    #[check] 'OPTION' combobox is enable
    symbol = tr( 'OPTION', 'QComboBox' )
    test.verify( waitForObjectExists( symbol ).enabled )
    #[check] 'OPTION' combobox value is 'TRAN'
    test.compare( waitForObjectExists( symbol ).currentText, 'TRAN' )

    #[check] 'DEPL' radiobutton has 'checked' state
    symbol = tr( 'DEPL', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'Displ' in 'DEPL' combobox
    symbol = tr( 'DEPL', 'QComboBox' )
    test.compare( waitForObjectExists( symbol ).currentText, 'Displ' )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Command's panel was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
