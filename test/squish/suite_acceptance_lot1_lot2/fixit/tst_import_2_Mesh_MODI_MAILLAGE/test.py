#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'MODI_MAILLAGE' command named 'Mesh'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'MODI_MAILLAGE' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    check_2_Mesh()

    pass

def check_2_Mesh():
    #[section] Check MODI_MAILLAGE command named 'Mesh'
    test.log( "Check MODI_MAILLAGE command named 'Mesh'" )

    #[step] Select second "Mesh" in first "Mesh" category
    selectItem( "Recette_asterstudy", "Mesh:-1", "Mesh:7" )
    #[step] Click 'Edit - Edit' in main menu
    waitForActivateMenuItem( "Edit", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'MODI_MAILLAGE'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )
    #[check] Command name is 'Mesh'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "Mesh" )

    #[check] Select 'Mesh' in 'MAILLAGE' combobox
    symbol = tr( 'MAILLAGE', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Mesh' )

    #[check] 'ORIE_PEAU_2D' checkbox is checked
    symbol = tr( 'ORIE_PEAU_2D', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'ORIE_PEAU_2D', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D" )
    #[check] Row with 'Edit...' and 'Remove' buttons is in the list
    symbol = tr( '0', 'QPushButton' )
    test.verify( object.exists( symbol ) )
    #[step] Click 'Edit...' button in the list
    clickButton( waitForObject( symbol ) )
    #[check] Panel of clicked item was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0]" )
    #[step] Click 'Cont_mast,Cont_slav' button of 'GROUP_MA' keyword
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] 'GROUP_MA' panel was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0] > GROUP_MA" )
    #[check] 'Mesh' mesh is set in combobox
    symbol = tr( 'MESH', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, 'Mesh' )

    test.warning("As currently function to get mesh groups is not implemented yet (only a dummy stub is used), it is only possible to enter group names by manual typing in line edit field." )
    ##[check] 'Cont_mast' and 'Cont_slav' groups are checked in the list
    # TODO: checking of groups should be added
    # workaround:
    #[check] 'Cont_mast, Cont_slav' are in 'Manual selection' field
    symbol = tr( 'MANUAL_INPUT', 'QLineEdit' )
    test.compare( waitForObject( symbol ).text, "Cont_mast,Cont_slav" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list item was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D > [0]" )
    #[check] Button of 'GROUP_MA' keyword has name of selected entities
    symbol = tr( 'GROUP_MA', 'QPushButton' )
    test.compare( waitForObject( symbol ).text, "Edit..." )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of list was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE > ORIE_PEAU_2D" )

    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Command's panel was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "MODI_MAILLAGE" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
