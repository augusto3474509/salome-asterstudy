#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'LIRE_MAILLAGE' command named 'Mesh'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'LIRE_MAILLAGE' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    check_1_Mesh()

    pass

def check_1_Mesh():
    #[section] Check LIRE_MAILLAGE command named 'Mesh'
    test.log( "Check LIRE_MAILLAGE command named 'Mesh'" )

    #[step] Select first "Mesh" in first "Mesh" category
    selectItem( "Recette_asterstudy", "Mesh:-1", "Mesh:6" )
    #[step] Click 'Edit - Edit' in main menu
    waitForActivateMenuItem( "Edit", "Edit" )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'LIRE_MAILLAGE'
    test.compare(str(waitForObjectExists(":_ParameterTitle").displayText), "LIRE_MAILLAGE")
    #[check] Command name is 'Mesh'
    test.compare(str(waitForObjectExists(":Name_QLineEdit").displayText), "Mesh")

    test.warning( "UNITE should be checked" )

    #[check] 'FORMAT' checkbox is checked
    symbol = tr( 'FORMAT', 'QCheckBox' )
    test.verify( waitForObject( symbol ).checked )
    #[check] 'FORMAT' combobox value is 'MED'
    symbol = tr( 'FORMAT', 'QComboBox' )
    test.compare( waitForObject( symbol ).currentText, "MED" )

    #[step] Click Close button to accept the changes
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
