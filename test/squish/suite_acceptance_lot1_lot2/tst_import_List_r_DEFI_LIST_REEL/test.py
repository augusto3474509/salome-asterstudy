#------------------------------------------------------------------------------
#  Checks the command content in the imported stage
#------------------------------------------------------------------------------

def main():
    source( findFile( "scripts", "common.py" ) )

    global_start()

def cleanup():
    global_cleanup()

def runCase():
    #[project] AsterStudy
    #[scenario] Check 'DEFI_LIST_REEL' command named 'List_r'
    #[topic] Checks the command content in the imported stage
    #[tested functionality] 'DEFI_LIST_REEL' command
    #[summary description] Edit the command and check keyword values
    #[expected results] No errors. No exceptions. Correct validation.

    #[section] New study creation
    test.log( "New study creation" )

    #[step] Create a new study
    clickButton( waitForObject( ":AsterStudy.New_QToolButton" ) )
    #[step] Activate 'Case View' tab
    clickTab( waitForObject( ":AsterStudy *.Case View_QTabWidget" ), "Case View" )
    #[check] 'CurrentCase' is in the tree
    checkOBItem( "CurrentCase" )

    #[step] Import stage
    importStage( os.path.join( casedir(), findFile( "testdata", 'Recette_asterstudy.comm' ) ), [":Undefined files.OK_QPushButton"] )

    #[check] 'Stage' was added to tree
    test.verify( isItemExpanded( "CurrentCase" ) )
    test.verify( isItemSelected( "CurrentCase", "Recette_asterstudy" ) )

    checkOBItem( "CurrentCase", "Recette_asterstudy" )

    checkList_r()

    pass

def checkList_r():
    #[section] Check DEFI_LIST_REEL command named 'List_r'
    test.log( "Check DEFI_LIST_REEL command named 'List_r'" )

    #[step] Double click on 'List_r' item in tree
    waitForDoubleClick( selectItem( "Recette_asterstudy", "Functions and Lists", "List_r" ), 10, 10, 0, Qt.LeftButton )
    #[check] 'Edit command' panel was opened
    #[check] Command is 'DEFI_LIST_REEL'
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL" )
    #[check] Command name is 'List_r'
    test.compare( str( waitForObjectExists( ":Name_QLineEdit" ).displayText ), "List_r" )

    #[check] 'DEBUT' radiobutton has 'checked' state
    symbol = tr( 'DEBUT', 'QRadioButton' )
    test.verify( waitForObject( symbol ).checked )
    #[check] '0.0' is value
    symbol = tr( 'DEBUT', 'QLineEdit' )
    test.compare( waitForObjectExists( symbol ).text, "0.0" )

    #[check] 'INTERVALLE' checkbox was checked automatically
    symbol = tr( 'INTERVALLE', 'QCheckBox' )
    test.verify( waitForObjectExists( symbol ).checked )
    #[step] Click the enabled 'Edit...' button
    symbol = tr( 'INTERVALLE', 'QPushButton' )
    clickButton( waitForObject( symbol ) )
    #[check] Panel of list was opened
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL > INTERVALLE" )
    
    #[check] Intervals table contains single row
    table = waitForObject(":_IntervalTable")
    test.compare(table.rowCount, 1)
    test.compare(table.item(0, 0).text(), "1.0")
    test.compare(table.item(0, 1).text(), "Step length")
    test.compare(table.item(0, 2).text(), "1.0")
    
    #[step] Click Cancel
    clickButton( waitForObject( ":Cancel_QPushButton" ) )
    #[check] Panel of 'DEFI_LIST_REEL' command was opened back
    test.compare( str( waitForObjectExists( ":_ParameterTitle" ).displayText ), "DEFI_LIST_REEL" )

    #[step] Click Close
    clickButton( waitForObject( ":Close_QPushButton" ) )
    #[check] 'Edit command' panel was closed
    test.xverify( object.exists( ":OK_QPushButton" ) )

    pass
