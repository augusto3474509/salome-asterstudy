#!/usr/bin/env python

"""Performance check"""

import argparse
import os
import os.path as osp
import sys

from hamcrest import *

if sys.version_info >= (3, 3):
    from time import perf_counter as time
else:
    from time import clock as time

from gc import disable as disable_garbage_collection
disable_garbage_collection()

def test():
    from asterstudy.common import to_unicode
    from asterstudy.datamodel.result.message import search_msg

    output = osp.join(os.getenv('ASTERSTUDYDIR'),
                      'data', 'export', 'ssnv128a.output')
    with open(output, "rb") as fobj:
        text = to_unicode(fobj.read())

    t_init = time()
    fact = 50
    text_i = "\n".join([text] * fact)
    print("file size: {0:.3f} MB".format(len(text_i) / 1024. / 1024))
    search_msg(text_i)

    return time() - t_init


def object_creation(n):
    """Create and use 'n' instances."""
    from math import cos, sin
    class Dummy:
        def __init__(self, i):
            self.attr1 = i * (sin(i)**2 + cos(i)**2)
            self.attr2 = n

        def check(self, value):
            assert abs(self.attr1 + self.attr2 - value) < 1.e-6

    for i in range(n):
        obj = Dummy(i)
        obj.check(i + n)


if __name__ == "__main__":
    formatter = lambda prog: argparse.ArgumentDefaultsHelpFormatter(prog, max_help_position=50,
                                                                    width=120)
    parser = argparse.ArgumentParser(formatter_class=formatter)
    parser.add_argument('-n', '--number', type=float, default=45, metavar='PARROTS',
                        help='number of parrots for this anaconda')
    args = parser.parse_args()

    start_time = time()
    object_creation(35000)
    a_parrot_time = time() - start_time
    print('parrot_time = {0:.6f} (secs)'
          .format(a_parrot_time))

    an_anaconda_time = test()
    print('anaconda_time = {0:.6f} (secs)'
          .format(an_anaconda_time))

    a_max_nb_parrots = args.number
    a_nb_parrots = an_anaconda_time / a_parrot_time
    assert_that(a_nb_parrots, less_than_or_equal_to(a_max_nb_parrots))
    print('number_parrots = {0:.3f} (less than {1})'
          .format(a_nb_parrots, a_max_nb_parrots))
