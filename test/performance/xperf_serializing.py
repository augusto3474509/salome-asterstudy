#!/usr/bin/env python

"""Performance check"""


import os
import sys
import tempfile

from asterstudy.datamodel import History
from asterstudy.datamodel.general import ConversionLevel
from hamcrest import *

if sys.version_info >= (3, 3):
    from time import perf_counter as time
else:
    from time import clock as time

from gc import disable as disable_garbage_collection
disable_garbage_collection()

def test_load(basename, tref, nmax):
    filename = os.path.join(os.getenv('ASTERSTUDYDIR'), 'data', 'performance',
                            basename)

    start_time = time()
    history = History.load(filename, strict=ConversionLevel.Syntaxic)
    elap = time() - start_time
    print('load of {1} = {0:.6f} (secs)'.format(elap, basename))

    nb_parrots = elap / tref
    assert_that(nb_parrots, less_than_or_equal_to(nmax))
    print('number_parrots = {0:.3f} (less than {1})'.format(nb_parrots, nmax))
    return history


def test_save(basename, history, tref, nmax):
    ajs = tempfile.mkstemp(suffix='.ajs')[1]
    try:
        start_time = time()
        History.save(history, ajs)
        elap = time() - start_time
        print('save of {1} = {0:.6f} (secs)'.format(elap, basename))

        nb_parrots = elap / tref
        assert_that(nb_parrots, less_than_or_equal_to(nmax))
        print('number_parrots = {0:.3f} (less than {1})\n'
              .format(nb_parrots, nmax))

    finally:
        os.remove(ajs)

def object_creation(n):
    """Create and use 'n' instances."""
    from math import cos, sin
    class Dummy:
        def __init__(self, i):
            self.attr1 = i * (sin(i)**2 + cos(i)**2)
            self.attr2 = n

        def check(self, value):
            assert abs(self.attr1 + self.attr2 - value) < 1.e-6

    for i in range(n):
        obj = Dummy(i)
        obj.check(i + n)


if __name__ == "__main__":
    start_time = time()
    object_creation(35000)
    a_parrot_time = time() - start_time
    print('parrot_time = {0:.6f} (secs)\n'.format(a_parrot_time))

    filename = 'study_anais.ajs'
    history = test_load(filename, a_parrot_time, nmax=24)
    test_save(filename, history, a_parrot_time, nmax=5)
