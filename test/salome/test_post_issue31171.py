# -*- coding: utf-8 -*-

# Copyright 2021 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic test of the performance for MED files with a large number
   of groups"""

import os
import os.path as osp
import time
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ColorRep, ResultFile
from asterstudy.post.utils import nb_points_cells
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_performance_large_nb_groups():
    """
    Test the post-pro performance for MED files with a large number of
    groups
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, 'issue31171.med')
    # This file contains 1 concept called MODES
    # issue31171 shows that Salome Meca V2020.0.1 takes a very long
    # time to load the result file because it contains several hunderds
    # of groups.

    # For the sake of testing let's load the file 3 times and measure the
    # total time needed to load the med file
    start = time.process_time()
    for i in range(5):
        result = ResultFile(medf)
        depl_noeu_field = result.lookup('MODES').lookup('DEPL')

    elapsed_time = time.process_time() - start

    # Check that loading the file 15 times takes less than 15 seconds!
    assert_that(elapsed_time, less_than(15))

    '''remove assert check discussion on MR21 asterstudy git
    try :
        # Check that loading the file 5 times takes less than 5 seconds!
        assert_that(elapsed_time, less_than(5))
    except :
        print(f" Elasped_time = {elapsed_time}")
        # Check that loading the file 15 times takes less than 5 seconds!
        print(f" New strategy. Checking if it's at least less than 15 sec")
        assert_that(elapsed_time, less_than(15))
    '''
#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
