# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for pvsimple integration in unittests"""

import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
# See : https://discourse.paraview.org/t/virtualbox-debian-loguru-caught-a-signal-sigabrt/6716/15

@paraview_post
def test_pvs_render_sigsegv():
    """
    Test if a segmentation fault, caused by pvs.Render when
    salome is in text mode, can be ignored by catching the SIGSEV
    signal and using os._exit to close the application.
    """
    import pvsimple as pvs
    pvs.Render()

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())
#------------------------------------------------------------------------------

# Simple paraview script revealing the segmentation fault
# from paraview import simple as pvs
# import sys
# pvs.Connect()
# ren_view = pvs.FindViewOrCreate('RenderView1', 'RenderView')
# # This is optional, and only available in 5.9.0...
# # ren_view.SMProxy.GetRenderWindow().SetShowWindow(False)
# pvs.Render()
# sys.exit(0)
