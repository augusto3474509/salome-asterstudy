# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ContourRep, ResultFile
from asterstudy.post.utils import get_array_range, nb_points_cells
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_contourrep():
    """
    Test the ContourRep representation.
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, '1d_quad.med')
    # This file contains 1 concept : reslin, and 2 fields
    # SIEF_ELGA (on integration pts), DEPL (on points)
    result = ResultFile(medf)

    sief_elga_field = result.lookup('reslin').lookup('SIEF_ELGA')
    depl_field = result.lookup('reslin').lookup('DEPL')

    #------------------------ ContourRep ----------------------------------------

    # ------------------------------------------
    # ContourRep on a displacement field (DEPL)
    # ------------------------------------------
    rep = ContourRep(depl_field, NbContours=4)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5, 5)))
    assert_that(rep.opts['Component'], equal_to('Magnitude'))

    # >>> ContourRep should automatically calculate a translational magnitude
    #     field and then color by that, let's do the check
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':MAGTRAN'))
    assert_that(rep.contour_by, equal_to(rep.array))
    mag_range = get_array_range(rep.source, rep.array, 0, atype='point')
    assert_that(mag_range[1], close_to(34.0354, 1.e-4))

    # >>> Let's update the representation by using a non translational component
    rep.update_({'Component': 'DRX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5, 5)))
    # It should be colored by an scalar (extracted) point field
    mag_range = get_array_range(rep.source, rep.array, 'DRX', atype='point')
    assert_that(rep.contour_by, equal_to(rep.array+'_DRX'))
    assert_that(mag_range[0], close_to(-41.3294, 1e-4))

    # >>> Let's update the representation on a constant component, the number
    # of contours shall be automatically forced to 1
    rep.update_({'Component': 'DRZ', 'NbContours':10})
    pvs.Render()
    assert_that(rep.opts['NbContours'], equal_to(1))

    # ----------------------------------------------------
    # ContourRep on an integration point field (ELGA)
    # ----------------------------------------------------
    # >>> Create a new ContourRep based on the SIEF ELGA field
    rep = ContourRep(sief_elga_field, Component='SIXY', NbContours=4)
    pvs.Render()
    assert_that(rep.contour_by, equal_to(rep.array+'_SIXY'))


    # Note that this must be improved with a 3d test case where the contour
    # actually results in surfaces, here it is only added as a coverage test



#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
