# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ResultFile
from asterstudy.post.utils import nb_points_cells
from hamcrest import *
from testutils import paraview_post


#------------------------------------------------------------------------------
@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_med_result_parsing():
    """
    Test the main result/concept/field datamodel upon parsing a MED
    result file for post-processing.
    """
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medfiles = ['1d_quad.med','3d_modes.med']

    ref_contents = {
    '1d_quad.med': [('reslin', [('SIEF_ELGA', 6, 'CELLS'), ('DEPL', 21, 'POINTS')]),
                    ('<root>', [])],
    '3d_modes.med': [('modes', [('DEPL', 3, 'POINTS')]),
                     ('<root>', [])],
    }

    ref_dims = {
    '1d_quad.med':  [(1, 3)],
    '3d_modes.med': [(3, 20), (2, 8), (1, 3)],
    }

    ref_maxdims = {
    '1d_quad.med':  1,
    '3d_modes.med': 3,
    }

    for medf in medfiles:
        med_fn = osp.join(root, medf)
        result = ResultFile(med_fn)

        test_contents = []
        for c in result.concepts:
            # Check that we can lookup this concept from the parent result
            assert_that(result.lookup(c.name), equal_to(c))

            c_info = [c.name]
            f_info = []
            for f in c.fields:
                # Check that we can lookup this concept from the parent concept
                assert_that(c.lookup(f.name), equal_to(f))
                f_info.append((f.name, len(f.info['components']), f.info['support']))

                if f.name == 'DEPL':
                    pvkey = f.info['pv-key']

            c_info.append(f_info)
            test_contents.append(tuple(c_info))

        assert_that(result.lookup('INVALID_CONCEPT'), equal_to(None))
        assert_that(c.lookup('INVALID_FIELD'), equal_to(None))

        assert_that(test_contents, equal_to(ref_contents[medf]))

        assert_that(result.dims_nbno, equal_to(ref_dims[medf]))
        assert_that(result.max_dim, equal_to(ref_maxdims[medf]))

        dupl = result.duplicate_source()
        mode = result.source_as_mode()

        # Select the displacement field
        result.source.FieldsStatus = [pvkey]
        result.source.UpdatePipeline()

        dupl.FieldsStatus = [pvkey]
        dupl.UpdatePipeline()

        mode.FieldsStatus = [pvkey]
        mode.UpdatePipeline()

        assert_that(nb_points_cells(dupl), equal_to(nb_points_cells(result.source)))
        assert_that(nb_points_cells(mode), equal_to(nb_points_cells(result.source)))

        assert_that(result.quadratic, equal_to(1))

        result.toggle_quadratic()
        assert_that(result.quadratic, equal_to(0))
        result.toggle_quadratic()
        assert_that(result.quadratic, equal_to(1))


#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
