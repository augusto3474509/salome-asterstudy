# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ResultFile, WarpRep
from asterstudy.post.utils import get_array_range, nb_points_cells
from hamcrest import *
from testutils import paraview_post


@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_warprep():
    """
    Test the WarpRep representation, including coloring by another
    field possibility.
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, '1d_quad.med')
    # This file contains 1 concept : reslin, and 2 fields
    # SIEF_ELGA (on integration pts), DEPL (on points)
    result = ResultFile(medf)
    result.toggle_quadratic()


    sief_elga_field = result.lookup('reslin').lookup('SIEF_ELGA')
    depl_field = result.lookup('reslin').lookup('DEPL')

    #------------------------ WarpRep ----------------------------------------

    # --------------------------------------
    # WarpRep on a displacement field (DEPL)
    # --------------------------------------
    rep = WarpRep(depl_field, ScaleFactorAuto=True)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    assert_that(rep.opts['Component'], equal_to('Magnitude'))
    assert_that(rep.opts['ScaleFactor'], close_to(1.46905e-3, 1.e-7))

    # >>> WarpRep should automatically extract a translational vector field
    #     and then color by that, let's do the check
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':MAGTRAN'))
    mag_range = get_array_range(rep.source, rep.array, 'Magnitude', atype='point')
    assert_that(mag_range[1], close_to(34.0354, 1.e-4))

    # >>> Let's update the representation by using a non translational component
    rep.update_({'Component': 'DRX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    # It should be colored by the original array in the field
    mag_range = get_array_range(rep.source, rep.array, 'DRX', atype='point')
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']))
    assert_that(mag_range[0], close_to(-47.2336, 1e-4))

    # >>> Let's check that the colorfield option refers to the field itself
    assert_that(rep.opts['ColorField'], equal_to(depl_field))

    # ----------------------------------------------------
    # WarpRep colored by an integration point field (ELGA)
    # ----------------------------------------------------
    # >>> Simply update the representation by changing the colorfield and comopnent
    rep.update_({'Component':'SIXY', 'ColorField': sief_elga_field})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    assert_that(rep.array, equal_to(sief_elga_field.info['pv-aident']+'_avg'))

    mag_range = get_array_range(rep.source, rep.array, 'SIXY', atype='cell')
    assert_that(mag_range[0], close_to(3.4246, 1e-4))
    assert_that(mag_range[1], close_to(6.9217, 1e-4))

    # >>> Back to the displacement field magnitude coloring
    rep.update_({'Component':'Magnitude', 'ColorField': depl_field})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':TRAN'))
    mag_range = get_array_range(rep.source, rep.array, 'Magnitude', atype='point')
    assert_that(mag_range[1], close_to(34.0354, 1.e-4))

    # >>> Toggle reference position
    prev_disp = True if rep.__class__._refdisp else False
    rep.toggle_reference()
    assert_that(rep.__class__._refdisp, equal_to(not(prev_disp)))
    rep.toggle_reference()
    assert_that(rep.__class__._refdisp, equal_to(prev_disp))

    # >>> Test (temporal) animation
    rep.animate()
    assert_that(rep.scene, is_not(None))

    # >>> Create new WarpRep with user set scale factor
    rep = WarpRep(depl_field, ScaleFactor=5.e-3)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    assert_that(rep.opts['ScaleFactor'], close_to(5.e-3, 1.e-7))
    assert_that(rep.opts['ScaleFactorAuto'], equal_to(False))
    rep.update_({'ScaleFactorAuto': True})

    # >>> Create new WarpRep with a scale factor of zero
    #     (it should be interpreted as auto scale)
    rep = WarpRep(depl_field, ScaleFactor=0.0)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((41, 20)))
    assert_that(rep.opts['ScaleFactor'], close_to(1.46905e-3, 1.e-7))
    assert_that(rep.opts['ScaleFactorAuto'], equal_to(True))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
