# -*- coding: utf-8 -*-

# Copyright 2019 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for the post-processing features."""

import os
import os.path as osp
import unittest
from copy import deepcopy as dc

from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.post import ColorRep, ContourRep, ResultFile, WarpRep
from asterstudy.post.utils import (get_array_range, nb_points_cells,
                                   show_min_max)
from hamcrest import *
from testutils import paraview_post

# def force_print(*args, **kwargs):
#     import sys
#     print(*args, **kwargs, file=sys.__stdout__)

# def assert_that_debug(*args, **kwargs):
#     force_print('Assert > {}'.format(args))
#     return assert_that(*args, **kwargs)

@unittest.skipIf(not has_salome(), "salome is required")
@paraview_post
def test_rep_combinations():
    """
    Test combinations of result representations for a thermo-mechanical analysis
    """
    import pvsimple as pvs
    #--------------------------------------------------------------------------
    root = osp.join(os.getenv("ASTERSTUDYDIR"), "data", "post")
    medf = osp.join(root, '3d_thermo_meca.med')
    # This file contains 2 concepts : RESU and TEMPE
    # TEMPE contains only the temperature field wheras RESU contains
    # multiple (mechanical) fields like displacement, stresses,
    # internal forces, etc.

    result = ResultFile(medf)
    result.toggle_quadratic()

    first = list(result.full_source.TimestepValues)[0]
    last = list(result.full_source.TimestepValues)[-1]

    depl_field = result.lookup('RESU').lookup('DEPL')
    sief_elga_field = result.lookup('RESU').lookup('SIEF_ELGA')
    sieq_elno_field = result.lookup('RESU').lookup('SIEQ_ELNO')
    reac_noda_field = result.lookup('RESU').lookup('REAC_NODA')
    sigm_noeu_field = result.lookup('RESU').lookup('SIGM_NOEU')
    temp_field = result.lookup('TEMPE').lookup('TEMP')

    # --------------------------------------------------
    # Simple coloring of each of the preceding fields
    # --------------------------------------------------
    rep = ColorRep(depl_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))

    rep.animate()
    rep.scene.GoToFirst()
    rep.ren_view.ViewTime = first
    rep.opts.update({'ColorBarType': 'Continuous',
                     'ColorBarAuto': 'Automatic: current step'})
    rep.update_colorbar()
    pvs.Render()

    # >>> ColorRep calculates a translational magnitude to allow easy picking
    #     of the results, even if those are 3D and displacement only has 3 components
    #     Let's do the check
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']+':MAGTRAN'))

    mag_range1 = get_array_range(rep.source, rep.array, 'Magnitude', atype='point')
    mag_range2 = get_array_range(rep.source, rep.array, -1, atype='point')

    assert_that(mag_range1[0], close_to(0.0, 1e-4))
    assert_that(mag_range1[1], close_to(6.8431e-4, 1.e-7))
    assert_that(mag_range2[1], close_to(6.8431e-4, 1.e-7))

    dep_mag_min = dc(rep.opts['ColorBarMin'])
    dep_mag_max = dc(rep.opts['ColorBarMax'])
    assert_that(dep_mag_max, close_to(6.8431e-4, 1.e-7))

    # >>> Let's update the representation by changing the coloring component
    rep.update_({'Component': 'DX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))
    # It should be colored by the original array in the field
    assert_that(rep.array, equal_to(depl_field.info['pv-aident']))

    mag_range = get_array_range(rep.source, rep.array, 'DX', atype='point')
    assert_that(mag_range[0], close_to(-5.0240e-4, 1e-7))
    assert_that(mag_range[1], close_to(1.1351e-4, 1e-7))

    dep_dx_min = dc(rep.opts['ColorBarMin'])
    dep_dx_max = dc(rep.opts['ColorBarMax'])

    rep = ColorRep(sief_elga_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((57600, 24300)))

    rep = ColorRep(sieq_elno_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((18000, 900)))

    rep = ColorRep(reac_noda_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))
    reac_min = dc(rep.opts['ColorBarMin'])
    reac_max = dc(rep.opts['ColorBarMax'])

    rep = ColorRep(sigm_noeu_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))

    rep = ColorRep(temp_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 2336)))
    rep.animate()
    rep.ren_view.ViewTime = last

    mag_range1 = get_array_range(rep.source, rep.array, 'TEMP', atype='point')
    mag_range2 = get_array_range(rep.source, rep.array, 0, atype='point')
    assert_that(mag_range1[1], close_to(70.0, 1e-2))
    assert_that(mag_range2[1], close_to(70.0, 1e-2))

    # --------------------------------------------------
    # Combined coloring of the deformed structure (Warp)
    # --------------------------------------------------

    rep = WarpRep(depl_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))

    rep.update_({'ColorField': temp_field, 'Component': 'TEMP'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))

    rep.animate()
    mag_range = get_array_range(rep.source, rep.array, 'TEMP', atype='point')
    assert_that(mag_range[1], close_to(70.0, 1e-7))

    rep.scene.GoToFirst()
    rep.opts.update({'ColorBarType': 'Continuous',
                     'ColorBarAuto': 'Automatic: current step'})
    rep.update_colorbar()
    pvs.Render()

    mag_range = get_array_range(rep.source, rep.array, 'TEMP', atype='point')
    assert_that(mag_range[1], close_to(20.0, 1e-7))

    rep.update_({'ColorField': sief_elga_field, 'Component': 'SIYY'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))

    rep.update_({'ColorField': sieq_elno_field, 'Component': 'VMIS'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))

    rep.update_({'ColorField': reac_noda_field, 'Component': 'Magnitude'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))
    assert_that(rep.opts['ColorBarMin'], close_to(reac_min, 1.e-7))
    assert_that(rep.opts['ColorBarMax'], close_to(reac_max, 1.e1))

    rep.update_({'ColorField': sigm_noeu_field, 'Component': 'SIXX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((5395, 900)))
    assert_that(rep.opts['ColorBarMax'], close_to(1.51833981e8, 1.e2))

    # --------------------------------------------------
    # Contour representation
    # --------------------------------------------------

    rep = ContourRep(depl_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((2736, 520)))

    assert_that(rep.opts['ColorBarMin'], close_to(dep_mag_min, 1.e-7))
    assert_that(rep.opts['ColorBarMax'], close_to(dep_mag_max, 1.e-7))

    rep.update_({'Component': 'DX'})
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((2836, 540)))

    rep = ContourRep(sief_elga_field)
    pvs.Render()
    nbpts, nbcells = nb_points_cells(rep.source)
    assert_that(nbpts, greater_than(0))
    assert_that(nbcells, greater_than(0))

    '''remove assert check discussion on MR21 asterstudy git
    # assert_that(nb_points_cells(rep.source), equal_to((18069, 3995)))
    try :
        assert_that(nb_points_cells(rep.source), equal_to((18071, 3995)))
    except :
        print(f"Failed to apply assert_that(nb_points_cells(rep.source), equal_to((18071, 3995)))")
        print(f"Try new values for singularity")
        assert_that(nb_points_cells(rep.source), equal_to((18069, 3995)))
    '''

    rep = ContourRep(sieq_elno_field)
    pvs.Render()
    assert_that(nb_points_cells(rep.source), equal_to((16144, 2039)))

#------------------------------------------------------------------------------
if __name__ == "__main__":
    import os
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    os._exit(not RET.wasSuccessful())

#------------------------------------------------------------------------------
