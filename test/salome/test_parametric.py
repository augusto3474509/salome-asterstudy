# -*- coding: utf-8 -*-

# Copyright 2016 - 2018 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.
"""Automatic tests for parametric studies."""

import os
import os.path as osp
import pickle
import random
import shutil
import unittest

import numpy

from asterstudy.api import ParametricCalculation
from asterstudy.common import debug_message, debug_mode
from asterstudy.datamodel.comm2study import comm2study
from asterstudy.datamodel.engine import Engine, runner_factory
from asterstudy.datamodel.engine.engine_utils import ExportCase
from asterstudy.datamodel.engine.salome_runner import has_salome
from asterstudy.datamodel.history import History
from asterstudy.datamodel.job_informations import JobInfos
from asterstudy.datamodel.parametric import (export_to_parametric,
                                             output_commands)
from asterstudy.datamodel.result import StateOptions as SO
from asterstudy.datamodel.study2comm import study2comm
from testcommon.engine_tests import _parameters, monitor_refresh
from hamcrest import *
from testutils import attr, tempdir

_multiprocess_can_split_ = False


def _setup_nominal(tmpdir):
    history = History()
    if not history.support["parametric"]:
        return None

    history.folder = tmpdir
    case = history.current_case
    case.name = 'BeamCase'

    # first stage
    stage1 = case.create_stage('deflection')
    cmd_file = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                        'parametric_beam.comm')
    with open(cmd_file) as comm:
        comm2study(comm.read(), stage1)

    info = stage1.handle2info[20]
    info.filename = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                             'sslv159b.mail')

    # second stage containing IMPR_TABLE
    stage2 = case.create_stage('output')
    cmd_file = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                        'parametric_beam.com1')
    with open(cmd_file) as comm:
        comm2study(comm.read(), stage2)

    info = stage2.handle2info[10]
    info.filename = osp.join(tmpdir, 'result_file.npy')

    # third stage containing unnecessary commands
    stage3 = case.create_stage('ignored')
    cmd_file = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                        'parametric_beam.com2')
    with open(cmd_file) as comm:
        comm2study(comm.read(), stage3)

    return case


def _setup_and_check_ot(case, tmpdir, mem_limit=None, run=True):
    # inputs for the widget
    varnames = case.variables.keys()
    outcmd = output_commands(case)
    assert_that(outcmd, has_length(1))
    cmd = outcmd[0]

    # outputs of the widget: user's selection
    if False in [case.job_infos.is_defined(key)
                 for key in ("input_vars", "output_unit", "output_vars")]:
        job_infos = case.job_infos
        job_infos.set("job_type", JobInfos.Persalys)
        job_infos.set("input_vars", ["E", "F"])
        job_infos.set("output_unit", cmd.storage['UNITE'])
        job_infos.set("output_vars", cmd.storage['NOM_PARA'])
    assert_that(varnames, has_items(*case.job_infos.get("input_vars")))

    otdata = export_to_parametric(case, case.folder)
    debug_message(("-" * 30 + "\n{0}\n" + "-" * 30).format(otdata.code))
    debug_message("Input files and directories:\n{0}".format(otdata.files))
    debug_message("Job parameters:\n{0}".format(otdata.parameters))
    assert_that(otdata.code.strip(), contains_string("def _exec("))
    assert_that(otdata.code, contains_string("ParametricCalculation("))

    export = osp.join(tmpdir, "BeamCase", "parametric.export")
    with open(export) as fexp:
        content = fexp.read()
    assert_that(content, contains_string("parametric.comm"))
    assert_that(content, contains_string("parametric.com1"))
    assert_that(content, is_not(contains_string("parametric.com2")))
    # check that parameters are taken from the previous execution
    assert_that(content,
                contains_string("memory_limit %s" % (mem_limit or 2048)))

    if not run:
        return

    # next steps are done in Persalys
    # check with F = twice the nominal value
    context = {}
    # run silently in unittest
    code = otdata.code.replace("calc.set_logger(print)", "# silent")
    exec(code, context)

    # executed by salome launcher in a working directory
    # copy input files
    prev = os.getcwd()
    wrkdir = osp.join(tmpdir, 'wrkdir')
    os.makedirs(wrkdir)
    os.chdir(wrkdir)
    for infile in ("parametric.comm", "parametric.com1", "parametric.export",
                   "sslv159b.mail"):
        shutil.copyfile(osp.join(tmpdir, "BeamCase", infile),
                        osp.join(wrkdir, infile))
    try:
        results = context['_exec'](2.e11, -1000.)
    finally:
        os.chdir(prev)

    assert_that(results, has_length(3))
    assert_that(results[0], equal_to(0.5))
    assert_that(results[1], equal_to(0.))
    assert_that(round(results[2], 6), equal_to(-0.005373))

    # check that files are deleted in case of success and not in debug mode
    # it will fail in debug mode: useful to keep temporary files
    assert_that(osp.exists(osp.join(wrkdir, ".run_param0001")),
                equal_to(bool(debug_mode())))


@tempdir
def test_parametric_study2comm(tmpdir):
    """Test generator of parametric command file"""
    case = _setup_nominal(tmpdir)
    if case is None:
        return

    varnames = case.variables.keys()
    assert_that(varnames, contains("F", "E", "nu", "h"))

    stage = case[0]
    text = study2comm(stage, parametric=True)
    assert_that(text, contains_string("E = VARIABLE(NOM_PARA="))
    assert_that(text, contains_string("nu = VARIABLE(NOM_PARA="))
    assert_that(text, contains_string("F = VARIABLE(NOM_PARA="))
    assert_that(text, contains_string("h = VARIABLE(NOM_PARA="))


@tempdir
def test_export_parametric(tmpdir):
    case = _setup_nominal(tmpdir)
    if case is None:
        return

    case.job_infos.set_parameters_from({
        'server': 'remote',
        'memory': 4096,
        'time': '1:00:00',
        'mpicpu': 1
    })
    export_name = osp.join(tmpdir, "parametric.export")
    export = ExportCase.factory(case, export_name, parametric=True)
    files = export.get_input_files()
    assert_that(files, has_length(0))

    export.generate()
    files = export.get_input_files()
    assert_that(files, has_length(5))

    with open(export_name) as fexp:
        content = fexp.read()

    assert_that(content, contains_string("server remote"))
    assert_that(content, contains_string("memory_limit 4096"))
    assert_that(content, contains_string("time_limit 3600"))


@unittest.skipIf(not has_salome(), "salome is required")
@tempdir
def test_parametric_generator(tmpdir):
    case = _setup_nominal(tmpdir)
    if case is None:
        return

    assert_that(case, has_length(3))

    # add a previous execution
    case.job_infos.set('memory', 1234)
    engine = Engine.Salome
    rc1 = case.model.create_run_case(name='fake')
    result = rc1.results()[2]
    runner = runner_factory(engine, case=rc1, unittest=True)
    _parameters(rc1, engine)
    runner.start()
    monitor_refresh(runner, estimated_time=50.)
    assert_that(runner.result_state(result) & SO.Success)

    _setup_and_check_ot(case, tmpdir, mem_limit=1234)


@unittest.skipIf(not has_salome(), "salome is required")
@tempdir
def test_parametric_generator_wo_run(tmpdir):
    case = _setup_nominal(tmpdir)
    if case is None:
        return

    assert_that(case, has_length(3))

    _setup_and_check_ot(case, tmpdir, run=False)


@unittest.skipIf(not has_salome(), "salome is required")
@tempdir
def test_parametric_generator_adao_wo_run(tmpdir):
    case = _setup_nominal(tmpdir)
    if case is None:
        return

    assert_that(case, has_length(3))

    csv = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                   'parametric_beam_obs.csv')
    shape = numpy.loadtxt(csv).shape
    shape = shape if len(shape) == 2 else (1, shape[0])
    lines, cols = shape
    assert_that(cols, equal_to(3))
    assert_that(lines, equal_to(1))

    outcmd = output_commands(case)
    assert_that(outcmd, has_length(1))
    cmd = outcmd[0]

    job_infos = case.job_infos
    job_infos.set("job_type", JobInfos.Adao)
    job_infos.set("input_vars", ["E", "F"])
    job_infos.set("output_unit", cmd.storage['UNITE'])
    job_infos.set("output_vars", cmd.storage['NOM_PARA'])
    job_infos.set("input_init", [1.8e11, -1000.])
    job_infos.set("input_bounds", [[1.5e11, 2.7e11], [None, 0.]])
    job_infos.set("observations_path", csv)

    _setup_and_check_ot(case, tmpdir, run=False)

@tempdir
def test_parametric_coverage(tmpdir):
    # for coverage
    with open(osp.join(tmpdir, "parametric.export"), "w") as fexp:
        fexp.write("P mpi_nbcpu 1\nP mpi_nbnoeud 1\nP ncpus 1\n")
    calc = ParametricCalculation(tmpdir, ["x0"], [1.0, 2.0])
    calc.setup()
    calc._rcname = None
    name = calc.run_case_name()
    assert_that(name, equal_to(".run_param_1.0_2.0"))
    calc.runcdir = osp.join(calc.basedir, calc.run_case_name())

    # test for long paths
    n = 26
    calc = ParametricCalculation(tmpdir, ["x{0}".format(i) for i in range(n)],
                                 [random.random() for _ in range(n)])
    assert_that(calc.runcdir, has_length(255))

    calc.set_logger(lambda _x: None)
    calc.read_output_file()
    assert_that(calc.output_values(), has_length(0))


@tempdir
def test_direct_runner(tmpdir):
    case = _setup_nominal(tmpdir)
    if case is None:
        return

    assert_that(case, has_length(3))

    engine = Engine.Direct
    rc1 = case.model.create_run_case(name='fake')
    expected_results = rc1.results()
    assert_that(expected_results, has_length(3))

    runner = runner_factory(engine, case=rc1, unittest=True)
    _parameters(rc1, engine)
    runner.start()

    assert_that(runner.is_started(), equal_to(True))
    assert_that(runner.is_finished(), equal_to(True))
    assert_that(runner.result_state(expected_results[0]) & SO.Success)
    assert_that(runner.result_state(expected_results[1]) & SO.Success)
    assert_that(runner.result_state(expected_results[2]) & SO.Success)


def _setup_zzzz159b(tmpdir, variant):
    assert variant in (1, 3)
    history = History()
    if not history.support["parametric"]:
        return None

    history.folder = tmpdir
    case = history.current_case
    case.name = 'zzzz159b'

    stage = case.create_stage('zzzz159b')
    if variant == 3:
        cmd_file = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                            'zzzz159b.3')
    elif variant == 1:
        cmd_file = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                            'zzzz159b_one.comm')
    with open(cmd_file) as comm:
        comm2study(comm.read(), stage)

    info = stage.handle2info[2]
    info.filename = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                             'zzzz159a.mail')

    info = stage.handle2info[3]
    info.filename = osp.join(tmpdir, 'result_file.npy')

    info = stage.handle2info[8]
    info.filename = osp.join(tmpdir, 'result_file.txt')

    return case


def test_parametric_data():
    jinf1 = JobInfos()
    jinf1.set("job_type", JobInfos.Persalys)
    jinf1.set("input_vars", ['a', 'b'])
    jinf1.set("output_unit", 123)
    jinf1.set("output_vars", ['res'])
    jinf2 = pickle.loads(pickle.dumps(jinf1))
    assert_that(jinf1, equal_to(jinf2))

    # not defined
    assert_that(jinf1.get("input_init"), contains(None, None))
    assert_that(jinf1.get("input_bounds"), equal_to([[None, None], [None, None]]))

    jinf1.set("job_type", JobInfos.Adao)
    jinf1.set("input_init", [0., 1.])
    jinf1.set("input_bounds", [[None, None], [0., 2.]])
    jinf1.set("observations_path", "/path/to/csv")
    jinf2.set("job_type", JobInfos.Adao)
    jinf2.set("input_init", [0., 1.])
    jinf2.set("input_bounds", [[None, None], [0., 2.]])
    jinf2.set("observations_path", "/path/to/csv")
    assert_that(jinf1, equal_to(jinf2))

    jinf2.set("input_bounds", [[0., 1.], [0., 2.]])
    assert_that(jinf1, is_not(equal_to(jinf2)))

    jinf1.set("job_type", JobInfos.Persalys)
    jinf2.set("job_type", JobInfos.Persalys)
    assert_that(jinf1.asdict(), is_not(equal_to(jinf2.asdict())))
    assert_that(jinf1, equal_to(jinf2))

    assert_that(calling(jinf1.set).with_args("xxxxx", None),
                raises(KeyError, "unknown parameter"))
    assert_that(calling(jinf1.set).with_args("job_type", "badtype"),
                raises(TypeError, "expecting type"))
    assert_that(calling(jinf1.set).with_args("job_type", 123456),
                raises(ValueError, "unexpected value"))

    assert_that(jinf1.mode, equal_to(JobInfos.Null))
    jinf1.mode = JobInfos.Interactive
    assert_that(jinf1.mode, equal_to(JobInfos.Interactive))
    jinf1.mode = JobInfos.BatchText
    assert_that(jinf1.mode, equal_to(JobInfos.Batch))

    assert_that(
        calling(jinf2.set).with_args("input_init", [1., 2., 3.]),
        raises(ValueError, "expecting 2 values"))
    assert_that(
        calling(jinf2.set).with_args("input_bounds", [1., 2., 3.]),
        raises(ValueError, "expecting 2 values"))
    assert_that(
        calling(jinf2.set).with_args("input_bounds", [1., 2.]),
        raises(ValueError, "expecting a list of 2 values"))
    assert_that(
        calling(jinf2.set).with_args("input_bounds", [[1., 2.], [
            1.,
        ]]), raises(ValueError, "expecting a list of 2 values"))


def test_jobinfos():
    jinf1 = JobInfos()
    assert_that(jinf1.is_empty(), equal_to(True))
    jinf1 = JobInfos(with_default=True)
    assert_that(jinf1.is_empty(), equal_to(False))
    # at least 'wckey' may depend on configuration, remove it
    dic1 = jinf1.asdict()
    dref = JobInfos.Commons.copy()
    dic1.pop("wckey", None)
    dref.pop("wckey", None)
    assert_that(list(dic1.keys()), contains_inanyorder(*list(dref.keys())))


def _go_zzzz159b(tmpdir, run):
    case = _setup_zzzz159b(tmpdir, variant=3)
    if case is None:
        return
    job_infos = case.job_infos

    # inputs for the widget
    varnames = case.variables.keys()
    outcmd = output_commands(case)
    assert_that(outcmd, has_length(1))
    command = outcmd[0]

    # outputs of the widget: user's selection : 'input_vars' & 'command'
    job_infos.set("job_type", JobInfos.Adao)
    job_infos.set("make_env", not run)
    job_infos.set("input_vars", ['DSDE__', 'SIGY__', 'YOUN__'])
    job_infos.set("output_unit", command.storage['UNITE'])
    job_infos.set("output_vars", command.storage.get("NOM_PARA"))
    job_infos.set("input_init", [1000.0, 30.0, 100000.0])
    job_infos.set("input_bounds", [[500.0, 10000.0], [5.0, 500.0], [50000.0, 500000.0]])
    csv = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                   'zzzz159b_short.csv')

    assert_that(varnames, has_items(*job_infos.get("input_vars")))
    assert_that(job_infos.get("output_vars"), contains('SIYY', 'V1'))
    job_infos.set("observations_path", csv)

    lines, cols = numpy.loadtxt(csv).shape
    assert_that(cols, equal_to(len(job_infos.get("output_vars"))))
    assert_that(lines, equal_to(11))

    # by dashboard
    history = case.model
    param_case = history.create_parametric_case()

    expected_results = param_case.results()
    assert_that(expected_results, has_length(1))
    result = expected_results[0]

    # select YacsRunner that calls export_to_parametric
    runner = runner_factory(case=param_case, unittest=True)
    _parameters(param_case, Engine.Salome)

    runner.start()
    # elapsed time: 375 s
    monitor_refresh(runner, estimated_time=10 * (100 if run else 1))

    assert_that(runner.result_state(result) & SO.Success)
    for data in ("adao_go.sh", "adao_script.comm", "adao_data.py"):
        filename = osp.join(result.stage.folder, data)
        assert_that(osp.isfile(filename), equal_to(True))

    if not run:
        return

    npy = osp.join(result.stage.folder, "analysis.npy")
    txt = osp.join(result.stage.folder, "analysis.txt")
    assert_that(osp.isfile(npy), equal_to(True))
    assert_that(osp.isfile(txt), equal_to(True))
    res = numpy.load(npy)
    assert_that(res, has_length(3))
    assert_that(res[0], close_to(2000., 2000. * 1.e-4))
    assert_that(res[1], close_to(200., 200. * 1.e-4))
    assert_that(res[2], close_to(200.e3, 200.e3 * 1.e-4))


def _go_zzzz159b_short(tmpdir, run):
    case = _setup_zzzz159b(tmpdir, variant=1)
    if case is None:
        return
    job_infos = case.job_infos

    # inputs for the widget
    varnames = case.variables.keys()
    outcmd = output_commands(case)
    assert_that(outcmd, has_length(1))
    command = outcmd[0]

    # outputs of the widget: user's selection : 'input_vars' & 'command'
    job_infos.set("job_type", JobInfos.Adao)
    job_infos.set("make_env", not run)
    job_infos.set("input_vars", ['deplY'])
    job_infos.set("output_unit", command.storage['UNITE'])
    job_infos.set("output_vars", command.storage.get("NOM_PARA"))
    job_infos.set("input_init", [0.002])
    job_infos.set("input_bounds", [[0.001, 0.008]])
    csv = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                   'siyy200.csv')

    assert_that(varnames, has_items(*job_infos.get("input_vars")))
    assert_that(job_infos.get("output_vars"), contains('SIYY'))
    job_infos.set("observations_path", csv)

    shape = numpy.loadtxt(csv).shape
    assert_that(shape, empty())

    # by dashboard
    history = case.model
    param_case = history.create_parametric_case()

    expected_results = param_case.results()
    assert_that(expected_results, has_length(1))
    result = expected_results[0]

    # select YacsRunner that calls export_to_parametric
    runner = runner_factory(case=param_case, unittest=True)
    _parameters(param_case, Engine.Salome)

    runner.start()
    # elapsed time: 125 s
    monitor_refresh(runner, estimated_time=10 * (30 if run else 1))

    assert_that(runner.result_state(result) & SO.Success)
    for data in ("adao_go.sh", "adao_script.comm", "adao_data.py"):
        filename = osp.join(result.stage.folder, data)
        assert_that(osp.isfile(filename), equal_to(True))

    if not run:
        return

    npy = osp.join(result.stage.folder, "analysis.npy")
    txt = osp.join(result.stage.folder, "analysis.txt")
    assert_that(osp.isfile(npy), equal_to(True))
    assert_that(osp.isfile(txt), equal_to(True))
    res = numpy.load(npy)
    assert_that(res, has_length(1))
    assert_that(res[0], close_to(0.005, 0.005 * 1.e-4))


@unittest.skipIf(not has_salome(), "salome is required, YacsRunner needed")
@tempdir
def test_zzzz159b_wo_run(tmpdir):
    _go_zzzz159b(tmpdir, run=False)

def debug_zzzz159b():
    # same as test_zzzz159b_wo_run with keeping directory
    # use: rm -rf /tmp/debug_zzzz159b && check_salome.sh test_parametric.py:debug_zzzz159b
    assert has_salome()
    _go_zzzz159b("/tmp/debug_zzzz159b", run=False)

@unittest.skipIf(not has_salome(), "salome is required, YacsRunner needed")
@tempdir
def test_zzzz159b_short_wo_run(tmpdir):
    _go_zzzz159b_short(tmpdir, run=False)

@unittest.skipIf(not has_salome(), "salome is required, YacsRunner needed")
@tempdir
def test_zzzz159b_short(tmpdir):
    _go_zzzz159b_short(tmpdir, run=True)

@unittest.skip("only 'short' version is automatically executed")
@unittest.skipIf(not has_salome(), "salome is required, YacsRunner needed")
@tempdir
def test_zzzz159b(tmpdir):
    _go_zzzz159b(tmpdir, run=True)


def test_compatibility():
    ajs = osp.join(os.getenv('ASTERSTUDYDIR'), 'data', 'export',
                   'backward_compatibility_ot.ajs')
    history = History.load(ajs)
    stage = history.current_case[0]
    assert_that(stage, has_length(1))
    cmd = stage[0]
    # in a real test, it should be IMPR_TABLE, not LIRE_MAILLAGE
    unit = cmd.storage['UNITE']

    job_infos = history.current_case.job_infos

    assert_that(job_infos.use_persalys(), equal_to(True))
    assert_that(job_infos.use_adao(), equal_to(False))
    assert_that(job_infos.get("input_vars"), contains('a', 'b', 'c'))
    assert_that(job_infos.get("output_unit"), equal_to(unit))
    assert_that(job_infos.get("output_vars"), empty())
    assert_that(job_infos.get("input_init"), contains(None, None, None))
    assert_that(job_infos.get("input_bounds"),
                equal_to([[None, None], [None, None], [None, None]]))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
