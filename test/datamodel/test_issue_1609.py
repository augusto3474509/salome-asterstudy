# -*- coding: utf-8 -*-

# Copyright 2016-2018 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.


import os.path as osp
import unittest

from asterstudy.datamodel import History
from asterstudy.datamodel.general import ConversionLevel
from asterstudy.datamodel.job_informations import JobInfos
from asterstudy.datamodel.parametric import output_commands
from hamcrest import *
from testutils import tempdir


def test_of_input_data():
    # Test for issue #1609 - Parametric widget, input data
    h = History()
    cc = h.current_case
    s1 = cc.create_stage('s1')
    s2 = cc.create_stage('s2')

    s1.add_variable('a').update('100')
    s1.add_variable('b').update('200')
    s1.add_variable('c').update('300')
    assert_that('a', is_in(cc.variables))
    assert_that('b', is_in(cc.variables))
    assert_that('c', is_in(cc.variables))
    assert_that(cc.variables, has_length(3))

    c1 = s1('IMPR_TABLE')
    assert_that(c1, is_not(is_in(output_commands(cc))))
    c1.init({'FORMAT':'NUMPY',})
    assert_that(c1, is_not(is_in(output_commands(cc))))
    c1.init({'NOM_PARA':'param',})
    assert_that(c1, is_not(is_in(output_commands(cc))))
    c1.init({'UNITE':{20:'aaa.txt'},})
    assert_that(c1, is_not(is_in(output_commands(cc))))
    c1.init({'FORMAT':'NUMPY', 'NOM_PARA':'param', 'UNITE': 20,})
    assert_that(c1, is_in(output_commands(cc)))
    c1.init({'FORMAT':'NUMPY', 'NOM_PARA':'param', 'UNITE': {20: 'aaa.txt'}})
    assert_that(c1, is_in(output_commands(cc)))
    assert_that(output_commands(cc), has_length(1))

    s2.add_variable('x').update('1')
    s2.add_variable('y').update('2')
    s2.add_variable('z').update('3')
    assert_that(cc.variables, has_length(6))
    assert_that('x', is_in(cc.variables))
    assert_that('y', is_in(cc.variables))
    assert_that('z', is_in(cc.variables))

    c2 = s2('IMPR_TABLE')
    c2.init({'FORMAT':'NUMPY', 'NOM_PARA':('aaa', 'bbb', 'ccc'),
             'UNITE':{30:'bbb.txt'}})
    assert_that(c2, is_in(output_commands(cc)))
    assert_that(output_commands(cc), has_length(2))

    # check export_to_parametric: unit to info
    def _check_file(unit, filename, last_exported_stage):
        info = None
        for stage in cc.stages:
            if unit in stage.handle2info:
                info = stage.handle2info[unit]
                break
        assert_that(info.filename, equal_to(filename))
        assert_that(stage.number, equal_to(last_exported_stage))

    _check_file(20, "aaa.txt", 1)
    _check_file(30, "bbb.txt", 2)


@tempdir
def test_of_output_data(tmpdir):
    # Test for issue #1609 - Parametric widget, output data
    h1 = History()
    cc = h1.current_case
    s1 = cc.create_stage('s1')

    infos = JobInfos()
    assert_that(cc.job_infos, instance_of(JobInfos))
    assert_that(cc.job_infos, infos)

    infos = cc.job_infos
    infos.set("job_type", JobInfos.Persalys)
    infos.set("input_vars", ['a'])
    infos.set("output_unit", 10)
    infos.set("output_vars", ['u'])
    assert_that(infos.use_persalys(), equal_to(True))
    assert_that(infos.get("input_vars"), contains('a'))
    assert_that(infos.get("output_unit"), 10)
    assert_that(infos.get("output_vars"), contains('u'))

    infos.set("job_type", JobInfos.Adao)
    infos.set("input_vars", ['a', 'b', 'c'])
    infos.set("output_unit", 20)
    infos.set("output_vars", ['u'])
    infos.set("input_init", [0., 1., 2.])
    infos.set("input_bounds", [[None, None], [0., 2.], [33., None]])
    infos.set("observations_path", "/path/to/csv")

    filename = osp.join(tmpdir, 'study.ajs')
    History.save(h1, filename)

    h2 = History.load(filename, strict=ConversionLevel.NoFail)
    read = h2.current_case.job_infos
    assert_that(read.get("job_type"), equal_to(JobInfos.Adao))
    assert_that(read.get("input_vars"), contains('a', 'b', 'c'))
    assert_that(read.get("output_unit"), 20)
    assert_that(read.get("output_vars"), contains('u'))
    assert_that(read.get("input_init"), contains(0., 1., 2.))
    assert_that(read.get("input_bounds")[0], contains(None, None))
    assert_that(read.get("input_bounds")[1], contains(0., 2.))
    assert_that(read.get("input_bounds")[2], contains(33., None))
    assert_that(read.get("observations_path"), equal_to("/path/to/csv"))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite
    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
