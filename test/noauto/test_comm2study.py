# coding=utf-8

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

"""Automatic tests for conversion functions."""


import os
import os.path as osp
import unittest

from asterstudy.common import ConversionError
from asterstudy.datamodel.history import History
from asterstudy.datamodel.comm2study import comm2study
from asterstudy.datamodel.study2comm import study2comm


def _generate():
    """Execute this function to generate elementary testcases."""
    from glob import glob
    code = '''
    def test_{0}(self):
        """Test for comm2study for {0}"""
        self._test_import("{0}.comm")'''
    for name in glob("data/import/*.comm"):
        name = os.path.splitext(os.path.basename(name))[0]
        print(code.format(name))


class TestComm2Study(unittest.TestCase):
    """Test of import of code_aster commands files"""
    _multiprocess_can_split_ = True

    def _test_import(self, name):
        """Import text, export newly created stage, compare text outputs"""
        dirname = osp.dirname(__file__)
        dirname = osp.abspath(osp.join(*([dirname] + 2 * [os.pardir])))
        filename = osp.join(dirname, "data", "import", name)

        history = History()
        case = history.current_case
        stage = case.create_stage(name)

        with open(filename) as file:
            content = file.read()
        try:
            comm2study(content, stage)
        except ConversionError as exc:
            print("Original traceback:\n", exc.details)
            raise

        text1 = study2comm(stage)

        stage = case.create_stage(name + '-debug')
        try:
            comm2study(text1, stage)
        except ConversionError as exc:
            print("Original traceback:\n", exc.details)
            raise

        text2 = study2comm(stage)

        self.assertEqual(text1, text2)

    def test_ssll107i(self):
        """Test for comm2study for ssll107i"""
        self._test_import("ssll107i.comm")

    def test_szlz111b(self):
        """Test for comm2study for szlz111b"""
        self._test_import("szlz111b.comm")

    def test_szlz102a(self):
        """Test for comm2study for szlz102a"""
        self._test_import("szlz102a.comm")

    def test_comp001d(self):
        """Test for comm2study for comp001d"""
        self._test_import("comp001d.comm")

    def test_zzzz289a(self):
        """Test for comm2study for zzzz289a"""
        self._test_import("zzzz289a.comm")

    def test_szlz108b(self):
        """Test for comm2study for szlz108b"""
        self._test_import("szlz108b.comm")

    def test_comp002b(self):
        """Test for comm2study for comp002b"""
        self._test_import("comp002b.comm")

    def test_comp001a(self):
        """Test for comm2study for comp001a"""
        self._test_import("comp001a.comm")

    def test_szlz101a(self):
        """Test for comm2study for szlz101a"""
        self._test_import("szlz101a.comm")

    def test_zzzz289f(self):
        """Test for comm2study for zzzz289f"""
        self._test_import("zzzz289f.comm")

    def test_zzzz102b(self):
        """Test for comm2study for zzzz102b"""
        self._test_import("zzzz102b.comm")

    def test_szlz111a(self):
        """Test for comm2study for szlz111a"""
        self._test_import("szlz111a.comm")

    def test_zzzz103a(self):
        """Test for comm2study for zzzz103a"""
        self._test_import("zzzz103a.comm")

    def test_comp002c(self):
        """Test for comm2study for comp002c"""
        self._test_import("comp002c.comm")

    def test_supv001b(self):
        """Test for comm2study for supv001b"""
        self._test_import("supv001b.comm")

    def test_comp002e(self):
        """Test for comm2study for comp002e"""
        self._test_import("comp002e.comm")

    def test_ssll107e(self):
        """Test for comm2study for ssll107e"""
        self._test_import("ssll107e.comm")

    def test_comp001c(self):
        """Test for comm2study for comp001c"""
        self._test_import("comp001c.comm")

    def test_zzzz289c(self):
        """Test for comm2study for zzzz289c"""
        self._test_import("zzzz289c.comm")

    def test_comp001f(self):
        """Test for comm2study for comp001f"""
        self._test_import("comp001f.comm")

    def test_comp001h(self):
        """Test for comm2study for comp001h"""
        self._test_import("comp001h.comm")

    def test_szlz100a(self):
        """Test for comm2study for szlz100a"""
        self._test_import("szlz100a.comm")

    def test_comp002d(self):
        """Test for comm2study for comp002d"""
        self._test_import("comp002d.comm")

    def test_szlz106a(self):
        """Test for comm2study for szlz106a"""
        self._test_import("szlz106a.comm")

    def test_comp001g(self):
        """Test for comm2study for comp001g"""
        self._test_import("comp001g.comm")

    def test_zzzz289b(self):
        """Test for comm2study for zzzz289b"""
        self._test_import("zzzz289b.comm")

    def test_comp002f(self):
        """Test for comm2study for comp002f"""
        self._test_import("comp002f.comm")

    def test_ssll107b(self):
        """Test for comm2study for ssll107b"""
        self._test_import("ssll107b.comm")

    def test_rccm09a(self):
        """Test for comm2study for rccm09a"""
        self._test_import("rccm09a.comm")

    def test_szlz103a(self):
        """Test for comm2study for szlz103a"""
        self._test_import("szlz103a.comm")

    def test_comp001j(self):
        """Test for comm2study for comp001j"""
        self._test_import("comp001j.comm")

    def test_zzzz289d(self):
        """Test for comm2study for zzzz289d"""
        self._test_import("zzzz289d.comm")

    def test_comp002k(self):
        """Test for comm2study for comp002k"""
        self._test_import("comp002k.comm")

    def test_zzzz289e(self):
        """Test for comm2study for zzzz289e"""
        self._test_import("zzzz289e.comm")

    def test_zzzz186a(self):
        """Test for comm2study for zzzz186a"""
        self._test_import("zzzz186a.comm")

    def test_ssll107c(self):
        """Test for comm2study for ssll107c"""
        self._test_import("ssll107c.comm")

    def test_erreu06a(self):
        """Test for comm2study for erreu06a"""
        self._test_import("erreu06a.comm")

    def test_comp002h(self):
        """Test for comm2study for comp002h"""
        self._test_import("comp002h.comm")

    def test_comp002a(self):
        """Test for comm2study for comp002a"""
        self._test_import("comp002a.comm")

    def test_szlz109a(self):
        """Test for comm2study for szlz109a"""
        self._test_import("szlz109a.comm")

    def test_szlz108a(self):
        """Test for comm2study for szlz108a"""
        self._test_import("szlz108a.comm")


if __name__ == '__main__':
    _generate()
