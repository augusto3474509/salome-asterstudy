#!/bin/bash

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

# FIXME: "coverage" should be in DEFAULTS in place of "tests"
declare -a DEFAULTS=( "lint" "docs" "tests" "comm2study" "comm2code"
                      "persistence" "performance" )
declare -a OTHER=( "coverage" "report" )
declare -a FUNCS=( "${DEFAULTS[@]}" "${OTHER[@]}" )

prefix=$(readlink -f $(dirname "${0}"))

usage()
{
    echo "Check project."
    echo
    echo "Usage: $(basename ${0}) [options] [func1|func2|...]"
    echo
    echo "Default functions:"
    echo "    ${DEFAULTS[@]}"
    echo "Others supported functions:"
    echo "    ${OTHER[@]}"
    echo
    echo "Options:"
    echo
    echo "  --help (-h)         Print this help information and exit."
    echo "  --serial (-s)       Do not run tests in parallel."
    echo "  --silent (-q)       Switch OFF verbosity."
    echo
    exit 0
}

is_supported()
{
    local unknown=1
    local str="${1}"
    for i in "${FUNCS[@]}"; do
        if [ "${str}" = "${i}" ]; then
            unknown=0
            break
        fi
    done
    test ${unknown} -eq 1 && echo "unsupported function: ${str}"
    return ${unknown}
}

check_func()
{
    # usage: check_func script
    local func=${1}
    local var=${func}_options
    local opts=($(eval eval -- echo -n "\${${var}[@]}"))

    echo '======================================================================'
    echo "Check ${func}"
    echo '======================================================================'

    if [ ${verbose} -eq 0 ]
    then
        ( set -x ; salome shell -- ${prefix}/dev/check_${func}.sh ${opts[@]} >& /dev/null )
    else
        ( set -x ; salome shell -- ${prefix}/dev/check_${func}.sh ${opts[@]} )
    fi
    return ${?}
}

check_coverage()
{
    rm -f ~/.asterstudy/.coverage*
    ( set -x ; salome test ${coverage_options[@]} || touch .failed ) | tee .log
    egrep -v '^[0-9]+:' .log > ctest.log
    egrep '^[0-9]+:' .log > ctest_details.log
    test ! -f .failed && ok=OK || ok=FAILED
    test "${ok}" = "OK" && check_report && ok=OK
    rm -f .log .failed
    test "${ok}" = "OK"
    return ${?}
}

check_report()
{
    local rcfile=${prefix}/test/.coveragerc
    module load python
    set -x
    export COVERAGE_FILE="${HOME}/.asterstudy/.coverage"
    cd /opt/salome_meca/V9_7_INTEGR_scibian_9/modules/ASTERSTUDY_master/share/salome/asterstudy
    coverage3 combine --rcfile=${rcfile}
    coverage3 report --rcfile=${rcfile} --show-missing --skip-covered --fail-under=97
    set +x
    return ${?}
}

check_tests()
{
    ( set -x ; salome test ${tests_options[@]} || touch .failed ) | tee .log
    egrep -v '^[0-9]+:' .log > ctest.log
    egrep '^[0-9]+:' .log > ctest_details.log
    test ! -f .failed && ok=OK || ok=FAILED
    rm -f .log .failed
    test "${ok}" = "OK"
    return ${?}
}

check_main()
{
    local parallel=1
    local verbose=1
    declare -a functions=${DEFAULTS[@]}

    OPTS=$(getopt -o hsq --long help,serial,step:,silent -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        _error "invalid arguments." >&2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) usage ; exit 1 ;;
            -s | --serial ) parallel=0 ;;
            -q | --silent ) verbose=0 ;;
            --step ) functions="$2"; shift ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done
    [ $# -ne 0 ] && functions="${@}"

    declare -a lint_options
    declare -a docs_options
    declare -a tests_options=("-R" "ASTERSTUDY")
    declare -a coverage_options=("-R" "ASTDYCOV")
    declare -a comm2study_options=( "clean" )
    declare -a comm2code_options=( "clean" )
    declare -a persistence_options=( "clean" )
    declare -a performance_options=( "clean" )

    # support parallel?
    if [ "${parallel}" = "1" ]; then
        lint_options+=( "--parallel" )
        tests_options+=( "-j" "$(nproc)" )
        coverage_options+=( "-j" "$(nproc)" )
        comm2study_options+=( "-j" "$(nproc)" )
        comm2code_options+=( "-j" "$(nproc)" )
        persistence_options+=( "-j" "$(nproc)" )
    fi

    if [ "${verbose}" = "1" ]; then
        tests_options+=( "-VV" )
    fi

    local result func
    result=0

    for func in ${functions}
    do
        local ok
        local call
        case "${func}" in
            tests | coverage | report ) call=check_${func} ;;
            * ) call=check_func ;;
        esac
        is_supported "${func}" && ${call} ${func}
        test "${?}" = "0" && ok=OK || ok=FAILED
        test "${ok}" = "OK" || result=$((result+1))
        eval ${func}_ok=${ok}
        if [ ${result} -gt 0 ]; then
            # stop after first error
            break
        fi
    done

    echo
    echo '======================================================================'
    echo "Summary"
    echo '======================================================================'

    for func in ${functions}
    do
        local ok=${func}_ok
        printf "%-15s : %s\n" ${func} ${!ok}
    done

    echo '----------------------------------------------------------------------'
    echo "Result: ${result}"
    echo

    return ${result}
}

check_main "${@}"
exit ${?}
