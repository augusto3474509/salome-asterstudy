<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ModalAnalysis</name>
    <message>
        <source>Modal analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modal analysis case study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mesh selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Specify input mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter a path to the MED file or select an existing Mesh object from the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Model definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>What kind of model do you want to work on?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose model type from the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Material properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Young&apos;s modulus (E)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter value &gt;= {val_min}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Density (p)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Poisson&apos;s ratio (v)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter value between {val_min} and {val_max}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Boundary conditions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Imposed degrees of freedom on groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DZ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select mesh groups and apply degrees of freedom on them</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of modes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Criterion for eigenmodes search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a way to define modes and specify frequency parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>N first eigenmodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of frequencies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency band</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lower frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upper frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency centered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Target frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Specify output mesh file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter a path to the result MED file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
