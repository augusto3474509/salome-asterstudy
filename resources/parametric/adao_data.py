import numpy

# Model variables
var_names = [%{input_names}]
var_nb = len(var_names)
var_init = numpy.array(%{input_initial})
var_bounds = %{input_bounds}

# Observations
obs_names = [%{output_names}]
obs_values = numpy.loadtxt(%{observations})
try:
    obs_values = numpy.loadtxt(%{observations})
except BaseException:
    obs_values = numpy.load(%{observations})
obs_asvect = obs_values.T.ravel()
obs_nb = len(obs_names)


# ADAO conventional variables names
# Background as a vector
Background = var_init

# Background Error covariance as a matrix
BackgroundError = 1.e8 * numpy.identity(var_nb) * var_init ** 2

# Observation as a vector
Observation = obs_values.T.ravel()

# Observation Error covariance as a matrix
ObservationError = numpy.identity(len(obs_asvect))

AlgorithmParameters = {
    "MaximumNumberOfSteps" : 50,
    "CostDecrementTolerance": 1.e-4,
    "Bounds": var_bounds,
}

%{code}

# Observation operator: Y = H(X)
def DirectOperator( X ):
    args = numpy.ravel(X)
    results = _exec(*args)
    # Y must have the same size as the observation
    Y = numpy.array(results).ravel()
    return Y
