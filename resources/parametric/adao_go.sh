#!/bin/bash

set_prefix() {
    local this=`readlink -n -f "$1"`
    wrkdir=`dirname "${this}"`
}

set_prefix "$0"

salome="%{salome_bin}"


salome_start()
{
    local portfile=$(mktemp /tmp/salome-port-XXXXX)
    printf "Starting SALOME in text mode...\n"
    "${salome}" start -t --ns-port-log="${portfile}" 2> /dev/null
    port=$(cat "${portfile}")
    rm -f "${portfile}"

    printf "\nSALOME session is running on port ${port}.\n\n"
}

salome_kill()
{
    printf "\nStopping SALOME session on port ${port}...\n"
    "${salome}" kill ${port} 2> /dev/null
}

run_yacs_schema()
{
    local runscript=$(mktemp /tmp/salome-runscript-XXXXX)
    printf "\nExecuting Yacs schema...\n"
    cat << EOF > ${runscript}
#!/bin/bash
printf "See 'NNNN_AdaoContainer_xxxx.log' file in the temporary directory \${SALOME_TMP_DIR}\n\n"
driver "${wrkdir}/adao_script.xml"
EOF
    chmod 755 "${runscript}"

    "${salome}" shell --port=${port} -- "${runscript}"
    iret=${?}
    rm  -f "${runscript}"
    return ${iret}
}

adao_go_main()
{
    pushd "${wrkdir}" > /dev/null
    salome_start
    run_yacs_schema
    iret=${?}
    salome_kill
    popd > /dev/null
    return ${iret}
}

adao_go_main "${@}"
exit ${?}
