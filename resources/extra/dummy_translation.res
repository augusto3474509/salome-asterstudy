# -*- coding: utf-8 -*-

# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

# This file contains dummy code for automatic inserting of some
# "implicit" translations into TS resource file(s).
# The file does not need to be installed.

# Commands categories
translate("Categories", "Analysis")
translate("Categories", "BC and Load")
translate("Categories", "Deprecated")
translate("Categories", "Finite Element") # deprecated
translate("Categories", "Fracture and Fatigue")
translate("Categories", "Functions and Lists")
translate("Categories", "Hidden")
translate("Categories", "Material")
translate("Categories", "Material Assignment") # deprecated
translate("Categories", "Mesh")
translate("Categories", "Model Definition")
translate("Categories", "Other")
translate("Categories", "Output")
translate("Categories", "Post Processing")
translate("Categories", "Pre Analysis")
translate("Categories", "Pre Processing") # deprecated
translate("Categories", "Variables")

# Rules
translate("Rules", "At Least One")
translate("Rules", "Exactly One")
translate("Rules", "At Most One")
translate("Rules", "If First All Present")
translate("Rules", "Only First Present")
translate("Rules", "All Together")
