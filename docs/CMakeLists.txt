# Copyright 2016 EDF R&D
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you may download a copy of license
# from https://www.gnu.org/licenses/gpl-3.0.

configure_file(conf.py ${CMAKE_CURRENT_BINARY_DIR}/conf.py)
configure_file(_static/switchers.js ${CMAKE_CURRENT_BINARY_DIR}/_static/switchers.js)

# generate and install documentation
install_sphinx_docs(DESTINATION ${ASTERSTUDY_DOC_DIR}
                    CFGDIR ${CMAKE_CURRENT_BINARY_DIR}
                    LANGUAGES fr
                    OPTIONS -W)

# install redirection file
install(FILES redirect_index.html
        DESTINATION ${ASTERSTUDY_DOC_DIR}
        RENAME index.html)
