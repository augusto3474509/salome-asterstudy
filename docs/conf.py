# -*- coding: utf-8 -*-
#
# AsterStudy documentation build configuration file, created by
# sphinx-quickstart on Thu Dec  3 18:21:13 2015.
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.


import sys
import os.path as osp
import shlex
import sphinx

src_dir = '@CMAKE_CURRENT_SOURCE_DIR@'
def get_src_dir():
    if src_dir in ('', '@''CMAKE_CURRENT_SOURCE_DIR''@'):
        return osp.dirname(__file__)
    return src_dir

# a) for each argument:
# - if path is relative, convert it to absolute by using location of this conf file as a base dir
# - check if path exists
# - ignore non-existing path
# b) return list containing only existing path(s)
def check_path(*args, **kwargs):
    root = kwargs.get('root')
    result = []
    for arg in args:
        if root and not root.startswith('@'):
            arg = osp.join(root, arg)
        if not osp.isabs(arg):
            arg = osp.join(get_src_dir(), arg)
        if osp.exists(arg):
            result.append(osp.abspath(arg))
    return result

# add directory(-ies) to sys.path
def add_to_syspath(*args, **kwargs):
    args = check_path(*args, **kwargs)
    for arg in reversed(args):
        sys.path.insert(0, arg)

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
add_to_syspath('_extensions')

#----add for root_dir
def root_dir():
    top_src_dir = '@PROJECT_SOURCE_DIR@'
    if top_src_dir == '@''PROJECT_SOURCE_DIR''@':
        return osp.realpath(osp.dirname(osp.dirname(osp.abspath(__file__))))
    else:
        return osp.realpath(top_src_dir)

# Extend sys.path to get access to Brides Python packages.
sys.path.insert(0, root_dir())

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
sys.path.insert(0, osp.join(root_dir(), 'docs', '_extensions'))


# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.viewcode',
    'sphinx.ext.inheritance_diagram',
    'sphinx.ext.napoleon',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = check_path('_templates')

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'AsterStudy'
copyright = u'2016-2023 EDF R&D'
author = u'OPEN CASCADE, EDF R&D'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The full version, including alpha/beta/rc tags.
from asterstudy.common import version
release = version()
# The short X.Y version.
version= release.split('-')[0]

#overload
version='2023'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = 'en'

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['_build']

# The reST default role (used for this markup: `text`) to use for all
# documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
#keep_warnings = False

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
if sphinx.version_info[:2] < (1,3):
    html_theme = 'default'
else:
    html_theme = 'sphinx_rtd_theme'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = u'AsterStudy v%s Documentation' % version

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = check_path('_static', root='@CMAKE_CURRENT_BINARY_DIR@')

# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
#html_extra_path = []

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Language to be used for generating the HTML full-text search index.
# Sphinx supports the following languages:
#   'da', 'de', 'en', 'es', 'fi', 'fr', 'hu', 'it', 'ja'
#   'nl', 'no', 'pt', 'ro', 'ru', 'sv', 'tr'
#html_search_language = 'en'

# A dictionary with options for the search language support, empty by default.
# Now only 'ja' uses this config value
#html_search_options = {'type': 'default'}

# The name of a javascript file (relative to the configuration directory) that
# implements a search results scorer. If empty, the default will be used.
#html_search_scorer = 'scorer.js'

# Output file base name for HTML help builder.
htmlhelp_basename = 'asterstudydoc'

# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
'papersize': 'a4paper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',

# Latex figure (float) alignment
#'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
  (master_doc,
   'AsterStudy-Documentation-v%s.tex' % version,
   u'AsterStudy v%s Documentation' % version,
   author, 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'asterstudy', u'AsterStudy Documentation',
     [author], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
  (master_doc, 'AsterStudy', u'AsterStudy Documentation',
   author, 'AsterStudy', 'One line description of project.',
   'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
#texinfo_appendices = []

# If false, no module index is generated.
#texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#texinfo_show_urls = 'footnote'

# If true, do not generate a @detailmenu in the "Top" node's menu.
#texinfo_no_detailmenu = False

# -- Options for napoleon extension  --------------------------------------

# If true, enables generation of the documentation for special class methods
# like __eq__, __get__, __repr__, etc.
napoleon_include_special_with_doc = True

# If true, enables generation of the documentation for private class methods,
# i.e. methods that start with single underscore: e.g. _func()
napoleon_include_private_with_doc = True

# List of modules which should be ignored when checking coverage.
#coverage_ignore_modules = []

# List of functions which should be ignored when checking coverage.
#coverage_ignore_functions = []

# List of classes which should be ignored when checking coverage.
coverage_ignore_classes = ["UndoMenu", "UndoRedoItem"]

# Set to False to not write headlines.
#coverage_write_headline = False

# Skip objects that are not documented in the source with a docstring. False by default.
#coverage_skip_undoc_in_source = True

# Directories in which to search for additional message catalogs.
locale_dirs = [
    'locale',
    '@CMAKE_CURRENT_BINARY_DIR@/_build/locale',
    ]

# If true, a document’s text domain is its docname if it is a top-level project file
# and its very base directory otherwise.
gettext_compact = False

# The filename format for language-specific figures. The default value is {root}.{language}{ext}.
# It will be expanded to dirname/filename.en.png from .. image:: dirname/filename.png. The available format tokens are:
#    {root} - the filename, including any path component, without the file extension, e.g. dirname/filename
#    {path} - the directory path component of the filename, with a trailing slash if non-empty, e.g. dirname/
#    {basename} - the filename without the directory path or file extension components, e.g. filename
#    {ext} - the file extension, e.g. .png
#    {language} - the translation language, e.g. en
# For example, setting this to {path}{language}/{basename}{ext} will expand to dirname/en/filename.png instead.
# NOTE: this feature appeared in Sphinx since version 1.4; {path} and {basename} tokens since version 1.5.
figure_language_filename = '{path}{language}/{basename}{ext}'
