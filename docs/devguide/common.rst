.. _devguide-common:

********************
Common functionality
********************

Class Diagram
=============
.. inheritance-diagram::
   asterstudy.common.configuration
   asterstudy.common.exceptions
   asterstudy.common.extfiles
   asterstudy.common.session
   asterstudy.common.features
   asterstudy.common.utilities
   asterstudy.common.execution
   asterstudy.common.conversion
   asterstudy.common.decoder

Details
=======

.. automodule:: asterstudy.common.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.version
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.utilities
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.features
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.execution
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.conversion
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.extfiles
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.remote_utils
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.exceptions
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.configuration
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.decoder
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.base_utils
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.template
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.excepthook
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.common.session
   :show-inheritance:
   :members:
   :special-members: __init__
