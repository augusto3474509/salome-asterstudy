.. _devguide-salome:

***************
SALOME wrapping
***************

Class Diagram
=============

.. inheritance-diagram::
    asterstudy.gui.salomegui
    asterstudy.gui.salomegui_utils
   :parts: 1

Details
=======

.. automodule:: asterstudy.gui.salomegui
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.salomegui_utils
   :show-inheritance:
   :members:
   :special-members: __init__
