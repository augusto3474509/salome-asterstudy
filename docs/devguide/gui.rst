.. _devguide-gui:

************************
Graphical User Interface
************************

Class Diagram
=============

.. inheritance-diagram::
    asterstudy.gui.__init__
    asterstudy.gui.actions
    asterstudy.gui.actions.action
    asterstudy.gui.actions.onfileaction
    asterstudy.gui.actions.listaction
    asterstudy.gui.actions.openwithaction
    asterstudy.gui.actions.undoaction
    asterstudy.gui.astergui
    asterstudy.gui.asterstdgui
    asterstudy.gui.behavior
    asterstudy.gui.runpanel
    asterstudy.gui.runpanel_model
    asterstudy.gui.editionpanel
    asterstudy.gui.favoritesmanager
    asterstudy.gui.parameterpanel
    asterstudy.gui.parameterpanel.basic
    asterstudy.gui.parameterpanel.editors
    asterstudy.gui.parameterpanel.items
    asterstudy.gui.parameterpanel.panel
    asterstudy.gui.parameterpanel.path
    asterstudy.gui.parameterpanel.views
    asterstudy.gui.parameterpanel.widgets
    asterstudy.gui.parameterpanel.windows
    asterstudy.gui.prefdlg
    asterstudy.gui.prefmanager
    asterstudy.gui.popupmanager
    asterstudy.gui.showallpanel
    asterstudy.gui.grouppanel
    asterstudy.gui.results
    asterstudy.gui.study
    asterstudy.gui.validityreport
    asterstudy.gui.unusedconcepts
    asterstudy.gui.widgets
    asterstudy.gui.widgets.aboutdlg
    asterstudy.gui.widgets.auxiliary
    asterstudy.gui.widgets.catalogsview
    asterstudy.gui.widgets.categoryview
    asterstudy.gui.widgets.colorbutton
    asterstudy.gui.widgets.conceptseditor
    asterstudy.gui.widgets.dialog
    asterstudy.gui.widgets.load_database_dialog
    asterstudy.gui.widgets.convert_mesh_dialog
    asterstudy.gui.widgets.elidedbutton
    asterstudy.gui.widgets.elidedlabel
    asterstudy.gui.widgets.filterpanel
    asterstudy.gui.widgets.fontwidget
    asterstudy.gui.widgets.graph_canvas
    asterstudy.gui.widgets.import_test
    asterstudy.gui.widgets.mainwindow
    asterstudy.gui.widgets.messagebox
    asterstudy.gui.widgets.popup_frame
    asterstudy.gui.widgets.searchwidget
    asterstudy.gui.widgets.shrinkingcombobox
    asterstudy.gui.widgets.splitter
    asterstudy.gui.widgets.tabwidget
    asterstudy.gui.widgets.texteditor
    asterstudy.gui.widgets.titlewidget
    asterstudy.gui.widgets.treewidget
    asterstudy.gui.editionwidget
    asterstudy.gui.workspace
    asterstudy.gui.controller
    asterstudy.gui.cmdtexteditor
    asterstudy.gui.stagetexteditor
    asterstudy.gui.textfileeditor
    asterstudy.gui.unit_model
    asterstudy.gui.datasettings
    asterstudy.gui.datasettings.category
    asterstudy.gui.datasettings.model
    asterstudy.gui.datasettings.view
    asterstudy.gui.datasettings.searcher
    asterstudy.gui.datafiles
    asterstudy.gui.datafiles.objects
    asterstudy.gui.datafiles.model
    asterstudy.gui.datafiles.view
    asterstudy.gui.datafiles.unitpanel
    asterstudy.gui.datafiles.dirspanel
    asterstudy.gui.datafiles.summary
    asterstudy.gui.infoview
    asterstudy.gui.infoview.view
    asterstudy.gui.dashboard
    asterstudy.gui.casesview
    asterstudy.gui.meshview
    asterstudy.gui.meshview.baseview
    asterstudy.gui.meshview.view
    asterstudy.gui.casesview.model
    asterstudy.gui.casesview.view
    asterstudy.gui.variablepanel
    asterstudy.gui.commentpanel
    asterstudy.gui.parametricpanel
    asterstudy.gui.parametric_links
    asterstudy.gui.remotefs
   :parts: 1

Details
=======

.. automodule:: asterstudy.gui.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.astergui
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.asterstdgui
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.study
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.workspace
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.behavior
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.controller
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.widgets.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.aboutdlg
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.auxiliary
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.catalogsview
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.categoryview
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.colorbutton
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.conceptseditor
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.dialog
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.load_database_dialog
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.convert_mesh_dialog
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.elidedbutton
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.elidedlabel
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.filterpanel
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.fontwidget
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.graph_canvas
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.import_test
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.mainwindow
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.messagebox
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.popup_frame
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.searchwidget
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.shrinkingcombobox
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.splitter
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.tabwidget
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.texteditor
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.titlewidget
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.widgets.treewidget
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.editionwidget
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.runpanel
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.runpanel_model
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.editionpanel
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.parameterpanel.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.basic
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.editors
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.items
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.panel
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.path
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.views
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.widgets
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.parameterpanel.windows
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.cmdtexteditor
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.stagetexteditor
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.textfileeditor
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.actions.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.actions.action
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.actions.onfileaction
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.actions.listaction
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.actions.openwithaction
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.actions.undoaction
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.prefmanager
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.popupmanager
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.prefdlg
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.showallpanel
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.grouppanel
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.unit_model
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.datafiles.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.datafiles.objects
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datafiles.view
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datafiles.model
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datafiles.unitpanel
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datafiles.dirspanel
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datafiles.summary
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.infoview.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.infoview.view
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.dashboard
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.casesview.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.casesview.view
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.casesview.model
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.datasettings.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.datasettings.category
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datasettings.view
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datasettings.model
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.datasettings.searcher
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.meshview.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.gui.meshview.baseview
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.gui.meshview.view
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.gui.variablepanel
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.commentpanel
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.favoritesmanager
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.parametricpanel
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.parametric_links
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.validityreport
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.unusedconcepts
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.remotefs
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.gui.results
   :show-inheritance:
   :members:
   :special-members: __init__
