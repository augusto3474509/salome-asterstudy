.. _devguide-link_to_persalys:

****************************************
Interface with Adao and Persalys modules
****************************************

Overview
========

The *Export As Parametric* feature needs:

- one or more Python variables (of *int* or *float* type),

- a file created by ``IMPR_TABLE/FORMAT='NUMPY'`` providing the results.

The *numpy* array must have only one line for an analysis in Persalys.
For an ADAO analysis, the number of rows must be identical to the observations.

Each result is named by the column title (given by ``NOM_PARA``, not stored in
the *numpy* file).


Export As Parametric
====================

The user chooses *Export As Parametric* on the CurrentCase,
selects the input variables and the numpy file of the results.

A Python function is created with the input and output variables and using
the ``ParametricCalculation`` object of the API .
The study is exported into a ``parametric.export`` file in the directory of
the CurrentCase.

The Persalys Study is defined from this Python function, and input files
used in the study and its execution parameters.

An Persalys Study is created and opened in the Persalys module.

The working directory is defined during this step.
The parametric study may be executed on a remote server.
That's why this directory is set in ``$HOME/salome_workdir_USERNAME``
(supposed to be the same of all machines).

.. note::
    If the ``working_directory`` attribute of *JobParameters* is not provided
    a default value is assigned, by example: in ``/tmp``.
    But this value is not recommended since it is not shared by all server
    frontends.


Execution
=========

The *Files and directories* tab (in Persalys) allows to adapt the
*Remote working directory* and the *Temporary local directory*.

The *idefix* files are exported into the *Temporary local directory*.

Global loggings of the locally executed commands are written into
``/tmp/libbatch-log-date***`` (sending files to the remote server, job
launching...).

``SALOME_TMP_DIR`` environment variable is used to create temporary files
during the YACS scheme execution:

- ``USER-host-batch_NNNN`` are the temporary directories for each execution,

- ``NNNN_container****.log`` for each container. This file contains standard
  output of each *ParametricCalculation* execution.

The *Remote working directory* contains a directory named ``USER-from_ot-date``
from which the containers are executed.

The RunCase directories for each execution are created in this location
(``.run_param0001``, ``.run_param0002``, etc.).
