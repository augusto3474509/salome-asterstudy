.. _devguide:

###############
Developer Guide
###############

This is the *Development Manual* for the *AsterStudy* application. If you look
for the information about usage of the *AsterStudy* application, refer to the
:ref:`userguide`.

.. toctree::
   :maxdepth: 2

   common
   datamodel
   api
   gui
   assistant
   salome

   link_to_persalys
