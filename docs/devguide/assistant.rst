.. _devguide-assistant:

*********************
Calculation assistant
*********************

Class Diagram
=============
.. inheritance-diagram::
   asterstudy.assistant.generator
   asterstudy.assistant.validator
   asterstudy.assistant.runner
   asterstudy.assistant.widgets

Details
=======

.. automodule:: asterstudy.assistant.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.assistant.generator
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.assistant.validator
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.assistant.runner
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.assistant.widgets
   :show-inheritance:
   :members:
   :special-members: __init__
