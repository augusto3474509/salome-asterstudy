.. _devguide-datamodel:

**********
Data Model
**********

Class Diagram
=============

.. inheritance-diagram::
    asterstudy.datamodel.abstract_data_model
    asterstudy.datamodel.case
    asterstudy.datamodel.command
    asterstudy.datamodel.command.basic
    asterstudy.datamodel.command.hidden
    asterstudy.datamodel.command.formula
    asterstudy.datamodel.command.comment
    asterstudy.datamodel.command.deleter
    asterstudy.datamodel.command.variable
    asterstudy.datamodel.command.constancy
    asterstudy.datamodel.command.mixing
    asterstudy.datamodel.command.helper
    asterstudy.datamodel.command.text
    asterstudy.datamodel.general
    asterstudy.datamodel.catalogs
    asterstudy.datamodel.history
    asterstudy.datamodel.stage
    asterstudy.datamodel.dataset
    asterstudy.datamodel.dataset.base
    asterstudy.datamodel.dataset.graphical
    asterstudy.datamodel.dataset.text
    asterstudy.datamodel.job_informations
    asterstudy.datamodel.result
    asterstudy.datamodel.result.mixing
    asterstudy.datamodel.result.execution
    asterstudy.datamodel.result.message
    asterstudy.datamodel.result.utils
    asterstudy.datamodel.engine.factory
    asterstudy.datamodel.engine.abstract_runner
    asterstudy.datamodel.engine.simulator
    asterstudy.datamodel.engine.salome_runner
    asterstudy.datamodel.engine.asrun_runner
    asterstudy.datamodel.engine.direct_runner
    asterstudy.datamodel.engine.basics
    asterstudy.datamodel.engine.engine_utils
    asterstudy.datamodel.engine.helper
    asterstudy.datamodel.engine.edf_servers
    asterstudy.datamodel.usages
    asterstudy.datamodel.usages.load_database
    asterstudy.datamodel.usages.convert_mesh
    asterstudy.datamodel.parametric
    asterstudy.datamodel.comm2study
    asterstudy.datamodel.study2comm
    asterstudy.datamodel.study2code
    asterstudy.datamodel.serializer
    asterstudy.datamodel.recovery
    asterstudy.datamodel.backup
    asterstudy.datamodel.sync
    asterstudy.datamodel.undo_redo
    asterstudy.datamodel.aster_syntax
    asterstudy.datamodel.aster_parser
    asterstudy.datamodel.visit_study
    asterstudy.datamodel.context_info
    asterstudy.datamodel.dict_categories
    asterstudy.datamodel.file_descriptors
    asterstudy.datamodel.global_dict
    asterstudy.datamodel.sd_dict
   :parts: 1

.. for later
    asterstudy.datamodel.engine.yacs_runner

Details
=======

.. automodule:: asterstudy.datamodel.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.general
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.abstract_data_model
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.history
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.case
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.stage
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.dataset.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.datamodel.dataset.base
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.dataset.graphical
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.dataset.text
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.datamodel.job_informations
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.result.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.datamodel.result.mixing
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.result.execution
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.result.message
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.result.utils
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.datamodel.engine.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.factory
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.abstract_runner
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.simulator
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.salome_runner
      :show-inheritance:
      :members:
      :special-members: __init__

   .. comment
      .. automodule:: asterstudy.datamodel.engine.yacs_runner
         :show-inheritance:
         :members:
         :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.asrun_runner
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.direct_runner
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.basics
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.engine_utils
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.helper
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.engine.edf_servers
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.datamodel.usages.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.datamodel.usages.load_database
      :show-inheritance:
      :members:
      :special-members: __init__

   .. automodule:: asterstudy.datamodel.usages.convert_mesh
      :show-inheritance:
      :members:
      :special-members: __init__

.. automodule:: asterstudy.datamodel.parametric
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.comm2study
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.study2comm
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.study2code
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.serializer
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.recovery
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.backup
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.command.__init__
   :show-inheritance:
   :members:
   :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.basic
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.hidden
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.formula
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.comment
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.deleter
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.variable
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.constancy
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.mixing
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.helper
     :show-inheritance:
     :members:
     :special-members: __init__

   .. automodule:: asterstudy.datamodel.command.text
     :show-inheritance:
     :members:
     :special-members: __init__

.. automodule:: asterstudy.datamodel.undo_redo
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.catalogs
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.aster_syntax
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.aster_parser
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.sync
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.visit_study
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.context_info
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.dict_categories
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.file_descriptors
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.global_dict
   :show-inheritance:
   :members:
   :special-members: __init__

.. automodule:: asterstudy.datamodel.sd_dict
   :show-inheritance:
   :members:
   :special-members: __init__
