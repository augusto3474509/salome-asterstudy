**********
AsterStudy
**********

============
Installation
============

--------------
Pre-requisites
--------------

AsterStudy has the following software requirements:

* Python 3.6
* Qt 5 (mostly to generate/compile translations resources)
* PyQt 5
* Sphinx (optionally, 1.7.6 or newer)
* Sphinx-intl (for French version of documentation, 0.9.1 or newer)
* Nose (optionally, for tests, 1.3 or newer)

------------------
Basic Installation
------------------

The build procedure of AsterStudy application is implemented with CMake.
In order to install the application, perform the following steps:

1. Set up environment for pre-requisites (see "Pre-requisites" section above).

2. Create a build directory (it is advised to create the build directory near
   the source directory):

   % mkdir build

3. Configure the build procedure:

   % cd build
   % cmake -DCMAKE_INSTALL_PREFIX=<installation_directory> <path_to_src_dir>

   Here,
   - <installation_directory> is a destination folder for AsterStudy application
     (default: /usr); 
   - <path_to_src_dir> is a path to the AsterStudy sources directory.

   Note:
   - By default (if CMAKE_INSTALL_PREFIX option is not given), AsterStudy
     application will be configured for installation to the /usr directory that
     requires root permissions to complete the installation.

4. Install:

   % make install

   This command will install AsterStudy to the <installation_directory>
   specified to "cmake" command on the previous step.

-------------------
Custom installation
-------------------

AsterStudy application supports a set of advanced configuration options.

* SALOME wrapping

AsterStudy application implements a wrapping for SALOME that enables the
functionality of the application as the SALOME ASTERSTUDY module.
By default, this wrapping is not installed along with the application.
To install SALOME wrapping, set ENABLE_SALOME option to ON:

   % cmake -DENABLE_SALOME=ON ...

* Documentation

The documentation of AsterStudy application is automatically built and installed
along with the application provided that Sphinx is properly detected during the
configuration step. If Sphinx is not found, CMake fails to configure build
procedure. To avoid a configuration error, switch off the documentation
installation by setting ENABLE_DOCUMENTATION option to OFF:

   % cmake -DENABLE_DOCUMENTATION=OFF ...

* Other CMake options
  
You can also use other options of "cmake" command to customize your
installation. Learn more about available options by typing

   % cmake --help

=======
Testing
=======

To test the AsterStudy installation, use "make test" command:

   % cd build
   % make install
   % make test
